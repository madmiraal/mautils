// (c) 2016 - 2017: Marcel Admiraal

#include "learner.h"

#include <algorithm>

namespace ma
{
    Learner::Learner(const unsigned int stateLength,
            const unsigned int outputLength, const double learnRate,
            const double discountRate) :
        stateLength(stateLength), outputLength(outputLength),
        learnRate(learnRate), discountRate(discountRate)
    {
    }

    Learner::~Learner()
    {
    }

    Learner::Learner(const Learner& source) :
        stateLength(source.stateLength), outputLength(source.outputLength),
        learnRate(source.learnRate), discountRate(source.discountRate)
    {
    }

    void swap(Learner& first, Learner& second)
    {
        std::swap(first.stateLength, second.stateLength);
        std::swap(first.outputLength, second.outputLength);
        std::swap(first.learnRate, second.learnRate);
        std::swap(first.discountRate, second.discountRate);
    }

    unsigned int Learner::getStateLength() const
    {
        return stateLength;
    }

    unsigned int Learner::getOutputLength() const
    {
        return outputLength;
    }

    void Learner::adjustLearner(const unsigned int stateLength,
            const unsigned int outputLength)
    {
        this->stateLength = stateLength;
        this->outputLength = outputLength;
    }

    double Learner::getLearningRate() const
    {
        return learnRate;
    }

    void Learner::setLearningRate(const double newRate)
    {
        learnRate = newRate;
    }

    double Learner::getDiscountRate() const
    {
        return discountRate;
    }

    void Learner::setDiscountRate(const double newRate)
    {
        discountRate = newRate;
    }
}
