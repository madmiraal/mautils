// (c) 2015: Marcel Admiraal

#include "vector.h"
#include "utils.h"

#include <algorithm>
#include <cmath>
#include <cfloat>
#include <fstream>

namespace ma
{
    Vector::Vector() :
        elements(0), element(0)
    {
    }

    Vector::~Vector()
    {
        if (element != 0)
            delete[] element;
    }

    Vector::Vector(const unsigned int elements) :
        elements(elements), element(0)
    {
        if (elements != 0)
            element = new double[elements]();
    }

    Vector::Vector(const int* element, const unsigned int elements) :
        elements(elements), element(0)
    {
        if (elements != 0)
        {
            this->element = new double[elements];
            for (unsigned int index = 0; index < elements; ++index)
            {
                this->element[index] = element[index];
            }
        }
    }

    Vector::Vector(const unsigned int* element, const unsigned int elements) :
        elements(elements), element(0)
    {
        if (elements != 0)
        {
            this->element = new double[elements];
            for (unsigned int index = 0; index < elements; ++index)
            {
                this->element[index] = element[index];
            }
        }
    }

    Vector::Vector(const short* element, const unsigned int elements) :
        elements(elements), element(0)
    {
        if (elements != 0)
        {
            this->element = new double[elements];
            for (unsigned int index = 0; index < elements; ++index)
            {
                this->element[index] = element[index];
            }
        }
    }

    Vector::Vector(const float* element, const unsigned int elements) :
        elements(elements), element(0)
    {
        if (elements != 0)
        {
            this->element = new double[elements];
            for (unsigned int index = 0; index < elements; ++index)
            {
                this->element[index] = element[index];
            }
        }
    }

    Vector::Vector(const double* element, const unsigned int elements) :
        elements(elements), element(0)
    {
        if (elements != 0)
        {
            this->element = new double[elements];
            for (unsigned int index = 0; index < elements; ++index)
            {
                this->element[index] = element[index];
            }
        }
    }

    Vector::Vector(const Vector& source) :
        elements(source.elements), element(0)
    {
        if (elements != 0)
        {
            this->element = new double[elements];
            for (unsigned int index = 0; index < elements; ++index)
            {
                element[index] = source.element[index];
            }
        }
    }

    Vector::Vector(const Vector& source, const unsigned int elements) :
        elements(elements), element(0)
    {
        if (elements != 0)
        {
            this->element = new double[elements]();
            for (unsigned int index = 0; index < elements && index < source.elements; ++index)
            {
                element[index] = source.element[index];
            }
        }
    }

    Vector::Vector(const char* filename) :
        elements(0), element(0)
    {
        load(filename);
    }

    void swap(Vector& first, Vector& second)
    {
        std::swap(first.elements, second.elements);
        std::swap(first.element, second.element);
    }

    Vector& Vector::operator=(Vector source)
    {
        swap(*this, source);
        return *this;
    }

    double& Vector::operator[](const unsigned int index)
    {
        return element[index];
    }

    const double& Vector::operator[](const unsigned int index) const
    {
        return element[index];
    }

    unsigned int Vector::size() const
    {
        return elements;
    }

    Vector Vector::getSubvector(const unsigned int start,
            const unsigned int length) const
    {
        Vector result(length);
        for (unsigned int index = 0; index < length; ++index)
        {
            if (start + index < elements)
            {
                result[index] = element[start + index];
            }
            else
            {
                result[index] = 0.0;
            }
        }
        return result;
    }

    Vector& Vector::setSubVector(const unsigned int start, const Vector& source)
    {
        if (start + source.elements > elements)
        {
            extend((start + source.elements) - elements);
        }
        for (unsigned int index = 0; index < source.elements; ++index)
        {
            element[start + index] = source[index];
        }
        return *this;
    }

    const double* Vector::array() const
    {
        return element;
    }

    void Vector::getArray(double* output) const
    {
        for (unsigned int index = 0; index < elements; ++index)
        {
            output[index] = element[index];
        }
    }

    void Vector::setArray(const int* source)
    {
        for (unsigned int index = 0; index < elements; ++index)
        {
            element[index] = source[index];
        }
    }

    void Vector::setArray(const float* source)
    {
        for (unsigned int index = 0; index < elements; ++index)
        {
            element[index] = source[index];
        }
    }

    void Vector::setArray(const double* source)
    {
        for (unsigned int index = 0; index < elements; ++index)
        {
            element[index] = source[index];
        }
    }

    Vector& Vector::extend(const unsigned int elements, const double value)
    {
        double* newElement = new double[this->elements + elements]();
        for (unsigned int index = 0; index < this->elements; ++index)
        {
            newElement[index] = element[index];
        }
        for (unsigned int index = this->elements;
                index < this->elements + elements; ++index)
        {
            newElement[index] = value;
        }
        this->elements += elements;
        std::swap(element, newElement);
        delete[] newElement;
        return *this;
    }

    Vector& Vector::operator+=(const Vector& right)
    {
        if (right.elements > elements)
        {
            extend(right.elements - elements);
        }
        for (unsigned int index = 0; index < elements && index < right.elements;
                ++index)
        {
            element[index] += right.element[index];
        }
        return *this;
    }

    Vector& Vector::operator-=(const Vector& right)
    {
        if (right.elements > elements)
        {
            extend(right.elements - elements);
        }
        for (unsigned int index = 0; index < elements && index < right.elements;
                ++index)
        {
            element[index] -= right.element[index];
        }
        return *this;
    }

    Vector& Vector::operator*=(const Vector& right)
    {
        if (right.elements > elements)
        {
            extend(right.elements - elements);
        }
        for (unsigned int index = 0; index < elements && index < right.elements;
                ++index)
        {
            element[index] *= right.element[index];
        }
        return *this;
    }

    Vector& Vector::operator*=(const double right)
    {
        for (unsigned int index = 0; index < elements; ++index)
        {
            element[index] *= right;
        }
        return *this;
    }

    Vector& Vector::operator/=(const Vector& right)
    {
        if (right.elements > elements)
        {
            extend(right.elements - elements);
        }
        for (unsigned int index = 0; index < elements && index < right.elements;
                ++index)
        {
            element[index] /= right.element[index];
        }
        return *this;
    }

    Vector& Vector::operator/=(const double right)
    {
        for (unsigned int index = 0; index < elements; ++index)
        {
            element[index] /= right;
        }
        return *this;
    }

    Vector& Vector::normalise()
    {
        double squaredSum = elementSquaredSum();
        if (elementSquaredSum() == 0)
        {
            fillWith(0);
        }
        else
        {
            double inverseSquareRoot = 1/sqrt(squaredSum);
            *this *= inverseSquareRoot;
        }
        return *this;
    }

    Vector& Vector::fastNormalise()
    {
        float squaredSum = (float)elementSquaredSum();
        if (elementSquaredSum() == 0)
        {
            fillWith(0);
        }
        else
        {
            float inverseSquareRoot = invSqrt(squaredSum);
            *this *= inverseSquareRoot;
        }
        return *this;
    }

    double Vector::abs() const
    {
        return sqrt(elementSquaredSum());
    }

    double Vector::dot(const Vector& other) const
    {
        Vector result = *this * other;
        return result.elementSum();
    }

    Vector Vector::concatenate(const Vector& other)
    {
        unsigned int start = elements;
        extend(other.elements);
        for (unsigned int index = 0; index < other.elements; ++index)
        {
            element[start + index] = other.element[index];
        }
        return *this;
    }

    double Vector::min() const
    {
        return element[minIndex()];
    }

    unsigned int Vector::minIndex() const
    {
        unsigned int minIndex = 0;
        double minumum = DBL_MAX;
        for (unsigned int index = 0; index < elements; ++index)
        {
            if (element[index] < minumum)
            {
                minIndex = index;
                minumum = element[index];
            }
        }
        return minIndex;
    }

    double Vector::max() const
    {
        return element[maxIndex()];
    }

    unsigned int Vector::maxIndex() const
    {
        unsigned int maxIndex = 0;
        double maximum = -DBL_MAX;
        for (unsigned int index = 0; index < elements; ++index)
        {
            if (element[index] > maximum)
            {
                maxIndex = index;
                maximum = element[index];
            }
        }
        return maxIndex;
    }

    double Vector::mean() const
    {
        double sum = 0;
        for (unsigned int index = 0; index < elements; ++index)
        {
            sum += element[index];
        }
        return sum / elements;
    }

    double Vector::var() const
    {
        if (elements <= 1) return 0;
        // Calculate the expectation of the squares.
        double squaredExpectation = elementSquaredSum();
        squaredExpectation /= elements;
        // Calculate the square of the expcetation.
        double expectationSquared = mean();
        expectationSquared *= expectationSquared;
        // Variance is expectation of the squares - square of the expectation.
        double result = squaredExpectation - expectationSquared;
        // Ensure variances are not rounded to negative with small variances.
        if (result < 0) result = 0;
        // Convert to variance of the sample.
        result *= 1. * elements / (elements - 1);

        return result;
    }

    double Vector::sd() const
    {
        // Standard deviation is the squareroot of the variance.
        return sqrt(var());
    }

    double Vector::sem() const
    {
        // Standard error of the mean is the standard deviation divided by the
        // square root of the sample size.
        return sd()/sqrt(elements);
    }

    double Vector::rsem() const
    {
        // Relative standard error of the mean is the standard error or the mean
        // divided by the mean expressed as a percentage.
        return sem()/mean()*100;
    }

    double Vector::elementSum() const
    {
        double sum = 0;
        for (unsigned int index = 0; index < elements; ++index)
        {
            sum += element[index];
        }
        return sum;
    }

    double Vector::elementSquaredSum() const
    {
        double sum = 0;
        for (unsigned int index = 0; index < elements; ++index)
        {
            sum += element[index] * element[index];
        }
        return sum;
    }

    double Vector::elementProduct() const
    {
        double product = 1;
        for (unsigned int index = 0; index < elements; ++index)
        {
            product *= element[index];
        }
        return product;
    }

    Vector& Vector::fillWith(const double value)
    {
        for (unsigned int index = 0; index < elements; ++index)
        {
            element[index] = value;
        }
        return *this;
    }

    Vector Vector::subVector(const unsigned int first,
        const unsigned int length) const
    {
        Vector result(length);
        for (unsigned int index = 0; index < length && first + index < elements;
                ++index)
        {
            result.element[index] = element[first + index];
        }
        return result;
    }

    void Vector::print(std::ostream& os) const
    {
        os << "[";
        if (elements > 0)
        {
            for (unsigned int index = 0; index < elements - 1; ++index)
            {
                os << element[index] << ", ";
            }
            os << element[elements-1];
        }
        os << "]";
    }

    bool Vector::save(const char* filename) const
    {
        std::ofstream fileStream(filename);
        if (fileStream.is_open())
        {
            fileStream << *this;
            fileStream << std::endl;
            if (fileStream.good())
            {
                fileStream.close();
                return true;
            }
        }
        return false;
    }

    bool Vector::load(const char* filename)
    {
        std::ifstream fileStream(filename, std::ifstream::in);
        if (fileStream.is_open())
        {
            Vector result;
            fileStream >> result;
            if (fileStream.good())
            {
                swap(*this, result);
                fileStream.close();
                return true;
            }
        }
        return false;
    }

    Vector operator+(Vector left, const Vector& right)
    {
        left += right;
        return left;
    }

    Vector operator-(Vector left, const Vector& right)
    {
        left -= right;
        return left;
    }

    Vector operator-(const Vector& source)
    {
        Vector result(source.size());
        result -= source;
        return result;
    }

    Vector operator*(Vector left, const Vector& right)
    {
        left *= right;
        return left;
    }

    Vector operator*(Vector left, const double right)
    {
        left *= right;
        return left;
    }

    Vector operator*(const double left, Vector right)
    {
        right *= left;
        return right;
    }

    Vector operator/(Vector left, const Vector& right)
    {
        left /= right;
        return left;
    }

    Vector operator/(Vector left, const double right)
    {
        left /= right;
        return left;
    }

    Vector operator/(const double left, const Vector& right)
    {
        Vector result(right.size());
        for (unsigned int index = 0; index < right.size(); ++index)
        {
            result[index] = left / right[index];
        }
        return result;
    }

    bool operator==(const Vector& left, const Vector& right)
    {
        if (left.size() != right.size()) return false;
        for (unsigned int index = 0; index < left.size(); ++index)
        {
            if (left[index] != right[index]) return false;
        }
        return true;
    }

    bool operator!=(const Vector& left, const Vector& right)
    {
        return !(left == right);
    }

    bool approxEqual(const Vector& left, const Vector& right,
            const double error)
    {
        if (left.size() != right.size()) return false;
        for (unsigned int index = 0; index < left.size(); ++index)
        {
            if (!approxEqual(left[index], right[index], error)) return false;
        }
        return true;
    }

    std::istream& operator>>(std::istream& is, Vector& destination)
    {
        unsigned int elements;
        is >> elements;
        if (is.good())
        {
            Vector result(elements);
            for (unsigned int index = 0; index < elements; ++index)
            {
                is >> result[index];
            }
            if (is.good())
            {
                swap(destination, result);
            }
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const Vector& source)
    {
        unsigned int elements = source.size();
        os << elements;
        if (elements > 0)
        {
            os << '\t';
            for (unsigned int index = 0; index < elements - 1; ++index)
            {
                os << source[index] << '\t';
            }
            os << source[elements-1];
        }
        return os;
    }

    Vector normalise(Vector source)
    {
        source.normalise();
        return source;
    }

    Vector fastNormalise(Vector source)
    {
        source.fastNormalise();
        return source;
    }

    double abs(const Vector& source)
    {
        return source.abs();
    }

    double dot(const Vector& left, const Vector& right)
    {
        return left.dot(right);
    }

    Vector zeros(const unsigned int elements)
    {
        Vector z(elements);
        z.fillWith(0.0);
        return z;
    }

    Vector ones(const unsigned int elements)
    {
        Vector o(elements);
        o.fillWith(1.0);
        return o;
    }

    Vector concatenate(const Vector& left, const Vector& right)
    {
        Vector result(left);
        result.concatenate(right);
        return result;
    }

    double min(const Vector& source)
    {
        return source.min();
    }

    unsigned int minIndex(const Vector& source)
    {
        return source.minIndex();
    }

    double max(const Vector& source)
    {
        return source.max();
    }

    unsigned int maxIndex(const Vector& source)
    {
        return source.maxIndex();
    }

    double mean(const Vector& source)
    {
        return source.mean();
    }

    double var(const Vector& source)
    {
        return source.var();
    }

    double sd(const Vector& source)
    {
        return source.sd();
    }

    double sem(const Vector& source)
    {
        return source.sem();
    }

    double rsem(const Vector& source)
    {
        return source.rsem();
    }

    double elementSum(const Vector& source)
    {
        return source.elementSum();
    }

    double elementSquaredSum(const Vector& source)
    {
        return source.elementSquaredSum();
    }

    double elementProduct(const Vector& source)
    {
        return source.elementProduct();
    }

    void print(const Vector& source, std::ostream& os)
    {
        source.print(os);
    }

    Vector softMax(const Vector& value, double beta)
    {
        const unsigned int length = value.size();
        double sum = 0;
        double resultArray[length];
        const double* valueArray = value.array();
        #pragma omp parallel for reduction(+:sum)
        for (unsigned int i = 0; i < length; ++i)
        {
            resultArray[i] = std::exp(beta * valueArray[i]);
            sum += resultArray[i];
        }
        #pragma omp parallel for
        for (unsigned int i = 0; i < length; ++i)
        {
            resultArray[i] /= sum;
        }
        Vector result(resultArray, length);
        return result;
    }

    Vector normalProbability(const Vector& value)
    {
        const unsigned int length = value.size();
        double sum = 0;
        double resultArray[length];
        const double* valueArray = value.array();
        #pragma omp parallel for reduction(+:sum)
        for (unsigned int i = 0; i < length; ++i)
        {
            resultArray[i] = valueArray[i];
            sum += resultArray[i];
        }
        #pragma omp parallel for
        for (unsigned int i = 0; i < length; ++i)
        {
            resultArray[i] /= sum;
        }
        Vector result(resultArray, length);
        return result;
    }
}
