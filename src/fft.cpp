// (c) 2016: Marcel Admiraal

#include "fft.h"

#include "algorithms.h"

#include <cmath>

namespace ma
{
    // Cooley-Tukey FFT (in-place, breadth-first, decimation-in-frequency)
    void fft(Complex data[], const unsigned int length)
    {
        // DFT
        unsigned int step = length;
        Complex phi(cos(M_PI / length), sin(M_PI / length));
        while (step > 1)
        {
            const unsigned int step2 = step;
            step >>= 1;

            Complex theta = 1.0L;
            phi = phi * phi;
            for (unsigned int l = 0; l < step; l++)
            {
                for (unsigned int a = l; a < length; a += step2)
                {
                    unsigned int b = a + step;
                    Complex temp = data[a] - data[b];
                    data[a] += data[b];
                    data[b] = temp * theta;
                }
                theta *= phi;
            }
        }

        // Decimate
        const unsigned int logLength = (unsigned int)log2(length);
        for (unsigned int a = 0; a < length; a++)
        {
            unsigned int b = a;
            // Reverse bits
            b = (((b & 0xaaaaaaaa) >> 1) | ((b & 0x55555555) << 1));
            b = (((b & 0xcccccccc) >> 2) | ((b & 0x33333333) << 2));
            b = (((b & 0xf0f0f0f0) >> 4) | ((b & 0x0f0f0f0f) << 4));
            b = (((b & 0xff00ff00) >> 8) | ((b & 0x00ff00ff) << 8));
            b = ((b >> 16) | (b << 16)) >> (32 - logLength);
            if (b > a) swapValues(data[a], data[b]);
        }

        // Conjugate the complex numbers
        for (unsigned int i = 0; i < length; ++i) data[i] = data[i].conj();
    }

    void ifft(Complex data[], const unsigned int length)
    {
        // Forward transform.
        fft(data, length);

        // Scale the complex numbers.
        for (unsigned int i = 0; i < length; ++i) data[i] /= length;
    }
}

