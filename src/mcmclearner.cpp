// (c) 2014 - 2017: Marcel Admiraal

#include "mcmclearner.h"
#include "iterator.h"

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

namespace ma
{
    MCMCLearner::MCMCLearner(const unsigned int stateLength,
            const unsigned int outputLength, const double learnRate,
            const double discountRate, const double defaultValue) :
        Learner(stateLength, outputLength, learnRate, discountRate)
    {
        setDefaultValue(defaultValue);
    }

    MCMCLearner::MCMCLearner(const char* filename) :
        Learner()
    {
        load(filename);
    }

    MCMCLearner::MCMCLearner(const MCMCLearner& source) :
        Learner(source), stateValue(source.stateValue),
        defaultValue(source.defaultValue), sequence(source.sequence)
    {
    }

    MCMCLearner::~MCMCLearner()
    {
    }

    void swap(MCMCLearner& first, MCMCLearner& second)
    {
        swap(static_cast<Learner&>(first), static_cast<Learner&>(second));
        swap(first.stateValue, second.stateValue);
        std::swap(first.defaultValue, second.defaultValue);
        swap(first.sequence, second.sequence);
    }

    MCMCLearner& MCMCLearner::operator=(MCMCLearner source)
    {
        swap(*this, source);
        return *this;
    }

    bool MCMCLearner::save(const char* filename) const
    {
        std::ofstream fileStream(filename, std::ofstream::out);
        if (fileStream.is_open())
        {
            fileStream << *this;
            fileStream << std::endl;
        }
        if (fileStream.good())
        {
            return true;
        }
        return false;
    }

    bool MCMCLearner::load(const char* filename)
    {
        std::ifstream fileStream(filename, std::ifstream::in);
        if (fileStream.good())
        {
            MCMCLearner result;
            fileStream >> result;
            if (fileStream.good())
            {
                swap(*this, result);
                return true;
            }
        }
        return false;
    }

    Vector MCMCLearner::getStateValue(const Vector& state)
    {
        Vector value = defaultValue;
        stateValue.getValue(state, value);
        return value;
    }

    void MCMCLearner::nextState(const Vector& state, const Vector& reward)
    {
        updateRewards(reward);
        // Add this state to the sequence.
        std::tuple<Vector, Vector> step(state, Vector(outputLength));
        sequence.append(step);

        // If a new state, create a new state-value pair.
        stateValue.append(state, defaultValue);
    }

    void MCMCLearner::finalReward(const Vector& reward)
    {
        updateRewards(reward);
        // For each state in the sequence.
        for (Iterator<std::tuple<Vector, Vector> > sequenceIter = sequence.first();
                sequenceIter.valid(); ++sequenceIter)
        {
            std::tuple<Vector, Vector> sequenceStateReward = *sequenceIter;
            Vector sequenceState = std::get<0>(sequenceStateReward);
            Vector sequenceReward = std::get<1>(sequenceStateReward);
            // Find the matching state-value pair.
            Vector currentValue;
            if (stateValue.getValue(sequenceState, currentValue))
            {
                currentValue += (sequenceReward - currentValue) * learnRate;
                stateValue.setValue(sequenceState, currentValue);
            }
        }
        // Start a new sequence.
        sequenceReset();
    }

    void MCMCLearner::sequenceReset()
    {
        sequence.clear();
    }

    void MCMCLearner::adjustLearner(const unsigned int newStateLength,
            const unsigned int newOutputLength)
    {
        // Adjust default value.
        defaultValue = Vector(defaultValue, newOutputLength);
        for (unsigned int index = outputLength; index < newOutputLength;
                ++index)
        {
            if (outputLength > 0)
                defaultValue[index] = defaultValue[outputLength - 1];
            else
                defaultValue[index] = 0.5;
        }

        // Adjust all state value pairs.
        Map<Vector, Vector> newStateValue;
        List<Vector> keyList = stateValue.getKeyList();
        Iterator<Vector> keyIter = keyList.getIterator();
        List<Vector> valueList = stateValue.getValueList();
        Iterator<Vector> valueIter = valueList.getIterator();
        while (keyIter.valid() && valueIter.valid())
        {
            Vector newKey(*keyIter, newStateLength);
            if (!newStateValue.exists(newKey))
            {
                Vector newValue(*valueIter, newOutputLength);
                for (unsigned int index = outputLength; index < newOutputLength;
                        ++index)
                {
                    newValue[index] = defaultValue[index];
                }
                newStateValue.append(newKey, newValue);
            }
            ++keyIter;
            ++valueIter;
        }
        stateValue = newStateValue;

        // Adjsut parent.
        Learner::adjustLearner(newStateLength, newOutputLength);
    }

    Map<Vector, Vector> MCMCLearner::getStateValueMap() const
    {
        return stateValue;
    }

    void MCMCLearner::setStateValueMap(
            Map<Vector, Vector> stateValue)
    {
        this->stateValue = stateValue;
    }

    Vector MCMCLearner::getDefaultValue() const
    {
        return defaultValue;
    }

    void MCMCLearner::setDefaultValue(const double defaultValue)
    {
        this->defaultValue = Vector(outputLength);
        this->defaultValue.fillWith(defaultValue);
    }

    void MCMCLearner::setDefaultValue(const Vector& defaultValue)
    {
        if (defaultValue.size() == outputLength)
        {
            this->defaultValue = defaultValue;
        }
    }

    void MCMCLearner::updateRewards(const Vector& reward)
    {
        if (reward.size() != outputLength) return;
        Vector discountedReward(reward);
        for (Iterator<std::tuple<Vector, Vector> > sequenceIter =
                sequence.last(); sequenceIter.valid(); --sequenceIter)
        {
            std::tuple<Vector, Vector> sequenceStateReward = *sequenceIter;
            Vector sequenceState = std::get<0>(sequenceStateReward);
            Vector sequenceReward = std::get<1>(sequenceStateReward);
            sequenceReward += discountedReward;
            std::tuple<Vector, Vector> newStateReward(
                    sequenceState, sequenceReward);
            sequenceIter.setData(newStateReward);
            discountedReward*= discountRate;
        }
    }

    std::istream& operator>>(std::istream& is, MCMCLearner& destination)
    {
        unsigned int stateLength;
        unsigned int outputLength;
        double learnRate;
        double discountRate;
        Map<Vector, Vector> stateValue;
        Vector defaultValue;
        is >> stateLength;
        is >> outputLength;
        is >> learnRate;
        is >> discountRate;
        is >> stateValue;
        is >> defaultValue;
        if (is.good())
        {
            destination.adjustLearner(stateLength, outputLength);
            destination.setLearningRate(learnRate);
            destination.setDiscountRate(discountRate);
            destination.setStateValueMap(stateValue);
            destination.setDefaultValue(defaultValue);
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const MCMCLearner& source)
    {
        os << source.getStateLength() << '\t';
        os << source.getOutputLength() << '\t';
        os << source.getLearningRate() << '\t';
        os << source.getDiscountRate() << '\t';
        os << source.getStateValueMap() << '\t';
        os << source.getDefaultValue();
        return os;
    }
}
