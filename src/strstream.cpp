// (c) 2016: Marcel Admiraal

#include "strstream.h"

namespace ma
{
    StrStreamBuf::StrStreamBuf(const Str& original) :
        buffer(original), bufferLength(256)
    {
        char* bstart = buffer.c_str();
        char* bend = bstart + buffer.length();
        setg(bstart, bstart, bend);
        setp(bend, bend);
    }

    Str StrStreamBuf::toStr() const
    {
        return buffer;
    }

    int StrStreamBuf::overflow (int c)
    {
        unsigned int currentLength = buffer.length();
        unsigned int goffset = gptr() - buffer.c_str();
        buffer += Str(bufferLength, '\0');
        char* gstart = buffer.c_str();
        char* gnext = gstart + goffset;
        char* pstart = buffer.c_str() + currentLength;
        char* pend = pstart + bufferLength;
        setg(gstart, gnext, pstart);
        setp(pstart, pend);
        return sputc(c);
    }

    int StrStreamBuf::underflow()
    {
        if (gptr() >= pptr()) return EOF;
        char* gstart = buffer.c_str();
        char* gnext = gptr();
        char* gend = pptr();
        setg(gstart, gnext, gend);
        return *gptr();
    }

    StrStream::StrStream() : StrStream(Str())
    {
    }

    StrStream::StrStream(const Str& source) :
            std::iostream(ssbuf = new StrStreamBuf(source))
    {
    }

    StrStream::~StrStream()
    {
        delete ssbuf;
    }

    Str StrStream::toStr() const
    {
        return ssbuf->toStr();
    }
}
