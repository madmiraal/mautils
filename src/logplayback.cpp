// (c) 2016 - 2019: Marcel Admiraal

#include "logplayback.h"

#include "strstream.h"

namespace ma
{
    LogPlayback::LogPlayback() :
        realTime(true), running(true), playing(false), paused(false),
        looping(false), loopCount(0), loopCountRemaining(0),
        readLine(false),
        start(std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::steady_clock::now().time_since_epoch())),
        played(0), loopThread(&LogPlayback::loop, this)
    {
    }

    LogPlayback::~LogPlayback()
    {
        running = false;
        loopThread.join();
    }

    Str LogPlayback::getLogFilename()
    {
        return filename;
    }

    void LogPlayback::setLogFilename(const Str& filename)
    {
        this->filename = filename;
        if (logFile.is_open())
        {
            logFile.close();
            logFile.open(filename.c_str(), std::fstream::out);
        }
    }

    void LogPlayback::setRealtime(bool realTime)
    {
        this->realTime = realTime;
    }

    void LogPlayback::startPlayback()
    {
        if (!playing)
        {
            logFile.open(filename.c_str(), std::fstream::in);
            if (logFile.is_open())
            {
                playing = true;
                readLine = false;
                if (loopCount <= 0) loopCountRemaining = 0;
                else loopCountRemaining = loopCount - 1;
                start = std::chrono::duration_cast<std::chrono::milliseconds>(
                        std::chrono::steady_clock::now().time_since_epoch());
            }
        }
        if (paused)
        {
            start = std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::steady_clock::now().time_since_epoch()) -
                    played;
            paused = false;
        }
    }

    void LogPlayback::pausePlayback()
    {
        if (playing)
        {
            paused = true;
        }
    }

    void LogPlayback::stopPlayback()
    {
        paused = false;
        if (playing)
        {
            playing = false;
            logFile.close();
        }
    }

    bool LogPlayback::isPlaying() const
    {
        return playing;
    }

    bool LogPlayback::isPaused() const
    {
        return paused;
    }

    bool LogPlayback::isPlaybackLooping() const
    {
        return looping;
    }

    void LogPlayback::loopPlayback(const bool enable)
    {
        looping = enable;
    }

    int LogPlayback::getLoopPlaybackCountRemaining() const
    {
        return loopCountRemaining;
    }

    int LogPlayback::getLoopPlaybackCount() const
    {
        return loopCount;
    }

    void LogPlayback::setLoopPlaybackCount(const int loopCount)
    {
        this->loopCount = loopCount;
    }

    void LogPlayback::insertDelayedEntry(const std::chrono::milliseconds delay,
        const Str& entry)
    {
        delayedEntries.insert((played + delay).count(), entry);
    }

    bool LogPlayback::processEntry(const Str& entry)
    {
        if (entry == "Logging Started")
            logFileStart();
        else if (entry == "Logging Stopped")
            logFileEnd();
        else
            return false;

        return true;
    }

    void LogPlayback::logFileStart()
    {
    }

    void LogPlayback::logFileEnd()
    {
    }

    void LogPlayback::loop()
    {
        while (running)
        {
            if (playing && !paused)
            {
                if (!logFile.good() && looping &&
                    (loopCountRemaining > 0 || loopCount == -1))
                {
                    --loopCountRemaining;
                    readLine = false;
                    logFile.clear();
                    logFile.seekg(0, logFile.beg);
                }
                if (!readLine && logFile.good())
                {
                    logLine = getNext(logFile);
                    unsigned int timeStamp = getTimeStamp(logLine);
                    if (timeStamp != (unsigned int)-1)
                    {
                        next = std::chrono::milliseconds(timeStamp);
                        readLine = true;
                    }
                }
                if (delayedEntries.hasItems())
                {
                    std::chrono::milliseconds nextDelayed(delayedEntries.peek());
                    if ((!readLine || nextDelayed < next) &&
                        processNow(nextDelayed))
                    {
                        Str delayedEntry = delayedEntries.pop();
                        if (!processEntry(delayedEntry))
                        {
                            std::cerr << "Delayed entry not processed:";
                            std::cerr << delayedEntry << std::endl;
                        }
                    }
                }
                if (readLine && processNow(next))
                {
                    if (!processLogLine(logLine))
                    {
                        std::cerr << "Log line not processed:";
                        std::cerr << logLine << std::endl;
                    }
                    readLine = false;
                }
            }
            std::this_thread::yield();
        }
    }

    unsigned int LogPlayback::getTimeStamp(const Str& logLine)
    {
        StrStream lineStream(logLine);
        unsigned int timeStamp = (unsigned int)-1;
        lineStream >> timeStamp;
        return timeStamp;
    }

    bool LogPlayback::processNow(std::chrono::milliseconds timeStamp)
    {
        if (!realTime) return true;
        std::chrono::milliseconds now =
            std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::steady_clock::now().time_since_epoch());
        played = now - start;
        if (played > next) return true;
        return false;
    }

    bool LogPlayback::processLogLine(const Str& logLine)
    {
        // Strip off the timestamp and the tab.
        StrStream lineStream(logLine);
        unsigned int timeStamp;
        Str entry;
        lineStream >> timeStamp;
        lineStream >> entry;
        entry = entry.trimLeft(1);
        // Process the entry.
        return processEntry(entry);
    }
}
