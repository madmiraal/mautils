// (c) 2011 - 2017: Marcel Admiraal

#include "vector3d.h"

namespace ma
{
    Vector3D::Vector3D() :
        Vector(3)
    {
    }

    Vector3D::Vector3D(const double x, const double y, const double z) :
        Vector(3)
    {
        element[0] = x;
        element[1] = y;
        element[2] = z;
    }

    Vector3D::Vector3D(const int* element) : Vector(element, 3)
    {
    }

    Vector3D::Vector3D(const unsigned int* element) : Vector(element, 3)
    {
    }

    Vector3D::Vector3D(const short* element) : Vector(element, 3)
    {
    }

    Vector3D::Vector3D(const float* element) : Vector(element, 3)
    {
    }

    Vector3D::Vector3D(const double* element) : Vector(element, 3)
    {
    }

    Vector3D::Vector3D(const Vector3D& source) : Vector(source)
    {
    }

    Vector3D::~Vector3D()
    {
    }

    void swap(Vector3D& first, Vector3D& second)
    {
        swap(static_cast<Vector&>(first), static_cast<Vector&>(second));
    }

    Vector3D& Vector3D::operator=(Vector3D source)
    {
        swap(*this, source);
        return *this;
    }

    double Vector3D::getX() const
    {
        return element[0];
    }

    double Vector3D::getY() const
    {
        return element[1];
    }

    double Vector3D::getZ() const
    {
        return element[2];
    }

    void Vector3D::setX(const double x)
    {
        element[0] = x;
    }

    void Vector3D::setY(const double y)
    {
        element[1] = y;
    }

    void Vector3D::setZ(const double z)
    {
        element[2] = z;
    }

    Vector3D Vector3D::unitVector() const
    {
        return ma::unitVector(*this);
    }

    Vector3D Vector3D::fastUnitVector() const
    {
        return ma::fastUnitVector(*this);
    }

    Vector3D& Vector3D::operator+=(const Vector3D& right)
    {
        Vector::operator+=(right);
        return *this;
    }

    Vector3D& Vector3D::operator-=(const Vector3D& right)
    {
        Vector::operator-=(right);
        return *this;
    }

    Vector3D& Vector3D::operator*=(const Vector3D& right)
    {
        Vector::operator*=(right);
        return *this;
    }

    Vector3D& Vector3D::operator*=(const double right)
    {
        Vector::operator*=(right);
        return *this;
    }

    Vector3D& Vector3D::operator/=(const Vector3D& right)
    {
        Vector::operator/=(right);
        return *this;
    }

    Vector3D& Vector3D::operator/=(const double right)
    {
        Vector::operator/=(right);
        return *this;
    }

    Vector3D Vector3D::cross(const Vector3D& right) const
    {
        Vector3D result;
        // newX = y * right.z - z * right.y;
        result[0] = element[1] * right.element[2]
                    - element[2] * right.element[1];
        // newY = z * right.x - x * right.z;
        result[1] = element[2] * right.element[0]
                    - element[0] * right.element[2];
        // newZ = x * right.y - y * right.x;
        result[2] = element[0] * right.element[1]
                    - element[1] * right.element[0];
        return result;
    }

    Vector3D unitVector(Vector3D source)
    {
        source.normalise();
        return source;
    }

    Vector3D fastUnitVector(Vector3D source)
    {
        source.fastNormalise();
        return source;
    }

    Vector3D operator+(Vector3D left, const Vector3D& right)
    {
        return left += right;
    }

    Vector3D operator-(Vector3D left, const Vector3D& right)
    {
        return left -= right;
    }

    Vector3D operator-(const Vector3D& source)
    {
        Vector3D result;
        result -= source;
        return result;
    }

    Vector3D operator*(Vector3D left, const Vector3D& right)
    {
        return left *= right;
    }

    Vector3D operator*(Vector3D left, const double right)
    {
        return left *= right;
    }

    Vector3D operator*(const double left, Vector3D right)
    {
        return right *= left;
    }

    Vector3D operator/(Vector3D left, const Vector3D& right)
    {
        return left /= right;
    }

    Vector3D operator/(Vector3D left, const double right)
    {
        return left /= right;
    }

    Vector3D operator/(const double left, const Vector3D& right)
    {
        Vector3D result;
        result[0] = left / right[0];
        result[1] = left / right[1];
        result[2] = left / right[2];
        return result;
    }

    Vector3D midPoint(const Vector3D& a, const Vector3D& b)
    {
        Vector3D result = (a + b) / 2;
        return result;
    }
}
