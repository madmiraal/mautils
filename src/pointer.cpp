// (c) 2016: Marcel Admiraal

#include "pointer.h"

#include "str.h"

#ifdef _WIN32
    #ifndef _WIN32_WINNT
        #define _WIN32_WINNT 0x500
    #endif // _WIN32_WINNT
    #include <windows.h>
#endif // _WIN32
#ifdef __unix
    #include "device.h"

    #include <linux/input.h>
    #include <sys/time.h>
    #include <unistd.h>
    #include <fcntl.h>
#endif // __unix

namespace ma
{
    Pointer::Pointer()
        #ifdef __unix
            : fd(0)
        #endif // __unix
    {
        #ifdef __unix
            setMouseEventStream();
        #endif // __unix
    }

    Pointer::~Pointer()
    {
        #ifdef __unix
            if (fd > 0) close(fd);
        #endif // __unix
    }

    bool Pointer::isValid()
    {
        #ifdef _WIN32
            return true;
        #endif // _WIN32
        #ifdef __unix
            return fd > 0;
        #endif // __unix
    }

    bool Pointer::moveX(const int pixels)
    {
        if (!isValid()) return false;
        #ifdef _WIN32
            INPUT xMoveEvent = {0};
            xMoveEvent.type = INPUT_MOUSE;
            xMoveEvent.mi.dwFlags = MOUSEEVENTF_MOVE;
            xMoveEvent.mi.dx = pixels;
            SendInput(1, &xMoveEvent, sizeof(INPUT));
        #endif // _WIN32
        #ifdef __unix
            struct input_event xMoveEvent = {0};
            struct input_event syncEvent = {0};
            gettimeofday(&xMoveEvent.time, 0);
            xMoveEvent.type = EV_REL;
            xMoveEvent.code = REL_X;
            xMoveEvent.value = pixels;
            gettimeofday(&syncEvent.time, 0);
            syncEvent.type = EV_SYN;
            syncEvent.code = SYN_REPORT;
            syncEvent.value = 0;
            if (write(fd, &xMoveEvent, sizeof(struct input_event))
                    != sizeof(struct input_event)) return false;
            if (write(fd, &syncEvent, sizeof(struct input_event))
                    != sizeof(struct input_event)) return false;
        #endif // __unix
        return true;
    }

    bool Pointer::moveY(const int pixels)
    {
        if (!isValid()) return false;
        #ifdef _WIN32
            INPUT yMoveEvent = {0};
            yMoveEvent.type = INPUT_MOUSE;
            yMoveEvent.mi.dwFlags = MOUSEEVENTF_MOVE;
            yMoveEvent.mi.dy = pixels;
            SendInput(1, &yMoveEvent, sizeof(INPUT));
        #endif // _WIN32
        #ifdef __unix
            struct input_event yMoveEvent = {0};
            struct input_event syncEvent = {0};
            gettimeofday(&yMoveEvent.time, 0);
            yMoveEvent.type = EV_REL;
            yMoveEvent.code = REL_Y;
            yMoveEvent.value = pixels;
            gettimeofday(&syncEvent.time, 0);
            syncEvent.type = EV_SYN;
            syncEvent.code = SYN_REPORT;
            syncEvent.value = 0;
            if (write(fd, &yMoveEvent, sizeof(struct input_event))
                    != sizeof(struct input_event)) return false;
            if (write(fd, &syncEvent, sizeof(struct input_event))
                    != sizeof(struct input_event)) return false;
        #endif // __unix
        return true;
    }

    bool Pointer::leftClick()
    {
        if (!isValid()) return false;
        #ifdef _WIN32
            INPUT leftPressEvent = {0};
            INPUT leftReleaseEvent = {0};
            leftPressEvent.type = INPUT_MOUSE;
            leftPressEvent.mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
            leftReleaseEvent.type = INPUT_MOUSE;
            leftReleaseEvent.mi.dwFlags = MOUSEEVENTF_LEFTUP;
            SendInput(1, &leftPressEvent, sizeof(INPUT));
            SendInput(1, &leftReleaseEvent, sizeof(INPUT));
        #endif // _WIN32
        #ifdef __unix
            struct input_event leftPressEvent = {0};
            struct input_event leftReleaseEvent = {0};
            struct input_event syncEvent1 = {0};
            struct input_event syncEvent2 = {0};
            gettimeofday(&leftPressEvent.time, 0);
            leftPressEvent.type = EV_KEY;
            leftPressEvent.code = BTN_LEFT;
            leftPressEvent.value = 1;
            gettimeofday(&leftReleaseEvent.time, 0);
            leftReleaseEvent.type = EV_KEY;
            leftReleaseEvent.code = BTN_LEFT;
            leftReleaseEvent.value = 0;
            gettimeofday(&syncEvent1.time, 0);
            syncEvent1.type = EV_SYN;
            syncEvent1.code = SYN_REPORT;
            syncEvent1.value = 0;
            gettimeofday(&syncEvent2.time, 0);
            syncEvent2.type = EV_SYN;
            syncEvent2.code = SYN_REPORT;
            syncEvent2.value = 0;
            if (write(fd, &leftPressEvent, sizeof(struct input_event))
                    != sizeof(struct input_event)) return false;
            if (write(fd, &syncEvent1, sizeof(struct input_event))
                    != sizeof(struct input_event)) return false;
            if (write(fd, &leftReleaseEvent, sizeof(struct input_event))
                    != sizeof(struct input_event)) return false;
            if (write(fd, &syncEvent2, sizeof(struct input_event))
                    != sizeof(struct input_event)) return false;
        #endif // __unix
        return true;
    }

    bool Pointer::rightClick()
    {
        if (!isValid()) return false;
        #ifdef _WIN32
            INPUT rightPressEvent = {0};
            INPUT rightReleaseEvent = {0};
            rightPressEvent.type = INPUT_MOUSE;
            rightPressEvent.mi.dwFlags = MOUSEEVENTF_RIGHTDOWN;
            rightReleaseEvent.type = INPUT_MOUSE;
            rightReleaseEvent.mi.dwFlags = MOUSEEVENTF_RIGHTUP;
            SendInput(1, &rightPressEvent, sizeof(INPUT));
            SendInput(1, &rightReleaseEvent, sizeof(INPUT));
        #endif // _WIN32
        #ifdef __unix
            struct input_event rightPressEvent = {0};
            struct input_event rightReleaseEvent = {0};
            struct input_event syncEvent1 = {0};
            struct input_event syncEvent2 = {0};
            gettimeofday(&rightPressEvent.time, 0);
            rightPressEvent.type = EV_KEY;
            rightPressEvent.code = BTN_RIGHT;
            rightPressEvent.value = 1;
            gettimeofday(&rightReleaseEvent.time, 0);
            rightReleaseEvent.type = EV_KEY;
            rightReleaseEvent.code = BTN_RIGHT;
            rightReleaseEvent.value = 0;
            gettimeofday(&syncEvent1.time, 0);
            syncEvent1.type = EV_SYN;
            syncEvent1.code = SYN_REPORT;
            syncEvent1.value = 0;
            gettimeofday(&syncEvent2.time, 0);
            syncEvent2.type = EV_SYN;
            syncEvent2.code = SYN_REPORT;
            syncEvent2.value = 0;
            if (write(fd, &rightPressEvent, sizeof(struct input_event))
                    != sizeof(struct input_event)) return false;
            if (write(fd, &syncEvent1, sizeof(struct input_event))
                    != sizeof(struct input_event)) return false;
            if (write(fd, &rightReleaseEvent, sizeof(struct input_event))
                    != sizeof(struct input_event)) return false;
            if (write(fd, &syncEvent2, sizeof(struct input_event))
                    != sizeof(struct input_event)) return false;
        #endif // __unix
        return true;
    }

    #ifdef __unix
        void Pointer::setMouseEventStream()
        {
            ma::List<Device> devices = getInputDevices();
            for (ma::Iterator<Device> iter = devices.first(); iter.valid(); ++iter)
            {
                Device thisDevice = *iter;
                if (thisDevice.isMouse())
                {
                    ma::Str mouseEvent = thisDevice.getMouseEvent();
                    ma::Str mouseEventFile = "/dev/input/" + mouseEvent;
                    fd = open(mouseEventFile.c_str(), O_RDWR);
                    if (fd > 0) return;
                }
            }
        }
    #endif // __unix
}
