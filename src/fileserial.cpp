// (c) 2018: Marcel Admiraal

#include "fileserial.h"

#include <string>
#include <sstream>

namespace ma
{
    FileSerial::FileSerial() : Serial()
    {
    }

    FileSerial::~FileSerial()
    {
    }

    Str FileSerial::getPortString() const
    {
        return filename;
    }

	bool FileSerial::setPort(const Str& fileName)
	{
        return setPort(fileName,
                std::fstream::in | std::fstream::out | std::fstream::app);
	}

	bool FileSerial::setPort(const Str& fileName, std::ios_base::openmode mode)
	{
        if (file.is_open()) file.close();
        file.open(fileName.c_str(), mode);
        if (isConnected())
        {
            this->filename = fileName.c_str();
            return true;
        }
        return false;
	}

    bool FileSerial::setParameters(
            const int baud,
            const int bits,
            const int parity,
            const int stopBits,
            const bool block
            )
    {
        return true;
    }

    unsigned long FileSerial::writeData(const unsigned char *buffer,
        const unsigned int bufferLength)
    {
        if (bufferLength == 0) return 0;
        file << (int)buffer[0];
        for (unsigned int index = 1; index < bufferLength; ++index)
            file << "," << (int)buffer[index];
        file << std::endl;
        if (!file.good()) return 0;
        file.flush();
        return bufferLength;
    }

    unsigned long FileSerial::readData(unsigned char *buffer,
        const unsigned int bufferLength)
    {
        unsigned int bytesRead = 0;
        std::string line;
        std::getline(file, line);
        std::stringstream lineStream(line);
        while (lineStream.good() && bytesRead < bufferLength)
        {
            int value;
            char comma = '\0';
            lineStream >> value;
            if (lineStream.good())
                buffer[bytesRead++] = (unsigned char)value;
            lineStream >> comma;
            if (comma != ',') break;
        }
        return bytesRead;
    }

    unsigned long FileSerial::readDoubleData(double *buffer,
        const unsigned int bufferLength)
    {
        unsigned int valuesRead = 0;
        std::string line;
        std::getline(file, line);
        std::stringstream lineStream(line);
        while (lineStream.good() && valuesRead < bufferLength)
        {
            double value;
            char comma;
            lineStream >> value;
            if (lineStream.good())
                buffer[valuesRead++] = value;
            lineStream >> comma;
            if (comma != ',') break;
        }
        return valuesRead;
    }

    void FileSerial::clearWriteBuffer()
    {
        if (file.is_open())
        {
            file.close();
            file.open(filename.c_str(),
                    std::fstream::in | std::fstream::out | std::fstream::trunc);
        }
    }

    void FileSerial::clearReadBuffer()
    {
        file.clear();
        file.seekg(0, std::ios::beg);
    }

    bool FileSerial::isConnected() const
    {
        return (file.is_open() && file.good());
    }

    void FileSerial::disconnect()
    {
        file.close();
    }

    bool FileSerial::eof()
    {
        return file.eof();
    }
}
