// (c) 2017: Marcel Admiraal

#include "vectorcomplex.h"
#include "utils.h"

#include <algorithm>
#include <cmath>
#include <cfloat>
#include <fstream>

namespace ma
{
    VectorComplex::VectorComplex() :
        elements(0), element(0)
    {
    }

    VectorComplex::~VectorComplex()
    {
        if (element != 0)
            delete[] element;
    }

    VectorComplex::VectorComplex(const unsigned int elements) :
        elements(elements), element(0)
    {
        if (elements != 0)
            element = new Complex[elements]();
    }

    VectorComplex::VectorComplex(const Complex* element,
            const unsigned int elements) : elements(elements), element(0)
    {
        if (elements != 0)
        {
            this->element = new Complex[elements];
            for (unsigned int index = 0; index < elements; ++index)
            {
                this->element[index] = element[index];
            }
        }
    }

    VectorComplex::VectorComplex(const int* element,
            const unsigned int elements) : elements(elements), element(0)
    {
        if (elements != 0)
        {
            this->element = new Complex[elements];
            for (unsigned int index = 0; index < elements; ++index)
            {
                this->element[index] = element[index];
            }
        }
    }

    VectorComplex::VectorComplex(const unsigned int* element,
            const unsigned int elements) : elements(elements), element(0)
    {
        if (elements != 0)
        {
            this->element = new Complex[elements];
            for (unsigned int index = 0; index < elements; ++index)
            {
                this->element[index] = element[index];
            }
        }
    }

    VectorComplex::VectorComplex(const short* element,
            const unsigned int elements) : elements(elements), element(0)
    {
        if (elements != 0)
        {
            this->element = new Complex[elements];
            for (unsigned int index = 0; index < elements; ++index)
            {
                this->element[index] = element[index];
            }
        }
    }

    VectorComplex::VectorComplex(const float* element,
            const unsigned int elements) : elements(elements), element(0)
    {
        if (elements != 0)
        {
            this->element = new Complex[elements];
            for (unsigned int index = 0; index < elements; ++index)
            {
                this->element[index] = element[index];
            }
        }
    }

    VectorComplex::VectorComplex(const double* element,
            const unsigned int elements) : elements(elements), element(0)
    {
        if (elements != 0)
        {
            this->element = new Complex[elements];
            for (unsigned int index = 0; index < elements; ++index)
            {
                this->element[index] = element[index];
            }
        }
    }

    VectorComplex::VectorComplex(const VectorComplex& source) :
        elements(source.elements), element(0)
    {
        if (elements != 0)
        {
            this->element = new Complex[elements];
            for (unsigned int index = 0; index < elements; ++index)
            {
                element[index] = source.element[index];
            }
        }
    }

    VectorComplex::VectorComplex(const char* filename) :
        elements(0), element(0)
    {
        load(filename);
    }

    void swap(VectorComplex& first, VectorComplex& second)
    {
        std::swap(first.elements, second.elements);
        std::swap(first.element, second.element);
    }

    VectorComplex& VectorComplex::operator=(VectorComplex source)
    {
        swap(*this, source);
        return *this;
    }

    Complex& VectorComplex::operator[](const unsigned int index)
    {
        return element[index];
    }

    const Complex& VectorComplex::operator[](const unsigned int index) const
    {
        return element[index];
    }

    unsigned int VectorComplex::size() const
    {
        return elements;
    }

    VectorComplex VectorComplex::getSubvector(const unsigned int start,
            const unsigned int length) const
    {
        VectorComplex result(length);
        for (unsigned int index = 0; index < length; ++index)
        {
            if (start + index < elements)
            {
                result[index] = element[start + index];
            }
            else
            {
                result[index] = 0.0;
            }
        }
        return result;
    }

    VectorComplex& VectorComplex::setSubVectorComplex(const unsigned int start,
            const VectorComplex& source)
    {
        if (start + source.elements > elements)
        {
            extend((start + source.elements) - elements);
        }
        for (unsigned int index = 0; index < source.elements; ++index)
        {
            element[start + index] = source[index];
        }
        return *this;
    }

    void VectorComplex::getArray(Complex* output) const
    {
        for (unsigned int index = 0; index < elements; ++index)
        {
            output[index] = element[index];
        }
    }

    void VectorComplex::setArray(const Complex* source)
    {
        for (unsigned int index = 0; index < elements; ++index)
        {
            element[index] = source[index];
        }
    }

    void VectorComplex::setArray(const int* source)
    {
        for (unsigned int index = 0; index < elements; ++index)
        {
            element[index] = source[index];
        }
    }

    void VectorComplex::setArray(const float* source)
    {
        for (unsigned int index = 0; index < elements; ++index)
        {
            element[index] = source[index];
        }
    }

    void VectorComplex::setArray(const double* source)
    {
        for (unsigned int index = 0; index < elements; ++index)
        {
            element[index] = source[index];
        }
    }

    VectorComplex& VectorComplex::extend(const unsigned int elements,
            const double value)
    {
        Complex* newElement = new Complex[this->elements + elements]();
        for (unsigned int index = 0; index < this->elements; ++index)
        {
            newElement[index] = element[index];
        }
        for (unsigned int index = this->elements;
                index < this->elements + elements; ++index)
        {
            newElement[index] = value;
        }
        this->elements += elements;
        std::swap(element, newElement);
        delete[] newElement;
        return *this;
    }

    VectorComplex& VectorComplex::operator+=(const VectorComplex& right)
    {
        if (right.elements > elements)
        {
            extend(right.elements - elements);
        }
        for (unsigned int index = 0; index < elements && index < right.elements;
                ++index)
        {
            element[index] += right.element[index];
        }
        return *this;
    }

    VectorComplex& VectorComplex::operator+=(const Complex& right)
    {
        for (unsigned int index = 0; index < elements; ++index)
        {
            element[index] += right;
        }
        return *this;
    }

    VectorComplex& VectorComplex::operator-=(const VectorComplex& right)
    {
        if (right.elements > elements)
        {
            extend(right.elements - elements);
        }
        for (unsigned int index = 0; index < elements && index < right.elements;
                ++index)
        {
            element[index] -= right.element[index];
        }
        return *this;
    }

    VectorComplex VectorComplex::operator-=(const Complex& right)
    {
        for (unsigned int index = 0; index < elements; ++index)
        {
            element[index] -= right;
        }
        return *this;
    }

    VectorComplex& VectorComplex::operator*=(const VectorComplex& right)
    {
        if (right.elements > elements)
        {
            extend(right.elements - elements);
        }
        for (unsigned int index = 0; index < elements && index < right.elements;
                ++index)
        {
            element[index] *= right.element[index];
        }
        return *this;
    }

    VectorComplex& VectorComplex::operator*=(const double right)
    {
        for (unsigned int index = 0; index < elements; ++index)
        {
            element[index] *= right;
        }
        return *this;
    }

    VectorComplex& VectorComplex::operator/=(const VectorComplex& right)
    {
        if (right.elements > elements)
        {
            extend(right.elements - elements);
        }
        for (unsigned int index = 0; index < elements && index < right.elements;
                ++index)
        {
            element[index] /= right.element[index];
        }
        return *this;
    }

    VectorComplex& VectorComplex::operator/=(const double right)
    {
        for (unsigned int index = 0; index < elements; ++index)
        {
            element[index] /= right;
        }
        return *this;
    }

    Vector VectorComplex::real() const
    {
        Vector result(elements);
        for (unsigned int index = 0; index < elements; ++index)
        {
            result[index] = element[index].real();
        }
        return result;
    }

    Vector VectorComplex::getReal(const double threshold) const
    {
        unsigned int realCount = 0;
        for (unsigned int index = 0; index < elements; ++index)
            if (element[index].isReal(threshold)) ++realCount;

        Vector result(realCount);
        realCount = 0;
        for (unsigned int index = 0; index < elements; ++index)
            if (element[index].isReal(threshold))
                result[realCount++] = element[index].real();

        return result;
    }

    Vector VectorComplex::imag() const
    {
        Vector result(elements);
        for (unsigned int index = 0; index < elements; ++index)
        {
            result[index] = element[index].imag();
        }
        return result;
    }

    VectorComplex VectorComplex::getComplex(const double threshold) const
    {
        unsigned int imagCount = 0;
        for (unsigned int index = 0; index < elements; ++index)
            if (!element[index].isReal(threshold))
                ++imagCount;

        VectorComplex result(imagCount);
        imagCount = 0;
        for (unsigned int index = 0; index < elements; ++index)
            if (!element[index].isReal(threshold))
                result[imagCount++] = element[index];

        return result;
    }

    VectorComplex VectorComplex::getPosImag(const double threshold) const
    {
        unsigned int imagCount = 0;
        for (unsigned int index = 0; index < elements; ++index)
            if (!element[index].isReal(threshold) && element[index].imag() > 0)
                ++imagCount;

        VectorComplex result(imagCount);
        imagCount = 0;
        for (unsigned int index = 0; index < elements; ++index)
            if (!element[index].isReal(threshold) && element[index].imag() > 0)
                result[imagCount++] = element[index];

        return result;
    }

    VectorComplex VectorComplex::getNegImag(const double threshold) const
    {
        unsigned int imagCount = 0;
        for (unsigned int index = 0; index < elements; ++index)
            if (!element[index].isReal(threshold) && element[index].imag() < 0)
                ++imagCount;

        VectorComplex result(imagCount);
        imagCount = 0;
        for (unsigned int index = 0; index < elements; ++index)
            if (!element[index].isReal(threshold) && element[index].imag() < 0)
                result[imagCount++] = element[index];

        return result;
    }

    Vector VectorComplex::abs() const
    {
        Vector result(elements);
        for (unsigned int index = 0; index < elements; ++index)
        {
            result[index] = element[index].abs();
        }
        return result;
    }

    Complex VectorComplex::dot(const VectorComplex& other) const
    {
        VectorComplex result = *this * other;
        return result.elementSum();
    }

    VectorComplex VectorComplex::concatenate(const VectorComplex& other)
    {
        unsigned int start = elements;
        extend(other.elements);
        for (unsigned int index = 0; index < other.elements; ++index)
        {
            element[start + index] = other.element[index];
        }
        return *this;
    }

    Complex VectorComplex::elementSum() const
    {
        Complex sum = 0;
        for (unsigned int index = 0; index < elements; ++index)
        {
            sum += element[index];
        }
        return sum;
    }

    Complex VectorComplex::elementSquaredSum() const
    {
        Complex sum = 0;
        for (unsigned int index = 0; index < elements; ++index)
        {
            sum += element[index] * element[index];
        }
        return sum;
    }

    Complex VectorComplex::elementProduct() const
    {
        Complex product = 1;
        for (unsigned int index = 0; index < elements; ++index)
        {
            product *= element[index];
        }
        return product;
    }

    VectorComplex& VectorComplex::fillWith(const double value)
    {
        for (unsigned int index = 0; index < elements; ++index)
        {
            element[index] = value;
        }
        return *this;
    }

    VectorComplex VectorComplex::subVectorComplex(const unsigned int first,
        const unsigned int length) const
    {
        VectorComplex result(length);
        for (unsigned int index = 0; index < length && first + index < elements;
                ++index)
        {
            result.element[index] = element[first + index];
        }
        return result;
    }

    void VectorComplex::print(std::ostream& os) const
    {
        os << "[";
        if (elements > 0)
        {
            for (unsigned int index = 0; index < elements - 1; ++index)
            {
                os << element[index] << ", ";
            }
            os << element[elements-1];
        }
        os << "]";
    }

    bool VectorComplex::save(const char* filename) const
    {
        std::ofstream fileStream(filename);
        if (fileStream.is_open())
        {
            fileStream << *this;
            fileStream << std::endl;
            if (fileStream.good())
            {
                fileStream.close();
                return true;
            }
        }
        return false;
    }

    bool VectorComplex::load(const char* filename)
    {
        std::ifstream fileStream(filename, std::ifstream::in);
        if (fileStream.is_open())
        {
            VectorComplex result;
            fileStream >> result;
            if (fileStream.good())
            {
                swap(*this, result);
                fileStream.close();
                return true;
            }
        }
        return false;
    }

    VectorComplex operator+(VectorComplex left, const VectorComplex& right)
    {
        left += right;
        return left;
    }

    VectorComplex operator+(VectorComplex left, const Complex& right)
    {
        left += right;
        return left;
    }

    VectorComplex operator+(const Complex& left, VectorComplex right)
    {
        right += left;
        return right;
    }

    VectorComplex operator-(VectorComplex left, const VectorComplex& right)
    {
        left -= right;
        return left;
    }

    VectorComplex operator-(VectorComplex left, const Complex& right)
    {
        left -= right;
        return left;
    }

    VectorComplex operator-(const Complex& left, const VectorComplex& right)
    {
        VectorComplex result = -right;
        result += left;
        return result;
    }

    VectorComplex operator-(const VectorComplex& source)
    {
        VectorComplex result(source.size());
        result -= source;
        return result;
    }

    VectorComplex operator*(VectorComplex left, const VectorComplex& right)
    {
        left *= right;
        return left;
    }

    VectorComplex operator*(VectorComplex left, const double right)
    {
        left *= right;
        return left;
    }

    VectorComplex operator*(const double left, VectorComplex right)
    {
        right *= left;
        return right;
    }

    VectorComplex operator/(VectorComplex left, const VectorComplex& right)
    {
        left /= right;
        return left;
    }

    VectorComplex operator/(VectorComplex left, const double right)
    {
        left /= right;
        return left;
    }

    VectorComplex operator/(const double left, const VectorComplex& right)
    {
        VectorComplex result(right.size());
        for (unsigned int index = 0; index < right.size(); ++index)
        {
            result[index] = left / right[index];
        }
        return result;
    }

    bool operator==(const VectorComplex& left, const VectorComplex& right)
    {
        if (left.size() != right.size()) return false;
        for (unsigned int index = 0; index < left.size(); ++index)
        {
            if (left[index] != right[index]) return false;
        }
        return true;
    }

    bool operator!=(const VectorComplex& left, const VectorComplex& right)
    {
        return !(left == right);
    }

    bool approxEqual(const VectorComplex& left, const VectorComplex& right,
            const double error)
    {
        if (left.size() != right.size()) return false;
        for (unsigned int index = 0; index < left.size(); ++index)
        {
            if (!approxEqual(left[index], right[index], error)) return false;
        }
        return true;
    }

    std::istream& operator>>(std::istream& is, VectorComplex& destination)
    {
        unsigned int elements;
        is >> elements;
        if (is.good())
        {
            VectorComplex result(elements);
            for (unsigned int index = 0; index < elements; ++index)
            {
                is >> result[index];
            }
            if (is.good())
            {
                swap(destination, result);
            }
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const VectorComplex& source)
    {
        unsigned int elements = source.size();
        os << elements;
        if (elements > 0)
        {
            os << '\t';
            for (unsigned int index = 0; index < elements - 1; ++index)
            {
                os << source[index] << '\t';
            }
            os << source[elements-1];
        }
        return os;
    }

    Vector real(const VectorComplex& source)
    {
        return source.real();
    }

    Vector imag(const VectorComplex& source)
    {
        return source.imag();
    }

    Complex dot(const VectorComplex& left, const VectorComplex& right)
    {
        return left.dot(right);
    }

    VectorComplex complexZeros(const unsigned int elements)
    {
        VectorComplex z(elements);
        z.fillWith(0.0);
        return z;
    }

    VectorComplex complexOnes(const unsigned int elements)
    {
        VectorComplex o(elements);
        o.fillWith(1.0);
        return o;
    }

    VectorComplex concatenate(const VectorComplex& left,
            const VectorComplex& right)
    {
        VectorComplex result(left);
        result.concatenate(right);
        return result;
    }

    Complex elementSum(const VectorComplex& source)
    {
        return source.elementSum();
    }

    Complex elementSquaredSum(const VectorComplex& source)
    {
        return source.elementSquaredSum();
    }

    Complex elementProduct(const VectorComplex& source)
    {
        return source.elementProduct();
    }

    void print(const VectorComplex& source, std::ostream& os)
    {
        source.print(os);
    }
}
