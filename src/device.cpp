// (c) 2016: Marcel Admiraal

#ifdef __unix

#include "device.h"

#include "strstream.h"

#include <linux/input.h>

#include <algorithm>
#include <fstream>

namespace ma
{
    IOEvent::IOEvent(Str name) : name(name)
    {
    }

    IOEvent::~IOEvent()
    {
    }

    void IOEvent::addValue(Str value)
    {
        values.append(value);
    }

    Str IOEvent::getName()
    {
        return name;
    }

    Device::Device() : null(true)
    {
    }

    Device::~Device()
    {
    }

    Device::Device(const Device& source) :
        bus(source.bus), vendor(source.vendor), product(source.product),
        version(source.version), name(source.name),
        physicalDevice(source.physicalDevice),
        systemFile(source.systemFile),
        handlers(source.handlers), events(source.events), null(source.null)
    {
    }

    void swap(Device& first, Device& second)
    {
        swap(first.bus, second.bus);
        swap(first.vendor, second.vendor);
        swap(first.product, second.product);
        swap(first.version, second.version);
        swap(first.name, second.name);
        swap(first.physicalDevice, second.physicalDevice);
        swap(first.systemFile, second.systemFile);
        swap(first.uniqueId, second.uniqueId);
        swap(first.handlers, second.handlers);
        swap(first.events, second.events);
        std::swap(first.null, second.null);
    }

    Device& Device::operator=(Device source)
    {
        swap(*this, source);
        return *this;
    }

    Str Device::getBus() const
    {
        return bus;
    }

    Str Device::getVendor() const
    {
        return vendor;
    }

    Str Device::getProduct() const
    {
        return product;
    }

    Str Device::getVersion() const
    {
        return version;
    }

    Str Device::getName() const
    {
        return name;
    }

    Str Device::getPhysicalDevice() const
    {
        return physicalDevice;
    }

    Str Device::getSystemFile() const
    {
        return systemFile;
    }

    Str Device::getUniqueIdentifier() const
    {
        return uniqueId;
    }

    Str Device::getHandlers() const
    {
        Str handlerStr;
        for (Iterator<Str> iter = handlers.first(); iter.valid(); ++iter)
        {
            handlerStr += (*iter) + " ";
        }
        return handlerStr;
    }

    bool Device::isNull() const
    {
        return null;
    }

    bool Device::setIdentity(Str& line)
    {
        StrStream lineStream(line);
        Str word = getNext(lineStream, ' ');
        if (word != "I:") return false;
        while (word.length() != 0)
        {
            word = getNext(lineStream, ' ');
            if (word.left(4) == "Bus=") bus = word.trimLeft(4);
            if (word.left(7) == "Vendor=") vendor = word.trimLeft(7);
            if (word.left(8) == "Product=") product = word.trimLeft(8);
            if (word.left(8) == "Version=") version = word.trimLeft(8);
        }
        null = false;
        return true;
    }

    bool Device::setName(const Str& line)
    {
        if (line.left(8) != "N: Name=") return false;
        if (line[8] == '\"' && line.right(1) == "\"")
            name = line.trimLeft(9).trimRight(1);
        else
            name = line.trimLeft(8);
        null = false;
        return true;
    }

    bool Device::setPhysicalDevice(const Str& line)
    {
        if (line.left(8) != "P: Phys=") return false;
        physicalDevice = line.trimLeft(8);
        null = false;
        return true;
    }

    bool Device::setSystemFile(const Str& line)
    {
        if (line.left(9) != "S: Sysfs=") return false;
        systemFile = line.trimLeft(9);
        null = false;
        return true;
    }

    bool Device::setUniqueIdentifier(const Str& line)
    {
        if (line.left(8) != "U: Uniq=") return false;
        uniqueId = line.trimLeft(8);
        null = false;
        return true;
    }

    bool Device::setHandlers(const Str& line)
    {
        if (line.left(12) != "H: Handlers=") return false;
        Str handlersStr = line.trimLeft(12);
        StrStream handleStream(handlersStr);
        Str handler = getNext(handleStream, ' ');
        while (handler.length() != 0)
        {
            handlers.append(handler);
            handler = getNext(handleStream, ' ');
        }
        null = false;
        return true;
    }

    bool Device::addCapability(const Str& line)
    {
        if (line.left(3) != "B: ") return false;
        Str capability = line.trimLeft(3);
        if (capability.left(3) == "EV=") getEventTypes(capability.trimLeft(3));
        if (capability.left(4) == "KEY=") getKeyCodes(capability.trimLeft(4));
        null = false;
        return true;
    }

    void Device::print(std::ostream& os)
    {
        if (bus.length() > 0 || vendor.length() > 0 || product.length() > 0
                || version.length() > 0)
        {
            os << "I:";
            if (bus.length() > 0) os << " Bus=" << bus;
            if (vendor.length() > 0) os << " Vendor=" << vendor;
            if (product.length() > 0) os << " Product=" << product;
            if (version.length() > 0) os << " Version=" << version;
            os << std::endl;
        }
        if (name.length() > 0)
        {
            os << "N: Name=\"" << name << "\"" << std::endl;
        }
        if (physicalDevice.length() > 0)
        {
            os << "P: Phys=" << physicalDevice << std::endl;
        }
        if (systemFile.length() > 0)
        {
            os << "S: Sysfs=" << systemFile << std::endl;
        }
        //if (uniqueId.length() > 0)
        {
            os << "U: Uniq=" << uniqueId << std::endl;
        }
        if (handlers.getSize() > 0)
        {
            os << "H: Handlers=";
            Iterator<Str> iter = handlers.first();
            while (iter.valid())
            {
                os << *iter;
                ++ iter;
                if (iter.valid()) os << " ";
            }
            os << std::endl;
        }
        os << "B: EV=" << getEventBitCode() << std::endl;

        os << std::endl;
    }

    bool Device::isMouse()
    {
        for (Iterator<Str> iter = handlers.first(); iter.valid(); ++iter)
        {
            if ((*iter).left(5) == "mouse") return true;
        }
        return false;
    }

    Str Device::getMouseEvent()
    {
        if (!isMouse()) return Str();
        return *handlers.last();
    }

    List<Device> getInputDevices()
    {
        Device newDevice;
        List<Device> devices;
        std::ifstream deviceFile("/proc/bus/input/devices");
        if (!deviceFile.is_open())
            std::cerr << "Unable to open /proc/bus/input/devices." << std::endl;
        while (deviceFile.good())
        {
            Str line = getNext(deviceFile);
            if (line.length() == 0)
            {
                if (!newDevice.isNull()) devices.append(newDevice);
                newDevice = Device();
            }
            else
            {
                if (line[0] == 'I') newDevice.setIdentity(line);
                else if (line[0] == 'N') newDevice.setName(line);
                else if (line[0] == 'P') newDevice.setPhysicalDevice(line);
                else if (line[0] == 'S') newDevice.setSystemFile(line);
                else if (line[0] == 'U') newDevice.setUniqueIdentifier(line);
                else if (line[0] == 'H') newDevice.setHandlers(line);
                else if (line[0] == 'B') newDevice.addCapability(line);
                else
                {
                    std::cerr << "Unrecognised line in /proc/bus/input/devices:";
                    std::cerr << line << std::endl;
                }
            }
        }
        return devices;
    }

    void Device::getEventTypes(Str bitTypes)
    {
        long type;
        unsigned long bit = 1;
        if (bitTypes.hexToLong(type))
        {
            for (unsigned int code = 0; code < EV_MAX; ++code)
            {
                if (bit << code & type)
                {
                    events.append(getEventType(code));
                }
            }
        }
    }

    Str Device::getEventBitCode()
    {
        long bitCode = 0;
        for (Iterator<IOEvent> iter = events.first(); iter.valid(); ++iter)
        {
            unsigned int eventCode = getEventCode((*iter).getName());
            long bit = 1;
            bitCode += bit << eventCode;
        }
        Str hexCode = toHex(bitCode, false, true);
        return hexCode;
    }

    void Device::getKeyCodes(Str keyCodes)
    {
        //TODO
    }

    Str getEventType(const unsigned int code)
    {
        switch (code)
        {
        case EV_SYN :
            return Str("SYN");
        case EV_KEY :
            return Str("KEY");
        case EV_REL :
            return Str("REL");
        case EV_ABS :
            return Str("ABS");
        case EV_MSC :
            return Str("MSC");
        case EV_SW :
            return Str("SW");
        case EV_LED :
            return Str("LED");
        case EV_SND :
            return Str("SND");
        case EV_REP :
            return Str("REP");
        case EV_FF :
            return Str("FF");
        case EV_PWR:
            return Str("PWR");
        case EV_FF_STATUS:
            return Str("FF_STATUS");
        default:
            return toStr(code);
        }
    }

    unsigned int getEventCode(const Str& type)
    {
        if (type == "SYN") return EV_SYN;
        if (type == "KEY") return EV_KEY;
        if (type == "REL") return EV_REL;
        if (type == "ABS") return EV_ABS;
        if (type == "MSC") return EV_MSC;
        if (type == "SW") return EV_SW;
        if (type == "LED") return EV_LED;
        if (type == "SND") return EV_SND;
        if (type == "REP") return EV_REP;
        if (type == "FF") return EV_FF;
        if (type == "PWR") return EV_PWR;
        if (type == "FF_STATUS") return EV_FF_STATUS;
        long code = -1;
        type.integerToLong(code);
        return (unsigned int)code;
    }

    Str getEventDescription(const unsigned int code)
    {
        // EV_SYN:      Used as markers to separate events. Events may
        //              be separated in time or in space, such as with
        //              the multitouch protocol.
        // EV_KEY:      Used to describe state changes of keyboards,
        //              buttons, or other key-like devices.
        // EV_REL:      Used to describe relative axis value changes,
        //              e.g. moving the mouse 5 units to the left.
        // EV_ABS:      Used to describe absolute axis value changes,
        //              e.g. describing the coordinates of a touch on a
        //              touchscreen.
        // EV_MSC:      Used to describe miscellaneous input data that
        //              do not fit into other types.
        // EV_SW:       Used to describe binary state input switches.
        // EV_LED:      Used to turn LEDs on devices on and off.
        // EV_SND:      Used to output sound to devices.
        // EV_REP:      Used for autorepeating devices.
        // EV_FF:       Used to send force feedback commands to an input
        //              device.
        // EV_PWR:      A special type for power button and switch input.
        // EV_FF_STATUS:Used to receive force feedback device status.
        switch (code)
        {
        case EV_SYN :
            return Str("Synch Event");
        case EV_KEY :
            return Str("Key or Button");
        case EV_REL :
            return Str("Relative Axis");
        case EV_ABS :
            return Str("Absolute Axis");
        case EV_MSC :
            return Str("Miscellaneous");
        case EV_SW :
            return Str("Binary Switch");
        case EV_LED :
            return Str("LED");
        case EV_SND :
            return Str("Sound");
        case EV_REP :
            return Str("Repeat");
        case EV_FF :
            return Str("Force Feedback");
        case EV_PWR:
            return Str("Power Management");
        case EV_FF_STATUS:
            return Str("Force Feedback Status");
        default:
            return Str("Unknown code");
        }
    }
}

#endif // __unix
