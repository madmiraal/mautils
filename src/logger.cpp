// (c) 2016: Marcel Admiraal

#include "logger.h"

#include "utils.h"

namespace ma
{
    Logger::Logger() :
        logging(false), paused(false)
    {
    }

    Logger::~Logger()
    {
    }

    Str Logger::getLogFilename() const
    {
        return filename;
    }

    void Logger::setLogFilename(const Str& filename)
    {
        this->filename = filename;
        if (logFile.is_open())
        {
            logFile.close();
            logFile.open(filename.c_str(), std::fstream::out);
        }
    }

    void Logger::startLogging()
    {
        if (!logging)
        {
            if (std::ifstream(filename.c_str())) renameFile(filename);
            logFile.open(filename.c_str(), std::fstream::out);
            if (logFile.is_open())
            {
                logging = true;
                start = std::chrono::duration_cast<std::chrono::milliseconds>(
                        std::chrono::steady_clock::now().time_since_epoch());
                log("Logging Started");
            }
        }
    }

    void Logger::pauseLogging()
    {
        if (logging && !paused)
        {
            pauseStart = std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::steady_clock::now().time_since_epoch());
            paused = true;
        }
    }

    void Logger::unPauseLogging()
    {
        if (logging && paused)
        {
            std::chrono::milliseconds now =
                    std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::steady_clock::now().time_since_epoch());
            start += now - pauseStart;
            paused = false;
        }
    }

    void Logger::stopLogging(unsigned int time)
    {
        unPauseLogging();
        log("Logging Stopped", time);
        logging = false;
        logFile.close();
    }

    bool Logger::isLogging() const
    {
        return logging;
    }

    bool Logger::isPaused() const
    {
        return paused;
    }

    void Logger::log(const Str& logLine, unsigned int time)
    {
        if (logging && !paused)
        {
            logAccess.lock();
            if (time == (unsigned int) -1)
                logFile << timeStamp() << '\t';
            else
                logFile << time << '\t';
            logFile << logLine;
            logFile << std::endl;
            logAccess.unlock();
        }
    }

    unsigned int Logger::timeStamp()
    {
        std::chrono::milliseconds now =
                std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::steady_clock::now().time_since_epoch());

        int timestamp = (now - start).count();
        return (unsigned int)timestamp;
    }
}
