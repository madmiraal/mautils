// (c) 2011 - 2014: Marcel Admiraal

#include "quaternion.h"
#include "utils.h"

#include <cmath>
#include <string>
#include <sstream>

namespace ma
{
    Quaternion::Quaternion() :
        w(0), x(0), y(0), z(0)
    {
    }

    Quaternion::Quaternion(const double a) :
        w(a), x(0), y(0), z(0)
    {
    }

    Quaternion::Quaternion(
        const double w,
        const double x,
        const double y,
        const double z) :
        w(w), x(x), y(y), z(z)
    {
    }

    Quaternion::Quaternion(double yaw, double pitch, double roll)
    {
        double cy2 = std::cos(yaw * 0.5);
        double sy2 = std::sin(yaw * 0.5);
        double cp2 = std::cos(pitch * 0.5);
        double sp2 = std::sin(pitch * 0.5);
        double cr2 = std::cos(roll * 0.5);
        double sr2 = std::sin(roll * 0.5);

        w = cy2 * cp2 * cr2 + sy2 * sp2 * sr2;
        x = cy2 * cp2 * sr2 - sy2 * sp2 * cr2;
        y = cy2 * sp2 * cr2 + sy2 * cp2 * sr2;
        z = sy2 * cp2 * cr2 - cy2 * sp2 * sr2;
    }

    Quaternion::Quaternion(const Vector3D& v) :
        w(0), x(v[0]), y(v[1]), z(v[2])
    {
    }

    Quaternion::Quaternion(const double w, const Vector3D& v) :
        w(w), x(v[0]), y(v[1]), z(v[2])
    {
    }

    Quaternion::Quaternion(const Vector3D& v, const double t) :
        w(cos(t/2)),
        x(v[0]/v.abs()*sin(t/2)),
        y(v[1]/v.abs()*sin(t/2)),
        z(v[2]/v.abs()*sin(t/2))
    {
    }

    Quaternion::Quaternion(
        const double abs, const Vector3D& u, const double arg) :
        w(abs * cos(arg)),
        x(u[0] * abs * sin(arg)),
        y(u[1] * abs * sin(arg)),
        z(u[2] * abs * sin(arg))
    {
    }

    Quaternion::Quaternion(const Quaternion& source) :
        w(source.w), x(source.x), y(source.y), z(source.z)
    {
    }

    void swap(Quaternion& first, Quaternion& second)
    {
        std::swap(first.w, second.w);
        std::swap(first.x, second.x);
        std::swap(first.y, second.y);
        std::swap(first.z, second.z);
    }

    Quaternion& Quaternion::operator=(Quaternion source)
    {
        swap(*this, source);
        return *this;
    }

    double Quaternion::getW() const
    {
        return w;
    }

    double Quaternion::getX() const
    {
        return x;
    }

    double Quaternion::getY() const
    {
        return y;
    }

    double Quaternion::getZ() const
    {
        return z;
    }

    void Quaternion::setW(const double w)
    {
        this->w = w;
    }

    void Quaternion::setX(const double x)
    {
        this->x = x;
    }

    void Quaternion::setY(const double y)
    {
        this->y = y;
    }

    void Quaternion::setZ(const double z)
    {
        this->z = z;
    }

    double Quaternion::getScalar() const
    {
        return w;
    }

    Vector3D Quaternion::getVector() const
    {
        Vector3D result(x, y, z);
        return result;
    }

    double Quaternion::abs() const
    {
        return sqrt(w * w + x * x + y * y + z * z);
    }

    double Quaternion::getArg() const
    {
        if (abs() == 0)
        {
            // Technically this should be indeterminate,
            // but if the quaterniion is zero...
            return 0;
        }
        else
        {
            return std::acos(w / abs());
        }
    }

    double Quaternion::getAngle() const
    {
        ma::Quaternion unit = unitQuaternion();
        double angle = std::acos(unit.getW()) * 2;
//        if (std::isnan(angle))
//        {
//            if (w > 1) angle = 0;
//            if (w < -1) angle = M_PI * 2;
//        }
        return angle;
    }

    Quaternion Quaternion::unitQuaternion() const
    {
        return *this / abs();
    }

    Quaternion Quaternion::fastUnitQuaternion() const
    {
        float ss = (float)(w * w + x * x + y * y + z * z);
        float isr = invSqrt(ss);
        return *this * isr;
    }

    bool Quaternion::isRight() const
    {
        return (w == 0);
    }

    Quaternion& Quaternion::operator+=(const Quaternion& right)
    {
        w += right.w;
        x += right.x;
        y += right.y;
        z += right.z;
        return *this;
    }

    Quaternion& Quaternion::operator+=(const double right)
    {
        w += right;
        return *this;
    }

    Quaternion& Quaternion::operator-=(const Quaternion& right)
    {
        w -= right.w;
        x -= right.x;
        y -= right.y;
        z -= right.z;
        return *this;
    }

    Quaternion& Quaternion::operator-=(const double right)
    {
        w -= right;
        return *this;
    }

    Quaternion& Quaternion::operator*=(const Quaternion& right)
    {
        double newW = (w * right.w) - (x * right.x) -
                (y * right.y) - (z * right.z);
        double newX = (w * right.x) + (x * right.w) +
                (y * right.z) - (z * right.y);
        double newY = (w * right.y) - (x * right.z) +
                (y * right.w) + (z * right.x);
        double newZ = (w * right.z) + (x * right.y) -
                (y * right.x) + (z * right.w);
        w = newW;
        x = newX;
        y = newY;
        z = newZ;
        return *this;
    }

    Quaternion& Quaternion::operator*=(const double right)
    {
        w *= right;
        x *= right;
        y *= right;
        z *= right;
        return *this;
    }

    Quaternion& Quaternion::operator/=(const Quaternion& right)
    {
        *this *= right.recipricol();
        return *this;
    }

    Quaternion& Quaternion::operator/=(const double right)
    {
        w /= right;
        x /= right;
        y /= right;
        z /= right;
        return *this;
    }

    Quaternion Quaternion::conjugate() const
    {
        Quaternion result(w, -x, -y, -z);
        return result;
    }

    Quaternion Quaternion::recipricol() const
    {
        // The conjugate divided by the square of the absolute value.
        return conjugate()/(w * w + x * x + y * y + z * z);
    }

    double Quaternion::dot(const Quaternion& right) const
    {
        return w * right.w + x * right.x + y * right.y + z * right.z;
    }

    Quaternion Quaternion::exp() const
    {
        double size = std::exp(w);
        if (x == 0 && y == 0 && z == 0)
        {
            Quaternion result(size, 0, 0, 0);
            return result;
        }
        else
        {
            Vector3D v = getVector();
            double s = v.abs();
            Vector3D u = v.unitVector();
            Quaternion result(cos(s), u * sin(s));
            return result *= size;
        }
    }

    Quaternion Quaternion::ln() const
    {
        double size = log(abs());
        Quaternion result(size, getVector().unitVector() * getArg());
        return result;
    }

    Quaternion Quaternion::pow(const double n) const
    {
        double newAbs = std::pow(abs(), n);
        double newArg = getArg() * n;
        // The interesting thing about this is that the unit vector doesn't change!
        Quaternion result(newAbs, getVector().unitVector(), newArg);
        return result;
    }

    Quaternion Quaternion::pow(const Quaternion& n) const
    {
        return (n * ln()).exp();
    }

    Matrix Quaternion::getRotationMatrix()
    {
        Matrix rotation(3, 3);
        double n = w * w + x * x + y * y + z * z;
        double s = (n == 0) ? 0 : 2 / n;
        double wx = s * w * x;
        double wy = s * w * y;
        double wz = s * w * z;
        double xx = s * x * x;
        double xy = s * x * y;
        double xz = s * x * z;
        double yy = s * y * y;
        double yz = s * y * z;
        double zz = s * z * z;
        rotation[0][0] = 1 - (yy + zz);
        rotation[0][1] = xy - wz;
        rotation[0][2] = xz + wy;
        rotation[1][0] = xy + wz;
        rotation[1][1] = 1 - (xx + zz);
        rotation[1][2] = yz - wx;
        rotation[2][0] = xz - wy;
        rotation[2][1] = yz + wx;
        rotation[2][2] = 1 - (xx + yy);
        return rotation;
    }

    Quaternion unitQuaternion(const Quaternion& source)
    {
        return source.unitQuaternion();
    }

    Quaternion fastUnitQuaternion(const Quaternion& source)
    {
        return source.fastUnitQuaternion();
    }

    Quaternion operator+(Quaternion left, const Quaternion& right)
    {
        return left += right;
    }

    Quaternion operator+(const Vector3D& left, const double right)
    {
        Quaternion result(right, left);
        return result;
    }

    Quaternion operator+(const double left, const Vector3D& right)
    {
        Quaternion result(left, right);
        return result;
    }

    Quaternion operator+(Quaternion left, const double right)
    {
        return left += right;
    }

    Quaternion operator+(const double left, Quaternion right)
    {
        return right += left;
    }

    Quaternion operator-(Quaternion left, const Quaternion& right)
    {
        return left -= right;
    }

    Quaternion operator-(const Quaternion& source)
    {
        Quaternion result(
            -source.getW(),
            -source.getX(),
            -source.getY(),
            -source.getZ());
        return result;
    }

    Quaternion operator-(const Vector3D& left, const double right)
    {
        Quaternion result(-right, left);
        return result;
    }

    Quaternion operator-(const double left, const Vector3D& right)
    {
        Quaternion result(left, -right);
        return result;
    }

    Quaternion operator-(Quaternion left, const double right)
    {
        return left -= right;
    }

    Quaternion operator-(const double left, const Quaternion& right)
    {
        return left + (-right);
    }

    Quaternion operator*(Quaternion left, const Quaternion& right)
    {
        return left *= right;
    }

    Quaternion operator*(Quaternion left, const double right)
    {
        return left *= right;
    }

    Quaternion operator*(const double left, Quaternion right)
    {
        return right *= left;
    }

    Quaternion operator/(Quaternion left, const Quaternion& right)
    {
        return left /= right;
    }

    Quaternion operator/(Quaternion left, const double right)
    {
        return left /= right;
    }

    Quaternion operator/(const double left, const Quaternion& right)
    {
        Quaternion divisor = right.recipricol();
        return divisor *= left;
    }

    bool operator==(const Quaternion& left, const Quaternion& right)
    {
        return (left.getW() == right.getW() &&
                left.getX() == right.getX() &&
                left.getY() == right.getY() &&
                left.getZ() == right.getZ());
    }

    bool operator!=(const Quaternion& left, const Quaternion& right)
    {
        return !(left == right);
    }

    bool approxEqual(const Quaternion& left, const Quaternion& right,
            const double error)
    {
        return (approxEqual(left.getW(), right.getW(), error) &&
                approxEqual(left.getX(), right.getX(), error) &&
                approxEqual(left.getY(), right.getY(), error) &&
                approxEqual(left.getZ(), right.getZ(), error));
    }

    std::ostream& operator<<(std::ostream& os, const Quaternion& source)
    {
        os << source.getW() << ' ';
        if (source.getX() >= 0) os << '+';
        os << source.getX() << 'i' << ' ';
        if (source.getY() >= 0) os << '+';
        os << source.getY() << 'j' << ' ';
        if (source.getZ() >= 0) os << '+';
        os << source.getZ() << 'k';
        return os;
    }

    std::istream& operator>>(std::istream& is, Quaternion& destination)
    {
        double w, x, y, z;
        char c;
        is >> w;
        if (!is.good()) return is;
        // x
        is >> c;
        if (c != '+' && c != '-')
        {
            is.setstate(std::ios::failbit);
            return is;
        }
        is >> x;
        if (!is.good()) return is;
        if (c == '-') x = -x;
        is >> c;
        if (c != 'i')
        {
            is.setstate(std::ios::failbit);
            return is;
        }
        // y
        is >> c;
        if (c != '+' && c != '-')
        {
            is.setstate(std::ios::failbit);
            return is;
        }
        is >> y;
        if (!is.good()) return is;
        if (c == '-') y = -y;
        is >> c;
        if (c != 'j')
        {
            is.setstate(std::ios::failbit);
            return is;
        }
        // z
        is >> c;
        if (c != '+' && c != '-')
        {
            is.setstate(std::ios::failbit);
            return is;
        }
        is >> z;
        if (!is.good()) return is;
        if (c == '-') z = -z;
        is >> c;
        if (c != 'k')
        {
            is.setstate(std::ios::failbit);
            return is;
        }
        // Create quaternion.
        destination.setW(w);
        destination.setX(x);
        destination.setY(y);
        destination.setZ(z);
        return is;
    }

    Quaternion exp(const Quaternion& power)
    {
        return power.exp();
    }

    Quaternion ln(const Quaternion& root)
    {
        return root.ln();
    }

    Quaternion pow(const Quaternion& base, const double power)
    {
        return base.pow(power);
    }

    Quaternion pow(const double base, const Quaternion& power)
    {
        return (exp(power * log(base)));
    }

    Quaternion pow(const Quaternion& base, const Quaternion& power)
    {
        return base.pow(power);
    }

    double dist(const Quaternion& first, const Quaternion& second)
    {
        return (first - second).abs();
    }

    Vector3D rotate(
        const Vector3D& source, const Vector3D& axis, const double angle)
    {
        Quaternion rotation(axis, angle);
        return rotate(source, rotation);
    }

    Vector3D rotate(
        const Vector3D& source, const Quaternion& rotation)
    {
        Quaternion result = rotation * source * rotation.recipricol();
        return result.getVector();
    }
}
