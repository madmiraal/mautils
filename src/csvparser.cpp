// (c) 2018: Marcel Admiraal

#include "csvparser.h"

#include <fstream>

namespace ma
{
    CSVParser::CSVParser(const Str& csvFile)
    {
        rows = 0;
        columns = 0;
        bool inQuotes = false;

        List<Str> row;
        std::ifstream file(csvFile.c_str());
        Str field;
        unsigned int rowColumns = 0;
        while (file.good())
        {
            char letter = (char)(file.get());
            if (letter == '\"') inQuotes = !inQuotes;
            else if (letter == '\r') { }
            else if (!inQuotes && (letter == ',' || letter == '\n'))
            {
                // End of field.
                row.append(field);
                field = Str();
                ++rowColumns;
                if (letter == '\n')
                {
                    // End of row.
                    elements.append(row);
                    row = List<Str>();
                    if (rows == 0) columns = rowColumns;
                    else if (rowColumns != columns)
                    {
                        std::cerr << "Row " << rows << " has " << rowColumns;
                        std::cerr << " instead of " << columns << std::endl;
                    }
                    ++rows;
                    rowColumns = 0;
                }
            }
            else
            {
                field += letter;
            }
        }
        if (row.getSize() > 0)
        {
            row.append(field);
            elements.append(row);
            ++rows;
        }
    }

    CSVParser::~CSVParser()
    {
    }

    List<Str> CSVParser::operator[](const unsigned int row)
    {
        return elements.get(row);
    }

    const List<Str> CSVParser::operator[](const unsigned int row) const
    {
        return elements.get(row);
    }

    Iterator<List<Str> > CSVParser::getIterator() const
    {
        return elements.getIterator();
    }

    unsigned int CSVParser::getRows() const
    {
        return rows;
    }

    unsigned int CSVParser::getColumns() const
    {
        return columns;
    }
}
