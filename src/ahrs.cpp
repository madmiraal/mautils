// (c) 2017: Marcel Admiraal

#include "ahrs.h"

#include "utils.h"

#include <cmath>

namespace ma
{
    void AHRS(Quaternion& quat,
            float gx, float gy, float gz,
            float ax, float ay, float az,
            float mx, float my, float mz,
            float samplePeriod, float alpha)
    {
        // Used for normalising the vectors.
        float inverseAbs;

        // Short name local variables for readability.
        float qw = quat.getW();
        float qx = quat.getX();
        float qy = quat.getY();
        float qz = quat.getZ();

        // Rate of change of quaternion from gyroscope
        // q̇ = 1/2 q·ω
        float qwDot = 0.5f * (- qx * gx - qy * gy - qz * gz);
        float qxDot = 0.5f * (  qw * gx + qy * gz - qz * gy);
        float qyDot = 0.5f * (  qw * gy - qx * gz + qz * gx);
        float qzDot = 0.5f * (  qw * gz + qx * gy - qy * gx);

        // Integrate rate of change of quaternion and update quaternion.
        // q(t+Δt) = q(t) + q̇(t)Δt
        qw += qwDot * samplePeriod;
        qx += qxDot * samplePeriod;
        qy += qyDot * samplePeriod;
        qz += qzDot * samplePeriod;

        // Intialise corrective step vector.
        float step[4] = {0};

        // Compute accelerometer error correction only if accelerometer
        // measurement is not zero.
        if (!((ax == 0) && (ay == 0) && (az == 0)))
        {
            // Normalise accelerometer measurement
            inverseAbs = invSqrt(ax * ax + ay * ay + az * az);
            ax *= inverseAbs;
            ay *= inverseAbs;
            az *= inverseAbs;

            // Jacobian with reference vector = [0, 0, -1]
            // ┌                          ┐
            // │   2 qy   - 2 qx   - 2 qw │
            // │ - 2 qz   - 2 qw     2 qx │
            // │   2 qw   - 2 qz     2 qy │
            // │ - 2 qx   - 2 qy   - 2 qz │
            // └                          ┘
            float J[4][3] = {
                {  2 * qy, - 2 * qx, - 2 * qw},
                {- 2 * qz, - 2 * qw,   2 * qx},
                {  2 * qw, - 2 * qz,   2 * qy},
                {- 2 * qx, - 2 * qy, - 2 * qz}
            };

            // Difference vector with reference vector = [0, 0, -1]
            // ┌                                        ┐
            // │   2 qw qy - 2 qx qz             - v_mx │
            // │ - 2 qw qx - 2 qy qz             - v_my │
            // │ - qw qw + qx qx + qy qy - qz qz - v_mz │
            // └                                        ┘
            float diff[3] = {
                  2 * qw * qy - 2 * qx * qz             - ax,
                - 2 * qw * qx - 2 * qy * qz             - ay,
                - qw * qw + qx * qx + qy * qy - qz * qz - az
            };

            // Multiply Jacobian by the difference.
            for (unsigned int row = 0; row < 4; ++row)
            {
                for (unsigned int col = 0; col < 3; ++ col)
                {
                    step[row] += J[row][col] * diff[col];
                }
            }

            // Compute magnetometer error correction only if magnetometer
            // measurement is not zero.
            if (!((mx == 0) && (my == 0) && (mz == 0)))
            {
                // Normalise magnetometer measurement
                inverseAbs = invSqrt(mx * mx + my * my + mz * mz);
                mx *= inverseAbs;
                my *= inverseAbs;
                mz *= inverseAbs;

                // Calculate reference direction of Earth's magnetic field using
                // forward rotation.
                // rx = mx (qw qw + qx qx - qy qy - qz qz)
                //    + my (- 2 qw qz + 2 qx qy)
                //    + mz (  2 qw qy + 2 qx qz)
                // ry = mx (  2 qw qz + 2 qx qy)
                //    + my (qw qw - qx qx + qy qy - qz qz)
                //    + mz (- 2 qw qx + 2 qy qz)
                // rz = mx (- 2 qw qy + 2 qx qz)
                //    + my (  2 qw qx + 2 qy qz)
                //    + mz (qw qw - qx qx - qy qy + qz qz)
                float rx = mx*(qw*qw + qx*qx - qy*qy - qz*qz)
                         + my*(- 2*qw*qz + 2*qx*qy)
                         + mz*(  2*qw*qy + 2*qx*qz);
                float ry = mx*(  2*qw*qz + 2*qx*qy)
                         + my*(qw*qw - qx*qx + qy*qy - qz*qz)
                         + mz*(- 2*qw*qx + 2*qy*qz);
                float rz = mx*(- 2*qw*qy + 2*qx*qz)
                         + my*(  2*qw*qx + 2*qy*qz)
                         + mz*(qw*qw - qx*qx - qy*qy + qz*qz);
                // Only use the declination, by rotating the vector onto the
                // horizontal x-z plane.
                //bool negativeRX = rx < 0;
                rx = std::sqrt(rx * rx + ry * ry);
                ry = 0;
                //if (negativeRX) rx = -rx;

                // Jacobian with reference vector = [rx, 0, rz]
                // ┌                                                               ┐
                // │   2 rx qw - 2 rz qy # - 2 rx qz + 2 rz qx # 2 rx qy + 2 rz qw │
                // │   2 rx qx + 2 rz qz #   2 rx qy + 2 rz qw # 2 rx qz - 2 rz qx │
                // │ - 2 rx qy - 2 rz qw #   2 rx qx + 2 rz qz # 2 rx qw - 2 rz qy │
                // │ - 2 rx qz + 2 rz qx # - 2 rx qw + 2 rz qy # 2 rx qx + 2 rz qz │
                // └                                                               ┘
                float J[4][3] = {
                    {  2*rx*qw - 2*rz*qy, - 2*rx*qz + 2*rz*qx, 2*rx*qy + 2*rz*qw},
                    {  2*rx*qx + 2*rz*qz,   2*rx*qy + 2*rz*qw, 2*rx*qz - 2*rz*qx},
                    {- 2*rx*qy - 2*rz*qw,   2*rx*qx + 2*rz*qz, 2*rx*qw - 2*rz*qy},
                    {- 2*rx*qz + 2*rz*qx, - 2*rx*qw + 2*rz*qy, 2*rx*qx + 2*rz*qz}
                };

                // Difference vector with reference vector = [rx, 0, rz]
                // ┌                                                                                ┐
                // │ rx(  qw qw + qx qx - qy qy - qz qz) + rz(- 2 qw qy + 2 qx qz)             - mx │
                // │ rx(- 2 qw qz + 2 qx qy)             + rz(  2 qw qx + 2 qy qz)             - my │
                // │ rx(  2 qw qy + 2 qx qz)             + rz(  qw qw - qx qx - qy qy + qz qz) - mz │
                // └                                                                                ┘
                float diff[3] = {
                    rx*(  qw*qw + qx*qx - qy*qy - qz*qz) + rz*(- 2*qw*qy + 2*qx*qz)             - mx,
                    rx*(- 2*qw*qz + 2*qx*qy)             + rz*(  2*qw*qx + 2*qy*qz)             - my,
                    rx*(  2*qw*qy + 2*qx*qz)             + rz*(  qw*qw - qx*qx - qy*qy + qz*qz) - mz,
                };

                // Multiply Jacobian by the difference.
                for (unsigned int row = 0; row < 4; ++row)
                {
                    for (unsigned int col = 0; col < 3; ++ col)
                    {
                        step[row] += J[row][col] * diff[col];
                    }
                }
            }
        }

        // Apply correction step
        qw -= alpha * step[0];
        qx -= alpha * step[1];
        qy -= alpha * step[2];
        qz -= alpha * step[3];

        // Normalise quaternion
        inverseAbs = invSqrt(qw * qw + qx * qx + qy * qy + qz * qz);
        qw *= inverseAbs;
        qx *= inverseAbs;
        qy *= inverseAbs;
        qz *= inverseAbs;

        // Set new quaternion values.
        quat.setW(qw);
        quat.setX(qx);
        quat.setY(qy);
        quat.setZ(qz);
    }

    void AHRS(Quaternion& quat,
            long gx, long gy, long gz,
            long ax, long ay, long az,
            long mx, long my, long mz,
            float samplePeriod, long ilalpha)
    {
        // Used for normalising the vectors.
        long abs;

        // Short name local variable for readability
        long qw = quat.getW();
        long qx = quat.getX();
        long qy = quat.getY();
        long qz = quat.getZ();

        // Rate of change of quaternion from gyroscope
        // q̇ = 1/2 q·ω
        //qwDot = 0.5 * (- qx * gx - qy * gy - qz * gz);
        //qxDot = 0.5 * (  qw * gx + qy * gz - qz * gy);
        //qyDot = 0.5 * (  qw * gy - qx * gz + qz * gx);
        //qzDot = 0.5 * (  qw * gz + qx * gy - qy * gx);
        long qwDot = (- qx * gx - qy * gy - qz * gz) >> 15;
        long qxDot = (  qw * gx + qy * gz - qz * gy) >> 15;
        long qyDot = (  qw * gy - qx * gz + qz * gx) >> 15;
        long qzDot = (  qw * gz + qx * gy - qy * gx) >> 15;

        // Integrate rate of change of quaternion and update quaternion.
        // q(t+Δt) = q(t) + q̇(t)Δt
        qw += qwDot * samplePeriod;
        qx += qxDot * samplePeriod;
        qy += qyDot * samplePeriod;
        qz += qzDot * samplePeriod;

        // Intialise corrective step vector.
        long step[4] = {0};

        abs = intSqrt(ax * ax + ay * ay + az * az);
        // Compute accelerometer error correction only if accelerometer
        // measurement is not zero.
        if (abs != 0)
        {
            // Normalise accelerometer measurement
            long inverseAbs = (1L<<29) / abs;
            ax = (ax * inverseAbs) >> 15;
            ay = (ay * inverseAbs) >> 15;
            az = (az * inverseAbs) >> 15;

            // Jacobian with reference vector = [0, 0, -1]
            // ┌                          ┐
            // │   2 qy   - 2 qx   - 2 qw │
            // │ - 2 qz   - 2 qw     2 qx │
            // │   2 qw   - 2 qz     2 qy │
            // │ - 2 qx   - 2 qy   - 2 qz │
            // └                          ┘
            long J[4][3] = {
                {  qy << 1, - qx << 1, - qw << 1},
                {- qz << 1, - qw << 1,   qx << 1},
                {  qw << 1, - qz << 1,   qy << 1},
                {- qx << 1, - qy << 1, - qz << 1}
            };

            // Difference vector with reference vector = [0, 0, -1]
            // ┌                                        ┐
            // │   2 qw qy - 2 qx qz             - v_mx │
            // │ - 2 qw qx - 2 qy qz             - v_my │
            // │ - qw qw + qx qx + qy qy - qz qz - v_mz │
            // └                                        ┘
            long diff[3] = {
                ((  qw * qy - qx * qz) >> 13)                     - ax,
                ((- qw * qx - qy * qz) >> 13)                     - ay,
                ((- qw * qw + qx * qx + qy * qy - qz * qz) >> 14) - az
            };

            // Multiply Jacobian by the difference.
            for (unsigned int row = 0; row < 4; ++row)
            {
                for (unsigned int col = 0; col < 3; ++ col)
                {
                    step[row] += J[row][col] * diff[col];
                }
                step[row] >>= 14;
            }

            abs = intSqrt(mx * mx + my * my + mz * mz);
            // Compute magnetometer error correction only if magnetometer
            // measurement is not zero.
            if (abs != 0)
            {
                // Normalise accelerometer measurement
                long inverseAbs = (1L<<29) / abs;
                mx = (mx * inverseAbs) >> 15;
                my = (my * inverseAbs) >> 15;
                mz = (mz * inverseAbs) >> 15;

                // Calculate reference direction of Earth's magnetic field using
                // forward rotation.
                // rx = mx (  qw qw + qx qx - qy qy - qz qz)
                //    + my (- 2 qw qz + 2 qx qy)
                //    + mz (  2 qw qy + 2 qx qz)
                // ry = mx (  2 qw qz + 2 qx qy)
                //    + my (  qw qw - qx qx + qy qy - qz qz)
                //    + mz (- 2 qw qx + 2 qy qz)
                // rz = mx (- 2 qw qy + 2 qx qz)
                //    + my (  2 qw qx + 2 qy qz)
                //    + mz (  qw qw - qx qx - qy qy + qz qz)
                long rx = (mx*((  qw*qw + qx*qx - qy*qy - qz*qz)>>14)
                         + my*((- qw*qz + qx*qy)>>13)
                         + mz*((  qw*qy + qx*qz)>>13))>>14;
                long ry = (mx*((  qw*qz + qx*qy)>>13)
                         + my*((  qw*qw - qx*qx + qy*qy - qz*qz)>>14)
                         + mz*((- qw*qx + qy*qz)>>13))>>14;
                long rz = (mx*((- qw*qy + qx*qz)>>13)
                         + my*((  qw*qx + qy*qz)>>13)
                         + mz*((  qw*qw - qx*qx - qy*qy + qz*qz)>>14))>>14;
                // Only use the declination, by rotating the vector onto the
                // horizontal x-z plane.
                //bool negativeRX = rx < 0;
                rx = intSqrt(rx * rx + ry * ry);
                ry = 0;
                //if (negativeRX) rx = -rx;

                // Jacobian with reference vector = [rx, 0, rz]
                // ┌                                                               ┐
                // │   2 rx qw - 2 rz qy # - 2 rx qz + 2 rz qx # 2 rx qy + 2 rz qw │
                // │   2 rx qx + 2 rz qz #   2 rx qy + 2 rz qw # 2 rx qz - 2 rz qx │
                // │ - 2 rx qy - 2 rz qw #   2 rx qx + 2 rz qz # 2 rx qw - 2 rz qy │
                // │ - 2 rx qz + 2 rz qx # - 2 rx qw + 2 rz qy # 2 rx qx + 2 rz qz │
                // └                                                               ┘
                long J[4][3] = {
                    {(  rx*qw - rz*qy)>>13, (- rx*qz + rz*qx)>>13, (rx*qy + rz*qw)>>13},
                    {(  rx*qx + rz*qz)>>13, (  rx*qy + rz*qw)>>13, (rx*qz - rz*qx)>>13},
                    {(- rx*qy - rz*qw)>>13, (  rx*qx + rz*qz)>>13, (rx*qw - rz*qy)>>13},
                    {(- rx*qz + rz*qx)>>13, (- rx*qw + rz*qy)>>13, (rx*qx + rz*qz)>>13}
                };

                // Difference vector with reference vector = [rx, 0, rz]
                // ┌                                                                                ┐
                // │ rx(  qw qw + qx qx - qy qy - qz qz) + rz(- 2 qw qy + 2 qx qz)             - mx │
                // │ rx(- 2 qw qz + 2 qx qy)             + rz(  2 qw qx + 2 qy qz)             - my │
                // │ rx(  2 qw qy + 2 qx qz)             + rz(  qw qw - qx qx - qy qy + qz qz) - mz │
                // └                                                                                ┘
                long diff[3] = {
                    ((rx*((  qw*qw + qx*qx - qy*qy - qz*qz)>>14) + rz*((- qw*qy + qx*qz)>>13))>>14)                 - mx,
                    ((rx*((- qw*qz + qx*qy)>>13)                 + rz*((  qw*qx + qy*qz)>>13))>>14)                 - my,
                    ((rx*((  qw*qy + qx*qz)>>13)                 + rz*((  qw*qw - qx*qx - qy*qy + qz*qz)>>14))>>14) - mz,
                };

                // Multiply Jacobian by the difference.
                for (unsigned int row = 0; row < 4; ++row)
                {
                    // Add to the calculated accelerometer correction.
                    step[row] <<= 14;
                    for (unsigned int col = 0; col < 3; ++ col)
                    {
                        step[row] += J[row][col] * diff[col];
                    }
                    step[row] >>= 14;
                }
            }
        }

        // Apply correction step
        qw -= step[0] >> ilalpha;
        qx -= step[1] >> ilalpha;
        qy -= step[2] >> ilalpha;
        qz -= step[3] >> ilalpha;

        // Normalise quaternion
        abs = intSqrt(qw * qw + qx * qx + qy * qy + qz * qz);
        if (abs != 0)
        {
            // Normalise accelerometer measurement
            long inverseAbs = (1L<<29) / abs;
            qw = (qw * inverseAbs) >> 15;
            qx = (qx * inverseAbs) >> 15;
            qy = (qy * inverseAbs) >> 15;
            qz = (qz * inverseAbs) >> 15;
        }

        // Set new quaternion values.
        quat.setW(qw);
        quat.setX(qx);
        quat.setY(qy);
        quat.setZ(qz);
    }
}
