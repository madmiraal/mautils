// (c) 2017: Marcel Admiraal

#include "log2inc.h"

namespace ma
{
    Log2Inc::Log2Inc(double value) :
        thousandIndex(0), tenIndex(0), twoIndex(0), value(0)
    {
        if (value <= 0) return;
        while (value >= 100)
        {
            ++thousandIndex;
            value /= 1000;
        }
        while (value < 0.1)
        {
            --thousandIndex;
            value *= 1000;
        }
        if ((int)value == value)
        while ((int)value % 10 == 0)
        {
            ++tenIndex;
            value /= 10;
        }
        while (value*10 <= 1)
        {
            --tenIndex;
            value *= 10;
        }
        while (value/2 >= 1)
        {
            ++twoIndex;
            value /= 2;
        }
        while (value*2 <= 1)
        {
            --twoIndex;
            value *= 2;
        }
        calculateValue();
    }

    Log2Inc::~Log2Inc()
    {
    }

    void swap(Log2Inc& first, Log2Inc& second)
    {
        std::swap(first.thousandIndex, second.thousandIndex);
        std::swap(first.tenIndex, second.tenIndex);
        std::swap(first.twoIndex, second.twoIndex);
        std::swap(first.value, second.value);
    }

    Log2Inc& Log2Inc::operator=(Log2Inc source)
    {
        swap(*this, source);
        return *this;
    }

    Log2Inc& Log2Inc::operator++()
    {
        ++twoIndex;
        calculateValue();
        return *this;
    }

    Log2Inc Log2Inc::operator++(int)
    {
        Log2Inc temp(*this);
        operator++();
        return temp;
    }

    Log2Inc& Log2Inc::operator--()
    {
        --twoIndex;
        calculateValue();
        return *this;
    }

    Log2Inc Log2Inc::operator--(int)
    {
        Log2Inc temp(*this);
        operator--();
        return temp;
    }

    double Log2Inc::getValue() const
    {
        return value;
    }

    void Log2Inc::calculateValue()
    {
        while (twoIndex > 6)
        {
            twoIndex -= 10;
            ++thousandIndex;
        }
        while (twoIndex < -3)
        {
            twoIndex += 10;
            --thousandIndex;
        }
        value = 1;
        for (int i = 0; i < thousandIndex; ++i) value *= 1000;
        for (int i = 0; i > thousandIndex; --i) value /= 1000;
        for (int i = 0; i < tenIndex; ++i) value *= 10;
        for (int i = 0; i > tenIndex; --i) value /= 10;
        for (int i = 0; i < twoIndex; ++i) value *= 2;
        for (int i = 0; i > twoIndex; --i) value /= 2;
    }

    bool operator==(const Log2Inc left, const Log2Inc right)
    {
        return left.getValue() == right.getValue();
    }

    bool operator!=(const Log2Inc left, const Log2Inc right)
    {
        return left.getValue() != right.getValue();
    }

    bool operator<(const Log2Inc left, const Log2Inc right)
    {
        return left.getValue() < right.getValue();
    }

    bool operator<=(const Log2Inc left, const Log2Inc right)
    {
        return left.getValue() <= right.getValue();
    }

    bool operator>(const Log2Inc left, const Log2Inc right)
    {
        return left.getValue() > right.getValue();
    }

    bool operator>=(const Log2Inc left, const Log2Inc right)
    {
        return left.getValue() >= right.getValue();
    }

    std::ostream& operator<<(std::ostream& os, const Log2Inc& source)
    {
        os << source.getValue();
        return os;
    }
}
