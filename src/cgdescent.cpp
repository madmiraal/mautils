// (c) 2015: Marcel Admiraal

#include "cgdescent.h"

#include "vector.h"

#include <cmath>

namespace ma
{
    Vector minimise(GradientFunction* f, const Vector& initial,
            const unsigned int maxIters)
    {
        // A bunch of constants for line searches.
        // RHO and SIG are the constants in the Wolfe-Powell conditions.
        const double RHO = 0.01;
        const double SIG = 0.5;
        // Don't reevaluate within 0.1 of the limit of the current bracket
        const double INT = 0.1;
        // Extrapolate maximum 3 times the current bracket
        const unsigned int EXT = 3;
        // Max 20 function evaluations per line search
        const unsigned int MAX = 20;
        // Maximum allowed slope ratio
        const double RATIO = 100;

        // Zero the run length counter
        unsigned int i = 0;
        // No previous line search has failed
        bool ls_failed = false;
        // Get function value and gradient
        Vector input1 = initial;

        double value1 = 0;
        Vector gradient1;
        f->getValueAndGradients(value1, gradient1, input1);
        // Initial search direction is steepest descent.
        Vector searchDirection = -gradient1;
        // This is the slope
        double slope1 = -dot(searchDirection, searchDirection);
        // Initial step is 1/(|searchDirection|+1)
        double alpha1 = 1/(1-slope1);

        // While not finished
        while (i < maxIters)
        {
            // Count iterations
            ++i;
            // Make a copy of current values
            Vector input0 = input1;
            Vector gradient0 = gradient1;
            // Begin line search.
            input1 += alpha1 * searchDirection;
            double value2 = 0;
            Vector gradient2;
            f->getValueAndGradients(value2, gradient2, input1);
            double slope2 = dot(gradient2, searchDirection);
            // Initialize point 3 equal to point 1.
            double value3 = value1;
            double slope3 = slope1;
            double alpha3 = -alpha1;
            unsigned int M = MAX;
            // Initialize quanteties
            bool success = false;
            double limit = -1;
            while (true)
            {
                double alpha2;
                while (((value2 > value1 + alpha1 * RHO * slope1) ||
                        (slope2 > -SIG * slope1))
                       && (M > 0))
                {
                    // Tighten the bracket
                    limit = alpha1;
                    if (value2 > value1)
                    {
                        // Quadratic fit
                        alpha2 = alpha3 - (0.5*slope3*alpha3*alpha3) /
                                (slope3*alpha3+value2-value3);
                    }
                    else
                    {
                        // Cubic fit
                        double A = 6*(value2-value3)/alpha3+3*(slope2+slope3);
                        double B = 3*(value3-value2)-alpha3*(slope3+2*slope2);
                        double C = B*B-A*slope2*alpha3*alpha3;
                        alpha2 = (sqrt(C) - B) / A;
                    }
                    // If we have a numerical problem
                    if (std::isnan(alpha2))
                    {
                        // Bisect.
                        alpha2 = alpha3/2;
                    }
                    // Don't accept too close to limits
                    alpha2 = std::min(alpha2, INT*alpha3);
                    alpha2 = std::max(alpha2, (1-INT)*alpha3);
                    // Update the alpha
                    alpha1 += alpha2;
                    input1 += alpha2 * searchDirection;
                    f->getValueAndGradients(value2, gradient2, input1);
                    --M;
                    slope2 = dot(gradient2, searchDirection);
                    // Alphalpha3 is now relative to the location of alpha2
                    alpha3 -= alpha2;
                }
                if (value2 > value1 + alpha1 * RHO * slope1 ||
                        slope2 > -SIG * slope1)
                {
                    // This is a failure
                    break;
                }
                else if (slope2 > SIG * slope1)
                {
                    // Success
                    success = true;
                    break;
                }
                else if (M == 0)
                {
                    // Failure
                    break;
                }
                // Make cubic extrapolation
                double A = 6*(value2-value3)/alpha3+3*(slope2+slope3);
                double B = 3*(value3-value2)-alpha3*(slope3+2*slope2);
                double C = B*B-A*slope2*alpha3*alpha3;
                double D = -slope2*alpha3*alpha3;
                alpha2 = D / (B + sqrt(C));
                // If we have a numerical problem or wrosearchDirection sign
                if (std::isnan(alpha2) || alpha2 < 0)
                {
                    // If we have no upper limit
                    if (limit < -0.5)
                    {
                        // Extrapolate the maximum amount
                        alpha2 = alpha1 * (EXT-1);
                    }
                    else
                    {
                        // Otherwise bisect
                        alpha2 = (limit - alpha1)/2;
                    }
                }
                // If extraplation beyond max
                else if ((limit > -0.5) && (alpha2 + alpha1 > limit))
                {
                    // Bisect
                    alpha2 = (limit - alpha1)/2;
                }
                // If extrapolation beyond limit
                else if ((limit < -0.5) && (alpha2 + alpha1 > alpha1 * EXT))
                {
                    // Set to extrapolation limit
                    alpha2 = alpha1 * (EXT - 1.0);
                }
                else if (alpha2 < -alpha3 * INT)
                {
                    alpha2 = -alpha3 * INT;
                }
                // If too close to limit
                else if ((limit > -0.5) &&
                         (alpha2 < (limit - alpha1) * (1.0 - INT)))
                {
                    alpha2 = (limit - alpha1) * (1.0 - INT);
                }
                // Set point 3 equal to point 2
                value3 = value2;
                slope3 = slope2;
                alpha3 = -alpha2;
                // Update current estimates
                alpha1 += alpha2;
                input1 += alpha2 * searchDirection;
                f->getValueAndGradients(value2, gradient2, input1);
                --M;
                slope2 = dot(gradient2, searchDirection);
            }
            // End of line search
            // If line search succeeded
            if (success)
            {
                value1 = value2;
                std::cout << "Iteration " << i << "| value: " << value1 << std::endl;
                // Polack-Ribiere direction
                double scaleValue = dot(gradient2, gradient2);
                scaleValue -= dot(gradient1, gradient2);
                scaleValue /= dot(gradient1, gradient1);
                searchDirection *= scaleValue;
                searchDirection -= gradient2;
                // Swap derivatives
                swap(gradient1, gradient2);
                slope2 = dot(gradient1, searchDirection);
                // New slope must be negative
                if (slope2 > 0)
                {
                    // Otherwise use steepest direction
                    searchDirection = - gradient1;
                    slope2 = -dot(searchDirection, searchDirection);
                }
                // Slope ratio but max RATIO
                alpha1 *= std::min(RATIO, slope1/slope2);
                slope1 = slope2;
                // This line search did not fail
                ls_failed = false;
            }
            else
            {
                // Restore point from before failed line search
                input1 = input0;
                f->getValueAndGradients(value1, gradient1, input1);

                // If line search failed twice in a row or we ran out of time
                if (ls_failed || i > maxIters)
                {
                    // Terminate.
                    break;
                }
                // Swap derivatives
                swap(gradient1, gradient2);
                // Try steepest
                searchDirection = -gradient1;
                slope1 = -dot(searchDirection, searchDirection);
                alpha1 = 1/(1 - slope1);
                // This line search failed
                ls_failed = true;
            }
        }
        return input1;
    }
}
