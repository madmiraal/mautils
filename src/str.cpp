// (c) 2016: Marcel Admiraal

#include "str.h"

#include <algorithm>

namespace ma
{
    Str::Str() :
        len(0)
    {
        character = new char[len+1];
        character[len] = '\0';
    }

    Str::~Str()
    {
        delete[] character;
    }

    Str::Str(const char* carray) :
        len(0)
    {
        char c = carray[len];
        while (c != '\0')
        {
            ++len;
            c = carray[len];
        }
        character = new char[len+1];
        len = copyStr(carray, 0, len);
        character[len] = '\0';
    }

    Str::Str(const char* buffer, const unsigned int length) :
        len(length)
    {
        character = new char[len+1];
        len = copyStr(buffer, 0, len);
        character[len] = '\0';
    }

    Str::Str(const Str& source) :
        len(source.len)
    {
        character = new char[len+1];
        len = copyStr(source, 0, len);
        character[len] = '\0';
    }

    Str::Str(const Str& source, const unsigned int first,
            const unsigned int length) :
        len(length)
    {
        character = new char[len+1];
        len = copyStr(source, first, len);
        character[len] = '\0';
    }

    Str::Str(const unsigned int length, const char c) :
        len(length)
    {
        character = new char[len+1];
        for (unsigned int index = 0; index < len; ++index)
        {
            character[index] = c;
        }
        character[len] = '\0';
    }

    void swap(Str& first, Str& second)
    {
        std::swap(first.character, second.character);
        std::swap(first.len, second.len);
    }

    Str& Str::operator=(Str source)
    {
        swap(*this, source);
        return *this;
    }

    char& Str::operator[](const unsigned int index)
    {
        if (index >= len) throw "Out of bounds exception.";
        return character[index];
    }

    const char& Str::operator[](const unsigned int index) const
    {
        if (index >= len) throw "Out of bounds exception.";
        return character[index];
    }

    Str& Str::operator+=(const Str& source)
    {
        Str result(len + source.len, '\0');
        result.copyStr(*this, 0, len);
        result.copyStr(source, 0, source.len, len);
        swap(*this, result);
        return *this;
    }

    Str& Str::operator+=(const char& character)
    {
        Str result(len + 1, '\0');
        result.copyStr(*this, 0, len);
        result[len] = character;
        swap(*this, result);
        return *this;
    }

    Str& Str::operator-=(const Str& source)
    {
        replaceAll(source, "");
        return *this;
    }

    unsigned int Str::length() const
    {
        return len;
    }

    char* Str::c_str()
    {
        return character;
    }

    const char* Str::c_str() const
    {
        return character;
    }

    Str Str::subStr(const unsigned int first, const unsigned int length) const
    {
        Str result(*this, first, length);
        return result;
    }

    Str Str::left(const unsigned int length) const
    {
        Str result(*this, 0, length);
        return result;
    }

    Str Str::trimLeft(const unsigned int length) const
    {
        Str result(*this, length, len - length);
        return result;
    }

    Str Str::right(const unsigned int length) const
    {
        Str result(*this, len - length, length);
        return result;
    }

    Str Str::trimRight(const unsigned int length) const
    {
        Str result(*this, 0, len - length);
        return result;
    }

    unsigned int Str::copyStr(const Str& source, unsigned int first,
            unsigned int length, unsigned int start)
    {
        return copyStr(source.character, first, length, start);
    }

    unsigned int Str::copyStr(const char* source, unsigned int first,
            unsigned int length, unsigned int start)
    {
        unsigned int fromIndex = first;
        unsigned int toIndex = start;
        unsigned int count = 0;
        while (fromIndex < first+length && toIndex < len &&
                source[fromIndex] != '\0')
        {
            character[toIndex] = source[fromIndex];
            ++fromIndex;
            ++toIndex;
            ++count;
        }
        return count;
    }

    unsigned int Str::find(const Str& source, unsigned int start) const
    {
        if (source.len == 0) return -1;
        unsigned int index = start;
        bool foundStart = false;
        while (!foundStart && index < len)
        {
            if (character[index] == source[0])
            {
                foundStart = true;
                for (unsigned sourceIndex = 0; sourceIndex < source.len;
                        ++sourceIndex)
                {
                    if (source[sourceIndex] != character[index + sourceIndex])
                    {
                        foundStart = false;
                        break;
                    }
                }
                break;
            }
            ++index;
        }
        if (foundStart)
            return index;
        else
            return -1;
    }

    bool Str::replaceFirst(const Str& remove, const Str& insert,
            unsigned int start)
    {
        unsigned int index = find(remove, start);
        if (index != (unsigned int)-1)
        {
            Str result = subStr(0, index);
            result += insert;
            result += subStr(index + remove.len, len-index);
            swap(*this, result);
            return true;
        }
        return false;
    }

    unsigned int Str::replaceAll(const Str& remove, const Str& insert,
            unsigned int start)
    {
        unsigned int count = 0;
        while (replaceFirst(remove, insert, start)) ++count;
        return count;
    }

    bool Str::isInteger() const
    {
        if (len == 0) return false;
        if (character[0] == '-')
        {
            if (len == 1 || character[1] == '-') return false;
            return trimLeft(1).isInteger();
        }
        for (unsigned int index = 0; index < len; ++index)
        {
            if (!isDigit(character[index])) return false;
        }
        return true;
    }

    bool Str::isBinaryNumber() const
    {
        if (len == 0) return false;
        if (len > 1 && character[0] == '0' && character[1] == 'b')
        {
            if (len > 3 && character[3] == 'b') return false;
            return trimLeft(2).isBinaryNumber();
        }
        for (unsigned int index = 0; index < len; ++index)
        {
            if (!isBinaryDigit(character[index])) return false;
        }
        if (character[0] == '1')
        {
            // Negative numbers require at least one other bit to be 1.
            for (unsigned int index = 1; index < len; ++index)
            {
                if (character[index] == '1') return true;
            }
            return false;
        }
        return true;
    }

    bool Str::isHexNumber() const
    {
        if (len == 0) return false;
        if (len > 1 && character[0] == '0' && character[1] == 'x')
        {
            if (len > 3 && character[3] == 'x') return false;
            return trimLeft(2).isHexNumber();
        }
        for (unsigned int index = 0; index < len; ++index)
        {
            if (!isHexDigit(character[index])) return false;
        }
        return true;
    }

    bool Str::twosComplement()
    {
        if (!isBinaryNumber()) return false;
        // Check for leading 0b
        if (len > 1 && character[0] == '0' && character[1] == 'b')
        {
            Str temp = trimLeft(2);
            if (!temp.twosComplement()) return false;
            temp = "0b" + temp;
            swap(*this, temp);
            return true;
        }
        // Positive number.
        if (character[0] == '0')
        {
            // Invert bits.
            for (unsigned int index = 0; index < len; ++index)
            {
                character[index] == '0' ?
                        character[index] = '1' :
                        character[index] = '0';
            }
            // Add 1.
            bool carry = true;
            unsigned int index = len;
            while (carry && index > 0)
            {
                if (character[index-1] == '0')
                {
                    character[index-1] = '1';
                    carry = false;
                }
                else
                {
                    character[index-1] = '0';
                    //carry = true;
                }
                --index;
            }
        }
        // Negative number.
        else
        {
            // Subtract 1.
            bool borrow = true;
            unsigned int index = len;
            while (borrow && index > 0)
            {
                if (character[index-1] == '0')
                {
                    character[index-1] = '1';
                    borrow = true;
                }
                else
                {
                    character[index-1] = '0';
                    borrow = false;
                }
                --index;
            }
            // Invert.
            for (unsigned int index = 0; index < len; ++index)
            {
                character[index] == '0' ?
                        character[index] = '1' :
                        character[index] = '0';
            }
        }
        return true;
    }

    bool Str::integerToLong(long& number) const
    {
        // Ensure we're working with an integer.
        if (!isInteger()) return false;
        // If negative, convert the postive and negate.
        if (character[0] == '-')
        {
            long negative = 0;
            if (!trimLeft(1).integerToLong(negative)) return false;
            number = -negative;
            return true;
        }
        number = 0;
        for (unsigned int index = 0; index < len; ++index)
        {
            number *= 10;
            number += character[index] - '0';
        }
        return true;
    }

    bool Str::binaryToLong(long& number) const
    {
        // Ensure we're working with a binary representation.
        if (!isBinaryNumber()) return false;
        // If necessary, drop the leading 0b.
        if (len > 1 && character[1] == 'b')
        {
            return trimLeft(2).binaryToLong(number);
        }
        // If negative, convert the positive and negate.
        if (character[0] == '1')
        {
            Str negativeStr(*this);
            if (!negativeStr.twosComplement()) return false;
            long negativeLong;
            if (!negativeStr.binaryToLong(negativeLong)) return false;
            number = -negativeLong;
            return true;
        }
        // Convert to a number.
        number = 0;
        for (unsigned int index = 0; index < len; ++index)
        {
            number *= 2;
            if (character[index] == '1') ++number;
        }
        return true;
    }

    bool Str::hexToLong(long& number) const
    {
        // Ensure we're working with a hexadecimal representation.
        if (!isHexNumber()) return false;
        // Convert to a binary representation and convert.
        Str binaryNumber;
        if (!hexToBinary(*this, binaryNumber)) return false;
        return binaryNumber.binaryToLong(number);
    }

    Str operator+(const Str& first, const Str& second)
    {
        Str result(first);
        result += second;
        return result;
    }

    Str operator+(const Str& first, const char& character)
    {
        Str result(first);
        result += character;
        return result;
    }

    Str operator+(const Str& first, const char* second)
    {
        return first + Str(second);
    }

    Str operator+(const char* first, const Str& second)
    {
        return Str(first) + second;
    }

    Str operator-(const Str& first, const Str& second)
    {
        Str result(first);
        result -= second;
        return result;
    }

    Str operator-(const Str& first, const char* second)
    {
        return first - Str(second);
    }

    Str operator-(const Str& first, const char second)
    {
        return first - Str(1, second);
    }

    Str operator-(const char* first, const Str& second)
    {
        return Str(first) - second;
    }

    bool operator==(const Str& left, const Str& right)
    {
        if (left.length() != right.length()) return false;
        for (unsigned int index = 0; index < left.length(); ++index)
        {
            if (left[index] != right[index]) return false;
        }
        return true;
    }

    bool operator!=(const Str& left, const Str& right)
    {
        return !(left == right);
    }

    bool isEqual(const char* left, const char* right)
    {
        return Str(left) == Str(right);
    }

    std::istream& operator>>(std::istream& is, Str& destination)
    {
        getNext(is, destination);
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const Str& source)
    {
        os << source.c_str();
        return os;
    }

    Str& operator<<(Str& first, const Str& second)
    {
        first += second;
        return first;
    }

    Str& operator<<(Str& first, const char* second)
    {
        first += Str(second);
        return first;
    }

    Str subStr(const Str& source, const unsigned int first,
            const unsigned int length)
    {
        Str result = source.subStr(first, length);
        return result;
    }

    Str subStr(const char* source, const unsigned int first,
            const unsigned int length)
    {
        Str result = subStr(Str(source), first, length);
        return result;
    }

    Str leftStr(const Str& source, const unsigned int length)
    {
        Str result = source.left(length);
        return result;
    }

    Str leftStr(const char* source, const unsigned int length)
    {
        Str result = leftStr(Str(source), length);
        return result;
    }

    Str trimLeftStr(const Str& source, const unsigned int length)
    {
        Str result = source.trimLeft(length);
        return result;
    }

    Str trimLeftStr(const char* source, const unsigned int length)
    {
        Str result = trimLeftStr(Str(source), length);
        return result;
    }

    Str rightStr(const Str& source, const unsigned int length)
    {
        Str result = source.right(length);
        return result;
    }

    Str rightStr(const char* source, const unsigned int length)
    {
        Str result = rightStr(Str(source), length);
        return result;
    }

    Str trimRightStr(const Str& source, const unsigned int length)
    {
        Str result = source.trimRight(length);
        return result;
    }

    Str trimRightStr(const char* source, const unsigned int length)
    {
        Str result = trimRightStr(Str(source), length);
        return result;
    }

    unsigned int copyStr(Str& dest, const Str& source, unsigned int first,
            unsigned int length, unsigned int start)
    {
        return dest.copyStr(source, first, length, start);
    }

    unsigned int copyStr(Str& dest, const char* source, unsigned int first,
            unsigned int length, unsigned int start)
    {
        return dest.copyStr(source, first, length, start);
    }

    std::istream& getNext(std::istream& is, Str& destination, char delim)
    {
        destination = Str(0, 'x');
        char c = delim+1;
        char buffer[256];
        while (is.good() && c != delim)
        {
            unsigned int index = 0;
            while (index < 256)
            {
                is.get(c);
                if (is.good() && c != delim)
                {
                    buffer[index] = c;
                    ++index;
                }
                else
                {
                    break;
                }
            }
            // Check for and remove carriage return.
            if (delim == '\n' && index > 0 && buffer[index-1] == '\r') --index;
            destination += Str(buffer, index);
        }
        return is;
    }

    Str getNext(std::istream& is, char delim)
    {
        Str next;
        getNext(is, next, delim);
        return next;
    }

    bool isInteger(const Str& str)
    {
        return str.isInteger();
    }

    bool isBinaryNumber(const Str& str)
    {
        return str.isBinaryNumber();
    }

    bool isHexNumber(const Str& str)
    {
        return str.isHexNumber();
    }

    bool twosComplement(const Str& binaryStr, Str& negativeStr)
    {
        Str temp(binaryStr);
        if (!temp.twosComplement()) return false;
        swap(negativeStr, temp);
        return true;
    }

    bool binaryToHex(const Str& binaryStr, Str& hexStr, bool lowerCase)
    {
        if (!binaryStr.isBinaryNumber()) return false;
        unsigned int binaryLength = binaryStr.length();
        if (binaryLength > 1 && binaryStr[1] == 'b')
        {
            Str temp;
            if (!binaryToHex(binaryStr.trimLeft(2), temp, lowerCase)) return false;
            temp = "0x" + temp;
            swap(hexStr, temp);
            return true;
        }
        // Determine length of hex string.
        unsigned int hexLength = binaryLength / 4;
        if (binaryLength % 4 != 0) ++hexLength;
        // Create the characters.
        char hexArray[hexLength] = {0};
        Str binaryRemainder(binaryStr);
        long number;
        for (unsigned int hexIndex = hexLength; hexIndex > 1; --hexIndex)
        {
            Str binaryFour = binaryRemainder.right(4);
            if (binaryFour[0] == '1') binaryFour = "0" + binaryFour;
            binaryRemainder = binaryRemainder.trimRight(4);
            binaryFour.binaryToLong(number);
            if (number < 10)
                hexArray[hexIndex-1] = '0' + number;
            else
            {
                if (lowerCase)
                    hexArray[hexIndex-1] = 'a' + number - 10;
                else
                    hexArray[hexIndex-1] = 'A' + number - 10;
            }
        }
        if (binaryRemainder[0] == '1' && !binaryRemainder.isBinaryNumber())
            binaryRemainder = "1" + binaryRemainder;
        binaryRemainder.binaryToLong(number);
        if (number < -6)
            hexArray[0] = '8' + number + 8;
        else if (number < 0)
        {
            if (lowerCase)
                hexArray[0] = 'A' + number + 6;
            else
                hexArray[0] = 'a' + number + 6;
        }
        else // (number < 8)
            hexArray[0] = '0' + number;
        // Create the string.
        Str numberString(hexArray, hexLength);
        swap(hexStr, numberString);
        return true;
    }

    bool hexToBinary(const Str& hexStr, Str& binaryStr)
    {
        if (!hexStr.isHexNumber()) return false;
        unsigned int hexLength = hexStr.length();
        if (hexLength > 1 && hexStr[1] == 'x')
        {
            Str temp;
            if (!hexToBinary(hexStr.trimLeft(2), temp)) return false;
            temp = "0b" + temp;
            swap(binaryStr, temp);
            return true;
        }
        // Determine length of binary string.
        unsigned int binaryLength = hexLength * 4;
        // Create the characters.
        char binaryArray[binaryLength] = {0};
        for (unsigned int hexIndex = 0; hexIndex < hexLength; ++hexIndex)
        {
            char hexChar = hexStr[hexIndex];
            unsigned int binaryBase = hexIndex * 4;
            // Bit 0
            if ((hexChar >= '8' && hexChar <= '9') ||
                    (hexChar >= 'A' && hexChar <= 'F') ||
                    (hexChar >= 'a' && hexChar <= 'f'))
                binaryArray[binaryBase] = '1';
            else
                binaryArray[binaryBase] = '0';
            // Bit 1
            if ((hexChar >= '4' && hexChar <= '7') ||
                    (hexChar >= 'C' && hexChar <= 'F') ||
                    (hexChar >= 'c' && hexChar <= 'f'))
                binaryArray[binaryBase+1] = '1';
            else
                binaryArray[binaryBase+1] = '0';
            // Bit 2
            if (hexChar == '2' || hexChar == '3' ||
                    hexChar == '6' || hexChar == '7' ||
                    hexChar == 'A' || hexChar == 'B' ||
                    hexChar == 'E' || hexChar == 'F' ||
                    hexChar == 'a' || hexChar == 'b' ||
                    hexChar == 'e' || hexChar == 'f')
                binaryArray[binaryBase+2] = '1';
            else
                binaryArray[binaryBase+2] = '0';
            // Bit 3
            if (hexChar == '1' || hexChar == '3' ||
                    hexChar == '5' || hexChar == '7' ||
                    hexChar == '9' || hexChar == 'B' ||
                    hexChar == 'D' || hexChar == 'F' ||
                    hexChar == '9' || hexChar == 'b' ||
                    hexChar == 'd' || hexChar == 'f')
                binaryArray[binaryBase+3] = '1';
            else
                binaryArray[binaryBase+3] = '0';
        }
        // Prune extra leading zeros and ones.
        unsigned int leadingzeros = 0;
        unsigned int leadingones = 0;
        unsigned int remainingones = 0;
        unsigned int binaryIndex = 0;
        while (binaryArray[binaryIndex] == '0')
        {
            ++leadingzeros;
            ++binaryIndex;
        }
        binaryIndex = 0;
        while (binaryArray[binaryIndex] == '1')
        {
            ++leadingones;
            ++binaryIndex;
        }
        for (; binaryIndex < binaryLength; ++binaryIndex)
        {
            if (binaryArray[binaryIndex] == '1') ++remainingones;
        }
        unsigned int trimLength = 0;
        if (leadingzeros > 0)
        {
            trimLength = leadingzeros - 1;
        }
        else if (leadingones > 0)
        {
            trimLength = leadingones - 1;
            if (remainingones == 0) --trimLength;
        }
        // Create the string.
        Str numberString(&binaryArray[trimLength], binaryLength - trimLength);
        swap(binaryStr, numberString);
        return true;
    }

    bool integerToLong(const Str& integerStr, long& number)
    {
        return integerStr.integerToLong(number);
    }

    bool binaryToLong(const Str& binaryStr, long& number)
    {
        return binaryStr.binaryToLong(number);
    }

    bool hexToLong(const Str& hexStr, long& number)
    {
        return hexStr.hexToLong(number);
    }

    Str toStr(const long number)
    {
        // If negative convert positive and add -.
        if (number < 0)
        {
            Str negativeString = toStr(-number);
            return "-" + negativeString;
        }
        // Determine the length of the string.
        unsigned int length = 0;
        unsigned int n = number;
        while (n != 0)
        {
            n /= 10;
            ++length;
        }
        if (length == 0) ++length;
        // Create the characters.
        char numberChar[length + 1];
        n = number;
        for (unsigned int i = length; i > 0; --i)
        {
            char zero = '0';
            numberChar[i - 1] = zero + (n % 10);
            n /= 10;
        }
        // Create the string.
        Str numberString(numberChar, length);
        return numberString;
    }

    Str toBinary(const long number, bool prefix)
    {
        if (number < 0)
        {
            Str negativeStr = toBinary(-number, prefix);
            negativeStr.twosComplement();
            return negativeStr;
        }
        // Determine the length of the string.
        unsigned int length = 0;
        unsigned int n = number;
        while (n != 0)
        {
            n /= 2;
            ++length;
        }
        // Create the characters.
        char numberChar[length + 1];
        n = number;
        for (unsigned int i = length; i > 0; --i)
        {
            char zero = '0';
            numberChar[i - 1] = zero + (n % 2);
            n /= 2;
        }
        // Create the string.
        Str numberString(numberChar, length);
        if (prefix)
            numberString = "0b0" + numberString;
        else
            numberString = "0" + numberString;

        return numberString;
    }

    Str toHex(const long number, bool prefix, bool lowerCase)
    {
        Str binaryStr = toBinary(number, prefix);
        Str hexStr;
        binaryToHex(binaryStr, hexStr, lowerCase);
        return hexStr;
    }

    bool isDigit(const char character)
    {
        return character >= '0' && character <= '9';
    }

    bool isBinaryDigit(const char character)
    {
        return character == '0' || character == '1';
    }

    bool isHexDigit(const char character)
    {
        return isDigit(character) ||
            (character >= 'a' && character <= 'f') ||
            (character >= 'A' && character <= 'F');
    }
}
