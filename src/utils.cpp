// (c) 2016: Marcel Admiraal

#include "utils.h"

#include <cstdio>
#include <fstream>

namespace ma
{
    bool approxEqual(const double left, const double right,
            const double error)
    {
        if (left < 0) return approxEqual(-left, -right, error);
        double e = error / 100;
        if (left < e)   // Close to zero.
        {
            if (right > -e && right < e) return true;
            else return false;
        }
        if (left > right - e * right && left < right + e * right) return true;
        else return false;
    }

    unsigned long intSqrt(unsigned long num)
    {
        unsigned long checkBit = 0x80000000;
        unsigned long guess = 0x80000000;

        while(checkBit > 0) {
            // If guess too big.
            if(guess * guess > num){
                // Clear the current guess bit.
                guess ^= checkBit;
            }
            // Set next bit to check.
            checkBit >>= 1;
            // Set next guess bit.
            guess |= checkBit;
        }
        return guess;
    }

    float invSqrt(const float num)
    {
        // Uses log(1/√x) = −½log(x)

        // Alias to an integer to get a shifted approximation of the logarithm.
        int temp  = *(int*)&num;
        // Correct for the shift.
        temp = 0x5f3759df - (temp >> 1);
        // Alias back to a float to get an approximation of the exponential.
        float result = *(float*)&temp;
        // Refine the approximation using a single iteration of Newton's method.
        float half = num * 0.5f;
        result *= 1.5f - (half * result * result);
        return result;
    }

    void displayPacket(const unsigned char* packet, const unsigned int packetLength,
            std::ostream& os)
    {
        os << packetLength << " bytes in packet." << std::endl;
        for (unsigned int i = 0; i < packetLength; ++i)
            os << (int)packet[i] << " ";
        if (packetLength > 0) std::cout << std::endl;
        for (unsigned int i = 0; i < packetLength; ++i)
            os << toHex((int)packet[i]) << " ";
        if (packetLength > 0) std::cout << std::endl;
        for (unsigned int i = 0; i < packetLength; ++i)
            os << (char)packet[i] << " ";
        if (packetLength > 0) std::cout << std::endl;
    }

    bool renameFile(const Str& filename, unsigned int version)
    {
        unsigned int nameLength = filename.length();
        unsigned int lastDot = 0;
        unsigned int nextDot = filename.find(".", lastDot);
        while (nextDot != (unsigned int) -1)
        {
            lastDot = nextDot;
            nextDot = filename.find(".", lastDot + 1);
        }
        if (lastDot == 0) lastDot = nameLength;

        Str oldFileName = filename;
        if (version != 0)
        {
            oldFileName = filename.subStr(0, lastDot) + toStr(version) +
                    filename.subStr(lastDot, nameLength - lastDot);
        }
        Str newFileName = filename.subStr(0, lastDot) + toStr(version + 1) +
                filename.subStr(lastDot, nameLength - lastDot);

        if (std::ifstream(newFileName.c_str()))
        {
            if (!renameFile(filename, version + 1))
            {
                return false;
            }
        }
        int result = rename(oldFileName.c_str(), newFileName.c_str());
        return (result == 0);
    }
}
