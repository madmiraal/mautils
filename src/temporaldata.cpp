// (c) 2016 - 2017: Marcel Admiraal

#include "temporaldata.h"

namespace ma
{
    TemporalData::TemporalData(unsigned int channels,
            unsigned int frequency, unsigned int period) :
        channels(channels), dataLength(frequency * period / 1000),
        updatePeriod(1000 / frequency)
    {
        initialise();
    }

    TemporalData::TemporalData(unsigned int channels, unsigned int dataLength,
            unsigned int updatePeriod, const double** data) :
        channels(channels), dataLength(dataLength),
        updatePeriod(updatePeriod)
    {
        initialise();
        if (data)
        {
            for (unsigned int channel = 0; channel < channels; ++channel)
                for (unsigned int index = 0; index < dataLength; ++index)
                    this->data[channel][index] = data[channel][index];
        }
    }

    TemporalData::TemporalData(const TemporalData& source) :
        channels(source.channels), dataLength(source.dataLength),
        updatePeriod(source.updatePeriod)
    {
        initialise();
    }

    TemporalData::~TemporalData()
    {
        for (unsigned int channel = 0; channel < channels; ++channel)
        {
            delete[] data[channel];
        }
        delete[] data;
        delete[] lastValue;
        delete[] timeRemainder;
        delete[] lastUpdate;
    }

    void swap(TemporalData& first, TemporalData& second)
    {
        std::swap(first.channels, second.channels);
        std::swap(first.dataLength, second.dataLength);
        std::swap(first.updatePeriod, second.updatePeriod);
        std::swap(first.data, second.data);
        std::swap(first.lastValue, second.lastValue);
        std::swap(first.timeRemainder, second.timeRemainder);
        std::swap(first.lastUpdate, second.lastUpdate);
    }

    TemporalData& TemporalData::operator=(TemporalData source)
    {
        swap(*this, source);
        return *this;
    }

    unsigned int TemporalData::getChannels() const
    {
        return channels;
    }

    unsigned int TemporalData::getDataLength() const
    {
        return dataLength;
    }

    unsigned int TemporalData::getUpdatePeriod() const
    {
        return updatePeriod;
    }

    unsigned int TemporalData::addData(double value, unsigned int channel,
                const unsigned int milliseconds)
    {
        std::lock_guard<std::mutex> lock(dataLock);
        lastValue[channel] = value;
        unsigned int updates = shiftData(channel, milliseconds);
        return updates;
    }

    Vector TemporalData::addData(const Vector& value,
                const unsigned int milliseconds)
    {
        Vector updates(channels);
        for (unsigned int channel = 0; channel < channels; ++channel)
            updates[channel] = addData(value[channel], channel, milliseconds);
        return updates;
    }

    Vector TemporalData::getData(const unsigned int channel,
            bool update, const unsigned int milliseconds)
    {
        std::lock_guard<std::mutex> lock(dataLock);
        if (update) shiftData(channel, milliseconds);
        return Vector(data[channel], dataLength);
    }

    unsigned int TemporalData::getData(double output[], const unsigned int channel,
            bool update, const unsigned int milliseconds)
    {
        std::lock_guard<std::mutex> lock(dataLock);
        unsigned int updates = 0;
        if (update) updates = shiftData(channel, milliseconds);
        for (unsigned int index = 0; index < dataLength; ++index)
            output[index] = data[channel][index];
        return updates;
    }

    Vector TemporalData::getData(double** output,
            bool update, const unsigned int milliseconds)
    {
        Vector updates(channels);
        for (unsigned int channel = 0; channel < channels; ++channel)
            updates[channel] = getData(output[channel], channel, update,
                    milliseconds);
        return updates;
    }

    double TemporalData::getLastData(const unsigned int channel) const
    {
        return data[channel][dataLength - 1];
    }

    Vector TemporalData::getLastData() const
    {
        Vector lastData(channels);
        for (unsigned int channel = 0; channel < channels; ++channel)
            lastData[channel] = data[channel][dataLength - 1];
        return lastData;
    }

    void TemporalData::initialise()
    {
        if (dataLength == 0) dataLength = 1;
        data = new double*[channels]();
        lastValue = new double[channels]();
        std::chrono::milliseconds now =
                std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::steady_clock::now().time_since_epoch());
        lastUpdate = new std::chrono::milliseconds[channels]();
        for (unsigned int channel = 0; channel < channels; ++channel)
        {
            data[channel] = new double[dataLength]();
            lastUpdate[channel] = now;
        }
        timeRemainder = new unsigned int[channels]();
    }

    unsigned int TemporalData::shiftData(const unsigned int channel,
            const unsigned int milliseconds)
    {
        std::chrono::milliseconds now =
                std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::steady_clock::now().time_since_epoch());

        // Calculate the number of updates.
        unsigned int timeDelta = milliseconds;
        if (timeDelta == 0) timeDelta = (now - lastUpdate[channel]).count();
        timeDelta += timeRemainder[channel];
        unsigned int updates = timeDelta / updatePeriod;
        if (updates == 0) return 0;
        if (updates > dataLength) updates = dataLength;

        // Shift data.
        for (unsigned int index = 0; index < dataLength-updates; ++index)
            data[channel][index] = data[channel][index+updates];

        // Smooth fill the data.
        if (updates == dataLength)
        {
            data[channel][0] = data[channel][dataLength - 1];
            double delta = (lastValue[channel] - data[channel][0])
                    / (dataLength - 1);
            for (unsigned int index = 1; index < dataLength - 1; ++index)
                data[channel][index] = data[channel][index - 1] + delta;
        }
        else
        {
            double delta = (lastValue[channel] - data[channel][dataLength - 1])
                    / updates;
            for (unsigned int index = dataLength - updates;
                    index < dataLength - 1; ++index)
                data[channel][index] = data[channel][index - 1] + delta;
        }

        data[channel][dataLength - 1] = lastValue[channel];

        // Update time trackers.
        lastUpdate[channel] = now;
        timeRemainder[channel] = timeDelta % updatePeriod;

        return updates;
    }

    std::istream& operator>>(std::istream& is, TemporalData& destination)
    {
        unsigned int channels, dataLength, updatePeriod;
        is >> channels;
        is >> dataLength;
        is >> updatePeriod;
        if (is.good())
        {
            destination = TemporalData(channels, dataLength, updatePeriod, 0);
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const TemporalData& source)
    {
        os << source.getChannels() << '\t';
        os << source.getDataLength() << '\t';
        os << source.getUpdatePeriod();
        return os;
    }
}
