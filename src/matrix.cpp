// (c) 2011 - 2017: Marcel Admiraal

#include "matrix.h"

#include "iterator.h"
#include "utils.h"
#include "strstream.h"

#include <algorithm>
#include <cmath>
#include <cfloat>
#include <fstream>

namespace ma
{
    Matrix::Matrix() :
        rows(0), cols(0), element(0)
    {
    }

    Matrix::~Matrix()
    {
        for (unsigned int row = 0; row < rows; ++row)
            if (element[row] != 0) delete[] element[row];
        if (element != 0) delete[] element;
    }

    Matrix::Matrix(const unsigned int rows, const unsigned int cols) :
        rows(rows), cols(cols), element(0)
    {
        if (rows != 0 && cols != 0)
        {
            element = new double*[rows]();
            for (unsigned int row = 0; row < rows; ++row)
            {
                element[row] = new double[cols]();
            }
        }
    }

    Matrix::Matrix(const int* const* num, const unsigned int rows,
            const unsigned int cols) :
            rows(rows), cols(cols), element(0)
    {
        if (rows != 0 && cols != 0)
        {
            element = new double*[rows]();
            #pragma omp parallel for
            for (unsigned int row = 0; row < rows; ++row)
            {
                element[row] = new double[cols]();
                for (unsigned int col = 0; col < cols; ++col)
                {
                    element[row][col] = (double)(num[row][col]);
                }
            }
        }
    }

    Matrix::Matrix(const int* num, const unsigned int rows,
            const unsigned int cols) :
            rows(rows), cols(cols), element(0)
    {
        if (rows != 0 && cols != 0)
        {
            element = new double*[rows]();
            for (unsigned int row = 0; row < rows; ++row)
            {
                element[row] = new double[cols]();
                unsigned int offset = row * cols;
                for (unsigned int col = 0; col < cols; ++col)
                {
                    element[row][col] = (double)(num[offset + col]);
                }
            }
        }
    }

    Matrix::Matrix(const float* const* num, const unsigned int rows,
            const unsigned int cols) :
            rows(rows), cols(cols), element(0)
    {
        if (rows != 0 && cols != 0)
        {
            element = new double*[rows]();
            #pragma omp parallel for
            for (unsigned int row = 0; row < rows; ++row)
            {
                element[row] = new double[cols]();
                for (unsigned int col = 0; col < cols; ++col)
                {
                    element[row][col] = (double)(num[row][col]);
                }
            }
        }
    }

    Matrix::Matrix(const float* num, const unsigned int rows,
            const unsigned int cols) :
            rows(rows), cols(cols), element(0)
    {
        if (rows != 0 && cols != 0)
        {
            element = new double*[rows]();
            for (unsigned int row = 0; row < rows; ++row)
            {
                element[row] = new double[cols]();
                unsigned int offset = row * cols;
                for (unsigned int col = 0; col < cols; ++col)
                {
                    element[row][col] = (double)(num[offset + col]);
                }
            }
        }
    }

    Matrix::Matrix(const double* const* num, const unsigned int rows,
            const unsigned int cols) :
            rows(rows), cols(cols), element(0)
    {
        if (rows != 0 && cols != 0)
        {
            element = new double*[rows]();
            #pragma omp parallel for
            for (unsigned int row = 0; row < rows; ++row)
            {
                element[row] = new double[cols]();
                for (unsigned int col = 0; col < cols; ++col)
                {
                    element[row][col] = num[row][col];
                }
            }
        }
    }

    Matrix::Matrix(const double* num, const unsigned int rows,
            const unsigned int cols) :
            rows(rows), cols(cols), element(0)
    {
        if (rows != 0 && cols != 0)
        {
            element = new double*[rows]();
            for (unsigned int row = 0; row < rows; ++row)
            {
                element[row] = new double[cols]();
                unsigned int offset = row * cols;
                for (unsigned int col = 0; col < cols; ++col)
                {
                    element[row][col] = num[offset + col];
                }
            }
        }
    }

    Matrix::Matrix(const List<List<double> >& num) :
            rows(0), cols(0), element(0)
    {
        rows = num.getSize();
        element = new double*[rows]();
        unsigned int row = 0;
        for (Iterator<List<double> > rowItr = num.first(); rowItr.valid();
                ++rowItr)
        {
            List<double> rowData = *rowItr;
            unsigned int lineCols = rowData.getSize();
            if (cols == 0) cols = lineCols;
            element[row] = new double[cols]();
            unsigned int col = 0;
            for (Iterator<double> colItr = rowData.first(); colItr.valid();
                    ++colItr)
            {
                element[row][col] = *colItr;
                ++col;
            }
            ++row;
        }
    }

    Matrix::Matrix(const List<Str>& num) :
            rows(0), cols(0), element(0)
    {
        unsigned int newCols= 0;
        unsigned int newRows = 0;
        List<List<double> > allElement;
        for (Iterator<Str> numItr = num.first(); numItr.valid(); ++numItr)
        {
            List<double> rowElement;
            Str row = *numItr;
            StrStream ss(row);
            unsigned int lineCols = 0;
            while(ss.good())
            {
                double number;
                ss >> number;
                if (!ss.fail())
                {
                    rowElement.append(number);
                    ++lineCols;
                }
            }
            if (lineCols != 0)
            {
                if (newCols== 0) newCols= lineCols;
                if (lineCols != newCols) return;
                {
                    allElement.append(rowElement);
                    ++newRows;
                }
            }
        }
        Matrix result(allElement);
        swap(*this, result);
    }

    Matrix::Matrix(const Vector& source) :
        rows(source.size()), cols(1), element(0)
    {
        if (rows != 0)
        {
            element = new double*[rows]();
            for (unsigned int row = 0; row < rows; ++row)
            {
                element[row] = new double[1]();
                element[row][0] = source[row];
            }
        }
    }

    Matrix::Matrix(const Matrix& source) :
        rows(source.rows), cols(source.cols), element(0)
    {
        if (rows != 0 && cols != 0)
        {
            element = new double*[rows]();
            #pragma omp parallel for
            for (unsigned int row = 0; row < rows; ++row)
            {
                element[row] = new double[cols]();
                for (unsigned int col = 0; col < cols; ++col)
                {
                    element[row][col] = source[row][col];
                }
            }
        }
    }

    Matrix::Matrix(const char* filename) :
        rows(0), cols(0), element(0)
    {
        load(filename);
        std::ifstream fileStream(filename, std::ifstream::in);
        if (fileStream.good())
        {
            Matrix result;
            fileStream >> result;
            if (fileStream.good())
            {
                swap(*this, result);
            }
            fileStream.close();
        }
    }

    void swap(Matrix& first, Matrix& second)
    {
        std::swap(first.rows, second.rows);
        std::swap(first.cols, second.cols);
        std::swap(first.element, second.element);
    }

    Matrix& Matrix::operator=(Matrix source)
    {
        swap(*this, source);
        return *this;
    }

    double* Matrix::operator[](const unsigned int row)
    {
        return element[row];
    }

    const double* Matrix::operator[](const unsigned int row) const
    {
        return element[row];
    }

    unsigned int Matrix::getRows() const
    {
        return rows;
    }

    unsigned int Matrix::getCols() const
    {
        return cols;
    }

    double Matrix::getElement(const unsigned int row,
            const unsigned int col) const
    {
        return element[row][col];
    }

    Matrix Matrix::getElements(const unsigned int rowStart,
            const unsigned int rowEnd, const unsigned int colStart,
            const unsigned int colEnd) const
    {
        Matrix result(rowEnd - rowStart + 1, colEnd - colStart + 1);
        unsigned int rowDest = 0;
        #pragma omp parallel for
        for (unsigned int rowSource = rowStart; rowSource <= rowEnd;
                ++rowSource)
        {
            unsigned int colDest = 0;
            for (unsigned int colSource = colStart; colSource <= colEnd;
                    ++colSource)
            {
                result[rowDest][colDest] = element[rowSource][colSource];
                colDest++;
            }
            rowDest++;
        }
        return result;
    }

    void Matrix::setElement(const int row, const int col, const double val)
    {
        element[row][col] = val;
    }

    void Matrix::setElements(const int rowStart, const int colStart,
                             const Matrix& source)
    {
        if (rowStart + source.rows > rows || colStart + source.cols > cols)
            throw("Matrix doesn't fit");
        #pragma omp parallel for
        for (unsigned int row = 0; row < source.rows; ++row)
            for (unsigned int col = 0; col < source.cols; ++col)
                element[rowStart + row][colStart + col] = source[row][col];
    }

    void Matrix::getRow(const int row, double* result) const
    {
        for (unsigned int col = 0; col < cols; ++col)
        {
            result[col] = element[row][col];
        }
    }

    Vector Matrix::getRow(const int row) const
    {
        Vector result(cols);
        for (unsigned int col = 0; col < cols; ++col)
        {
            result[col] = element[row][col];
        }
        return result;
    }

    void Matrix::setRow(const int row, const int* source)
    {
        for (unsigned int col = 0; col < cols; ++col)
            element[row][col] = (double) source[col];
    }

    void Matrix::setRow(const int row, const float* source)
    {
        for (unsigned int col = 0; col < cols; ++col)
            element[row][col] = (double) source[col];
    }

    void Matrix::setRow(const int row, const double* source)
    {
        for (unsigned int col = 0; col < cols; ++col)
            element[row][col] = source[col];
    }

    void Matrix::setRow(const int row, const Vector& source)
    {
        if (source.size() != cols)
            throw("Vector is the wrong size.");
        for (unsigned int col = 0; col < cols; ++col)
        {
            element[row][col] = source[col];
        }
    }

    void Matrix::setRows(const int rowStart, const Matrix& source)
    {
        setElements(rowStart, 0, source);
    }

    void Matrix::getCol(const int col, double* result) const
    {
        for (unsigned int row = 0; row < rows; ++row)
        {
            result[row] = element[row][col];
        }
    }

    Vector Matrix::getCol(const int col) const
    {
        Vector result(rows);
        for (unsigned int row = 0; row < rows; ++row)
        {
            result[row] = element[row][col];
        }
        return result;
    }

    void Matrix::setCol(const int col, const int* newCol)
    {
        for (unsigned int row = 0; row < rows; ++row)
            element[row][col] = (double) newCol[row];
    }

    void Matrix::setCol(const int col, const float* newCol)
    {
        for (unsigned int row = 0; row < rows; ++row)
            element[row][col] = (double) newCol[row];
    }

    void Matrix::setCol(const int col, const double* newCol)
    {
        for (unsigned int row = 0; row < rows; ++row)
            element[row][col] = newCol[row];
    }

    void Matrix::setCol(const int col, const Vector& source)
    {
        if (source.size() != rows)
            throw("Vector is the wrong size.");
        for (unsigned int row = 0; row < rows; ++row)
        {
            element[row][col] = source[row];
        }
    }

    void Matrix::setCols(const int colStart, const Matrix& source)
    {
        setElements(0, colStart, source);
    }

    void Matrix::addRows(const int r)
    {
        double** newElement = new double*[rows + r];
        for (unsigned int row = 0; row < rows; ++row)
        {
            std::swap(element[row], newElement[row]);
        }
        std::swap(element, newElement);
        delete[] newElement;
        for (unsigned int row = rows; row < rows + r; ++row)
        {
            element[row] = new double[cols];
        }
        rows += r;
    }

    void Matrix::addCols(const int c)
    {
        for (unsigned int row = 0; row < rows; ++row)
        {
            double* newRow = new double[cols + c];
            for (unsigned int col = 0; col < cols; ++col)
            {
                newRow[col] = element[row][col];
            }
            std::swap(element[row], newRow);
            delete[] newRow;
        }
        cols += c;
    }

    Matrix& Matrix::operator+=(const Matrix& right)
    {
        if (getRows() != right.getRows() || getCols() != right.getCols())
            throw("Arithmetic Exception: Matrix dimensions not equal");
        #pragma omp parallel for
        for (unsigned int row = 0; row < getRows(); ++row)
            for (unsigned int col = 0; col < getCols(); ++col)
                element[row][col] += right[row][col];
        return *this;
    }

    Matrix& Matrix::operator+=(const Vector& right)
    {
        if (getCols() != right.size())
            throw("Arithmetic Exception: Matrix and vector dimensions not equal");
        #pragma omp parallel for
        for (unsigned int row = 0; row < getRows(); ++row)
            for (unsigned int col = 0; col < getCols(); ++col)
                element[row][col] += right[col];
        return *this;
    }

    Matrix& Matrix::operator+=(const double& right)
    {
        #pragma omp parallel for
        for (unsigned int row = 0; row < getRows(); ++row)
            for (unsigned int col = 0; col < getCols(); ++col)
                element[row][col] += right;
        return *this;
    }

    Matrix& Matrix::operator-=(const Matrix& right)
    {
        if (getRows() != right.getRows() || getCols() != right.getCols())
            throw("Arithmetic Exception: Matrix dimensions not equal");
        #pragma omp parallel for
        for (unsigned int row = 0; row < getRows(); ++row)
            for (unsigned int col = 0; col < getCols(); ++col)
                element[row][col] -= right[row][col];
        return *this;
    }

    Matrix& Matrix::operator-=(const Vector& right)
    {
        if (getCols() != right.size())
            throw("Arithmetic Exception: Matrix and vector dimensions not equal");
        #pragma omp parallel for
        for (unsigned int row = 0; row < getRows(); ++row)
            for (unsigned int col = 0; col < getCols(); ++col)
                element[row][col] -= right[col];
        return *this;
    }

    Matrix& Matrix::operator-=(const double& right)
    {
        #pragma omp parallel for
        for (unsigned int row = 0; row < getRows(); ++row)
            for (unsigned int col = 0; col < getCols(); ++col)
                element[row][col] -= right;
        return *this;
    }

    Matrix& Matrix::operator*=(const double scalar)
    {
        #pragma omp parallel for
        for (unsigned int row = 0; row < getRows(); ++row)
            for (unsigned int col = 0; col < getCols(); ++col)
                element[row][col] *= scalar;
        return *this;
    }

    Matrix& Matrix::operator/=(const double scalar)
    {
        #pragma omp parallel for
        for (unsigned int row = 0; row < getRows(); ++row)
            for (unsigned int col = 0; col < getCols(); ++col)
                element[row][col] /= scalar;
        return *this;
    }

    Matrix Matrix::dot(const Matrix& other) const
    {
        if (rows != other.rows || cols != other.cols)
            throw("Arithmetic Exception: Matrix dimensions not equal");
        Matrix result(rows, cols);
        #pragma omp parallel for
        for (unsigned int row = 0; row < rows; ++row)
            for (unsigned int col = 0; col < cols; ++col)
                result.element[row][col] = element[row][col] * other[row][col];
        return result;
    }

    double Matrix::elementSum() const
    {
        double result = 0.0;
        #pragma omp parallel for reduction(+:result)
        for (unsigned int row = 0; row < rows; ++row)
            for (unsigned int col = 0; col < cols; ++col)
                result += element[row][col];
        return result;
    }

    double Matrix::elementSquaredSum() const
    {
        double result = 0.0;
        #pragma omp parallel for reduction(+:result)
        for (unsigned int row = 0; row < rows; ++row)
            for (unsigned int col = 0; col < cols; ++col)
                result += element[row][col] * element[row][col];
        return result;
    }

    double Matrix::det() const
    {
        if (rows != cols)
            throw("Arithmetic Exception: Matrix is not square");
        if (rows == 1)
            return element[0][0];
        double det = 0;
        #pragma omp parallel for reduction(+:det)
        for (unsigned int col = 0; col < cols; ++col) {
            det += element[0][col] * coFactor(0, col);
        }
        return det;
    }

    double Matrix::coFactor(const unsigned int row,
            const unsigned int col) const
    {
        Matrix minor = getMinor(row, col);
        double det = minor.det();
        int sign = ((row + col) % 2) == 0 ? 1 : -1;
        return det * sign;
    }

    Matrix Matrix::getMinor(const unsigned int dropRow,
            const unsigned int dropCol) const
    {
        Matrix result(rows - 1, cols - 1);
        unsigned int newRow = 0;
        for (unsigned int row = 0; row < rows; ++row)
        {
            if (row == dropRow) continue;
            int newCol = 0;
            for (unsigned int col = 0; col < cols; ++col) {
                if (col == dropCol) continue;
                result[newRow][newCol] = element[row][col];
                newCol++;
            }
            newRow++;
        }
        return result;
    }

    void Matrix::fillWith(const double value)
    {
        #pragma omp parallel for
        for (unsigned int row = 0; row < rows; ++row)
            for (unsigned int col = 0; col < cols; ++col)
                element[row][col] = value;
    }

    void Matrix::swapRows(const unsigned int row1, const unsigned int row2)
    {
        std::swap(element[row1], element[row2]);
    }

    void Matrix::swapCols(const unsigned int col1, const unsigned int col2)
    {
        for (unsigned int row = 0; row < rows; ++row)
        {
            double temp = element[row][col1];
            element[row][col1] = element[row][col2];
            element[row][col2] = temp;
        }
    }

    void Matrix::linearCombineRows(
        const unsigned int row1, const double factor1,
        const unsigned int row2, const double factor2
        )
    {
        for (unsigned int col = 0; col < cols; ++col)
        {
            element[row1][col] =
                element[row1][col] * factor1 +
                element[row2][col] * factor2;
        }
    }

    Matrix Matrix::horizontalMerge(const Matrix& right)
    {
        if (rows != right.rows)
            throw("Arithmetic Exception: Matrix rows not equal");
        Matrix result(rows, cols + right.cols);
        #pragma omp parallel for
        for (unsigned int row = 0; row < rows; ++row)
        {
            for (unsigned int col = 0; col < cols; ++col)
                result[row][col] = element[row][col];
            for (unsigned int col = 0; col < right.cols; ++col)
                result[row][cols + col] = right[row][col];
        }
        swap(result, *this);
        return *this;
    }

    Matrix Matrix::verticalMerge(const Matrix& bottom)
    {
        if (cols != bottom.cols)
            throw("Arithmetic Exception: Matrix columns not equal");
        Matrix result(rows + bottom.rows, cols);
        #pragma omp parallel for
        for (unsigned int row = 0; row < rows; ++row)
            for (unsigned int col = 0; col < cols; ++col)
                result[row][col] = element[row][col];
        #pragma omp parallel for
        for (unsigned int row = 0; row < bottom.rows; ++row)
            for (unsigned int col = 0; col < bottom.cols; ++col)
                result[rows + row][col] = bottom[row][col];
        swap(result, *this);
        return *this;
    }

    Matrix Matrix::reshape(const unsigned int rows, const unsigned int cols)
    {
        if (this->rows * this->cols != rows * cols)
            throw("Arithmetic Exception: Element count doesn't match");
        Matrix result(rows, cols);
        unsigned int thisRow = 0;
        unsigned int thisCol = 0;
        for (unsigned int row = 0; row < rows; ++row)
        {
            for (unsigned int col = 0; col < cols; ++ col)
            {
                result[row][col] = element[thisRow][thisCol];
                ++thisCol;
                if (thisCol == this->cols)
                {
                    thisCol = 0;
                    ++thisRow;
                }
            }
        }
        swap(result, *this);
        return *this;
    }

    Matrix Matrix::transpose()
    {
        Matrix result = Matrix(cols, rows);
        // Parallelism doesn't work due to memory thrashing.
        //#pragma omp parallel for
        for (unsigned int row = 0; row < rows; ++row)
            for (unsigned int col = 0; col < cols; ++col)
                result[col][row] = element[row][col];
        swap(result, *this);
        return *this;
    }

    Matrix Matrix::inverse()
    {
        // Make sure matrix is square
        if (rows != cols)
            throw("Arithmetic Exception: Not a square matrix.");
        //unsigned int n = rows;
        // Initialise the result matrix.
        //Matrix working(source);
        Matrix result = identity(cols);
        for (unsigned int col = 0; col < cols; ++col)
        {
            // Partial (rows only) pivot for numerical stability:
            // Ensure pivot row has largest pivot element.
            unsigned int rowLargest = col;
            double largest = abs(element[col][col]);
            for (unsigned int row = col + 1; row < rows; ++row)
            {
                if (abs(element[row][col]) > largest)
                {
                    rowLargest = row;
                    largest = abs(element[row][col]);
                }
            }
            if (rowLargest != col)
            {
                result.swapRows(col, rowLargest);
                swapRows(col, rowLargest);
            }

            // Divide the row by it's pivot element to make it one
            result.linearCombineRows(col, 1.0/element[col][col]);
            linearCombineRows(col, 1.0/element[col][col]);
            // Set other rows' column to zero
            for (unsigned int row = 0; row < rows; ++row)
            {
                if (row != col)
                {
                    result.linearCombineRows(row, 1.0, col, -element[row][col]);
                    linearCombineRows(row, 1.0, col, -element[row][col]);
                }
            }
        }
        swap(result, *this);
        return *this;
    }

    Vector Matrix::min(const int dim) const
    {
        if (dim == 1) return ma::transpose(*this).min();
        Vector index = minIndex();
        Vector result(cols);
        for (unsigned int col = 0; col < cols; ++col)
        {
            unsigned int row = index[col];
            result[col] = element[row][col];
        }
        return result;
    }

    Vector Matrix::minIndex(const int dim) const
    {
        if (dim == 1) return ma::transpose(*this).minIndex();
        Vector index(cols);
        Vector minimum(cols);
        for (unsigned int col = 0; col < cols; ++col)
        {
            minimum[col] = DBL_MAX;
            for (unsigned int row = 0; row < rows; ++row)
                if (element[row][col] < minimum[col])
                {
                    index[col] = row;
                    minimum[col] = element[row][col];
                }
        }
        return index;
    }

    Vector Matrix::max(const int dim) const
    {
        if (dim == 1) return ma::transpose(*this).max();
        Vector index = maxIndex();
        Vector result(cols);
        for (unsigned int col = 0; col < cols; ++col)
        {
            unsigned int row = index[col];
            result[col] = element[row][col];
        }
        return result;
    }

    Vector Matrix::maxIndex(const int dim) const
    {
        if (dim == 1) return ma::transpose(*this).maxIndex();
        Vector index(cols);
        Vector maximum(cols);
        for (unsigned int col = 0; col < cols; ++col)
        {
            maximum[col] = -DBL_MAX;
            for (unsigned int row = 0; row < rows; ++row)
                if (element[row][col] > maximum[col])
                {
                    index[col] = row;
                    maximum[col] = element[row][col];
                }
        }
        return index;
    }

    Vector Matrix::mean(const int dim) const
    {
        if (dim == 1) return ma::transpose(*this).mean();
        Vector result(cols);
        for (unsigned int col = 0; col < cols; ++col)
        {
            for (unsigned int row = 0; row < rows; ++row)
                result[col] += element[row][col];
            result[col] /= rows;
        }
        return result;
    }

    Vector Matrix::var(const int dim) const
    {
        if (dim == 1) return ma::transpose(*this).var();
        if (rows <= 1) return Vector(cols);
        Vector result(cols);
        // Calculate the expectations of the squares.
        for (unsigned int col = 0; col < cols; ++col)
        {
            for (unsigned int row = 0; row < rows; ++row)
                result[col] += element[row][col] * element[row][col];
            result[col] /= rows;
        }
        // Calculate the square of the expcetations.
        Vector means = mean();
        means *= means;
        // Variance is the expectation of the squares - square of the expectations.
        result -= means;
        // Ensure variances are not rounded to negative with small variances.
        for (unsigned int col = 0; col < cols; ++col)
            if (result[col] < 0) result[col] = 0;
        // Convert to variance of the sample.
        result *= 1. * rows / (rows - 1);

        return result;
    }

    Vector Matrix::sd(const int dim) const
    {
        if (dim == 1) return ma::transpose(*this).sd();
        Vector result = var();
        for (unsigned int col = 0; col < cols; ++col)
            result[col] = sqrt(result[col]);
        return result;
    }

    Vector Matrix::sem(const int dim) const
    {
        if (dim == 1) return ma::transpose(*this).sem();
        Vector result = sd();
        result /= sqrt(rows);
        return result;
    }

    Vector Matrix::rsem(const int dim) const
    {
        if (dim == 1) return ma::transpose(*this).rsem();
        Vector result = sem();
        Vector means = mean();
        result /= means * 100;
        return result;
    }

    void Matrix::print(std::ostream& os) const
    {
        unsigned int width = os.precision() + 3;
        for (unsigned int row = 0; row < rows; ++row)
        {
            if (rows == 1)
                os << "[";
            else if (row == 0)
                os << "┌";
            else if (row == rows -1)
                os << "└";
            else
                os << "│";
            for (unsigned int col = 0; col < cols; ++col)
            {
                os.width(width);
                os << element[row][col];
                if (col != cols-1)
                    os << ' ';
            }
            if (rows == 1)
                os << "]";
            else if (row == 0)
                os << "┐";
            else if (row == rows -1)
                os << "┘";
            else
                os << "│";
            os << std::endl;
        }
    }

    void Matrix::printDelimited(std::ostream& os, const char delimiter) const
    {
        for (unsigned int row = 0; row < rows; ++row)
        {
            for (unsigned int col = 0; col < cols; ++col)
            {
                os << element[row][col];
                if (col != cols-1)
                {
                    os << delimiter;
                }
            }
            os << std::endl;
        }
    }

    void Matrix::outputASCII(std::ostream& os) const
    {
        printDelimited(os, '\t');
    }

    void Matrix::outputCSV(std::ostream& os) const
    {
        printDelimited(os, ',');
    }

    bool Matrix::save(const char* filename) const
    {
        std::ofstream fileStream(filename);
        if (fileStream.is_open())
        {
            if (rightStr(filename, 4) == Str(".prn"))
                print(fileStream);
            else if (rightStr(filename, 4) == Str(".txt"))
                outputASCII(fileStream);
            else if (rightStr(filename, 4) == Str(".csv"))
                outputCSV(fileStream);
            else
                fileStream << *this;
            fileStream << std::endl;
            fileStream.close();
            return true;
        }
        else
        {
            return false;
        }
    }

    bool Matrix::loadPrint(std::istream& is)
    {
        List<Str> input;
        while (!is.eof())
        {
            Str line = getNext(is);
            line = line - "[" - "┌" - "└" - "│" - "]" - "┐" - "┘";
            input.append(line);
        }
        Matrix result(input);
        if (result.getRows() > 0 && result.getCols() > 0)
        {
            swap(*this, result);
            return true;
        }
        return false;
    }

    bool Matrix::loadASCII(std::istream& is)
    {
        List<Str> input;
        while (!is.eof())
        {
            Str line = getNext(is);
            input.append(line);
        }
        Matrix result(input);
        if (result.getRows() > 0 && result.getCols() > 0)
        {
            swap(*this, result);
            return true;
        }
        return false;
    }

    bool Matrix::loadCSV(std::istream& is)
    {
        List<Str> input;
        while (!is.eof())
        {
            Str line = getNext(is);
            line.replaceAll(",", "\t");
            input.append(line);
        }
        Matrix result(input);
        if (result.getRows() > 0 && result.getCols() > 0)
        {
            swap(*this, result);
            return true;
        }
        return false;
    }

    bool Matrix::loadStream(std::istream& is)
    {
        Matrix result;
        is >> result;
        if (is.good())
        {
            swap(*this, result);
            return true;
        }
        return false;
    }

    bool Matrix::load(const char* filename)
    {
        std::ifstream fileStream(filename, std::ifstream::in);
        if (fileStream.good())
        {
            if (rightStr(filename, 4) == Str(".prn"))
                return loadPrint(fileStream);
            else if (rightStr(filename, 4) == Str(".txt"))
                return loadASCII(fileStream);
            else if (rightStr(filename, 4) == Str(".csv"))
                return loadCSV(fileStream);
            else
                return loadStream(fileStream);
            fileStream.close();
        }
        return false;
    }

    Matrix operator+(Matrix left, const Matrix& right)
    {
        left += right;
        return left;
    }

    Matrix operator+(Matrix left, const Vector& right)
    {
        left += right;
        return left;
    }

    Matrix operator+(const Vector& left, Matrix right)
    {
        right += left;
        return right;
    }

    Matrix operator+(Matrix left, const double& right)
    {
        left += right;
        return left;
    }

    Matrix operator+(const double& left, Matrix right)
    {
        right += left;
        return right;
    }

    Matrix operator-(Matrix left, const Matrix& right)
    {
        left -= right;
        return left;
    }

    Matrix operator-(Matrix left, const Vector& right)
    {
        left -= right;
        return left;
    }

    Matrix operator-(const Vector& left, Matrix right)
    {
        right += left;
        return right;
    }

    Matrix operator-(Matrix left, const double& right)
    {
        left -= right;
        return left;
    }

    Matrix operator-(const double& left, Matrix right)
    {
        right += left;
        return right;
    }

    Matrix operator-(const Matrix& matrix)
    {
        Matrix result(matrix);
        #pragma omp parallel for
        for (unsigned int row = 0; row < result.getRows(); ++row)
        {
            for (unsigned int col = 0; col < result.getCols(); ++col)
            {
                result[row][col] = -matrix[row][col];
            }
        }
        return result;
    }

    Matrix operator*(const Matrix& left, const Matrix& right)
    {
        if (left.getCols() != right.getRows())
            throw("Arithmetic Exception: Matrix inner dimensions not equal");
        Matrix result(left.getRows(), right.getCols());
        #pragma omp parallel for
        for (unsigned int row = 0; row < result.getRows(); ++row)
        {
            for (unsigned int col = 0; col < result.getCols(); ++col)
            {
                double product = 0.0;
                for (unsigned int inner = 0; inner < left.getCols(); ++inner)
                {
                    product += left[row][inner] * right[inner][col];
                }
                result[row][col] = product;
            }
        }
        return result;
    }

    Vector operator*(const Matrix& left, const Vector& right)
    {
        if (left.getCols() != right.size())
            throw("Arithmetic Exception: Matrix and vector dimensions not equal");
        Vector result(left.getRows());
        #pragma omp parallel for
        for (unsigned int row = 0; row < left.getRows(); ++row)
        {
            double sum = 0.0;
            for (unsigned int col = 0; col < left.getCols(); ++col)
            {
                sum += left[row][col] * right[col];
            }
            result[row] = sum;
        }
        return result;
    }

    Matrix operator*(Matrix left, const double scalar)
    {
        left *= scalar;
        return left;
    }

    Matrix operator*(const double scalar, Matrix right)
    {
        right *= scalar;
        return right;
    }

    Matrix operator/(Matrix left, const double scalar)
    {
        left /= scalar;
        return left;
    }

    bool operator==(const Matrix& left, const Matrix& right)
    {
        unsigned int leftRows = left.getRows();
        unsigned int leftCols = left.getCols();

        if (leftRows != right.getRows() || leftCols != right.getCols())
            return false;

        bool same = true;
        #pragma omp parallel for
        for (unsigned int row = 0; row < leftRows; ++row)
            for (unsigned int col = 0; col < leftCols; ++col)
                if (left[row][col] != right[row][col])
                    same = false;
        return same;
    }

    bool operator!=(const Matrix& left, const Matrix& right)
    {
        return !(left == right);
    }

    bool approxEqual(const Matrix& left, const Matrix& right,
            const double error)
    {
        unsigned int leftRows = left.getRows();
        unsigned int leftCols = left.getCols();

        if (leftRows != right.getRows() || leftCols != right.getCols())
            return false;

        bool same = true;
        //#pragma omp parallel for
        for (unsigned int row = 0; row < left.getRows(); ++row)
            for (unsigned int col = 0; col < left.getCols(); ++col)
                if (left[row][col] != right[row][col])
        {
            if (!approxEqual(left[row][col], right[row][col], error))
            {
                same = false;
            }
        }
        return same;
    }

    std::istream& operator>>(std::istream& is, Matrix& destination)
    {
        unsigned int rows;
        unsigned int cols;
        is >> rows >> cols;
        if (is.good())
        {
            Matrix result(rows, cols);
            for (unsigned int row = 0; row < rows; ++row)
            {
                for (unsigned int col = 0; col < cols; ++col)
                {
                    is >> result[row][col];
                }
            }
            if (is.good())
            {
                swap(destination, result);
            }
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const Matrix& source)
    {
        unsigned int rows = source.getRows();
        unsigned int cols = source.getCols();
        os << rows << '\t' << cols << '\t';
        for (unsigned int row = 0; row < rows; ++row)
        {
            for (unsigned int col = 0; col < cols; ++col)
            {
                os << source[row][col];
                if (rows != rows-1 || col != cols-1)
                {
                    os << '\t';
                }
            }
        }
        return os;
    }

    Matrix outer(const Vector& left, const Vector& right)
    {
        unsigned int rows = left.size();
        unsigned int cols = right.size();
        Matrix result(rows, cols);
        #pragma omp parallel for
        for (unsigned int row = 0; row < rows; ++row)
        {
            for (unsigned int col = 0; col < cols; ++col)
            {
                result[row][col] = left[row] * right[col];
            }
        }
        return result;
    }

    Matrix zeros(const unsigned int rows, const unsigned int cols)
    {
        Matrix Z(rows, cols);
        Z.fillWith(0.0);
        return Z;
    }

    Matrix ones(const unsigned int rows, const unsigned int cols)
    {
        Matrix O(rows, cols);
        O.fillWith(1.0);
        return O;
    }

    Matrix identity(const unsigned int n)
    {
        Vector o = ones(n);
        Matrix I = diag(o);
        return I;
    }

    Matrix diag(const Vector d)
    {
        unsigned int length = d.size();
        Matrix D(length, length);
        D.fillWith(0.0);
        for (unsigned int i = 0; i < length; ++i)
            D[i][i] = d[i];
        return D;
    }

    Matrix householder(const Matrix& source)
    {
        unsigned int rows = source.getRows();
        unsigned int cols = source.getCols();
        // Initialise the Householder and reduced matrices.
        Matrix H = identity(rows);
        Matrix R(source);

        // For each sub-matrix we find the Householder matrix Hi = I - 2vv^T
        // Where for each first column a, Ha = b = [b, 0, 0, ...]
        // is the reflection about the plane normal to v.
        // fv = a - b
        // v = [a1 - b, a2, a3...]
        // H = I - 2vv^T
        double* a = new double[rows];
        for (unsigned int row = 0; row < rows && row < cols; ++row)
        {
            // Extract the next column of the reduced matrix.
            R.getCol(row, a);
            // Calculate the length of the sub-matrix' first column vector.
            double length = 0;
            for (unsigned int i = row; i < rows; ++i)
                length += a[i] * a[i];
            length = sqrt(length);
            // Determine the value of the reflected vectors only value.
            // The selection of the sign prevents f being small or = 0.
            double b = a[row] > 0 ? -length : length;
            // Calculate the length of the (a - b) vector.
            double f = sqrt(-2 * (a[row] - b) * b);
            // Create the reflection hyperplane's normal vector: (a - b)/f.
            double* v = new double[rows - row];
            v[0] = (a[row] - b) / f;
            for (unsigned int i = 1; i < rows - row; ++i)
            {
                v[i] = a[row + i] / f;
            }
            // Create the householder sub-matrix from the hyperplane vector.
            // H = I -2vv^T
            Matrix Hi = identity(rows);
            for (unsigned int i = 0; i < rows - row; ++i)
                for (unsigned int j = 0; j < rows - row; ++j)
                    Hi[row + i][row + j] -= 2*v[i]*v[j];

            // Merge the Householder matrices.
            H = Hi * H;
            // Update the reduced matrix.
            R = Hi * R;

            // Clean up.
            delete[] v;
        }
        // Clean up.
        delete[] a;

        return H;
    }

    Matrix horizontalMerge(Matrix left, const Matrix& right)
    {
        left.horizontalMerge(right);
        return left;
    }

    Matrix verticalMerge(Matrix top, const Matrix& bottom)
    {
        top.verticalMerge(bottom);
        return top;
    }

    Matrix reshape(Matrix source, const unsigned int rows,
        const unsigned int cols)
    {
        source.reshape(rows, cols);
        return source;
    }

    Matrix transpose(Matrix source)
    {
        source.transpose();
        return source;
    }

    Matrix inverse(Matrix source)
    {
        source.inverse();
        return source;
    }

    Vector min(const Matrix& source, const int dim)
    {
        return source.min(dim);
    }

    Vector minIndex(const Matrix& source, const int dim)
    {
        return source.minIndex(dim);
    }

    Vector max(const Matrix& source, const int dim)
    {
        return source.max(dim);
    }

    Vector maxIndex(const Matrix& source, const int dim)
    {
        return source.maxIndex(dim);
    }

    Vector mean(const Matrix source, const int dim)
    {
        return source.max(dim);
    }

    Matrix var(const Matrix source, const int dim)
    {
        return source.var(dim);
    }

    Matrix sd(const Matrix source, const int dim)
    {
        return source.sd(dim);
    }

    Matrix sem(const Matrix source, const int dim)
    {
        return source.sem(dim);
    }

    Matrix rsem(const Matrix source, const int dim)
    {
        return source.rsem(dim);
    }

    void print(const Matrix& source, std::ostream& os)
    {
        source.print(os);
    }

    void printDelimited(const Matrix& source, std::ostream& os,
            const char delimiter)
    {
        source.printDelimited(os, delimiter);
    }

    void outputASCII(const Matrix& source, std::ostream& os)
    {
        source.outputASCII(os);
    }

    void outputCSV(const Matrix& source, std::ostream& os)
    {
        source.outputCSV(os);
    }

    bool save(const Matrix& source, const char* filename)
    {
        return source.save(filename);
    }

    bool loadPrint(Matrix& destination, std::istream& is)
    {
        return destination.loadPrint(is);
    }

    bool loadASCII(Matrix& destination, std::istream& is)
    {
        return destination.loadASCII(is);
    }

    bool loadCSV(Matrix& destination, std::istream& is)
    {
        return destination.loadCSV(is);
    }

    bool loadStream(Matrix& destination, std::istream& is)
    {
        return destination.loadStream(is);
    }

    bool load(Matrix& destination, const char* filename)
    {
        return destination.load(filename);
    }
}
