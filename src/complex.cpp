// (c) 2011 - 2017: Marcel Admiraal

#include "complex.h"

#include "utils.h"

#define M_PI 3.14159265358979323846264338327
#include <cmath>
#include <cfloat>

namespace ma
{
    Complex::Complex() :
        realnum(0), imagnum(0)
    {
    }

    Complex::Complex(double realnum) :
        realnum(realnum), imagnum(0)
    {
    }

    Complex::Complex(double realnum, double imagnum) :
        realnum(realnum), imagnum(imagnum)
    {
    }

    Complex::Complex(const Complex& source) :
        realnum(source.realnum), imagnum(source.imagnum)
    {
    }

    Complex::~Complex()
    {
    }

    void swap(Complex& first, Complex& second)
    {
        std::swap(first.realnum, second.realnum);
        std::swap(first.imagnum, second.imagnum);
    }

    Complex& Complex::operator=(Complex source)
    {
        swap(*this, source);
        return *this;
    }

    double Complex::real() const
    {
        return realnum;
    }

    bool Complex::isReal(double threshold) const
    {
        double upper = threshold * DBL_EPSILON;
        double lower = -upper;
        return imagnum < upper && imagnum > lower;
    }

    void Complex::setReal(double realnum)
    {
        this->realnum = realnum;
    }

    double Complex::imag() const
    {
        return imagnum;
    }

    void Complex::setImaginary(double imagnum)
    {
        this->imagnum = imagnum;
    }

    double Complex::abs() const
    {
        return sqrt(realnum * realnum + imagnum * imagnum);
    }

    double Complex::arg() const
    {
        double arg;
        if (realnum > 0)
            arg = atan(imagnum / realnum);
        else if (realnum < 0)
            arg = atan(imagnum / realnum) + (imagnum >= 0 ? M_PI : -M_PI);
        else if (imagnum == 0)
            // Technically this should be indeterminate,
            // but if real and imag are zero then...
            arg = 0;
        else
            arg = imagnum > 0 ? (M_PI / 2) : (-M_PI / 2);
        return arg;
    }

    Complex& Complex::operator+=(const Complex& right)
    {
        realnum += right.realnum;
        imagnum += right.imagnum;
        return *this;
    }

    Complex& Complex::operator+=(const double right)
    {
        realnum += right;
        return *this;
    }

    Complex& Complex::operator-=(const Complex& right)
    {
        realnum -= right.realnum;
        imagnum -= right.imagnum;
        return *this;
    }

    Complex& Complex::operator-=(const double right)
    {
        realnum -= right;
        return *this;
    }

    Complex& Complex::operator*=(const Complex& right)
    {
        double currentReal = realnum;
        realnum = realnum * right.realnum - imagnum * right.imagnum;
        imagnum = currentReal * right.imagnum + right.realnum * imagnum;
        return *this;
    }

    Complex& Complex::operator*=(const double right)
    {
        realnum *= right;
        imagnum *= right;
        return *this;
    }

    Complex& Complex::operator/=(const Complex& right)
    {
        double newAbs = abs() / right.abs();
        double newArg = arg() - right.arg();
        realnum = newAbs * cos(newArg);
        imagnum = newAbs * sin(newArg);
        return *this;
    }

    Complex& Complex::operator/=(const double right)
    {
        realnum /= right;
        imagnum /= right;
        return *this;
    }

    Complex Complex::conj() const
    {
        Complex result(realnum, -imagnum);
        return result;
    }

    Complex Complex::exp() const
    {
        double size = std::exp(realnum);
        double newReal = size * cos(imagnum);
        double newImaginary = size * sin(imagnum);
        Complex result(newReal, newImaginary);
        return result;
    }

    Complex Complex::ln() const
    {
        Complex result(log(abs()), arg());
        return result;
    }

    Complex Complex::pow(const int n) const
    {
        double powAbs = std::pow(abs(), n);
        double powArg = arg() * n;
        Complex result(powAbs * cos(powArg), powAbs * sin(powArg));
        return result;
    }

    Complex Complex::pow(const Complex& n) const
    {
        return (n * ln()).exp();
    }

    void Complex::root(const int n, Complex* roots) const
    {
        double rootAbs = std::pow(abs(), 1.0 / n);
        for (int k = 0; k < n; k++)
        {
            double rootArg = (arg() + 2 * M_PI * k) / n;
            roots[k].realnum = rootAbs * cos(rootArg);
            roots[k].imagnum = rootAbs * sin(rootArg);
        }
    }

    Complex operator+(Complex left, const Complex& right)
    {
        return left += right;
    }

    Complex operator+(Complex left, const double right)
    {
        return left += right;
    }

    Complex operator+(const double left, const Complex& right)
    {
        Complex result(left + right.real(), right.imag());
        return result;
    }

    Complex operator-(Complex left, const Complex& right)
    {
        return left -= right;
    }

    Complex operator-(const Complex& source)
    {
        Complex result(-source.real(), -source.imag());
        return result;
    }

    Complex operator-(Complex left, const double right)
    {
        return left -= right;
    }

    Complex operator-(const double left, const Complex& right)
    {
        return left + (-right);
    }

    Complex operator*(Complex left, const Complex& right)
    {
        return left *= right;
    }

    Complex operator*(Complex left, const double right)
    {
        return left *= right;
    }

    Complex operator*(const double left, const Complex& right)
    {
        Complex result(left * right.real(), left * right.imag());
        return result;
    }

    Complex operator/(Complex left, const Complex& right)
    {
        return left /= right;
    }

    Complex operator/(Complex left, const double right)
    {
        return left /= right;
    }

    Complex operator/(const double left, const Complex& right)
    {
        double abs = left / right.abs();
        double arg = - right.arg();
        Complex result(abs * cos(arg), abs * sin(arg));
        return result;
    }

    bool operator==(const Complex& left, const Complex& right)
    {
        return (left.real() == right.real() &&
                left.imag() == right.imag());
    }

    bool operator!=(const Complex& left, const Complex& right)
    {
        return !(left == right);
    }

    bool operator<(const Complex& left, const Complex& right)
    {
        double leftAbs = left.abs();
        double rightAbs = right.abs();
        if (leftAbs != rightAbs) return leftAbs < rightAbs;
        return left.arg() < right.arg();
    }

    bool operator<=(const Complex& left, const Complex& right)
    {
        return left == right || left < right;
    }

    bool operator>(const Complex& left, const Complex& right)
    {
        return !(left <= right);
    }

    bool operator>=(const Complex& left, const Complex& right)
    {
        return !(left < right);
    }

    bool approxEqual(const Complex& left, const Complex& right,
            const double error)
    {
        return (approxEqual(left.real(), right.real(), error) &&
                approxEqual(left.imag(), right.imag(), error));
    }

    std::ostream& operator<<(std::ostream& os, const Complex& source)
    {
        os << source.real();
        os << ' ';
        if (source.imag() >= 0) os << "+ ";
        os << source.imag();
        os << 'i';
        return os;
    }

    std::istream& operator>>(std::istream& is, Complex& destination)
    {
        double realnum, imagnum;
        char c;
        is >> realnum;
        if (!is.good()) return is;
        is >> c;
        if (c != '+' && c != '-')
        {
            is.setstate(std::ios::failbit);
            return is;
        }
        is >> imagnum;
        if (!is.good()) return is;
        if (c == '-') imagnum = -imagnum;
        is >> c;
        if (c != 'i')
        {
            is.setstate(std::ios::failbit);
            return is;
        }
        destination.setReal(realnum);
        destination.setImaginary(imagnum);
        return is;
    }

    Complex conj(const Complex& source)
    {
        return source.conj();
    }

    Complex exp(const Complex& power)
    {
        return power.exp();
    }

    Complex ln(const Complex& root)
    {
        return root.ln();
    }

    Complex pow(const Complex& base, const int power)
    {
        return base.pow(power);
    }

    Complex pow(const Complex& base, const Complex& power)
    {
        return base.pow(power);
    }

    void root(const Complex& base, const int n, Complex* roots)
    {
        base.root(n, roots);
    }
}
