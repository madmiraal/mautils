// (c) 2013 - 2016 Marcel Admiraal

#include "serial.h"

#if defined(__unix) || defined(__APPLE__)
#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#endif // __unix || __APPLE__

namespace ma
{
    Serial::Serial() :
        connected(false)
    {
    }

    Serial::~Serial()
    {
        disconnect();
    }

    Str Serial::getPortString() const
    {
        return portNameString;
    }

    bool Serial::setPort(const Str& portName)
    {
        if (connected)
        {
            disconnect();
        }
        // Try to connect to the given port
        #ifdef _WIN32
            Str longPortName = "\\\\.\\" + portName;
            hSerial = CreateFile(
                longPortName.c_str(),           // Port name "\\\\.\\COM<number>"
                GENERIC_READ | GENERIC_WRITE,	// Read and write access
                0,								// COM cannot be shared
                NULL,							// No security
                OPEN_EXISTING,					// COM uses existing "file"
                FILE_ATTRIBUTE_NORMAL,			// Synchronous access only
                NULL							// COM has no template file
            );
            connected = (hSerial != INVALID_HANDLE_VALUE);
        #endif // _WIN32
        #if defined(__unix) || defined(__APPLE__)
            fileDescriptor = open(
                portName.c_str(),
                //O_RDONLY            // Open for reading only
                //O_WRONLY            // Open for writing only
                O_RDWR              // Open for reading and writing
                //| O_APPEND          // Write operations apend to the end of the file
                //| O_CREAT           // Create the file if it doesn't exist
                //| O_EXCL            // With O_CREAT: open fails if the file exists.
                //| O_TRUNC           // If file exists and opened for write access truncate it to zero length
                | O_NOCTTY          // If a terminal device do not make it the process' controlling terminal
                //| O_NONBLOCK        // Prevents open from blocking until the file is ready - the DCD (Data Carrier Detect) line signal - returns immediately
                //| O_NDELAY          // Same as O_NONBLOCK: O_NONBLOCK takes precedence.
                // O_NONBLOCK and O_NDELAY impact later read and write:
                // Reading or writing a file with a write lock returns -1 and sets errno to EAGAIN.
                // Reading or writing a pipe or terminal with a write lock O_NONBLOCK read returns -1 and sets errno to EAGAIN O_NDELAY read returns 0.
                | O_SYNC            // Writes will wait for both the file data and file status to be physically updated.
            );
            connected = (fileDescriptor >= 0);
        #endif // __unix || __APPLE__
        if (connected)
        {
            portNameString = portName;
        }
        return connected;
    }

    bool Serial::setParameters(const int baud, const int bits, const int parity,
             const int stopBits, const bool block)
    {
        bool success = connected;
        // If connected we try to set the port parameters
        if (success)
        {
            #ifdef _WIN32
                COMMTIMEOUTS timeOuts = {0};
                // If the read timeouts are set to block then Get can hang!
                //success &= GetCommTimeouts(hSerial, &timeOuts) != 0;
                if (success)
                {
                    // Define serial read timeout
                    timeOuts.ReadIntervalTimeout = (block ? MAXDWORD : 0);
                    timeOuts.ReadTotalTimeoutMultiplier = (block ? 0 : 10);
                    timeOuts.ReadTotalTimeoutConstant = (block ? 0 : 100);
                    timeOuts.WriteTotalTimeoutMultiplier = 10;
                    timeOuts.WriteTotalTimeoutConstant = 100;
                    // Set the parameters and check for their proper application
                    success = SetCommTimeouts(hSerial, &timeOuts) != 0;
                }
                DCB dcbSerialParams = {0};
                if (success)
                {
                    // Try to get the current parameters
                    success &= GetCommState(hSerial, &dcbSerialParams) != 0;
                }
                if (success)
                {
                    // Define serial connection parameters
                    dcbSerialParams.BaudRate = baud;
                    dcbSerialParams.ByteSize = bits;
                    dcbSerialParams.StopBits = stopBits;
                    dcbSerialParams.Parity = parity;
                    // Set the parameters and check for their proper application
                    success = SetCommState(hSerial, &dcbSerialParams) != 0;
                }
            #endif // _WIN32
            #if defined(__unix) || defined(__APPLE__)
                struct termios tty = {0};
                success &= (tcgetattr(fileDescriptor, &tty) == 0);
                if (success)
                {
                    // Determine the port speed
                    int speed = 0;
                    if (baud < 50) speed = B0;
                    else if (baud < 75) speed = B50;
                    else if (baud < 110) speed = B75;
                    else if (baud < 134) speed = B110;
                    else if (baud < 150) speed = B134;
                    else if (baud < 200) speed = B150;
                    else if (baud < 300) speed = B200;
                    else if (baud < 600) speed = B300;
                    else if (baud < 1200) speed = B600;
                    else if (baud < 1800) speed = B1200;
                    else if (baud < 2400) speed = B1800;
                    else if (baud < 4800) speed = B2400;
                    else if (baud < 9600) speed = B4800;
                    else if (baud < 19200) speed = B9600;
                    else if (baud < 38400) speed = B19200;
                    else if (baud < 57600) speed = B38400;
                    else if (baud < 115200) speed = B57600;
                    else if (baud < 230400) speed = B115200;
                    #ifdef __APPLE__
                        else speed = B230400;
                    #else // __unix
                        else if (baud < 460800) speed = B230400;
                        else if (baud < 500000) speed = B460800;
                        else if (baud < 576000) speed = B500000;
                        else if (baud < 921600) speed = B576000;
                        else if (baud < 1000000) speed = B921600;
                        else if (baud < 1152000) speed = B1000000;
                        else if (baud < 1500000) speed = B1152000;
                        else if (baud < 2000000) speed = B1500000;
                        else if (baud < 2500000) speed = B2000000;
                        else if (baud < 3000000) speed = B2500000;
                        else if (baud < 3500000) speed = B3000000;
                        else if (baud < 4000000) speed = B3500000;
                        else speed = B4000000;
                    #endif
                    // Set baud rate
                    success &= (cfsetospeed (&tty, speed) == 0);
                    success &= (cfsetispeed (&tty, speed) == 0);

                    // Set control options: number of data bits, parity, stop bits,
                    // and hardware flow control.

                    // Set the number of data bits.
                    // Note the CSIZE mask for data bit setting as multiple bits are used.
                    if (bits == 5) tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS5;
                    else if (bits == 6) tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS6;
                    else if (bits == 7) tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS7;
                    else tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;

                    // Set the parity
                    // No parity
                    if (parity == 0) tty.c_cflag &= ~(PARENB | PARODD);
                    // Odd parity
                    if (parity == 1) tty.c_cflag |= (PARENB | PARODD);
                    // Even parity
                    else
                    {
                        tty.c_cflag |= PARENB;
                        tty.c_cflag &= ~PARODD;
                    }

                    // Set the number of stop bits
                    if (stopBits == 2) tty.c_cflag |= CSTOPB;
                    else tty.c_cflag &= ~CSTOPB;

                    // Enable receiver and set local mode.
                    // CLOCAL: Local line - do not change owner of the port.
                    // CREAD: Enable the receiver.
                    tty.c_cflag |= (CLOCAL | CREAD);
                    // Disable hardware flow control.
                    #ifdef CRTSCTS
                    tty.c_cflag &= ~CRTSCTS;
                    #endif

                    // Set local options:
                    // ICANON: Canonical (line orientated) input used by terminals.
                    // ECHO: Enable echoing of input characters.
                    // ECHOE: Echo erase character as BS-SP-BS
                    // ISIG: Enable SIGINTR, SIGSUSP, SIGDSUSP, and SIGQUIT signals.
                    // Note: Never enable input echo (ECHO, ECHOE) when sending commands to a
                    //       MODEM or other computer that is echoing characters, as you will
                    //       generate a feedback loop between the two serial interfaces!
                    // Canonical mode:
                    //tty.c_lflag |= (ICANON | ECHO | ECHOE);
                    // Raw mode:
                    tty.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

                    // Set input options
                    // INPCK: Enable parity check.
                    // ISTRIP: Strip parity bits.
                    // IGNBRK: Ignore break condition.
                    // IXON: Outgoing software flow control
                    // IXOFF: Incoming software flow control
                    // IXANY: Allow any character to start flow again.
                    // If using parity enable parity checking.
                    //tty.c_iflag |= (INPCK | ISTRIP);
                    // Do not ignore break condition.
                    tty.c_iflag &= ~IGNBRK;
                    // Disable software flow control:
                    tty.c_iflag &= ~(IXON | IXOFF | IXANY);

                    // Set output options
                    // OPOST: Post process the output.
                    // Note: Of all the different options, you will only probably use the ONLCR
                    //       option which maps newlines into CR-LF pairs. The rest of the output
                    //       options are primarily historic. When the OPOST option is disabled,
                    //       (in RAW mode) all other option bits in c_oflag are ignored.
                    // Raw mode: Disable post processing the output.
                    tty.c_oflag &= ~OPOST;

                    // Set control characters
                    // VMIN: Minimum number of characters to read.
                    // VTIME: Time to wait for data in 1/10 seconds.
                    // Set whether to block on read
                    tty.c_cc[VMIN] = block ? 1 : 0;
                    // Reading timeout between characters in number of 0.1 seconds
                    tty.c_cc[VTIME] = 5;

                    // Apply the changes now (without waiting for data to complete)
                    success &= (tcsetattr(fileDescriptor, TCSANOW, &tty) == 0);

                    // Flush the input buffer
                    tcflush(fileDescriptor, TCIFLUSH);
                }
            #endif // __unix || __APPLE__
        }
        return success;
    }

    unsigned long Serial::writeData(
        const unsigned char *buffer, const unsigned int bufferLength)
    {
        unsigned long bytesSent = 0;
        if (connected)
        {
            #ifdef _WIN32
                WriteFile(hSerial, (void *)buffer, bufferLength, &bytesSent, NULL);
            #endif // _WIN32
            #if defined(__unix) || defined(__APPLE__)
                bytesSent = write(fileDescriptor, buffer , bufferLength);
            #endif // __unix || __APPLE__
        }
        return bytesSent;
    }

    unsigned long Serial::readData(
        unsigned char *buffer, const unsigned int bufferLength)
    {
        unsigned long bytesRead = 0;
        if (connected)
        {
            unsigned int bytesToRead = bufferLength;
            #ifdef _WIN32
                ReadFile(hSerial, buffer, bytesToRead, &bytesRead, NULL);
            #endif // _WIN32
            #if defined(__unix) || defined(__APPLE__)
                bytesRead = read(fileDescriptor, buffer, bytesToRead);
            #endif // __unix || __APPLE__
            // If bytes read less than required, zero the remainder of the buffer.
            for (unsigned int byte = bytesRead; byte < bufferLength; byte++)
                buffer[byte] = 0x0;
        }
        return bytesRead;
    }

    void Serial::clearWriteBuffer()
    {
        #ifdef _WIN32
            PurgeComm(hSerial, PURGE_TXCLEAR);
        #endif // _WIN32
        #if defined(__unix) || defined(__APPLE__)
            tcflush(fileDescriptor, TCOFLUSH);
        #endif // __unix || __APPLE__
    }

    void Serial::clearReadBuffer()
    {
        #ifdef _WIN32
            PurgeComm(hSerial, PURGE_RXCLEAR);
        #endif // _WIN32
        #if defined(__unix) || defined(__APPLE__)
            tcflush(fileDescriptor, TCIFLUSH);
        #endif // __unix || __APPLE__
    }

    bool Serial::isConnected() const
    {
        // Simply return the connection status
        return connected;
    }

    void Serial::disconnect()
    {
        // Check if we are connected before trying to disconnect
        if(connected)
        {
            #ifdef _WIN32
                CloseHandle(hSerial);
            #endif // _WIN32
            #if defined(__unix) || defined(__APPLE__)
                close(fileDescriptor);
            #endif // __unix || __APPLE__
            connected = false;
            portNameString = Str();
        }
    }
}
