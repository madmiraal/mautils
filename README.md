# MAUtils #

Cross-platform C++ code for simple utilities:

## String Utilities ##
* ma::Str: A class for manipulating character arrays as strings.
* ma::StrStream: A class for streaming to and from the Str class strings.

## Maths Utilities ##
* ma::Complex: A class for creating and manipulating complex numbers.
* ma::Vector: A class for creating and manipulating vectors.
* ma::Vector3D: A class for creating and manipulating three-dimensional vectors.
* ma::VectorComplex: A class for creating and manipulating vectors of complex numbers.
* ma::Quaternion: A class for creating and manipulating quaternions.
* ma::Matrix: A class for creating and manipulating matrices.

## Communication Utilities ##
* ma::Serial: A cross-platform class for connecting to, reading from and writing to serial ports.
* ma::Device: Enables working with input devices used by a Linux system.
* ma::Pointer: Cross-platform class for programatical controling the pointer.

## Collection Utilities ##
* ma::LinkedNode: A template class for creating a node in a linked list.
* ma::List: A template class that can be used as a variable length array.
* ma::Stack: A template class for creating a stack of objects.
* ma::Collection: A template class for bundling objects of a particular class together.
* ma::Map: A template class for storing key-value pairs.
* ma::Iterator: A template class for iterating though objects in a ma::Collection or a linked list of ma::LinkedNode.

## Optimisation Utilities ##
* ma::Learner: A base abstract class for creating temporal learning algorithms.
* ma::MCMCLearner: A Markov Chain Monte Carlo temporal learning algorithm.
### CGDescent ###
* ma::minimise: Returns the minimum of a continuous differentiable multivariate function.

## Logging Utilities ##
* ma::Logger: Logs data to a log file along with a time stamp.
* ma::LogPlayback: Replays files created by ma::Logger.
* ma::TemporalData: Keeps a record of data values over a period of historic time.

## Other Utilities ##
### Utils ###
* ma::approxEqual: Enables comparison of doubles with rounding errors.
* ma::displayPacket: Enables viewing a array of bytes in various output formats.
* ma::intSqrt: A quick integer approximation of the square root of a integer without using division or a squareroot fuction.
* ma::invSqrt: A quick approximation of the inverse square root of a float without using division or a squareroot fuction.
* ma::renameFile: Renames a filename by adding or incrementing the version number.
### Algorithms ###
* ma::AHRS: Attitude and heading reference system quaternion calculations and corrections from gyroscopse, accelerometer and magnetometer readings.
* ma::fft: Cooley-Tukey Fast Fourier Transform, in-place, breadth-first, decimation-in-frequency.
* ma::mergeSort: Sorts an array of items that have the < operator defined using the merge sort algorithm.
* ma::quickSort: Sorts an array of items that have the < operator defined using the quick sort algorithm.
* ma::quickSelect: Returns the ith smallest element using the quick select algortihm.
* ma::inversions: Returns the number of inversions in an array.
* Compile-time series calculations for sine and cosine functions.
