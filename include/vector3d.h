// A three dimensional vector class
//
// (c) 2011-17: Marcel Admiraal

#ifndef VECTOR3D_H
#define VECTOR3D_H

#include "vector.h"

#include <string>
#include <iostream>

namespace ma
{
    class Vector3D : public Vector
    {
    public:
        /**
         * Default constructor.
         * Creates a zero vector.
         */
        Vector3D();

        /**
         * Doubles constructor.
         *
         * @param x The x dimension of this vector.
         * @param y The y dimension of this vector.
         * @param z The z dimension of this vector.
         */
        Vector3D(const double x, const double y, const double z);

        /**
         * Array of integers constructor.
         *
         * @param element   The integer array of three elements.
         */
        Vector3D(const int* element);

        /**
         * Array of unsigned integers constructor.
         *
         * @param element   The unsigned integer array of three elements.
         */
        Vector3D(const unsigned int* element);

        /**
         * Array of shorts constructor.
         *
         * @param element   The short array of three elements.
         */
        Vector3D(const short* element);

        /**
         * Array of floats constructor.
         *
         * @param element   The float array of three elements.
         */
        Vector3D(const float* element);

        /**
         * Array of doubles constructor.
         *
         * @param element   The double array of three elements.
         */
        Vector3D(const double* element);

        /**
         * Copy constructor.
         *
         * @param source The vector to copy.
         */
        Vector3D(const Vector3D& source);

        /**
         * Default destructor.
         */
        virtual ~Vector3D();

        /**
         * Swap function.
         *
         * @param first  The first vector to swap with.
         * @param second The second vector to swap with.
         */
        friend void swap(Vector3D& first, Vector3D& second);

        /**
         * Assignment operator.
         * Returns a copy of the vector.
         *
         * @param source The vector to copy.
         * @return       A copy of the vector.
         */
        Vector3D& operator=(Vector3D source);

        /**
         * Returns the x dimension of this 3D vector.
         *
         * @return The x dimension of this 3D vector.
         */
        double getX() const;

        /**
         * Returns the y dimension of this 3D vector.
         *
         * @return The y dimension of this 3D vector.
         */
        double getY() const;

        /**
         * Returns the z dimension of this 3D vector.
         *
         * @return The z dimension of this 3D vector.
         */
        double getZ() const;

        /**
         * Sets the x dimension of this 3D vector.
         *
         * @param The x dimension of this 3D vector.
         */
        void setX(const double x);

        /**
         * Sets the y dimension of this 3D vector.
         *
         * @param The y dimension of this 3D vector.
         */
        void setY(const double y);

        /**
         * Sets the z dimension of this 3D vector.
         *
         * @param The z dimension of this 3D vector.
         */
        void setZ(const double z);

        /**
         * Returns the unit vector associated with this vector.
         *
         * @return The unit vector associated with this vector.
         */
        Vector3D unitVector() const;

        /**
         * Returns the unit vector associated with this vector using a
         * quick approximation of the inverse square root.
         *
         * @return The unit vector associated with this vector.
         */
        Vector3D fastUnitVector() const;

        /**
         * Addition equals operator.
         * Adds the vector to this vector and returns the sum.
         *
         * @param right The vector to add to this vector.
         * @return      The vector sum of the two vectors.
         */
        Vector3D& operator+=(const Vector3D& right);

        /**
         * Subtraction equals operator.
         * Subtracts the vector from this vector and returns the difference.
         *
         * @param right The vector to subtract.
         * @return      The vector difference between the two vectors.
         */
        Vector3D& operator-=(const Vector3D& right);

        /**
         * Multiplication equals operator.
         * Element multiplication of this vector with the right vector, and
         * returns the products.
         *
         * @param right The vector to element multiply this vector by.
         * @return      The element products of the two vectors.
         */
        Vector3D& operator*=(const Vector3D& right);

        /**
         * Multiplication equals double operator.
         * Scales this vector by the scalar and returns the scaled vector.
         *
         * @param right The scalar to scale the vector by.
         * @return      The vector scaled by the scalar.
         */
        Vector3D& operator*=(const double right);

        /**
         * Division equals operator.
         * Element division of this vector by the right vector, and
         * returns the quotients.
         *
         * @param right The vector to element divide this vector by.
         * @return      The quotients of the two vectors.
         */
        Vector3D& operator/=(const Vector3D& right);

        /**
         * Division double operator.
         * Reduces this vector by the scalar and returns the reduced vector.
         *
         * @param right The scalar to reduce the vector by.
         * @return      The vector reduced by the scalar.
         */
        Vector3D& operator/=(const double right);

        /**
         * Returns the cross product of this vector
         * multiplied by another using the right-hand coordinate system.
         *
         * @param right The other vector to multiply this vector by.
         * @return      The cross product of the two vectors.
         */
        Vector3D cross(const Vector3D& right) const;
    };

    /**
     * Returns the unit vector associated with this vector.
     *
     * @param source    The vector to obtain the unit vector from.
     * @return          The unit vector associated with this vector.
     */
    Vector3D unitVector(Vector3D source);

    /**
     * Returns the unit vector associated with this vector using a
     * quick approximation of the inverse square root.
     *
     * @param source    The vector to obtain the unit vector from.
     * @return          The unit vector associated with this vector.
     */
    Vector3D fastUnitVector(Vector3D source);

    /**
     * Addition operator.
     * Returns the vector sum of the two vectors.
     *
     * @param  left  The first vector to add.
     * @param  right The second vector to add.
     * @return       The vector sum of the two vectors.
     */
    Vector3D operator+(Vector3D left, const Vector3D& right);

    /**
     * Subtraction operator.
     * Returns the vector difference between the two vectors.
     *
     * @param  left  The vector to subtract from.
     * @param  right The vector to subtract.
     * @return       The vector difference between the two vectors.
     */
    Vector3D operator-(Vector3D left, const Vector3D& right);

    /**
     * Negative operator.
     * Returns the negative vector.
     *
     * @param  source The vector to form the negative of.
     * @return        The negative vector.
     */
    Vector3D operator-(const Vector3D& source);

    /**
     * Multiplication operator.
     * Returns the element product of the two vectors.
     *
     * @param left  The first vector to multiply.
     * @param right The second vector to multiply.
     * @return      The element product of the two vectors.
     */
    Vector3D operator*(Vector3D left, const Vector3D& right);

    /**
     * Multiplication double right operator.
     * Returns the vector scaled by the scalar.
     *
     * @param  left  The vector to scale.
     * @param  right The scalar to scale the vector by.
     * @return       The vector scaled by the scalar.
     */
    Vector3D operator*(Vector3D left, const double right);

    /**
     * Multiplication double left operator.
     * Returns the vector scaled by the scalar.
     *
     * @param  left  The scalar to scale the vector by.
     * @param  right The vector to scale.
     * @return       The vector scaled by the scalar.
     */
    Vector3D operator*(const double left, Vector3D right);

    /**
     * Division operator.
     * Returns the element quotient of the two vectors.
     *
     * @param left  The vector to divide.
     * @param right The vector to divide by.
     * @return      The element quotient of the two vectors.
     */
    Vector3D operator/(Vector3D left, const Vector3D& right);

    /**
     * Division double right operator.
     * Returns the vector reduced by the scalar.
     *
     * @param  left  The vector to reduce.
     * @param  right The scalar to reduce the vector by.
     * @return       The vector reduced by the scalar.
     */
    Vector3D operator/(Vector3D left, const double right);

    /**
     * Division double left operator.
     * Returns the element inverse of the vector multiplied by the scalar.
     *
     * @param left  The scalar to multiply the vector by.
     * @param right The vector to invert.
     * @return      The element inverse of the vector multiplied by the scalar.
     */
    Vector3D operator/(const double left, const Vector3D& right);

    /**
     * Returns the vector that is the mid-point between two vectors.
     *
     * @param left  The first vector.
     * @param right The second vector.
     * @return      The vector that is the mid-point between the two vectors.
     */
    Vector3D midPoint(const Vector3D& a, const Vector3D& b);
}

#endif // VECTOR3D_H
