// Logs data to a log file along with a time stamp.
//
// (c) 2016: Marcel Admiraal

#ifndef LOGGER_H
#define LOGGER_H

#include "str.h"

#include <fstream>
#include <chrono>
#include <mutex>

namespace ma
{
    class Logger
    {
    public:
        /**
         * Default constructor.
         */
        Logger();

        /**
         * Default destructor.
         */
        virtual ~Logger();

        /**
         * Returns the current log file name.
         *
         * @return The current log file name.
         */
        Str getLogFilename() const;

        /**
         * Sets the current log file's file name.
         *
         * @param filename  The new log file name.
         */
        void setLogFilename(const Str& filename);

        /**
         * Starts logging, if the log file can be opened.
         * If the log file exists, the file is renamed and a new log file
         * is created.
         */
        void startLogging();

        /**
         * Pauses logging, if logging has been started.
         */
        void pauseLogging();

        /**
         * Unpauses logging, if logging was previously paused.
         */
        void unPauseLogging();

        /**
         * Stops logging.
         *
         * @param time      The time of the log entry. Default ms since start.
         */
        void stopLogging(unsigned int time = -1);

        /**
         * Returns whether or not the logger is logging.
         *
         * @return Whether or not the logger is logging.
         */
        bool isLogging() const;

        /**
         * Returns whether or not the logger is paused.
         *
         * @return Whether or not the logger is paused.
         */
        bool isPaused() const;

        /**
         * If logging, adds the log line to the log file.
         *
         * @param logLine   The line to add to the log file.
         * @param time      The time of the log entry. Default ms since start.
         */
        void log(const Str& logLine, unsigned int time = -1);

    private:
        unsigned int timeStamp();

        Str filename;
        std::fstream logFile;
        bool logging;
        bool paused;
        std::chrono::milliseconds start;
        std::chrono::milliseconds pauseStart;
        std::mutex logAccess;
    };
}

#endif // LOGGER_H
