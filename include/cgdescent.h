// Minimizes a continuous differentialble multivariate function.
//
// The Polack-Ribiere flavour of conjugate gradients is used to compute
// search directions, and a line search using quadratic and cubic
// polynomial approximations and the Wolfe-Powell stopping criteria is
// used together with the slope ratio method for guessing initial step
// sizes. Additionally a bunch of checks are made to make sure that
// exploration is taking place and that extrapolation will not be
// unboundedly large.
//
// The "length" gives the length of the run: if it is positive, it gives
// the maximum number of line searches, if negative its absolute gives
// the maximum allowed number of function evaluations. You can
// (optionally) give "length" a second component, which will indicate
// the reduction in function value to be expected in the first
// line-search (defaults to 1.0).
//
// The function returns when either its length is up, or if no further
// progress can be made (ie, we are at a minimum, or so close that due
// to numerical problems, we cannot get any closer). If the function
// terminates within a few iterations, it could be an indication that
// the function value and derivatives are not consistent (ie, there may
// be a bug in the implementation of your "f" function).
//
// (c) 2015 - 2016: Marcel Admiraal

#ifndef CGDESCENT_H
#define CGDESCENT_H

#include "vector.h"

namespace ma

{
    class GradientFunction
    {
    public:
        /**
         * Populates the value and gradients variables for and with respect to
         * the input parameters.
         *
         * @param value     The function value at the input.
         * @param gradient  The gradients with respect to the inputs at the
         * @param input     The vector of input parameters.
         *                  input.
         */
        virtual void getValueAndGradients(double& value, Vector& gradient,
                const Vector& input) = 0;
    };

    /**
     * Minimizes a continuous differentialble multivariate function.
     *
     * The Polack-Ribiere flavour of conjugate gradients is used to compute
     * search directions, and a line search using quadratic and cubic
     * polynomial approximations and the Wolfe-Powell stopping criteria is
     * used together with the slope ratio method for guessing initial step
     * sizes. Additionally a bunch of checks are made to make sure that
     * exploration is taking place and that extrapolation will not be
     * unboundedly large.
     *
     * The function returns when either its length is up, or if no further
     * progress can be made (ie, we are at a minimum, or so close that due
     * to numerical problems, we cannot get any closer). If the function
     * terminates within a few iterations, it could be an indication that
     * the function value and derivatives are not consistent (ie, there may
     * be a bug in the implementation of your "f" function).
     *
     * @param f         The function to minimise.
     * @param initial   The initial input.
     * @param maxIters  The maximum number of iterations. Default is 100.
     * @return          The input at the minimum.
     */
    Vector minimise(GradientFunction* f, const Vector& initial,
            const unsigned int maxIters = 100);
}

#endif // CGDESCENT_H
