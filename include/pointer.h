// A class for controlling the mouse pointer.
//
// (c) 2016: Marcel Admiraal

#ifndef POINTER_H
#define POINTER_H

namespace ma
{
    class Pointer
    {
    public:
        /**
         * Default constructor.
         */
        Pointer();

        /**
         * Default destructor.
         */
        virtual ~Pointer();

        /**
         * If unix, returns whether or not the construtor found a mouse event
         * stream.
         * If windows, it always returns true.
         */
        bool isValid();

        /**
         * Moves the mouse pointer in the horizontal direction the specified
         * number of pixels; positive is right and negative is left.
         *
         * @param pixels    The number of pixels to move the pointer.
         */
        bool moveX(const int pixels);

        /**
         * Moves the mouse pointer in the vertical direction the specified
         * number of pixels; positive is down and negative is up.
         *
         * @param pixels    The number of pixels to move the pointer.
         */
        bool moveY(const int pixels);

        /**
         * Sends a left click signal at the current pointer position.
         */
        bool leftClick();

        /**
         * Sends a right click signal at the current pointer position.
         */
        bool rightClick();

    private:
        #ifdef __unix
            void setMouseEventStream();
            int fd;
        #endif // __unix
    };
}

#endif // POINTER_H
