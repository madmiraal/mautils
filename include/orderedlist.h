// An ordered list template class.
//
// (c) 2019: Marcel Admiraal

#ifndef ORDEREDLIST_H
#define ORDEREDLIST_H

#include "iterator.h"
#include "linkednode.h"
#include "map.h"

namespace ma
{
    template <class T> class OrderedList
    {
    public:
        /**
         * Default constructor.
         * Creates an empty ordered list.
         */
        OrderedList();

        /**
         * Copy constructor.
         *
         * @param source The ordered list to copy.
         */
        OrderedList(const OrderedList<T>& source);

        /**
         * Default destructor.
         */
        virtual ~OrderedList();

        /**
         * Swap function.
         *
         * @param first  The first ordered list to swap with.
         * @param second The second ordered list to swap with.
         */
        template <class S> friend void swap(OrderedList<S>& first,
                OrderedList<S>& second);

        /**
         * Assignment operator.
         * Returns a copy of the ordered list.
         *
         * @param source The ordered list to copy.
         * @return       A copy of the ordered list.
         */
        OrderedList<T>& operator=(OrderedList<T> source);

        /**
         * Returns the number of elements in the ordered list.
         *
         * @return The number of elements in the ordered list.
         */
        unsigned int getSize() const;

        /**
         * Inserts an item into the ordered list.
         *
         * @param index The index to insert the item after.
         * @param item  The item to add to the list.
         */
        void insert(unsigned int index, const T& item);

        /**
         * Inserts an ordered list into the ordered list.
         *
         * @param item The ordered list to insert into this ordered list.
         */
        void insert(const OrderedList<T>& orderedList);

        /**
         * Returns whether or not the ordered list has items.
         *
         * @return Whether or not the ordered list has items.
         */
        bool hasItems() const;

        /**
         * Returns the index of the first item in the ordered list.
         *
         * @return The index of the first item in the ordered list.
         */
        unsigned int peek() const;

        /**
         * Returns and removes the first item in the ordered list.
         *
         * @return The first item in the ordered list.
         */
        T pop();

        /**
         * Returns the index of the last item in the ordered list.
         *
         * @return The index of the last item in the ordered list.
         */
        unsigned int peekLast() const;

        /**
         * Returns and removes the last item in the list.
         *
         * @return The last item in the ordered list.
         */
        T popLast();

        /**
         * Removes all items from the ordered list.
         */
        void clear();

        /**
         * Returns a List of the indices.
         *
         * @return A list of the indices.
         */
        List<unsigned int> getIndexList() const;

        /**
         * Returns a List of the data items.
         *
         * @return A list of the data items.
         */
        List<T> getItemList() const;

    private:
        Iterator<unsigned int> getIndexIterator() const;
        Iterator<T> getItemIterator() const;
        LinkedNode<unsigned int>* indexHead;
        LinkedNode<T>* itemHead;
        LinkedNode<unsigned int>* indexTail;
        LinkedNode<T>* itemTail;
        unsigned int size;
    };

    /**
     * Returns whether or not the ordered lists are equal.
     *
     * @param left  The first list.
     * @param right The second list.
     * @return      Whether or not the lists are equal.
     */
    template <class T> bool operator==(
        const OrderedList<T>& left, const OrderedList<T>& right);

    /**
     * Returns whether or not the ordered lists are not equal.
     *
     * @param left  The first list.
     * @param right The second list.
     * @return      Whether or not the lists are equal.
     */
    template <class T> bool operator!=(
        const OrderedList<T>& left, const OrderedList<T>& right);

    /**
     * Input stream operator.
     * Builds an ordered list from an input stream and returns the input stream.
     *
     * @param is            The input stream.
     * @param destination   The ordered list to build.
     * @return              The input stream.
     */
    template <class T> std::istream& operator>>(std::istream& is,
            OrderedList<T>& destination);

    /**
     * Output stream operator.
     * Appends the ordered list to the output stream and returns the output stream.
     *
     * @param os     The output stream.
     * @param source The list to stream.
     * @return       The output stream.
     */
    template <class T> std::ostream& operator<<(std::ostream& os,
            const OrderedList<T>& source);

// Function definitions.

    template <class T> OrderedList<T>::OrderedList() :
        indexHead(0), itemHead(0), indexTail(0), itemTail(0), size(0)
    {
    }

    template <class T> OrderedList<T>::OrderedList(const OrderedList<T>& source) :
        indexHead(0), itemHead(0), indexTail(0), itemTail(0), size(0)
    {
        if (source.indexHead != 0)
        {
            indexHead = new LinkedNode<unsigned int>(source.indexHead->getData());
            itemHead = new LinkedNode<T>(source.itemHead->getData());
            LinkedNode<unsigned int>* indexPointer = indexHead;
            LinkedNode<unsigned int>* sourceIndexPointer = source.indexHead;
            LinkedNode<T>* itemPointer = itemHead;
            LinkedNode<T>* sourceItemPointer = source.itemHead;
            ++size;
            while(sourceIndexPointer->getNext() != 0)
            {
                sourceIndexPointer = sourceIndexPointer->getNext();
                sourceItemPointer = sourceItemPointer->getNext();
                indexPointer->addNext(sourceIndexPointer->getData());
                itemPointer->addNext(sourceItemPointer->getData());
                indexPointer = indexPointer->getNext();
                itemPointer = itemPointer->getNext();
                ++size;
            }
            indexTail = indexPointer;
            itemTail = itemPointer;
        }
    }

    template <class T> OrderedList<T>::~OrderedList()
    {
        if (indexHead != 0) deleteAll(indexHead);
        if (itemHead != 0) deleteAll(itemHead);
    }

    template <class T> void swap(OrderedList<T>& first, OrderedList<T>& second)
    {
        std::swap(first.indexHead, second.indexHead);
        std::swap(first.itemHead, second.itemHead);
        std::swap(first.indexTail, second.indexTail);
        std::swap(first.itemTail, second.itemTail);
        std::swap(first.size, second.size);
    }

    template <class T> OrderedList<T>& OrderedList<T>::operator=(
            OrderedList<T> source)
    {
        swap(*this, source);
        return *this;
    }

    template <class T> unsigned int OrderedList<T>::getSize() const
    {
        return size;
    }

    template <class T> void OrderedList<T>::insert(unsigned int index,
            const T& item)
    {
        size++;
        if (indexHead == 0)
        {
            indexHead = new LinkedNode<unsigned int>(index);
            itemHead = new LinkedNode<T>(item);
            indexTail = indexHead;
            itemTail = itemHead;
        }
        else if (index < indexHead->getData())
        {
            indexHead->addPrevious(index);
            itemHead->addPrevious(item);
            indexHead = indexHead->getPrevious();
            itemHead = itemHead->getPrevious();
        }
        else
        {
            LinkedNode<unsigned int>* indexPointer = indexHead;
            LinkedNode<T>* itemPointer = itemHead;
            while(indexPointer != indexTail)
            {
                indexPointer = indexPointer->getNext();
                itemPointer = itemPointer->getNext();
                if (index < indexPointer->getData())
                {
                    indexPointer->addPrevious(index);
                    itemPointer->addPrevious(item);
                    return;
                }
            }
            indexTail->addNext(index);
            itemTail->addNext(item);
            indexTail= indexTail->getNext();
            itemTail = itemTail->getNext();
        }
    }

    template <class T> void OrderedList<T>::insert(
            const OrderedList<T>& orderedList)
    {
        Iterator<unsigned int> indexIter = orderedList.getIndexIterator();
        Iterator<T> itemIter = orderedList.getItemIterator();
        while (indexIter.valid())
            insert(*indexIter++, *itemIter++);
    }

    template <class T> bool OrderedList<T>::hasItems() const
    {
        return size > 0;
    }

    template <class T> unsigned int OrderedList<T>::peek() const
    {
        return indexHead->getData();
    }

    template <class T> T OrderedList<T>::pop()
    {
        T data = itemHead->getData();
        LinkedNode<unsigned int>* indexPointer = indexHead;
        LinkedNode<T>* itemPoitner = itemHead;
        indexHead = indexHead->getNext();
        itemHead = itemHead->getNext();
        delete indexPointer;
        delete itemPoitner;
        size--;
        return data;
    }

    template <class T> unsigned int OrderedList<T>::peekLast() const
    {
        return indexTail->getData();
    }

    template <class T> T OrderedList<T>::popLast()
    {
        T data = itemTail.getData();
        LinkedNode<unsigned int>* indexPointer = indexTail;
        LinkedNode<T>* itemPoitner = itemTail;
        indexTail = indexTail->getPrevious();
        itemTail = itemTail->getPrevious();
        delete indexPointer;
        delete itemPoitner;
        size--;
        return data;
    }

    template <class T> void OrderedList<T>::clear()
    {
        if (indexHead != 0) deleteAll(indexHead);
        if (itemHead != 0) deleteAll(itemHead);
        indexHead = 0;
        indexTail = 0;
        itemHead = 0;
        itemTail = 0;
        size = 0;
    }

    template <class T> List<unsigned int> OrderedList<T>::getIndexList() const
    {
        List<unsigned int> indexList;
        for (Iterator<unsigned int> indexIter = getIndexIterator();
                indexIter.valid(); ++indexIter)
        {
            indexList.append(*indexIter);
        }
        return indexList;
    }

    template <class T> List<T> OrderedList<T>::getItemList() const
    {
        List<T> itemList;
        for (Iterator<T> itemIter = getItemIterator();
                itemIter.valid(); ++itemIter)
        {
            itemList.append(*itemIter);
        }
        return itemList;
    }

    template <class T> Iterator<unsigned int>
            OrderedList<T>::getIndexIterator() const
    {
        Iterator<unsigned int> indexIter(indexHead);
        return indexIter;
    }

    template <class T> Iterator<T>
            OrderedList<T>::getItemIterator() const
    {
        Iterator<T> itemIter(itemHead);
        return itemIter;
    }

    template <class T> bool operator==(const OrderedList<T>& left,
            const OrderedList<T>& right)
    {
        if (left.getSize() != right.getSize()) return false;
        if (left.getIndexList() != right.getIndexList()) return false;
        if (right.getItemList() != right.getItemList()) return false;
        return true;
    }

    template <class T> bool operator!=(
            const OrderedList<T>& left, const OrderedList<T>& right)
    {
        return !(left == right);
    }

    template <class T> std::istream& operator>>(std::istream& is,
            OrderedList<T>& destination)
    {
        List<unsigned int> indexList;
        List<T> itemList;
        is >> indexList;
        is >> itemList;
        if (is.good() && indexList.getSize() == itemList.getSize())
        {
            Iterator<unsigned int> indexIter = indexList.getIterator();
            Iterator<T> itemIter = itemIter = itemList.getIterator();
            unsigned int index;
            T item;
            while(indexIter.valid())
            {
                destination.insert(*indexIter++, *itemIter++);
            }
        }
        return is;
    }

    template <class T> std::ostream& operator<<(std::ostream& os,
            const OrderedList<T>& source)
    {
        os << source.getIndexList() << '\t';
        os << source.getItemList();
        return os;
    }
}

#endif // ORDEREDLIST_H
