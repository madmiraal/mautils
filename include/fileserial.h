// A class for reading serial data recorded in a csv file.
//
// (c) 2018: Marcel Admiraal

#ifndef FILESERIAL_H
#define FILESERIAL_H

#include "serial.h"
#include "str.h"

#include <fstream>

namespace ma
{
    class FileSerial : public Serial
    {
    public:
        /**
         * Default constructor.
         */
        FileSerial();

        /**
         * Default destructor.
         */
        virtual ~FileSerial();

        /**
         * Returns the filename of the file currently connected to.
         *
         * @return The filename of the file currently connected to.
         */
        virtual Str getPortString() const;

        /**
         * Sets the file that contains the recorded serial data.
         * Returns whether or not the connection was successful.
         *
         * @param fileName The filename that contains the recorded data.
         * @return         Whehter or not the connection was successful.
         */
        virtual bool setPort(const Str& fileName);

        /**
         * Sets the file that contains the recorded serial data.
         * Returns whether or not the connection was successful.
         *
         * @param fileName The filename that contains the recorded data.
         * @param mode     Flags describing the input/output mode for the file.
         * @return         Whehter or not the connection was successful.
         */
        virtual bool setPort(const Str& fileName, std::ios_base::openmode mode);

        /**
         * Intercepts the base class's setParameters method.
         *
         * @return true.
         */
        virtual bool setParameters(
            const int baud = 9600,
            const int bits = 8,
            const int parity = NOPARITY,
            const int stopBits = ONESTOPBIT,
            const bool block = false
            );

        /**
         * Writes an array of byte data as line of comma separated integers
         * to the file.
         * Returns the number of bytes written.
         *
         * @param buffer       An unsigned char array of bytes that
         *                     contains the data to be written.
         * @param bufferLength The length of the array.
         * @return             The number of bytes written.
         */
        virtual unsigned long writeData(const unsigned char *buffer,
            const unsigned int bufferLength);

        /**
         * Reads up to a line of comma separated byte integers
         * from the file and writes them to the byte array passed.
         * Returns the number of byte integers read.
         *
         * @param buffer       An unsigned char array of bytes to
         *                     write the data read to.
         * @param bufferLength The length of the array
         *                     (the maximum number of bytes to read).
         * @return             The number of bytes in the line read.
         */
        virtual unsigned long readData(unsigned char *buffer,
            const unsigned int bufferLength);

        /**
         * Reads up to a line of comma separated floating point numbers
         * from the file and writes them to the double array passed.
         * Returns the number of floating point numbers read.
         *
         * @param buffer       A double array to write the data read to.
         * @param bufferLength The length of the array
         *                     (the maximum number of bytes to read).
         * @return             The number of bytes in the line read.
         */
        virtual unsigned long readDoubleData(double *buffer,
            const unsigned int bufferLength);

        /**
         * Deletes the current contents of the file.
         */
        virtual void clearWriteBuffer();

        /**
         * Resets the read pointer to the beginning of the file.
         */
        virtual void clearReadBuffer();

        /**
         * Returns whether the file is open and good.
         *
         * @return Whether the file is open and good.
         */
        virtual bool isConnected() const;

        /**
         * Closes the file.
         */
        virtual void disconnect();

        /**
         * Returns whether the eof file bit is set.
         *
         * @return Whether the eof file bit is set.
         */
        bool eof();

    private:
        Str filename;
        std::fstream file;
    };
}

#endif // FILESERIAL_H
