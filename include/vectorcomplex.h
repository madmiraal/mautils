// A complex number complex vector class
//
// (c) 2017: Marcel Admiraal

#ifndef VECTORCOMPLEX_H
#define VECTORCOMPLEX_H

#include "complex.h"
#include "vector.h"

#include <iostream>

namespace ma
{
    class VectorComplex
    {
    public:
        /**
         * Default constructor.
         * Creates a zero dimensional complex vector.
         */
        VectorComplex();

        /**
         * Default destructor.
         */
        ~VectorComplex();

        /**
         * Size constructor.
         *
         * @param elements  The number of elements in the complex vector.
         */
        VectorComplex(const unsigned int elements);

        /**
         * Array of complex numbers constructor.
         *
         * @param element   The array of elements.
         * @param elements  The number of elements.
         */
        VectorComplex(const Complex* element, const unsigned int elements);

        /**
         * Array of integers constructor.
         *
         * @param element   The array of elements.
         * @param elements  The number of elements.
         */
        VectorComplex(const int* element, const unsigned int elements);

        /**
         * Array of unsigned integers constructor.
         *
         * @param element   The array of elements.
         * @param elements  The number of elements.
         */
        VectorComplex(const unsigned int* element, const unsigned int elements);

        /**
         * Array of shorts constructor.
         *
         * @param element   The array of elements.
         * @param elements  The number of elements.
         */
        VectorComplex(const short* element, const unsigned int elements);

        /**
         * Array of floats constructor.
         *
         * @param element   The array of elements.
         * @param elements  The number of elements.
         */
        VectorComplex(const float* element, const unsigned int elements);

        /**
         * Array of doubles constructor.
         *
         * @param element   The array of elements.
         * @param elements  The number of elements.
         */
        VectorComplex(const double* element, const unsigned int elements);

        /**
         * Copy constructor.
         *
         * @param source The complex vector to copy.
         */
        VectorComplex(const VectorComplex& source);

        /**
         * Constructs a complex vector from the specified file.
         *
         * @param filename The name of the file that contains
         *                 the complex vector specification.
         */
        VectorComplex(const char* filename);

        /**
         * Swap function.
         *
         * @param first  The first complex vector to swap with.
         * @param second The second complex vector to swap with.
         */
        friend void swap(VectorComplex& first, VectorComplex& second);

        /**
         * Assignment operator.
         * Returns a copy of the complex vector.
         *
         * @param source The complex vector to copy.
         * @return       A copy of the complex vector.
         */
        VectorComplex& operator=(VectorComplex source);

        /**
         * Array subscription operator:
         * Allows direct access to the elements.
         *
         * @param index The 0-based index to retrieve.
         * @return      The required element.
         */
        Complex& operator[](const unsigned int index);
        const Complex& operator[](const unsigned int index) const;

        /**
         * Returns the number of elements in the complex vector.
         *
         * @return The number of elements in the complex vector.
         */
        unsigned int size() const;

        /**
         * Returns the complex sub-vector specified.
         *
         * @param start     The 0-based index of the first element required.
         * @param length    The number of elements to retrieve.
         * @return          The complex sub-vector specified.
         */
        VectorComplex getSubvector(const unsigned int start,
                const unsigned int length) const;

        /**
         * Copies a complex vector into the complex vector starting at the
         * location specified, and returns the modified complex vector.
         *
         * @param start     The 0-based index to start the copy at.
         * @param source    The source complex vector to copy from.
         * @return          The modified complex vector.
         */
        VectorComplex& setSubVectorComplex(const unsigned int start,
                const VectorComplex& source);

        /**
         * Copies the complex vector into the array passed.
         *
         * @param output    The array to populate.
         */
        void getArray(Complex* output) const;

        /**
         * Copies the array of complex numbers into the complex vector.
         *
         * @param source    The complex number array to copy from.
         */
        void setArray(const Complex* source);

        /**
         * Copies the array of integers into the complex vector.
         *
         * @param source    The integer array to copy from.
         */
        void setArray(const int* source);

        /**
         * Copies the array of floats into the complex vector.
         *
         * @param source    The float array to copy from.
         */
        void setArray(const float* source);

        /**
         * Copies the array of doubles into the complex vector.
         *
         * @param source    The double array to copy from.
         */
        void setArray(const double* source);

        /**
         * Extends the length of the complex vector, and returns the extened
         * complex vector.
         *
         * @param elements  The number of elements to add.
         * @param value     The value of the elements added. Default: 0
         * @return          The exetned complex vector.
         */
        VectorComplex& extend(const unsigned int elements,
                const double value = 0);

        /**
         * Addition equals operator.
         * Adds the right complex vector to this complex vector, and returns the
         * sum.
         *
         * @param right The complex vector to add to this complex vector.
         * @return      The element sums of the two complex vectors.
         */
        VectorComplex& operator+=(const VectorComplex& right);

        /**
         * Complex addition operator.
         * Adds the complex number to each element of this complex vector, and
         * returns the updated complex number.
         *
         * @param right The complex number to add to each element.
         * @return      The complex vector with the complex number added to
         *              each element.
         */
        VectorComplex& operator+=(const Complex& right);

        /**
         * Subtraction equals operator.
         * Subtracts the right complex vector from this complex vector, and
         * returns the differnce.
         *
         * @param right The complex vector to subtract.
         * @return      The element subtractions of the two complex vectors.
         */
        VectorComplex& operator-=(const VectorComplex& right);

        /**
         * Complex subtraction operator.
         * Subtracts the complex number from each element of this complex
         * vector, and returns the updated complex number.
         *
         * @param right The complex number to subtract from each element.
         * @return      The complex vector with the complex number subtracted
         *              from each element.
         */
        VectorComplex operator-=(const Complex& right);

        /**
         * Multiplication equals operator.
         * Element multiplication of this complex vector with the right complex
         * vector, and returns the products.
         *
         * @param right The complex vector to element multiply this complex
                        vector by.
         * @return      The element products of the two complex vectors.
         */
        VectorComplex& operator*=(const VectorComplex& right);

        /**
         * Double multiplication equals operator.
         * Scales this complex vector by the scalar and returns the scaled
         * complex vector.
         *
         * @param right The scalar to multiply the complex vector by.
         * @return      The scaled complex vector.
         */
        VectorComplex& operator*=(const double right);

        /**
         * Division equals operator.
         * Element division of this complex vector by the right complex vector,
         * and returns the quotients.
         *
         * @param right The complex vector to element divide this complex vector
         *              by.
         * @return      The quotients of the two complex vectors.
         */
        VectorComplex& operator/=(const VectorComplex& right);

        /**
         * Double division equals operator.
         * Reduces this complex vector by the scalar and returns the reduced
         * complex vector.
         *
         * @param right The scalar to divide the complex vector by.
         * @return      The reduced complex vector.
         */
        VectorComplex& operator/=(const double right);

        /**
         * Returns a vector containing the real parts of the complex numbers.
         *
         * @return  A vector containing the real parts of the complex numbers.
         */
        Vector real() const;

        /**
         * Returns a vector containing the elements that are real:
         * Imaginary part is zero to within the threshold x precision.
         *
         * @param threshold The number to multiply the precision by.
         * @return          A vector containing the the elements that are real.
         */
        Vector getReal(const double threshold = 100) const;

        /**
         * Returns a vector containing the imaginary parts of the complex
         * numbers.
         *
         * @return  A vector containing the imaginary parts of the complex
         *          numbers.
         */
        Vector imag() const;

        /**
         * Returns a vector containing the elements that have imaginary parts.
         * Imaginary part is not zero to within the threshold x precision.
         *
         * @param threshold The number to multiply the precision by.
         * @return          A complex vector containing the the elements that
         *                  have imaginary parts.
         */
        VectorComplex getComplex(const double threshold = 100) const;

        /**
         * Returns a vector containing the elements that have positive imaginary
         * parts. Imaginary part is not zero to within the threshold x precision.
         *
         * @param threshold The number to multiply the precision by.
         * @return          A complex vector containing the the elements that
         *                  have positive imaginary parts.
         */
        VectorComplex getPosImag(const double threshold = 100) const;

        /**
         * Returns a vector containing the elements that have negative imaginary
         * parts. Imaginary part is not zero to within the threshold x precision.
         *
         * @param threshold The number to multiply the precision by.
         * @return          A complex vector containing the the elements that
         *                  have negative imaginary parts.
         */
        VectorComplex getNegImag(const double threshold = 100) const;

        /**
         * Returns a vector containing the absolute size of the complex numbers.
         * @return A vector containing the absolute size of the complex numbers.
         */
        Vector abs() const;

        /**
         * Returns the dot product of this complex vector and another: v1'*v2.
         *
         * @param other The other complex vector to dot multiply this one with.
         * @return      The dot product.
         */
        Complex dot(const VectorComplex& other) const;

        /**
         * Concatenatenates this complex vector with another, and
         * returns the concatenated complex vector.
         *
         * @param other The other complex vector to concatenate this one with.
         * @return      The concatenated complex vector.
         */
        VectorComplex concatenate(const VectorComplex& other);

        /**
         * Returns the sum of the elements.
         *
         * @return The sum of the elements.
         */
        Complex elementSum() const;

        /**
         * Returns the squared sum of the elements.
         *
         * @return The squared sum of the elements.
         */
        Complex elementSquaredSum() const;

        /**
         * Returns the product of the elements.
         *
         * @return The product of the elements.
         */
        Complex elementProduct() const;

        /**
         * Fills the complex vector with the value and returns the filled
         * complex vector.
         *
         * @param value The value to fill the complex vector with.
         * @return      The filled complex vector.
         */
        VectorComplex& fillWith(const double value);

        /**
         * Returns a subcomplex vector.
         *
         * @param first     The index of the first element.
         * @param length    The length of the subcomplex vector to return.
         * @return          The subcomplex vector requested.
         */
        VectorComplex subVectorComplex(const unsigned int first,
                const unsigned int length) const;

        /**
         * Streams the contents of the complex vector laid out in human readable
         * form.
         *
         * @param os     The output stream to stream to.
         */
        void print(std::ostream& os = std::cout) const;

        /**
         * Saves the complex vector to the specified file name.
         *
         * @param filename  The name of the file to save the complex vector to.
         * @return          Whether or not the save was successful.
         */
        bool save(const char* filename) const;

        /**
         * Loads the complex vector from the specified file name.
         *
         * @param filename  The name of the file to load the complex vector
         *                  from.
         * @return          Whether or not the load was successful.
         */
        bool load(const char* filename);

    protected:
        unsigned int elements;
        Complex* element;
    };

    /**
     * Addition operator.
     * Returns the element sum of the two complex vectors.
     *
     * @param left  The first complex vector to sum.
     * @param right The second complex vector to sum.
     * @return      The element sum of the two complex vectors.
     */
    VectorComplex operator+(VectorComplex left, const VectorComplex& right);

    /**
     * Complex addition operator.
     * Returns the complex vector with the complex number added to each element.
     *
     * @param left  The complex vector to add the complex number to.
     * @param right The complex number to add to each element.
     * @return      The complex vector with the complex number added to  each
     *              element.
     */
    VectorComplex operator+(VectorComplex left, const Complex& right);

    /**
     * Complex addition operator.
     * Returns the complex vector with the complex number added to each element.
     *
     * @param left  The complex number to add to each element.
     * @param right The complex vector to add the complex number to.
     * @return      The complex vector with the complex number added to  each
     *              element.
     */
    VectorComplex operator+(const Complex& left, VectorComplex right);

    /**
     * Subtraction operator.
     * Returns the element subtraction of the two complex vectors.
     *
     * @param left  The complex vector to subtract from.
     * @param right The complex vector to subtract.
     * @return      The element subtraction of the two complex vectors.
     */
    VectorComplex operator-(VectorComplex left, const VectorComplex& right);

    /**
     * Complex subtraction operator.
     * Returns the complex vector with the complex number subtracted from each
     * element.
     *
     * @param left  The complex vector to subtract the complex number from.
     * @param right The complex number to subtract from each element.
     * @return      The complex vector with the complex number subtracted from
     *              each element.
     */
    VectorComplex operator-(VectorComplex left, const Complex& right);

    /**
     * Complex subtraction operator.
     * Returns the negative complex vector with the complex number added to each
     * element.
     *
     * @param left  The complex number to add to each element.
     * @param right The complex vector to negate and add the complex number to.
     * @return      The negated complex vector with the complex number added to
     *              each element.
     */
    VectorComplex operator-(const Complex& left, const VectorComplex& right);

    /**
     * Negative operator.
     * Returns the negative complex vector.
     *
     * @param  source The complex vector to form the negative of.
     * @return        The negative complex vector.
     */
    VectorComplex operator-(const VectorComplex& source);

    /**
     * Multiplication operator.
     * Returns the element product of the two complex vectors.
     *
     * @param left  The first complex vector to multiply.
     * @param right The second complex vector to multiply.
     * @return      The element product of the two complex vectors.
     */
    VectorComplex operator*(VectorComplex left, const VectorComplex& right);

    /**
     * Double multiplication operator.
     * Returns the complex vector scaled by a scalar.
     *
     * @param left  The complex vector to multiply.
     * @param right The scalar to multiply the complex vector by.
     * @return      The scaled complex vector.
     */
    VectorComplex operator*(VectorComplex left, const double right);

    /**
     * Double multiplication operator.
     * Returns the complex vector scaled by a scalar.
     *
     * @param left  The scalar to multiply the complex vector by.
     * @param right The complex vector to multiply.
     * @return      The scaled complex vector.
     */
    VectorComplex operator*(const double left, VectorComplex right);

    /**
     * Division operator.
     * Returns the element quotient of the two complex vectors.
     *
     * @param left  The complex vector to divide.
     * @param right The complex vector to divide by.
     * @return      The element quotient of the two complex vectors.
     */
    VectorComplex operator/(VectorComplex left, const VectorComplex& right);

    /**
     * Division double right operator.
     * Returns the complex vector reduced by a scalar.
     *
     * @param left  The complex vector to divide.
     * @param right The scalar to divide the complex vector by.
     * @return      The reduced complex vector.
     */
    VectorComplex operator/(VectorComplex left, const double right);

    /**
     * Division double left operator.
     * Returns the element inverse of the complex vector multiplied by the
     * scalar.
     *
     * @param left  The scalar to multiply the complex vector by.
     * @param right The complex vector to invert.
     * @return      The element inverse of the complex vector multiplied by the
     *              scalar.
     */
    VectorComplex operator/(const double left, const VectorComplex& right);

    /**
     * Equals operator.
     * Returns whether or not the complex vectors are equal.
     *
     * @param left  The first complex vector.
     * @param right The second complex vector.
     * @return      True if the complex vectors are of equal size and
     *              all the elements are equal.
     */
    bool operator==(const VectorComplex& left, const VectorComplex& right);

    /**
     * Not equals operator.
     * Returns whether or not the complex vectors are not equal.
     *
     * @param left  The first complex vector.
     * @param right The second complex vector.
     * @return      True if the complex vectors are not of equal size or
     *              at least one element is not equal.
     */
    bool operator!=(const VectorComplex& left, const VectorComplex& right);

    /**
     * Returns whether two complex vectors' elements are approximately equal.
     *
     * @param left  The first complex vector.
     * @param right The second complex vector.
     * @param error The percentage error allowed. Defaults to 0.01%
     * @return      True if the complex vectors are of equal size and
     *              all the elements are within the percentage error.
     */
    bool approxEqual(const VectorComplex& left, const VectorComplex& right,
            const double error = 0.01);

    /**
     * Input stream operator.
     * Builds a complex vector from an input stream and returns the input
     * stream.
     *
     * @param is            The input stream.
     * @param destination   The complex vector to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, VectorComplex& destination);

    /**
     * Output stream operator.
     * Appends the complex vector to the output stream and returns the output
     * stream.
     *
     * @param os     The output stream.
     * @param source The complex vector to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const VectorComplex& source);

    /**
     * Returns a vector containing the real parts of the complex numbers of the
     * complex vector.
     *
     * @return  A vector containing the real parts of the complex numbers of the
     *          complex vector.
     */
    Vector real(const VectorComplex& source);

    /**
     * Returns a vector containing the imaginary parts of the complex
     * numbers of the complex vector.
     *
     * @return  A vector containing the imaginary parts of the complex
     *          numbers of the complex vector.
     */
    Vector imag(const VectorComplex& source);

    /**
     * Returns the dot product of two complex vectors: left'*right.
     *
     * @param left  The first complex vector to dot multiply.
     * @param right The second complex vector to dot multiply.
     * @return      The dot product.
     */
    Complex dot(const VectorComplex& left, const VectorComplex& right);

    /**
     * Returns a complex vector of the specified size filled with zeros.
     *
     * @param elements  The number of elements in the complex vector.
     * @return          A complex vector of the specified size filled with
     *                  zeros.
     */
    VectorComplex complexZeros(const unsigned int elements);

    /**
     * Returns a complex vector of the specified size filled with ones.
     *
     * @param elements  The number of elements in the complex vector.
     * @return          A complex vector of the specified size filled with ones.
     */
    VectorComplex complexOnes(const unsigned int elements);

    /**
     * Returns the concatenation of two complex vectors.
     *
     * @param left  The first complex vector to concatenate.
     * @param right The second complex vector to concatenate.
     * @return      The concatenation of the two complex vectors.
     */
    VectorComplex concatenate(const VectorComplex& left,
            const VectorComplex& right);

    /**
     * Returns the sum of the elements of the complex vector.
     *
     * @param source    The complex vector whose elements to sum.
     * @return          The sum of the elements.
     */
    Complex elementSum(const VectorComplex& source);

    /**
     * Returns the squared sum of the elements of the complex vector.
     *
     * @param source    The complex vector whose elements to square and sum.
     * @return          The squared sum of the elements.
     */
    Complex elementSquaredSum(const VectorComplex& source);

    /**
     * Returns the product of the elements of the complex vector.
     *
     * @param source    The complex vector whose elements to multiply.
     * @return          The product of the elements.
     */
    Complex elementProduct(const VectorComplex& source);

    /**
     * Streams the contents of the complex vector laid out in human readable
     * form.
     *
     * @param source The complex vector to stream.
     * @param os     The output stream to stream to.
     */
    void print(const VectorComplex& source, std::ostream& os = std::cout);
}

#endif // VECTORCOMPLEX_H
