// A vector class
//
// (c) 2015 - 2017: Marcel Admiraal

#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>

namespace ma
{
    class Vector
    {
    public:
        /**
         * Default constructor.
         * Creates a zero dimensional vector.
         */
        Vector();

        /**
         * Default destructor.
         */
        ~Vector();

        /**
         * Size constructor.
         *
         * @param elements  The number of elements in the vector.
         */
        Vector(const unsigned int elements);

        /**
         * Array of integers constructor.
         *
         * @param element   The array of elements.
         * @param elements  The number of elements.
         */
        Vector(const int* element, const unsigned int elements);

        /**
         * Array of unsigned integers constructor.
         *
         * @param element   The array of elements.
         * @param elements  The number of elements.
         */
        Vector(const unsigned int* element, const unsigned int elements);

        /**
         * Array of shorts constructor.
         *
         * @param element   The array of elements.
         * @param elements  The number of elements.
         */
        Vector(const short* element, const unsigned int elements);

        /**
         * Array of floats constructor.
         *
         * @param element   The array of elements.
         * @param elements  The number of elements.
         */
        Vector(const float* element, const unsigned int elements);

        /**
         * Array of doubles constructor.
         *
         * @param element   The array of elements.
         * @param elements  The number of elements.
         */
        Vector(const double* element, const unsigned int elements);

        /**
         * Copy constructor.
         *
         * @param source The vector to copy.
         */
        Vector(const Vector& source);

        /**
         * Copy constructor with a new length.
         * Creates an extended or truncated source vector as required.
         *
         * @param source    The vector to base the new vector on.
         * @param elements  The number of elements.
         */
        Vector(const Vector& source, const unsigned int elements);

        /**
         * Constructs a vector from the specified file.
         *
         * @param filename The name of the file that contains
         *                 the vector specification.
         */
        Vector(const char* filename);

        /**
         * Swap function.
         *
         * @param first  The first vector to swap with.
         * @param second The second vector to swap with.
         */
        friend void swap(Vector& first, Vector& second);

        /**
         * Assignment operator.
         * Returns a copy of the vector.
         *
         * @param source The vector to copy.
         * @return       A copy of the vector.
         */
        Vector& operator=(Vector source);

        /**
         * Array subscription operator:
         * Allows direct access to the elements.
         *
         * @param index The 0-based index to retrieve.
         * @return      The required element.
         */
        double& operator[](const unsigned int index);
        const double& operator[](const unsigned int index) const;

        /**
         * Returns the number of elements in the vector.
         *
         * @return The number of elements in the vector.
         */
        unsigned int size() const;

        /**
         * Returns the sub-vector specified.
         *
         * @param start     The 0-based index of the first element required.
         * @param length    The number of elements to retrieve.
         * @return          The sub-vector specified.
         */
        Vector getSubvector(const unsigned int start,
                const unsigned int length) const;

        /**
         * Copies a vector into the vector starting at the location specified,
         * and returns the modified vector.
         *
         * @param start     The 0-based index to start the copy at.
         * @param source    The source vector to copy from.
         * @return          The modified vector.
         */
        Vector& setSubVector(const unsigned int start, const Vector& source);

        /**
         * Return read-only access to the underlying array.
         */
        const double* array() const;

        /**
         * Copies the vector into the array passed.
         *
         * @param output    The array to populate.
         */
        void getArray(double* output) const;

        /**
         * Copies the array of integers into the vector.
         *
         * @param source    The integer array to copy from.
         */
        void setArray(const int* source);

        /**
         * Copies the array of floats into the vector.
         *
         * @param source    The float array to copy from.
         */
        void setArray(const float* source);

        /**
         * Copies the array of doubles into the vector.
         *
         * @param source    The double array to copy from.
         */
        void setArray(const double* source);

        /**
         * Extends the length of the vector, and returns the extened vector.
         *
         * @param elements  The number of elements to add.
         * @param value     The value of the elements added. Default: 0
         * @return          The exetned vector.
         */
        Vector& extend(const unsigned int elements, const double value = 0);

        /**
         * Addition equals operator.
         * Adds the right vector to this vector, and returns the sum.
         *
         * @param right The vector to add to this vector.
         * @return      The element sums of the two vectors.
         */
        Vector& operator+=(const Vector& right);

        /**
         * Subtraction equals operator.
         * Subtracts the right vector from this vector, and
         * returns the differnce.
         *
         * @param right The vector to subtract.
         * @return      The element subtractions of the two vectors.
         */
        Vector& operator-=(const Vector& right);

        /**
         * Multiplication equals operator.
         * Element multiplication of this vector with the right vector, and
         * returns the products.
         *
         * @param right The vector to element multiply this vector by.
         * @return      The element products of the two vectors.
         */
        Vector& operator*=(const Vector& right);

        /**
         * Double multiplication equals operator.
         * Scales this vector by the scalar and returns the scaled vector.
         *
         * @param right The scalar to multiply the vector by.
         * @return      The scaled vector.
         */
        Vector& operator*=(const double right);

        /**
         * Division equals operator.
         * Element division of this vector by the right vector, and
         * returns the quotients.
         *
         * @param right The vector to element divide this vector by.
         * @return      The quotients of the two vectors.
         */
        Vector& operator/=(const Vector& right);

        /**
         * Double division equals operator.
         * Reduces this vector by the scalar and returns the reduced vector.
         *
         * @param right The scalar to divide the vector by.
         * @return      The reduced vector.
         */
        Vector& operator/=(const double right);

        /**
         * Normalises the vector and returns the normalised vector.
         *
         * @return The normalised vector.
         */
        Vector& normalise();

        /**
         * Normalises the vector using a quick approximation of the inverse
         * square root, and returns the normalised vector.
         *
         * @return The normalised vector.
         */
        Vector& fastNormalise();

        /**
         * Returns the absolute size of this vector.
         *
         * @return The absolute size of this vector.
         */
        double abs() const;

        /**
         * Returns the dot product of this vector and another: v1'*v2.
         *
         * @param other The other vector to dot multiply this one with.
         * @return      The dot product.
         */
        double dot(const Vector& other) const;

        /**
         * Concatenatenates this vector with another, and
         * returns the concatenated vector.
         *
         * @param other The other vector to concatenate this one with.
         * @return      The concatenated vector.
         */
        Vector concatenate(const Vector& other);

        /**
         * Returns the minimum value of the vector.
         *
         * @return  The minimum of the vector.
         */
        double min() const;

        /**
         * Returns the index of the minimum value of the vector.
         *
         * @return  The index of the minimum of the vector.
         */
        unsigned int minIndex() const;

        /**
         * Returns the maximum value of the vector.
         *
         * @return  The maximum of the vector.
         */
        double max() const;

        /**
         * Returns the index of the maximum value of the vector.
         *
         * @return  The index of the maximum of the vector.
         */
        unsigned int maxIndex() const;

        /**
         * Returns the mean value of the vector.
         *
         * @return  The mean of the vector.
         */
        double mean() const;

        /**
         * Returns the variance of the vector.
         *
         * @return  The variance of the vector.
         */
        double var() const;

        /**
         * Returns the standard deviation of the vector.
         *
         * @return  The standard deviation of the vector.
         */
        double sd() const;

        /**
         * Returns the standard error of the mean of the vector.
         *
         * @return The standard error of the mean of the vector.
         */
        double sem() const;

        /**
         * Returns the relative standard error of the mean of the vector.
         *
         * @return The relative standard error of the mean of the vector.
         */
        double rsem() const;

        /**
         * Returns the sum of the elements.
         *
         * @return The sum of the elements.
         */
        double elementSum() const;

        /**
         * Returns the squared sum of the elements.
         *
         * @return The squared sum of the elements.
         */
        double elementSquaredSum() const;

        /**
         * Returns the product of the elements.
         *
         * @return The product of the elements.
         */
        double elementProduct() const;

        /**
         * Fills the vector with the value and returns the filled vector.
         *
         * @param value The value to fill the vector with.
         * @return      The filled vector.
         */
        Vector& fillWith(const double value);

        /**
         * Returns a subvector.
         *
         * @param first     The index of the first element.
         * @param length    The length of the subvector to return.
         * @return          The subvector requested.
         */
        Vector subVector(const unsigned int first,
                const unsigned int length) const;

        /**
         * Streams the contents of the vector laid out in human readable form.
         *
         * @param os     The output stream to stream to.
         */
        void print(std::ostream& os = std::cout) const;

        /**
         * Saves the vector to the specified file name.
         *
         * @param filename  The name of the file to save the vector to.
         * @return          Whether or not the save was successful.
         */
        bool save(const char* filename) const;

        /**
         * Loads the vector from the specified file name.
         *
         * @param filename  The name of the file to load the vector from.
         * @return          Whether or not the load was successful.
         */
        bool load(const char* filename);

    protected:
        unsigned int elements;
        double* element;
    };

    /**
     * Addition operator.
     * Returns the element sum of the two vectors.
     *
     * @param left  The first vector to sum.
     * @param right The second vector to sum.
     * @return      The element sum of the two vectors.
     */
    Vector operator+(Vector left, const Vector& right);

    /**
     * Subtraction operator.
     * Returns the element subtraction of the two vectors.
     *
     * @param left  The vector to subtract from.
     * @param right The vector to subtract.
     * @return      The element subtraction of the two vectors.
     */
    Vector operator-(Vector left, const Vector& right);

    /**
     * Negative operator.
     * Returns the negative vector.
     *
     * @param  source The vector to form the negative of.
     * @return        The negative vector.
     */
    Vector operator-(const Vector& source);

    /**
     * Multiplication operator.
     * Returns the element product of the two vectors.
     *
     * @param left  The first vector to multiply.
     * @param right The second vector to multiply.
     * @return      The element product of the two vectors.
     */
    Vector operator*(Vector left, const Vector& right);

    /**
     * Double multiplication operator.
     * Returns the vector scaled by a scalar.
     *
     * @param left  The vector to multiply.
     * @param right The scalar to multiply the vector by.
     * @return      The scaled vector.
     */
    Vector operator*(Vector left, const double right);

    /**
     * Double multiplication operator.
     * Returns the vector scaled by a scalar.
     *
     * @param left  The scalar to multiply the vector by.
     * @param right The vector to multiply.
     * @return      The scaled vector.
     */
    Vector operator*(const double left, Vector right);

    /**
     * Division operator.
     * Returns the element quotient of the two vectors.
     *
     * @param left  The vector to divide.
     * @param right The vector to divide by.
     * @return      The element quotient of the two vectors.
     */
    Vector operator/(Vector left, const Vector& right);

    /**
     * Division double right operator.
     * Returns the vector reduced by a scalar.
     *
     * @param left  The vector to divide.
     * @param right The scalar to divide the vector by.
     * @return      The reduced vector.
     */
    Vector operator/(Vector left, const double right);

    /**
     * Division double left operator.
     * Returns the element inverse of the vector multiplied by the scalar.
     *
     * @param left  The scalar to multiply the vector by.
     * @param right The vector to invert.
     * @return      The element inverse of the vector multiplied by the scalar.
     */
    Vector operator/(const double left, const Vector& right);

    /**
     * Equals operator.
     * Returns whether or not the vectors are equal.
     *
     * @param left  The first vector.
     * @param right The second vector.
     * @return      True if the vectors are of equal size and
     *              all the elements are equal.
     */
    bool operator==(const Vector& left, const Vector& right);

    /**
     * Not equals operator.
     * Returns whether or not the vectors are not equal.
     *
     * @param left  The first vector.
     * @param right The second vector.
     * @return      True if the vectors are not of equal size or
     *              at least one element is not equal.
     */
    bool operator!=(const Vector& left, const Vector& right);

    /**
     * Returns whether two vectors' elements are approximately equal.
     *
     * @param left  The first vector.
     * @param right The second vector.
     * @param error The percentage error allowed. Defaults to 0.01%
     * @return      True if the vectors are of equal size and
     *              all the elements are within the percentage error.
     */
    bool approxEqual(const Vector& left, const Vector& right,
            const double error = 0.01);

    /**
     * Input stream operator.
     * Builds a vector from an input stream and returns the input stream.
     *
     * @param is            The input stream.
     * @param destination   The vector to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, Vector& destination);

    /**
     * Output stream operator.
     * Appends the vector to the output stream and returns the output stream.
     *
     * @param os     The output stream.
     * @param source The vector to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const Vector& source);

    /**
     * Returns a normalised vector.
     *
     * @param  source   The vector to normalise.
     * @return          The normalised vector.
     */
    Vector normalise(Vector source);

    /**
     * Returns a normalised vector using a quick approximation of the inverse
     * square root.
     *
     * @param  source   The vector to normalise.
     * @return          The normalised vector.
     */
    Vector fastNormalise(Vector source);

    /**
     * Returns the absolute size of the vector.
     *
     * @param  source   The vector to obtain the absolute size of.
     * @return The absolute size of the vector.
     */
    double abs(const Vector& source);

    /**
     * Returns the dot product of two vectors: left'*right.
     *
     * @param left  The first vector to dot multiply.
     * @param right The second vector to dot multiply.
     * @return      The dot product.
     */
    double dot(const Vector& left, const Vector& right);

    /**
     * Returns a vector of the specified size filled with zeros.
     *
     * @param elements  The number of elements in the vector.
     * @return          A vector of the specified size filled with zeros.
     */
    Vector zeros(const unsigned int elements);

    /**
     * Returns a vector of the specified size filled with ones.
     *
     * @param elements  The number of elements in the vector.
     * @return          A vector of the specified size filled with ones.
     */
    Vector ones(const unsigned int elements);

    /**
     * Returns the concatenation of two vectors.
     *
     * @param left  The first vector to concatenate.
     * @param right The second vector to concatenate.
     * @return      The concatenation of the two vectors.
     */
    Vector concatenate(const Vector& left, const Vector& right);

    /**
     * Returns the minimum value of a vector.
     *
     * @param source The vector whose minimum is required.
     * @return       The minimum of the vector.
     */
    double min(const Vector& source);

    /**
     * Returns the index of the minimum value of a vector.
     *
     * @param source The vector whose minimum index is required.
     * @return       The index of the minimum of the vector.
     */
    unsigned int minIndex(const Vector& source);

    /**
     * Returns the maximum value of a vector.
     *
     * @param source The vector whose maximum is required.
     * @return       The maximum of the vector.
     */
    double max(const Vector& source);

    /**
     * Returns the index of the maximum value of a vector.
     *
     * @param source The vector whose maximum index is required.
     * @return       The index of the maximum of the vector.
     */
    unsigned int maxIndex(const Vector& source);

    /**
     * Returns the mean value of a vector.
     *
     * @param source The vector whose mean is required.
     * @return       The mean of the vector.
     */
    double mean(const Vector& source);

    /**
     * Returns the variance of a vector.
     *
     * @param source The vector whose variance is required.
     * @return       The variance of the vector.
     */
    double var(const Vector& source);

    /**
     * Returns the standard deviation of a vector.
     *
     * @param source The vector whose standard deviation is required.
     * @return       The standard deviation of the vector.
     */
    double sd(const Vector& source);

    /**
     * Returns the standard error of the menan of a vector.
     *
     * @param source The vector whose standard error is required.
     * @return       The standard error of the vector.
     */
    double sem(const Vector& source);

    /**
     * Returns the relative standard error of the mean of a vector.
     *
     * @param source The vector whose relative standard error of the mean is
     *               required.
     * @return       The relative standard error of the mean of the vector.
     */
    double rsem(const Vector& source);

    /**
     * Returns the sum of the elements of the vector.
     *
     * @param source    The vector whose elements to sum.
     * @return          The sum of the elements.
     */
    double elementSum(const Vector& source);

    /**
     * Returns the squared sum of the elements of the vector.
     *
     * @param source    The vector whose elements to square and sum.
     * @return          The squared sum of the elements.
     */
    double elementSquaredSum(const Vector& source);

    /**
     * Returns the product of the elements of the vector.
     *
     * @param source    The vector whose elements to multiply.
     * @return          The product of the elements.
     */
    double elementProduct(const Vector& source);

    /**
     * Streams the contents of the vector laid out in human readable form.
     *
     * @param source The vector to stream.
     * @param os     The output stream to stream to.
     */
    void print(const Vector& source, std::ostream& os = std::cout);

    /**
     * Converts arbitrary real values to real values in the range (0, 1) that
     * add up to 1.
     *
     * @param value The vector of original values.
     * @param beta  The inverse of the comparison value.
     * @return      The vector of soft-maxed values.
     */
    Vector softMax(const Vector& value, double beta = 1);

    /**
     * Converts arbitrary probability values to normailised values that
     * add up to 1.
     *
     * @param value The vector of original values.
     * @return      The vector of normal probability values.
     */
    Vector normalProbability(const Vector& value);
}

#endif // VECTOR_H
