// Replays files created by Logger.
//
// (c) 2016 - 2019: Marcel Admiraal

#ifndef LOGPLAYBACK_H
#define LOGPLAYBACK_H

#include "orderedlist.h"
#include "str.h"

#include <chrono>
#include <fstream>
#include <thread>

namespace ma
{
    class LogPlayback
    {
    public:
        /**
         * Default constructor.
         */
        LogPlayback();

        /**
         * Default destructor.
         */
        virtual ~LogPlayback();

        /**
         * Returns the current log file name.
         *
         * @return The current log file name.
         */
        Str getLogFilename();

        /**
         * Sets the current log file's file name.
         *
         * @param filename  The new log file name.
         */
        void setLogFilename(const Str& filename);

        /**
         * Sets whether or not to use real-time playback.
         *
         * @param realTime  Whether or not to use real-time playback.
         */
        void setRealtime(bool realTime = true);

        /**
         * If not started, starts playback, if the log file can be opened.
         * If previously started and paused, continues playback.
         */
        void startPlayback();

        /**
         * Pauses playback, if playing.
         */
        void pausePlayback();

        /**
         * If playing (or paused) stops playback and closes the log file.
         */
        void stopPlayback();

        /**
         * Returns whether or not the playback is playing.
         *
         * @return Whether or not the playback is playing.
         */
        bool isPlaying() const;

        /**
         * Returns whether or not the playback is paused.
         * Note: Returns false if playback is not playing.
         *
         * @return Whether or not the playback is playing.
         */
        bool isPaused() const;

        /**
         * Returns whether or not the playback loops.
         *
         * @return Whether or not the playback loops.
         */
        bool isPlaybackLooping() const;

        /**
         * Sets whether or not to loop the playback.
         *
         * @param enable Whether or not to loop the playback.
         */
        void loopPlayback(const bool enable = true);

        /**
         * Returns the number of playback loops remaining.
         *
         * @return The number of times the playback loops.
         */
        int getLoopPlaybackCountRemaining() const;

        /**
         * Returns the number of times the playback loops.
         * Note: -1 indicates the playback loops continuously.
         *
         * @return The number of times the playback loops.
         */
        int getLoopPlaybackCount() const;

        /**
         * Sets the number of times the playback loops.
         * Note: Use -1 to indicate that the playback should loop continuously.
         *
         * @param loopCount The number of times the playback loops.
         */
        void setLoopPlaybackCount(const int loopCount);

        /**
         * Insert an entry to be processed after a delay.
         *
         * @param delay The milliseconds to delay the processing.
         * @param entry The entry to process later.
         */
        void insertDelayedEntry(const std::chrono::milliseconds delay,
                const Str& entry);

    protected:
        /**
         * Processes the log entry, and returns whether or not the log entry was
         * processed.
         * Note: When overriding this function, call the parent function first,
         * and only process the entry if the parent function returns false i.e.
         * it didn't process the entry.
         *
         * @param entry     The entry to process.
         * @return          Whether or not the log entry was processed.
         */
        virtual bool processEntry(const Str& entry);

        /**
         * Called when the logfile contains "Logging Started"
         * i.e. the logger opened the log file.
         */
        virtual void logFileStart();

        /**
         * Called when the logfile contains "Logging Stopped"
         * i.e. the logger closed the log file.
         */
        virtual void logFileEnd();

        // Whether or not the log is being played back in real-time.
        bool realTime;

    private:
        // Loop thread function.
        void loop();
        // Returns the log line's timestamp.
        unsigned int getTimeStamp(const Str& logLine);
        // Returns whether or not the timeStamp is before now.
        bool processNow(std::chrono::milliseconds timeStamp);
        // Processes a log line: strips the timestamp and calls process entry.
        bool processLogLine(const Str& logLine);

        Str filename;
        std::fstream logFile;
        Str logLine;
        bool running;
        bool playing;
        bool paused;
        bool looping;
        int loopCount;
        int loopCountRemaining;
        bool readLine;
        OrderedList<Str> delayedEntries;
        std::chrono::milliseconds start;
        std::chrono::milliseconds played;
        std::chrono::milliseconds next;
        std::thread loopThread;
    };
}

#endif // LOGPLAYBACK_H
