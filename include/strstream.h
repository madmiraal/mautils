// A string stream class
//
// (c) 2016: Marcel Admiraal

#ifndef STRSTREAM_H
#define STRSTREAM_H

#include "str.h"

#include <iostream>

namespace ma
{
    class StrStreamBuf : public std::streambuf
    {
    public:
        /**
         * String constructor.
         */
        StrStreamBuf(const Str& source);

        /**
         * Converts the buffer into a string.
         */
        Str toStr() const;

    protected:
        /**
         * Extends the buffer when required.
         */
        virtual int overflow (int c = EOF);
        /**
         * Checks whether characters have been added to the buffer.
         */
        virtual int underflow();

    private:
        Str buffer;
        const unsigned int bufferLength;
    };

    class StrStream : public std::iostream
    {
    public:
        /**
         * Default constructor.
         */
        StrStream();

        /**
         * String constructor.
         * Creates a stream from a string.
         */
        StrStream(const Str& source);

        /**
         * Default destructor.
         */
        virtual ~StrStream();

        /**
         * Converts the stream into a string.
         */
        Str toStr() const;

    private:
        StrStreamBuf* ssbuf;
    };
}

#endif // STRSTREAM_H
