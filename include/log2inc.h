// A log base 2 incremented and decremented counter.
//
// Values are doubled when incremented and halved when decremented.
// At 128 values are rounded to 125 to provide a neat series of log base 2
// numbers.
// When created values are adjusted to fit within a neat series.
//
// (c) 2017: Marcel Admiraal

#ifndef LOG2INC_H
#define LOG2INC_H

#include <iostream>

namespace ma
{
    class Log2Inc
    {
    public:
        /**
         * Constructor.
         * Defaults to 1.
         */
        Log2Inc(double value = 1);

        /**
         * Default destructor.
         */
        virtual ~Log2Inc();

        /**
         * Swap function.
         *
         * @param first  The first Log2Inc to swap with.
         * @param second The second Log2Inc to swap with.
         */
        friend void swap(Log2Inc& first, Log2Inc& second);

        /**
         * Assignment operator.
         * Returns a copy of the Log2Inc.
         *
         * @param source The Log2Inc to copy.
         * @return       A copy of the Log2Inc.
         */
        Log2Inc& operator=(Log2Inc source);

        /**
         * Pre-increment operator.
         * Doubles the value of the Log2Inc and returns the new value.
         */
        Log2Inc& operator++();

        /**
         * Post-increment operator.
         * Doubles the value of the Log2Inc and returns the old value.
         */
        Log2Inc operator++(int);

        /**
         * Pre-decrement operator.
         * Halves the value of the Log2Inc and returns the new value.
         */
        Log2Inc& operator--();

        /**
         * Post-decrement operator.
         * Halves the value of the Log2Inc and returns the old value.
         */
        Log2Inc operator--(int);

        /**
         * Returns the current value.
         */
        double getValue() const;

    private:
        void calculateValue();

        int thousandIndex;
        int tenIndex;
        int twoIndex;
        double value;
    };

    /**
     * Returns whether or not a Log2Inc value equals a double.
     *
     * @param left  The Log2Inc.
     * @param right The double.
     * @return      Whether or not the Log2Inc value equals the double.
     */
    bool operator==(const Log2Inc left, const Log2Inc right);

    /**
     * Returns whether or not a Log2Inc value does not equal a double.
     *
     * @param left  The Log2Inc.
     * @param right The double.
     * @return      Whether or not the Log2Inc value does not equal the double.
     */
    bool operator!=(const Log2Inc left, const Log2Inc right);

    /**
     * Returns whether or not a Log2Inc value is less than a double.
     *
     * @param left  The Log2Inc.
     * @param right The double.
     * @return      Whether or not the Log2Inc value is less than the double.
     */
    bool operator<(const Log2Inc left, const Log2Inc right);

    /**
     * Returns whether or not a Log2Inc value is less than or equals a double.
     *
     * @param left  The Log2Inc.
     * @param right The double.
     * @return      Whether or not the Log2Inc value is less than or equals the
     *              double.
     */
    bool operator<=(const Log2Inc left, const Log2Inc right);

    /**
     * Returns whether or not a Log2Inc value is greater than a double.
     *
     * @param left  The Log2Inc.
     * @param right The double.
     * @return      Whether or not the Log2Inc value is greater than the double.
     */
    bool operator>(const Log2Inc left, const Log2Inc right);

    /**
     * Returns whether or not a Log2Inc value is greater than or equals a
     * double.
     *
     * @param left  The Log2Inc.
     * @param right The double.
     * @return      Whether or not the Log2Inc value is greater than or equals
     *              the double.
     */
    bool operator>=(const Log2Inc left, const Log2Inc right);

    /**
     * Output stream operator.
     * Appends the Log2Inc value to the output stream and
     * returns the output stream.
     *
     * @param os     The output stream.
     * @param source The Log2Inc to append to the output stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const Log2Inc& source);
}

#endif // LOG2INC_H
