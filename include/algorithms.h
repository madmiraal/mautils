// Useful template algorithms.

// (c) 2015 - 2017: Marcel Admiraal

#ifndef ALGORITHMS_H
#define ALGORITHMS_H

#define M_PI 3.14159265358979323846

#include <cstdlib>

namespace ma
{
    /**
     * Sorts an array of items that have the < operator defined using the
     * merge sort algorithm.
     *
     * @param unsorted  The original array.
     * @param sorted    The sorted array.
     * @param length    The length of the array.
     */
    template<typename T> void mergeSort(const T unsorted[], T sorted[],
            const unsigned int length);

    /**
     * Sorts an array of items that have the < operator defined using the
     * quick sort algorithm.
     *
     * @param contents  The array to be sorted.
     * @param length    The length of the array.
     */
    template<typename T> void quickSort(T contents[],
            const unsigned int length);

    /**
     * Returns the ith smallest element using the quick select algortihm.
     *
     * @param contents  The array of unsorted elements.
     * @param length    The length of the array.
     * @param k         The kth smallest element to find.
     * @return          The kth smallest element.
     */
    template<typename T> T quickSelect(const T contents[],
            const unsigned int length, const unsigned int k);

    /**
     * Returns the number of inversions in an array.
     * An inversion is when the elements are out of their natural order.
     *
     * @param contents  The array.
     * @param length    The length of the array.
     * @return          The number of inversions in the array.
     */
    template<typename T> unsigned int inversions(const T contents[],
            const unsigned int length);

    /**
     * Returns the number of inversions in an array.
     * Note: This is a helper function for the actual inversions function.
     *
     * @param unsorted  The array.
     * @param sorted    The array sorted.
     * @param length    The length of the array.
     * @return          The number of inversions in the array.
     */
    template<typename T> unsigned int inversions(const T unsorted[], T sorted[],
            const unsigned int length);

    /**
     * Swaps two values.
     *
     * @param first     The first value to swap.
     * @param second    The second value to swap.
     */
    template<typename T> void swapValues(T& first, T& second);

    /**
     * Compile-time series calculation of sine and cosine functions.
     */
    template<unsigned M, unsigned N, unsigned A, unsigned B> struct SinCosSeries;
    template<unsigned N, unsigned A, unsigned B> struct SinCosSeries <N, N, A, B>;

    /**
     * Compile-time calculation of sin(A * M_PI / B) function.
     */
    template<unsigned A, unsigned B> struct Sin;

    /**
     * Compile-time calculation of cos(A * M_PI / B) function.
     */
    template<unsigned A, unsigned B> struct Cos;

// Function definitions.

    template<typename T> void mergeSort(const T unsorted[], T sorted[],
            const unsigned int length)
    {
        if (length == 1)
        {
            sorted[0] = unsorted[0];
        }
        else
        {
            unsigned int lengthLeft = length / 2;
            unsigned int lengthRight = length - lengthLeft;
            T* sortedLeft = new T[lengthLeft]{};
            T* sortedRight = new T[lengthRight]{};
            mergeSort(unsorted, sortedLeft, lengthLeft);
            mergeSort(&unsorted[lengthLeft], sortedRight, lengthRight);
            unsigned int indexLeft = 0;
            unsigned int indexRight = 0;
            for (unsigned int index = 0; index < length; ++index)
            {
                if (indexRight == lengthRight ||
                    (indexLeft < lengthLeft &&
                     sortedLeft[indexLeft] < sortedRight[indexRight]))
                {
                    sorted[index] = sortedLeft[indexLeft];
                    ++indexLeft;
                }
                else
                {
                    sorted[index] = sortedRight[indexRight];
                    ++indexRight;
                }
            }
            delete[] sortedLeft;
            delete[] sortedRight;
        }
    }

    template<typename T> void quickSort(T contents[],
            const unsigned int length)
    {
        if (length <= 1) return;
        // Pick a random value to pivot around.
        unsigned int p = (unsigned int)(rand() % length);
        // Store the pivot value in the first slot.
        if (p != 0) swapValues(contents[0], contents[p]);
        // Set the pointer to the first value greater than the pivot value.
        unsigned int i = 1;
        // Check each value against the pivot.
        for (unsigned int j = 1; j < length; ++j)
        {
            // If the value is less than the pivot.
            if (contents[j] < contents[0])
            {
                // If necessary, move the value to the left.
                if (i != j) swapValues(contents[i], contents[j]);
                // and increment the pointer to the first value greater than.
                ++i;
            }
        }
        // If necessary move the pivot value to the sit in the middle.
        if (i != 1) swapValues(contents[0], contents[i - 1]);
        // Sort the lesser values.
        quickSort(contents, i-1);
        // Sort the greater values.
        quickSort(&contents[i], length - i);
    }

    template<typename T> T quickSelect(const T contents[],
            const unsigned int length, const unsigned int k)
    {
        if (length == 0) throw "Array has no elements.";
        if (length == 1) return contents[0];
        // Create a working array.
        T working[length];
        for (unsigned int i = 0; i < length; ++i) working[i] = contents[i];
        // Pick a random value to pivot around.
        unsigned int p = (unsigned int)(rand() % length);
        // Store the pivot value in the first slot.
        if (p != 0) swapValues(working[0], working[p]);
        // Set the pointer to the first value greater than the pivot value.
        unsigned int i = 1;
        // Check each value against the pivot.
        for (unsigned int j = 1; j < length; ++j)
        {
            // If the value is less than the pivot.
            if (working[j] < working[0])
            {
                // If necessary, move the value to the left.
                if (i != j) swapValues(working[i], working[j]);
                // and increment the pointer to the first value greater than.
                ++i;
            }
        }
        // If necessary move the pivot value to the sit in the middle.
        if (i != 1) swapValues(working[0], working[i - 1]);
        // If the pivot value is in the position required return it.
        if (i == k)
            return working[k - 1];
        // If the pivot value is greater search the smaller values.
        else if (i > k)
            return quickSelect(working, i - 1, k);
        // If the pivot value is smaller search the greater values.
        else // (i < k)
            return quickSelect(&working[i], length - i, k - i);
    }

    template<typename T> unsigned int inversions(const T contents[],
            const unsigned int length)
    {
        T* sorted = new T[length]{};
        unsigned int count = inversions(contents, sorted, length);
        delete[] sorted;
        return count;
    }

    template<typename T> unsigned int inversions(const T unsorted[], T sorted[],
            const unsigned int length)
    {
        if (length == 1)
        {
            sorted[0] = unsorted[0];
            return 0;
        }
        else
        {
            unsigned int lengthLeft = length / 2;
            unsigned int lengthRight = length - lengthLeft;
            T* sortedLeft = new T[lengthLeft]{};
            T* sortedRight = new T[lengthRight]{};
            unsigned int count = inversions(unsorted, sortedLeft, lengthLeft);
            count += inversions(&unsorted[lengthLeft], sortedRight, lengthRight);
            unsigned int indexLeft = 0;
            unsigned int indexRight = 0;
            for (unsigned int index = 0; index < length; ++index)
            {
                if (indexRight == lengthRight ||
                    (indexLeft < lengthLeft &&
                     sortedLeft[indexLeft] <= sortedRight[indexRight]))
                {
                    sorted[index] = sortedLeft[indexLeft];
                    ++indexLeft;
                }
                else
                {
                    sorted[index] = sortedRight[indexRight];
                    ++indexRight;
                    count += lengthLeft - indexLeft;
                }
            }
            delete[] sortedLeft;
            delete[] sortedRight;
            return count;
        }
    }

    template<typename T> void swapValues(T& first, T& second)
    {
        T temp = first;
        first = second;
        second = temp;
    }

    /**
     * Compile-time series calculation of sine and cosine functions.
     */
    template<unsigned M, unsigned N, unsigned A, unsigned B> struct SinCosSeries
    {
        static double value()
        {
            return 1 - (A * M_PI / B) * (A * M_PI / B) / M / (M+1) *
                    SinCosSeries<M+2, N, A, B>::value();
        }
    };
    template<unsigned N, unsigned A, unsigned B> struct SinCosSeries <N, N, A, B>
    {
        static double value() { return 1; }
    };

    /**
     * Compile-time calculation of sin(A * M_PI / B) function.
     */
    template<unsigned A, unsigned B> struct Sin
    {
        static double value()
        {
            return (A * M_PI / B) * SinCosSeries<2, 34, A, B>::value();
        }
    };

    /**
     * Compile-time calculation of cos(A * M_PI / B) function.
     */

    template<unsigned A, unsigned B> struct Cos
    {
        static double value()
        {
            return SinCosSeries<1, 33, A, B>::value();
        }
    };

}

#endif // ALGORITHMS_H
