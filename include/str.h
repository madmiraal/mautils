// A string class
//
// (c) 2016: Marcel Admiraal

#ifndef STR_H
#define STR_H

#include <iostream>

namespace ma
{
    class Str
    {
    public:
        /**
         * Default constructor.
         * Creates a zero length string.
         */
        Str();

        /**
         * Default destructor.
         */
        virtual ~Str();

        /**
         * Character array constructor.
         *
         * @param carray    The character array.
         */
        Str(const char* carray);

        /**
         * Buffer constructor.
         *
         * @param buffer    The character buffer.
         * @param length    The number of characters to copy.
         */
        Str(const char* buffer, const unsigned int length);

        /**
         * Copy constructor.
         *
         * @param source    The string to copy.
         */
        Str(const Str& source);

        /**
         * Substring constructor.
         *
         * @param source    The string to get a substring from.
         * @param first     The index of the first character.
         * @param length    The length of the string to create.
         */
        Str(const Str& source, const unsigned int first,
            const unsigned int length);

        /**
         * Fill constructor.
         *
         * @param length    The length of the string to create.
         * @param c         The character to fill the string with.
         */
        Str(const unsigned int length, const char c);

        /**
         * Swap function.
         *
         * @param first  The first string to swap with.
         * @param second The second string to swap with.
         */
        friend void swap(Str& first, Str& second);

        /**
         * Assignment operator.
         * Returns a copy of the string.
         *
         * @param source The string to copy.
         * @return       A copy of the string.
         */
        Str& operator=(Str source);

        /**
         * Array subscription operator:
         * Allows direct access to the string's characters.
         * Returns the specified character.
         *
         * @param index The 0 base character to retrieve.
         * @return      The specified character.
         */
        char& operator[](const unsigned int index);
        const char& operator[](const unsigned int index) const;

        /**
         * Plus equals operator.
         * Extends this string with another string.
         * Returns the extended string.
         *
         * @param source The string to append.
         * @return       The appended string.
         */
        Str& operator+=(const Str& source);

        /**
         * Plus equals operator.
         * Extends this string with the character.
         * Returns the extended string.
         *
         * @param character The character to append.
         * @return          The appended string.
         */
        Str& operator+=(const char& character);

        /**
         * Minus equals operator.
         * Removes all occurences of the string from this string.
         * Returns the edited string.
         *
         * @param source The string to remove.
         * @return       The edited string.
         */
        Str& operator-=(const Str& source);

        /**
         * Returns the length of the string.
         *
         * @return The length of the string.
         */
        unsigned int length() const;

        /**
         * Returns a character string of the string.
         *
         * @param first     The index of the first character.
         * @param length    The length of the substring to return.
         * @return          The substring requested.
         */
        char* c_str();
        const char* c_str() const;

        /**
         * Returns a substring of the string.
         *
         * @param first     The index of the first character.
         * @param length    The length of the substring to return.
         * @return          The substring requested.
         */
        Str subStr(const unsigned int first, const unsigned int length) const;

        /**
         * Returns a string consisting of the required first characters of the
         * string.
         *
         * @param length    The length of the substring to return.
         * @return          The substring requested.
         */
        Str left(const unsigned int length) const;

        /**
         * Returns a substring of the string without the specified first
         * characters.
         *
         * @param length    The number of the characters to remove.
         * @return          The substring requested.
         */
        Str trimLeft(const unsigned int length) const;

        /**
         * Returns a string consisting of the required last characters of the
         * string.
         *
         * @param length    The length of the substring to return.
         * @return          The substring requested.
         */
        Str right(const unsigned int length) const;

        /**
         * Returns a substring of the string without the specified last
         * characters.
         *
         * @param length    The number of the characters to remove.
         * @return          The substring requested.
         */
        Str trimRight(const unsigned int length) const;

        /**
         * Copies the required characters into the required position.
         *
         * @param source    The string to copy from.
         * @param first     The index of the first character to copy.
         * @param length    The number of characters to copy.
         * @param start     The index to start the copy to.
         * @return          The number of characters copied.
         */
        unsigned int copyStr(const Str& source, unsigned int first,
                unsigned int length, unsigned int start = 0);

        /**
         * Copies the required characters into the required position.
         *
         * @param source    The character array to copy from.
         * @param first     The index of the first character to copy.
         * @param length    The number of characters to copy.
         * @param start     The index to start the copy to.
         * @return          The number of characters copied.
         */
        unsigned int copyStr(const char* source, unsigned int first,
                unsigned int length, unsigned int start = 0);

        /**
         * Returns the index of the first character of the searched for string.
         *
         * @param source    The string to search for.
         * @param start     The index to start the search from.
         * @return          If found, the index of the first character of the
         *                  searched for string in this string,
         *                  if not found, -1 (the largest unsigned int value).
         */
        unsigned int find(const Str& source, unsigned int start = 0) const;

        /**
         * Replaces the first occurence of the first string with the second
         * string.
         *
         * @param remove    The string to replace.
         * @param insert    The string to replace with.
         * @param start     The index to start the search from.
         * @return          Whether or not the string was found.
         */
        bool replaceFirst(const Str& remove, const Str& insert,
                unsigned int start = 0);

        /**
         * Replaces all occurences of the first string with the second string.
         *
         * @param remove    The string to replace.
         * @param insert    The string to replace with.
         * @param start     The index to start the search from.
         * @return          The number of replacements.
         */
        unsigned int replaceAll(const Str& remove, const Str& insert,
                unsigned int start = 0);

        /**
         * Returns whether or not this is a representation of an integer.
         *
         * @return Whether or not this is a representation of an integer.
         */
        bool isInteger() const;

        /**
         * Returns whether or not this is a representation of a binary number.
         *
         * @return Whether or not this is a representation of a binary number.
         */
        bool isBinaryNumber() const;

        /**
         * Returns whether or not this is a representation of a hexadecimal
         * number.
         *
         * @return Whether or not this is a representation of a hexadecimal
         * number.
         */
        bool isHexNumber() const;

        /**
         * Converts this string representation of a binary number to the string
         * representation of its two's complement.
         *
         * @return Whether or not the conversion was successful.
         */
        bool twosComplement();

        /**
         * Converts this string representation of an integer to a long.
         *
         * @param long  The variable to place the conversion into.
         * @return      Whether or not the conversion was successful.
         */
         bool integerToLong(long& number) const;

        /**
         * Converts this string representation of a binary number to a long.
         *
         * @param str   The string to convert.
         * @param long  The variable to place the conversion into.
         * @return      Whether or not the conversion was successful.
         */
        bool binaryToLong(long& number) const;

        /**
         * Converts this string representation of an hex number to a long.
         *
         * @param long  The variable to place the conversion into.
         * @return      Whether or not the conversion was successful.
         */
         bool hexToLong(long& number) const;

    private:
        char* character;
        unsigned int len;
    };

    /**
     * Plus operator.
     * Returns the concantenation of two strings.
     *
     * @param first  The first string.
     * @param second The second string.
     * @return       The concantenated string.
     */
    Str operator+(const Str& first, const Str& second);

    /**
     * Plus operator.
     * Returns the concantenation of a string and a character array.
     *
     * @param first  The string.
     * @param second The character array.
     * @return       The concantenated string.
     */
    Str operator+(const Str& first, const char* second);

    /**
     * Plus operator.
     * Returns the concantenation of a character array and a string.
     *
     * @param first  The character array.
     * @param second The string.
     * @return       The concantenated string.
     */
    Str operator+(const char* first, const Str& second);

    /**
     * Minus operator.
     * Removes all occurences of the second string from the first string.
     * Returns the edited string.
     *
     * @param first     The string to edit.
     * @param second    The string to remove.
     * @return          The edited string.
     */
    Str operator-(const Str& first, const Str& second);

    /**
     * Minus operator.
     * Removes all occurences of the character array from a string.
     * Returns the edited string.
     *
     * @param first     The string to edit.
     * @param second    The character array to remove.
     * @return          The edited string.
     */
    Str operator-(const Str& first, const char* second);

    /**
     * Minus operator.
     * Removes all occurences of the character from a string.
     * Returns the edited string.
     *
     * @param first     The string to edit.
     * @param second    The character to remove.
     * @return          The edited string.
     */
    Str operator-(const Str& first, const char second);

    /**
     * Minus operator.
     * Removes all occurences of a string from a character array.
     * Returns the edited character array as a string.
     *
     * @param first     The character array to edit.
     * @param second    The string to remove.
     * @return          The edited character array as a string.
     */
    Str operator-(const char* first, const Str& second);

    /**
     * Equals operator.
     * Returns whether or not the strings are the same.
     *
     * @param left  The first string.
     * @param right The second string.
     * @return      True if the strings are of equal size and
     *              all the characters are the same.
     */
    bool operator==(const Str& left, const Str& right);

    /**
     * Not equals operator.
     * Returns whether or not the strings are not the same.
     *
     * @param left  The first string.
     * @param right The second string.
     * @return      True if the strings are not of equal size or
     *              at least one character is not equal.
     */
    bool operator!=(const Str& left, const Str& right);

    /**
     * Returns whether or not two character arrays are the same.
     *
     * @param left  The first character array.
     * @param right The second character array.
     * @return      True if the character arrays are of equal size and
     *              all the characters are the same.
     */
    bool isEqual(const char* left, const char* right);

    /**
     * Input stream operator.
     * Builds a string from an input stream and returns the input stream.
     *
     * @param is            The input stream.
     * @param destination   The string to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, Str& destination);

    /**
     * Output stream operator.
     * Appends the string to the output stream and
     * returns the output stream.
     *
     * @param os     The output stream.
     * @param source The string to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const Str& source);

    /**
     * Output stream operator.
     * Appends the second string to the first string and returns the
     * appended string.
     *
     * @param first     The first string.
     * @param second    The second string.
     * @return          The appended string.
     */
    Str& operator<<(Str& first, const Str& second);

    /**
     * Output stream operator.
     * Appends the character array to the string and returns the appended string.
     *
     * @param first     The string.
     * @param second    The character array.
     * @return          The appended string.
     */
    Str& operator<<(Str& first, const char* second);

    /**
     * Returns a substring of a string.
     *
     * @param source    The string to obtain the substring from.
     * @param first     The index of the first character.
     * @param length    The length of the substring to return.
     * @return          The substring requested.
     */
    Str subStr(const Str& source, const unsigned int first,
            const unsigned int length);

    /**
     * Returns a substring of a character array.
     *
     * @param source    The character array to obtain the substring from.
     * @param first     The index of the first character.
     * @param length    The length of the substring to return.
     * @return          The substring requested.
     */
    Str subStr(const char* source, const unsigned int first,
            const unsigned int length);

    /**
     * Returns a string consisting of the required first characters of a string.
     *
     * @param source    The string to obtain the substring from.
     * @param length    The length of the substring to return.
     * @return          The substring requested.
     */
    Str leftStr(const Str& source, const unsigned int length);

    /**
     * Returns a string consisting of the required first characters of a
     * character array.
     *
     * @param source    The character array to obtain the substring from.
     * @param length    The length of the substring to return.
     * @return          The substring requested.
     */
    Str leftStr(const char* source, const unsigned int length);

    /**
     * Returns a string without the required first characters of a string.
     *
     * @param source    The string to obtain the substring from.
     * @param length    The number of characters to remove.
     * @return          The substring requested.
     */
    Str trimLeftStr(const Str& source, const unsigned int length);

    /**
     * Returns a string without the required first characters of a
     * character array.
     *
     * @param source    The character array to obtain the substring from.
     * @param length    The number of characters to remove.
     * @return          The substring requested.
     */
    Str trimLeftStr(const char* source, const unsigned int length);
    /**
     * Returns a string consisting of the required last characters of a string.
     *
     * @param source    The string to obtain the substring from.
     * @param length    The length of the substring to return.
     * @return          The substring requested.
     */
    Str rightStr(const Str& source, const unsigned int length);

    /**
     * Returns a string consisting of the required last characters of a
     * character array.
     *
     * @param source    The character array to obtain the substring from.
     * @param length    The length of the substring to return.
     * @return          The substring requested.
     */
    Str rightStr(const char* source, const unsigned int length);

    /**
     * Returns a string without the required last characters of a string.
     *
     * @param source    The string to obtain the substring from.
     * @param length    The number of characters to remove.
     * @return          The substring requested.
     */
    Str trimRightStr(const Str& source, const unsigned int length);

    /**
     * Returns a string without the required last characters of a
     * character array.
     *
     * @param source    The character array to obtain the substring from.
     * @param length    The number of characters to remove.
     * @return          The substring requested.
     */
    Str trimRightStr(const char* source, const unsigned int length);

    /**
     * Copies the required characters from one string into another at the
     * required position.
     *
     * @param dest      The string to copy to.
     * @param source    The string to copy from.
     * @param first     The index of the first character to copy.
     * @param length    The number of characters to copy.
     * @param start     The index to start the copy to.
     * @return          The number of characters copied.
     */
    unsigned int copyStr(Str& dest, const Str& source, unsigned int first,
            unsigned int length, unsigned int start = 0);

    /**
     * Copies the required characters from a character array into a string at
     * the required position.
     *
     * @param dest      The string to copy to.
     * @param source    The character array to copy from.
     * @param first     The index of the first character to copy.
     * @param length    The number of characters to copy.
     * @param start     The index to start the copy to.
     * @return          The number of characters copied.
     */
    unsigned int copyStr(Str& dest, const char* source, unsigned int first,
            unsigned int length, unsigned int start = 0);

    /**
     * Extracts characters from an input stream into a string until the
     * delimiting character, and returns the input stream.
     *
     * @param is            The input stream to extract from.
     * @param destination   The string to extract to.
     * @param delim         The delimiting character. Defaults to new line.
     * @return              The input stream.
     */
    std::istream& getNext(std::istream& is, Str& destination,
            char delim = '\n');

    /**
     * Extracts characters from an input stream into a string until the
     * delimiting character, and returns them as a string.
     *
     * @param is            The input stream to extract from.
     * @param delim         The delimiting character. Defaults to new line.
     * @return              The string of characters.
     */
    Str getNext(std::istream& is, char delim = '\n');

    /**
     * Returns whether or not the string is a representation of an integer.
     *
     * @param str   The string to check.
     * @return      Whether or not the string is a representation of an integer.
     */
    bool isInteger(const Str& str);

    /**
     * Returns whether or not the string is a representation of a binary number.
     *
     * @param str   The string to check.
     * @return      Whether or not the string is a representation of a binary
     *              number.
     */
    bool isBinaryNumber(const Str& str);

    /**
     * Returns whether or not the string is a representation of a hexadecimal
     * number.
     *
     * @param str   The string to check.
     * @return      Whether or not the string is a representation of a
     *              hexadecimal number.
     */
    bool isHexNumber(const Str& str);

    /**
     * Converts a string representation of a binary number to the string
     * representation of its two's complement.
     *
     * @param binaryNumber      The string to convert.
     * @param negativeBinary    The string to place the conversion into.
     * @return Whether or not the conversion was successful.
     */
    bool twosComplement(const Str& binaryStr, Str& negativeStr);

    /**
     * Converts this string representation of a binary number to the string
     * representation of a hexadecimal number.
     *
     * @return Whether or not the conversion was successful.
     */
    bool binaryToHex(const Str& binaryStr, Str& hexStr, bool lowerCase = false);

    /**
     * Converts this string representation of a hexadecimal number to the
     * string representation of a decimal number.
     *
     * @return Whether or not the conversion was successful.
     */
    bool hexToBinary(const Str& hexStr, Str& binaryStr);

    /**
     * Converts a string representation of an integer to a long.
     *
     * @param str   The string to convert.
     * @param long  The variable to place the conversion into.
     * @return      Whether or not the conversion was successful.
     */
    bool integerToLong(const Str& integerStr, long& number);

    /**
     * Converts a string representation of a binary number to a long.
     *
     * @param str   The string to convert.
     * @param long  The variable to place the conversion into.
     * @return      Whether or not the conversion was successful.
     */
    bool binaryToLong(const Str& binaryStr, long& number);

    /**
     * Converts a string representation of an hex number to a long.
     *
     * @param str   The string to convert.
     * @param long  The variable to place the conversion into.
     * @return      Whether or not the conversion was successful.
     */
    bool hexToLong(const Str& hexStr, long& number);

    /**
     * Converts an long integer to a string.
     *
     * @param number    The number to convert.
     * @return          The number as a string.
     */
    Str toStr(const long number);

    /**
     * Converts a long to a string representation of the binary number.
     *
     * @param long  The long to convert.
     * @return      A string representation of the binary number.
     */
    Str toBinary(const long number, bool prefix = true);

    /**
     * Converts a long to a string representation of the hex number.
     *
     * @param long  The long to convert.
     * @return      A string representation of the hex number.
     */
    Str toHex(const long number, bool prefix = true, bool lowerCase = false);

    /**
     * Returns whether or not a character is a decimal digit.
     *
     * @param character The character to check.
     * @return          Whether or not the character is a decimal digit.
     */
    bool isDigit(const char character);

    /**
     * Returns whether or not a character is a binary digit.
     *
     * @param character The character to check.
     * @return          Whether or not the character is a binary digit.
     */
    bool isBinaryDigit(const char character);

    /**
     * Returns whether or not a character is a hexadecimal digit.
     *
     * @param character The character to check.
     * @return          Whether or not the character is a hexadecimal digit.
     */
    bool isHexDigit(const char character);
}

#endif // STR_H
