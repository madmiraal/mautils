// A simple iterator template class
//
// (c) 2014: Marcel Admiraal

#ifndef ITERATOR_H
#define ITERATOR_H

#include "linkednode.h"

namespace ma
{
    template <class T> class Iterator
    {
    public:
        /**
         * Constructor for an iterator of linked nodes.
         *
         * @param node  The initial node to point to.
         */
        Iterator(LinkedNode<T>* node);

        /**
         * Copy constructor.
         *
         * @param source    The iterator to copy.
         */
        Iterator(const Iterator<T>& source);

        /**
         * Default destructor.
         */
        virtual ~Iterator();

        /**
         * Swap function.
         *
         * @param first     The first iterator to swap with.
         * @param second    The second iterator to swap with.
         */
        template <class S> friend void swap(
            Iterator<S>& left, Iterator<S>& right);

        /**
         * Equals operator.
         *
         * @param first     The first iterator to compare.
         * @param second    The second iterator to compare.
         * @return          Whether or not both iterators point to the same node.
         */
        template <class S> friend bool operator==(const Iterator<S>& left,
            const Iterator<S> & right);

        /**
         * Assignment operator.
         * Returns a copy of the iterator.
         *
         * @param source The iterator to copy.
         * @return       A copy of the iterator.
         */
        Iterator<T>& operator=(Iterator<T> source);

        /**
         * Prefix increment operator.
         * Moves the iterator to the next node and returns the iterator.
         *
         * @return  The iterator.
         */
        Iterator<T>& operator++();

        /**
         * Postfix increment operator.
         * Returns the iterator and then moves the iterator to the next node.
         *
         * @return  The iterator.
         */
        Iterator<T> operator++(int);

        /**
         * Prefix decrement operator.
         * Moves the iterator to the previous node and returns the iterator.
         *
         * @return  The iterator.
         */
        Iterator<T>& operator--();

        /**
         * Postfix decrement operator.
         * Returns the iterator and then moves the iterator to the previous node.
         *
         * @return  The iterator.
         */
        Iterator<T> operator--(int);

        /**
         * Dereference operator.
         * Returns the current node's data.
         *
         * @return The current item.
         */
        T operator*() const;

        /**
         * Returns whether or not the iterator is valid.
         *
         * @return Whether or not the iterator is valid.
         */
        bool valid() const;

        /**
         * Returns whether or not there is a next item.
         *
         * @return Whether or not there is a next item.
         */
        bool hasNext() const;

        /**
         * Moves the pointer to the next node and returns the node's data.
         *
         * @return The next item's data.
         */
        T getNext();

        /**
         * Returns whether or not there is a previous item.
         *
         * @return Whether or not there is a previous item.
         */
        bool hasPrevious() const;

        /**
         * Moves the pointer to the previous node and returns the node's data.
         *
         * @return The previous item's data.
         */
        T getPrevious();

        /**
         * Returns the current node's data.
         *
         * @return The current node's data.
         */
        T getData() const;

        /**
         * Sets the current node's data.
         *
         * @param The new data to be associated with the node.
         */
        void setData(const T& data);

    private:
        LinkedNode<T>* pointer;
    };

    /**
     * Not equals operator.
     *
     * @param first     The first iterator to compare.
     * @param second    The second iterator to compare.
     * @return          Whether or not iterators point to different nodes.
     */
    template <class T> bool operator!=(const Iterator<T>& left,
            const Iterator<T> & right);

// Function definitions.

    template <class T> Iterator<T>::Iterator(LinkedNode<T>* node) :
        pointer(node)
    {
    }

    template <class T> Iterator<T>::Iterator(const Iterator<T>& source) :
        pointer(source.pointer)
    {
    }

    template <class T> Iterator<T>::~Iterator()
    {
    }

    template <class T> void swap(Iterator<T>& first, Iterator<T>& second)
    {
        std::swap(first.pointer, second.pointer);
    }

    template <class T> Iterator<T>& Iterator<T>::operator=(Iterator<T> source)
    {
        swap(*this, source);
        return *this;
    }

    template <class T> Iterator<T>& Iterator<T>::operator++()
    {
        pointer = pointer->getNext();
        return *this;
    }

    template <class T> Iterator<T> Iterator<T>::operator++(int)
    {
        Iterator<T> temp(*this);
        ++(*this);
        return temp;
    }

    template <class T> Iterator<T>& Iterator<T>::operator--()
    {
        pointer = pointer->getPrevious();
        return *this;
    }

    template <class T> Iterator<T> Iterator<T>::operator--(int)
    {
        Iterator<T> temp(*this);
        --(*this);
        return temp;
    }

    template <class T> T Iterator<T>::operator*() const
    {
        T data = getData();
        return data;
    }

    template <class T> bool Iterator<T>::valid() const
    {
        return (pointer != 0);
    }

    template <class T> bool Iterator<T>::hasNext() const
    {
        return (pointer->getNext() != 0);
    }

    template <class T> T Iterator<T>::getNext()
    {
        pointer = pointer->getNext();
        T data = getData();
        return data;
    }

    template <class T> bool Iterator<T>::hasPrevious() const
    {
        return (pointer->getPrevious() != 0);
    }

    template <class T> T Iterator<T>::getPrevious()
    {
        pointer = pointer->getPrevious();
        T data = getData();
        return data;
    }

    template <class T> T Iterator<T>::getData() const
    {
        T data = pointer->getData();
        return data;
    }

    template <class T> void Iterator<T>::setData(const T& data)
    {
        pointer->setData(data);
    }

    template <class T> bool operator==(const Iterator<T>& left,
            const Iterator<T> & right)
    {
        return (left.pointer == right.pointer);
    }

    template <class T> bool operator!=(const Iterator<T>& left,
            const Iterator<T> & right)
    {
        return !(left == right);
    }
}

#endif // ITERATOR_H
