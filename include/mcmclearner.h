// A Markov Chain Monte Carlo learning algorithm.

// (c) 2014 - 2017: Marcel Admiraal

#ifndef MCMCLEARNER_H
#define MCMCLEARNER_H

#include "learner.h"
#include "map.h"
#include "vector.h"

#include <tuple>

namespace ma
{
    class MCMCLearner : public Learner
    {
    public:
        /**
         * Constructor.
         *
         * @param stateLength   The length of the state vector. Default: 2.
         * @param outputLength  The length of the output vector. Default: 1.
         * @param learnRate     The learning rate. Default: 0.1.
         * @param discountRate  The reward discount rate. Default: 1.0.
         * @param defaultValue  The default vector values. Default: 0.5.
         */
        MCMCLearner(const unsigned int stateLength = 2,
                const unsigned int outputLength = 1,
                const double learnRate = 0.1,
                const double discountRate = 1.0,
                const double defaultValue = 0.5);

        /**
         * File constructor.
         *
         * @param filename  The filename to load the learner from.
         */
        MCMCLearner(const char* filename);

        /**
         * Copy constructor.
         *
         * @param source    The learner to copy.
         */
        MCMCLearner(const MCMCLearner& source);

        /**
         * Default destructor.
         */
        virtual ~MCMCLearner();

        /**
         * Swap function.
         *
         * @param first  The first learner to swap with.
         * @param second The second learner to swap with.
         */
        friend void swap(MCMCLearner& first, MCMCLearner& second);

        /**
         * Assignment operator.
         * Returns a copy of the learner.
         *
         * @param source The learner to copy.
         * @return       A copy of the learner.
         */
        MCMCLearner& operator=(MCMCLearner source);

        /**
         * Saves the current learner to a file with the given filename.
         *
         * @param filename  The filename to save the learner to.
         * @return          Return true if the save was successful.
         */
        virtual bool save(const char* filename) const;

        /**
         * Loads a previously saved learner from a file with the given filename.
         *
         * @param filename  The filename to save the learner to.
         * @return          Return true if the load was successful.
         */
        virtual bool load(const char* filename);

        /**
         * Returns the current value of the state.
         *
         * @param state A vector representation of the state.
         * @return      The learner's current value of the state.
         */
        virtual Vector getStateValue(const Vector& state);

        /**
         * Inform the learner of the next state in a sequence, and any interim
         * reward received.
         *
         * @param state     The next state in the sequence.
         * @param reward    Interim reward. Defaults to zero.
         */
        virtual void nextState(const Vector& state,
                const Vector& reward = Vector());

        /**
         * Inform the learner of the final reward received at the end of a
         * sequence.
         *
         * @param reward    The final reward received.
         */
        virtual void finalReward(const Vector& reward);

        /**
         * Inform the learner to discard the current sequence, because
         * squence terminated before a final reward was received.
         */
        virtual void sequenceReset();

        /**
         * Adjusts the input and output state vector lengths.
         *
         * @param stateLength   The new state vector length.
         * @param outputLength  The new output vector length.
         */
        void adjustLearner(const unsigned int stateLength,
                const unsigned int outputLength);

        /**
         * Returns the state value map.
         *
         * @return The state value map.
         */
        Map<Vector, Vector> getStateValueMap() const;

        /**
         * Sets the state value map.
         *
         * @param newRate   The new state value map.
         */
        void setStateValueMap(Map<Vector, Vector> stateValue);

        /**
         * Returns the current default value.
         *
         * @return The current default value.
         */
        Vector getDefaultValue() const;

        /**
         * Sets the default value vector.
         *
         * @param defaultValue  The value to fill the default value vector with.
         *                      Defaults to 0.5.
         */
        void setDefaultValue(const double defaultValue = 0.5);

        /**
         * Sets the default value vector.
         *
         * @param defaultValue  The new default value vector.
         */
        void setDefaultValue(const Vector& defaultValue);

    private:
        // Updates the sequence rewards.
        void updateRewards(const Vector& reward);

        // The state id value pairs.
        Map<Vector, Vector> stateValue;
        // The default value if no state value pair found.
        Vector defaultValue;

        // The states and rewards encountered in the current sequence.
        List<std::tuple<Vector, Vector> > sequence;
    };

    /**
     * Input stream operator.
     * Builds a learner from the input stream and returns the input stream.
     *
     * @param is            The input stream.
     * @param destination   The learner to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, MCMCLearner& destination);

    /**
     * Output stream operator.
     * Appends the learner to the output stream and returns the output stream.
     *
     * @param os     The output stream.
     * @param source The learner to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const MCMCLearner& source);
}

#endif //MCMCLEARNER_H
