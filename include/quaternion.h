// A quaternion class
// i^2 = j^2 = k^2 = -1
//
// (c) 2011 - 2014: Marcel Admiraal

#ifndef QUATERNION_H
#define QUATERNION_H

#include "vector3d.h"
#include "matrix.h"

#include <string>
#include <iostream>

namespace ma
{
    class Quaternion
    {
    public:
        /**
         * Default constructor.
         * Creates a zero.
         */
        Quaternion();

        /**
         * Scalar constructor.
         *
         * @param a The real number.
         */
        Quaternion(const double a);

        /**
         * Doubles constructor.
         *
         * @param w The real part of this quaternion.
         * @param x The <b>i</b> part of this quaternion.
         * @param y The <b>j</b> part of this quaternion.
         * @param z The <b>k</b> part of this quaternion.
         */
        Quaternion(
            const double w,
            const double x,
            const double y,
            const double z);

        /**
         * Euler angle constructor.
         *
         * @param yaw   Rotation around the z axis (heading) then
         * @param pitch Rotation around the y axis (elevation) then
         * @param roll  Rotation around the x axis (bank).
         */
        Quaternion(double yaw, double pitch, double roll);

        /**
         * Vector constructor.
         * Sets the real part to zero.
         *
         * @param v The three-dimensional vector to be converted
         *          into a quaternion.
         */
        Quaternion(const Vector3D& v);

        /**
         * Scalar, vector constructor.
         *
         * @param w The real number part of this quaternion.
         * @param v The three-dimensional vector part of this quaternion.
         */
        Quaternion(const double w, const Vector3D& v);

        /**
         * Vector rotation constructor.
         *
         * @param v     The vector axis of rotation.
         * @param theta The clockwise (right-hand) rotation angle.
         */
        Quaternion(const Vector3D& v, const double theta);

        /**
         * Polar constructor.
         *
         * q = |q|e<sup>[0,θ<b>u</b>]</sup><br />
         * q = |q|(cos(θ), sin(θ)<b>u</b>)
         *
         * @param abs   The absolute size or norm of this quaternion: |q|
         * @param u     The unit vector of this quaternion.
         * @param arg   The argument or angle of the unit vector with the real.
         */
        Quaternion(const double abs, const Vector3D& u, const double arg);

        /**
         * Copy constructor.
         *
         * @param source    The quaternion to copy.
         */
        Quaternion(const Quaternion& source);

        /**
         * Swap function.
         *
         * @param first  The first quaternion to swap with.
         * @param second The second quaternion to swap with.
         */
        friend void swap(Quaternion& first, Quaternion& second);

        /**
         * Assignment operator.
         * Returns a copy of the quaternion.
         *
         * @param source The quaternion to copy.
         * @return       A copy of the quaternion.
         */
        Quaternion& operator=(Quaternion source);

        /**
         * Returns the real number part of this quaterion.
         *
         * @return The real number associated with this quaterion.
         */
        double getW() const;

        /**
         * Returns the i part of this quaterion.
         *
         * @return The i dimension associated with this quaterion.
         */
        double getX() const;

        /**
         * Returns the j part of this quaterion.
         *
         * @return The j dimension associated with this quaterion.
         */
        double getY() const;

        /**
         * Returns the k part of this quaterion.
         *
         * @return The k dimension associated with this quaterion.
         */
        double getZ() const;

        /**
         * Sets the real number part of this quaterion.
         *
         * @param The real number to be associated with this quaterion.
         */
        void setW(const double w);

        /**
         * Sets the i part of this quaterion.
         *
         * @param The i dimension to be associated with this quaterion.
         */
        void setX(const double x);

        /**
         * Sets the j part of this quaterion.
         *
         * @param The j dimension to be associated with this quaterion.
         */
        void setY(const double y);

        /**
         * Sets the k part of this quaterion.
         *
         * @param The k dimension to be associated with this quaterion.
         */
        void setZ(const double z);

        /**
         * Returns the real number part of this quaterion.
         *
         * @return The real number associated with this quaterion.
         */
        double getScalar() const;

        /**
         * Returns the vector part of this quaterion.
         *
         * @return The vector associated with this quaterion.
         */
        Vector3D getVector() const;

        /**
         * Gets the absolute size of the quaternion a.k.a. the norm.
         *
         * @return The absolute size of the quaternion.
         */
        double abs() const;

        /**
         * Returns the argument or "angle" of the vector part from the real.
         *
         * @return The versor associated with this quaternion.
         */
        double getArg() const;

        /**
         * Returns the rotation angle of this rotation quaternion.
         *
         * @return The rotation angle of this rotation quaternion.
         */
        double getAngle() const;

        /**
         * Returns the unit quaternion or versor of this quaternion.
         *
         * @return The unit quaternion (or versor) of this quaternion.
         */
        Quaternion unitQuaternion() const;

        /**
         * Returns the unit quaternion or versor of this quaternion using a
         * quick approximation of the inverse square root.
         *
         * @return The unit quaternion (or versor) of this quaternion.
         */
        Quaternion fastUnitQuaternion() const;

        /**
         * Returns whether or not this quaternion is a right quaternion.
         * A right quaternion has an angle of pi/2.
         * A right quaternion has a real number component of 0.
         *
         * @return True if this quaternion is a right quaternion.
         */
        bool isRight() const;

        /**
         * Addition equals operator.
         * Adds the quaternion to this quaternion, and returns the sum.
         *
         * @param right The quaternion to add to this quaternion.
         * @return      The sum of the two quaternions.
         */
        Quaternion& operator+=(const Quaternion& right);

        /**
         * Addition equals double operator.
         * Adds the real number to this quaternion, and returns the sum.
         *
         * @param right The real number to add.
         * @return      The sum of the quaternion and the double.
         */
        Quaternion& operator+=(const double right);

        /**
         * Subtraction equals operator.
         * Subtracts the quaterion from this quaternion,
         * and returns the differnce.
         *
         * @param right The quaternion to subtract.
         * @return      The difference between the two quaternions.
         */
        Quaternion& operator-=(const Quaternion& right);

        /**
         * Subtraction equals double operator.
         * Subtracts the real number from this quaternion,
         * and returns the difference.
         *
         * @param right The real number to subtract.
         * @return      The difference betwen the quaternion and the double.
         */
        Quaternion& operator-=(const double right);

        /**
         * Multiplication equals operator.
         * Multiplies this quaternion with the right quaterion
         * using a right-handed co-ordinate system,
         * and returns the product.
         *
         * @param right The quaternion to multiply by.
         * @return      The product of the two quaternions.
         */
        Quaternion& operator*=(const Quaternion& right);

        /**
         * Multiplication equals double operator.
         * Multiplies this quaternion with the real number,
         * and returns the product.
         *
         * @param right The real number to multiply by.
         * @return      The product of the quaternion and the double.
         */
        Quaternion& operator*=(const double right);

        /**
         * Division equals operator.
         * Divides this quaternion by the right quaternion,
         * and returns the quotient.
         *
         * @param right The quaternion divisor.
         * @return      The quotient of the two quaternions.
         */
        Quaternion& operator/=(const Quaternion& right);

        /**
         * Division double operator.
         * Divides this quaternion by the real number,
         * and returns the quotient.
         *
         * @param right The real number divisor.
         * @return      The quotient of the quaternion and the real number.
         */
        Quaternion& operator/=(const double right);

        /**
         * Returns the quaternion's conjugate: q<sup>*</sup>. <p />
         * Eqivalent to multiplying the vector component by -1.
         *
         * @return The quaternion's conjugate.
         */
        Quaternion conjugate() const;

        /**
         * Returns the quaternion's recipricol: q<sup>-1</sup>.
         *
         * @return The quaternion's recipricol.
         */
        Quaternion recipricol() const;

        /**
         * Returns the dot product of this quaternion and the right quaternion.
         * Returns the four-dimensional vector dot product of the two quaterions.
         *
         * @param right The other quaternion.
         * @return      The dot product.
         */
        double dot(const Quaternion& right) const;

        /**
         * Returns e<sup>q</sup>, where q is this quaterion.<p />
         *
         * By definition e<sup>q</sup> = Σq<sup>n</sup>&#47;n!<p />
         *
         * For q = (w,x,y,z) = (w,0,0,0) + (0,x,y,z) or w + [0,<b>v</b>]<br />
         * &nbsp;&nbsp;e<sup>q</sup> = e<sup>w + [0,<b>v</b>]</sup> =
         * e<sup>w</sup>e<sup>[0,<b>v</b>]</sup><br />
         * With <b>v</b> = s<b>u</b>, where s = |v| and <b>u</b> = <b>v</b>&#47;s<br />
         * Since <b>u</b><sup>n</sup> = (-1)<sup>n</sup> we get:<br />
         * &nbsp;&nbsp;[0,s<b>u</b>]<sup>0</sup> = [1,<b>0</b>],<br />
         * &nbsp;&nbsp;[0,s<b>u</b>]<sup>1</sup> = [0,s<b>u</b>],<br />
         * &nbsp;&nbsp;[0,s<b>u</b>]<sup>2</sup> = [-s<sup>2</sup>,<b>0</b>],<br />
         * &nbsp;&nbsp;[0,s<b>u</b>]<sup>3</sup> = [0,-s<sup>3</sup><b>u</b>],...<br />
         * In general:<br />
         * &nbsp;&nbsp;[0,s<b>u</b>]<sup>2n</sup> = [(-1)<sup>n</sup>s<sup>2n</sup>,<b>0</b>],<br />
         * &nbsp;&nbsp;[0,s<b>u</b>]<sup>2n+1</sup> = [0,(-1)<sup>n</sup>s<sup>2n+1</sup><b>u</b>],<br />
         * So:<br/>
         * &nbsp;&nbsp;e<sup>[0,<b>v</b>]</sup> = [Σ(-1)<sup>n</sup>s<sup>2n</sup>&#47;2n! , Σ(-1)<sup>n</sup>s<sup>2n+1</sup>&#47;(2n+1)! <b>u</b>]<br />
         * Which is equivalent to:<br />
         * &nbsp;&nbsp;e<sup>[0,<b>v</b>]</sup> = [cos(s), sin(s) <b>u</b>]<br />
         * Therefore:<br />
         * &nbsp;&nbsp;e<sup>q</sup> = e<sup>w</sup>[cos(s), sin(s) <b>u</b>]
         *
         * @return e<sup>q</sup>
         */
        Quaternion exp() const;

        /**
         * Returns ln(q), where q is this quaterion.<p />
         *
         * For q = |q|e<sup>[0,θ<b>u</b>]</sup><br />
         * ln(q) = ln(|q|)+[0,θ<b>u</b>] = [ln(q),θ<b>u</b>]
         *
         * @return ln(q)
         */
        Quaternion ln() const;

        /**
         * Returns this quaternion raised to a real power.<p />
         *
         * &nbsp;&nbsp;q<sup>n</sup>
         *
         * @param n The power to raise this quaternion by.
         * @return  This quaternion raised to the power.
         */
        Quaternion pow(const double n) const;

        /**
         * Returns this quaternion raised by another quaternion.<p />
         *
         * &nbsp;&nbsp;q<sup>n</sup>
         *
         * @param n The power to raise this quaternion by.
         * @return  This quaternion raised to the power.
         */
        Quaternion pow(const Quaternion& power) const;

        /**
         * Returns a rotation matrix for the rotation quaternion.
         *
         * @return A rotation matrix for the rotation quaternion.
         */
        Matrix getRotationMatrix();

    private:
        double w, x, y, z;
    };

    /**
     * Returns the unit quaternion or versor of the quaternion.
     *
     * @param source    The quaternion to obtain the unit quaternion from.
     * @return The unit quaternion (or versor) of this quaternion.
     */
    Quaternion unitQuaternion(const Quaternion& source);

    /**
     * Returns the unit quaternion or versor of this quaternion using a
     * quick approximation of the inverse square root.
     *
     * @param source    The quaternion to obtain the unit quaternion from.
     * @return The unit quaternion (or versor) of this quaternion.
     */
    Quaternion fastUnitQuaternion(const Quaternion& source);

    /**
     * Addition operator.
     * Returns the sum of the two quaternions.
     *
     * @param  left  The first quaternion to add.
     * @param  right The second quaternion to add.
     * @return       The sum of the two quaternions.
     */
    Quaternion operator+(Quaternion left, const Quaternion& right);

    /**
     * Vector scalar addition operator.
     * Returns the quaternion created when adding the vector to the scalar.
     *
     * @param left  The vector that the scalar is added to.
     * @param right The scalar that is added to the vector.
     * @return      The quaternion created when adding the scalar to the vector.
     */
    Quaternion operator+(const Vector3D& left, const double right);

    /**
     * Scalar vector addition operator.
     * Returns the quaternion created when adding the scalar to the vector.
     *
     * @param left  The vector that the scalar is added to.
     * @param right The scalar that is added to the vector.
     * @return      The quaternion created when adding the scalar to the vector.
     */
    Quaternion operator+(const double left, const Vector3D& right);

    /**
     * Quaternion double addition operator.
     * Returns the sum of the quaternion and the real number.
     *
     * @param  left  The quaternion to add.
     * @param  right The real number to add.
     * @return       The sum of the quaternion and the real number.
     */
    Quaternion operator+(Quaternion left, const double right);

    /**
     * Double quaternion addition operator.
     * Returns the sum of the real number and the quaternion.
     *
     * @param  left  The real number to add.
     * @param  right The quaternion to add.
     * @return       The sum of the real number and the quaternion.
     */
    Quaternion operator+(const double, Quaternion right);

    /**
     * Subtraction operator.
     * Returns the difference between the two quaternions.
     *
     * @param  left  The quaternion to subtract from.
     * @param  right The quaternion to subtract.
     * @return       The difference between the two quaternions.
     */
    Quaternion operator-(Quaternion left, const Quaternion& right);

    /**
     * Negative operator.
     * Returns the negative quaternion.
     *
     * @param  source The quaternion to form the negative of.
     * @return        The negative quaternion.
     */
    Quaternion operator-(const Quaternion& source);

    /**
     * Vector scalar subtaction operator.
     * Returns the quaternion created when the scalar is subtracted from
     * the vector.
     *
     * @param left  The vector that the scalar is subtracted from.
     * @param right The scalar that is subtracted from the vector.
     * @return      The quaternion created when the scalar is subtracted from
     *              the vector.
     */
    Quaternion operator-(const Vector3D& left, const double right);

    /**
     * Scalar vector subtraction operator.
     * Returns the quaternion created when the vector is subtracted from
     * the scalar.
     *
     * @param left  The scalar that is the vector is subtracted from.
     * @param right The vector that is subtracted from the vector.
     * @return      The quaternion created when the vector is subtracted from
     *              the scalar.
     */
    Quaternion operator-(const double left, const Vector3D& right);

    /**
     * Quaternion double subtraction operator.
     * Returns the difference between the quaternion and the real number.
     *
     * @param  left  The quaternion to subtract from.
     * @param  right The real number to subtract.
     * @return       The difference between the quaternion and the real number.
     */
    Quaternion operator-(Quaternion left, const double right);

    /**
     * Double quaternion subtraction operator.
     * Returns the difference between the real number quaternion and
     * the quaternion.
     *
     * @param  left  The real number to subtract from.
     * @param  right The quaternion to subtract.
     * @return       The difference between the real number quaternion and
     *               the quaternion.
     */
    Quaternion operator-(const double left, const Quaternion& right);

    /**
     * Multiplication operator.
     * Returns the product of the two quaternions.
     *
     * @param  left  The first quaternion to multiply.
     * @param  right The second quaternion to multiply.
     * @return       The product of the two quaternions.
     */
    Quaternion operator*(Quaternion left, const Quaternion& right);

    /**
     * Quaternion double multiplication operator.
     * Returns the quaternion scaled by the real number.
     *
     * @param  left  The quaternion to scale.
     * @param  right The real number to scale the quaternion by.
     * @return       The quaternion scaled by the real number.
     */
    Quaternion operator*(Quaternion left, const double right);

    /**
     * Double quaternion multiplication operator.
     * Returns the quaternion scaled by the real number.
     *
     * @param  left  The real number to scale the quaterinion by.
     * @param  right The quaternion to scale.
     * @return       The quaternion scaled by the real number.
     */
    Quaternion operator*(const double left, Quaternion right);

    /**
     * Division operator.
     * Returns the quotient of the two quaternions.
     *
     * @param  left  The quaternion dividend.
     * @param  right The quaternion divisor.
     * @return       The quotient of the two quaternions.
     */
    Quaternion operator/(Quaternion left, const Quaternion& right);

    /**
     * Quaternion double division operator.
     * Returns the quaternion reduced by the real number.
     *
     * @param  left  The quaternion to reduce.
     * @param  right The real number to reduce the quaternion by.
     * @return       The quaternion reduced by the real number.
     */
    Quaternion operator/(Quaternion left, const double right);

    /**
     * Double quaternion division operator.
     * Returns the quaternion quotient of the real number divided by
     * the quaternion.
     *
     * @param  left  The real number dividend.
     * @param  right The quaternion divisor.
     * @return       The quaternion quotient of the real number divided by
     * the quaternion.
     */
    Quaternion operator/(const double left, const Quaternion& right);

    /**
     * Equals operator.
     * Returns whether or not the quaternions are equal.
     *
     * @param left  The first quaternion to compare.
     * @param right The second quaternion to compare.
     * @return      Whether or not the quaternions are equal.
     */
    bool operator==(const Quaternion& left, const Quaternion& right);

    /**
     * Not equals operator.
     * Returns whether or not the quaternions are not equal.
     *
     * @param left  The first quaternion to compare.
     * @param right The second quaternion to compare.
     * @return      Whether or not the quaternions are not equal.
     */
    bool operator!=(const Quaternion& left, const Quaternion& right);

    /**
     * Returns whether or not the quaternions are approximately equal.
     *
     * @param left  The first quaternion to compare.
     * @param right The second quaternion to compare.
     * @param error The percentage error allowed. Defaults to 0.01%
     * @return      True if all components are are within the percentage error.
     */
    bool approxEqual(const Quaternion& left, const Quaternion& right,
            const double error = 0.01);

    /**
     * Output stream operator.
     * Appends the quaternion to the output stream and
     * returns the output stream.
     *
     * @param os     The output stream.
     * @param source The quaternion to append to the output stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const Quaternion& source);

    /**
     * Input stream operator.
     * Inputs a quaternion into the destination (if possible) and
     * returns the input stream.
     *
     * @param is          The input stream.
     * @param destination The quaternion to input into.
     * @return            The input stream.
     */
    std::istream& operator>>(std::istream& is, Quaternion& destination);

    /**
     * Returns e<sup>q</sup>, where q is this quaterion.<p />
     *
     * By definition e<sup>q</sup> = Σq<sup>n</sup>&#47;n!<p />
     *
     * For q = (w,x,y,z) = (w,0,0,0) + (0,x,y,z) or w + [0,<b>v</b>]<br />
     * &nbsp;&nbsp;e<sup>q</sup> = e<sup>w + [0,<b>v</b>]</sup> =
     * e<sup>w</sup>e<sup>[0,<b>v</b>]</sup><br />
     * With <b>v</b> = s<b>u</b>, where s = |v| and <b>u</b> = <b>v</b>&#47;s<br />
     * Since <b>u</b><sup>n</sup> = (-1)<sup>n</sup> we get:<br />
     * &nbsp;&nbsp;[0,s<b>u</b>]<sup>0</sup> = [1,<b>0</b>],<br />
     * &nbsp;&nbsp;[0,s<b>u</b>]<sup>1</sup> = [0,s<b>u</b>],<br />
     * &nbsp;&nbsp;[0,s<b>u</b>]<sup>2</sup> = [-s<sup>2</sup>,<b>0</b>],<br />
     * &nbsp;&nbsp;[0,s<b>u</b>]<sup>3</sup> = [0,-s<sup>3</sup><b>u</b>],...<br />
     * In general:<br />
     * &nbsp;&nbsp;[0,s<b>u</b>]<sup>2n</sup> = [(-1)<sup>n</sup>s<sup>2n</sup>,<b>0</b>],<br />
     * &nbsp;&nbsp;[0,s<b>u</b>]<sup>2n+1</sup> = [0,(-1)<sup>n</sup>s<sup>2n+1</sup><b>u</b>],<br />
     * So:<br/>
     * &nbsp;&nbsp;e<sup>[0,<b>v</b>]</sup> = [Σ(-1)<sup>n</sup>s<sup>2n</sup>&#47;2n! , Σ(-1)<sup>n</sup>s<sup>2n+1</sup>&#47;(2n+1)! <b>u</b>]<br />
     * Which is equivalent to:<br />
     * &nbsp;&nbsp;e<sup>[0,<b>v</b>]</sup> = [cos(s), sin(s) <b>u</b>]<br />
     * Therefore:<br />
     * &nbsp;&nbsp;e<sup>q</sup> = e<sup>w</sup>[cos(s), sin(s) <b>u</b>]
     *
     * @return e<sup>q</sup>
     */
    Quaternion exp(const Quaternion& power);

    /**
     * Returns ln(q), where q is this quaterion.<p />
     *
     * For q = |q|e<sup>[0,θ<b>u</b>]</sup><br />
     * ln(q) = ln(|q|)+[0,θ<b>u</b>] = [ln(q),θ<b>u</b>]
     *
     * @return ln(q)
     */
    Quaternion ln(const Quaternion& root);

    /**
     * Returns the quaternion raised to the real power.<p />
     *
     * &nbsp;&nbsp;base<sup>power</sup>
     *
     * @param base  The quaternion to be raised by the power.
     * @param power The power to raise the quaternion by.
     * @return      This quaternion raised to the power.
     */
    Quaternion pow(const Quaternion& base, const double power);

    /**
     * Returns a real raised by a quaternion.<p />
     *
     * &nbsp;&nbsp;n<sup>q</sup><p />
     *
     * &nbsp;&nbsp;n<sup>q</sup> = e<sup>ln(n<sup>q</sup>)</sup> = e<sup>q ln(n)</sup>
     *
     * @param base  The real to be raised by the quaternion power.
     * @param power The quaternion to raise the real by.
     * @return      The integer raised by this quaternion.
     */
    Quaternion pow(const double base, const Quaternion& power);

    /**
     * Returns a quaternion raised by another quaternion.<p />
     *
     * &nbsp;&nbsp;n<sup>q</sup><p />
     *
     * &nbsp;&nbsp;n<sup>q</sup> = e<sup>ln(n<sup>q</sup>)</sup> = e<sup>q ln(n)</sup>
     *
     * @param base  The quaternion to be raised.
     * @param power The quaternion to raise by.
     * @return      The base quaternion raised by the power quaternion.
     */
    Quaternion pow(const Quaternion& base, const Quaternion& power);

    /**
     * Returns the distance between two quaternions.
     *
     * @param first  The first quaternion.
     * @param second The second quaternion.
     * @return       The distance between the two quaternions.
     */
    double dist(const Quaternion& first, const Quaternion& second);

    /**
     * Returns a vector rotated about an axis by an angle in radians.
     *
     * @param source The vector to be rotated.
     * @param axis   The axis about which to rotate the vector.
     * @param angle  The angle in radians to rotate by.
     * @return       The vector rotated about the axis and the angle given.
     */
    Vector3D rotate(
        const Vector3D& source, const Vector3D& axis, const double angle);

    /**
     * Returns a vector rotated by a rotation quaternion.
     *
     * @param source   The vector to be rotated.
     * @param rotation The rotation quaternion.
     * @return         The rotated vector.
     */
    Vector3D rotate(
        const Vector3D& source, const Quaternion& rotation);
}
#endif  // QUATERNION_H
