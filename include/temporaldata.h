// Keeps a record of data values over a period of historic time.
//
// (c) 2016: Marcel Admiraal

#ifndef TEMPORALDATA_H
#define TEMPORALDATA_H

#include "vector.h"

#include <chrono>
#include <mutex>

namespace ma
{
    class TemporalData
    {
    public:
        /**
         * Constructor.
         *
         * @param channels  The number of data channels. Default: 1
         * @param frequency The expected sample frequency in Hz.  Default: 1
         * @param period    The period in milliseconds to record. Default: 1000
         */
        TemporalData(unsigned int channels = 1, unsigned int frequency = 1,
                unsigned int period = 1000);

        /**
         * Constructor.
         *
         * @param channels      The number of data channels.
         * @param dataLength    The number of data points in each channel.
         * @param updatePeriod  The update period in milliseconds.
         * @param data          The two dimensional array with the initial data.
         *                      Note: Set to zero if not used.
         */
        TemporalData(unsigned int channels, unsigned int dataLength,
                unsigned int updatePeriod, const double** data);

        /**
         * Copy constructor.
         *
         * @param source    The temporal data to copy.
         */
        TemporalData(const TemporalData& source);

        /**
         * Default destructor.
         */
        virtual ~TemporalData();

        /**
         * Swap function.
         *
         * @param first  The first temporal data to swap with.
         * @param second The second temporla data to swap with.
         */
        friend void swap(TemporalData& first, TemporalData& second);

        /**
         * Assignment operator.
         * Returns a copy of the temporal data.
         *
         * @param source The temporal data to copy.
         * @return       A copy of the temporal data.
         */
        TemporalData& operator=(TemporalData source);

        /**
         * Returns the number of channels.
         *
         * @return The number of channels.
         */
        unsigned int getChannels() const;

        /**
         * Returns the data length.
         *
         * @return The data length.
         */
        unsigned int getDataLength() const;

        /**
         * Returns the update period.
         *
         * @return The update period.
         */
        unsigned int getUpdatePeriod() const;

        /**
         * Adds data point to a channel.
         *
         * @param value         The data value to add to the channel.
         * @param channel       The channel to add the data to. Default: 0
         * @param milliSeconds  If required, override the number of milliseconds
         *                      that have passed. Default: 0 = real-time.
         * @return              The actual number of update periods.
         */
        unsigned int addData(double value, unsigned int channel,
                const unsigned int milliseconds = 0);

        /**
         * Adds data points to all channels.
         *
         * @param value         The vector with all the channels' new data.
         * @param milliSeconds  If required, override the number of milliseconds
         *                      that have passed. Default: 0 = real-time.
         * @return              A vector with the actual number of update
         *                      periods for each channel.
         */
        Vector addData(const Vector& value, const unsigned int milliseconds = 0);

        /**
         * Returns the specified channel's temporal data.
         *
         * @param channel       The channel requried. Default: 0
         * @param update        Whether or not to update the temporal data.
         *                      Default: true
         * @param milliSeconds  If required, override the number of milliseconds
         *                      that have passed. Default: 0 = real-time.
         * @return              The specified channel's temporal data.
         */
        Vector getData(const unsigned int channel = 0,
                bool update = true, const unsigned int milliseconds = 0);

        /**
         * Copies the specified channel's temporal data into the array.
         *
         * @param output        The array to populate.
         * @param channel       The channel's data to return. Default: 0
         * @param update        Whether or not to update the temporal data.
         *                      Default: true
         * @param milliSeconds  If required, override the number of milliseconds
         *                      that have passed. Default: 0 = real-time.
         * @return              The actual number of update periods.
         */
        unsigned int getData(double output[], const unsigned int channel = 0,
                bool update = true, const unsigned int milliseconds = 0);

        /**
         * Copies the temporal data into the two dimensional array.
         *
         * @param output        The two dimensional array to populate.
         * @param update        Whether or not to update the temporal data.
         *                      Default: true
         * @param milliSeconds  If required, override the number of milliseconds
         *                      that have passed. Default: 0 = real-time.
         * @return              A vector with the actual number of update
         *                      periods for each channel.
         */
        Vector getData(double** output, bool update = true,
                const unsigned int milliseconds = 0);

        /**
         * Returns the specified channel's most recent data.
         *
         * @param channel       The channel requried. Default: 0
         * @return              The specified channel's most recent data.
         */
        double getLastData(const unsigned int channel) const;

        /**
         * Returns all channels' most recent data.
         *
         * @return All channels' most recent data.
         */
        Vector getLastData() const;

    private:
        void initialise();
        // Shift the data values by the specified amount of time, filling
        // skipped samples with the previous data.
        unsigned int shiftData(const unsigned int channel,
                const unsigned int milliseconds = 0);

        unsigned int channels, dataLength, updatePeriod;
        double** data;
        double* lastValue;
        unsigned int* timeRemainder;

        std::chrono::milliseconds* lastUpdate;
        std::mutex dataLock;
    };

    /**
     * Input stream operator.
     * Builds a temporal data from an input stream and returns the input stream.
     *
     * @param is            The input stream.
     * @param destination   The temporal data to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, TemporalData& destination);

    /**
     * Output stream operator.
     * Appends the temporal data structure to the output stream and returns the
     * output stream.
     *
     * @param os     The output stream.
     * @param source The temporal data to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const TemporalData& source);
}

#endif // TEMPORALDATA_H
