// A simple list template class
//
// (c) 2014: Marcel Admiraal

#ifndef LIST_H
#define LIST_H

#include "linkednode.h"
#include "iterator.h"

namespace ma
{
    template <class T> class List
    {
    public:
        /**
         * Default constructor.
         * Creates an empty list.
         */
        List();

        /**
         * Copy constructor.
         *
         * @param source The list to copy.
         */
        List(const List<T>& source);

        /**
         * Default destructor.
         */
        virtual ~List();

        /**
         * Swap function.
         *
         * @param first  The first list to swap with.
         * @param second The second list to swap with.
         */
        template <class S> friend void swap(List<S>& first, List<S>& second);

        /**
         * Assignment operator.
         * Returns a copy of the list.
         *
         * @param source The list to copy.
         * @return       A copy of the list.
         */
        List<T>& operator=(List<T> source);

        /**
         * Array subscription operator:
         * Allows direct access to the elements.
         * Returns the element at the index.
         *
         * @param row   The 0 base index to retrieve.
         * @return      The elements at the index.
         */
        T operator[](const unsigned int index);
        const T operator[](const unsigned int index) const;

        /**
         * Returns the number of elements in the list.
         *
         * @return The number of elements in the list.
         */
        unsigned int getSize() const;

        /**
         * Returns the element at the index.
         *
         * @param index The index of the element.
         * @return      The element at the index.
         */
        T get(const unsigned int index) const;

        /**
         * Sets the element at the index.
         *
         * @param index The index of the element.
         * @param data  The data to set the element to.
         */
        void set(const unsigned int index, T data);

        /**
         * Adds an item to the end of the list.
         *
         * @param item The item to add to the list.
         */
        void append(const T& item);

        /**
         * Adds a list to the end of the list.
         *
         * @param item The list to add to the list.
         */
        void extend(const List<T>& list);

        /**
         * Inserts an item in the list after the index.
         *
         * @param item  The item to add to the list.
         * @param index The index to insert the item after.
         */
        void insert(const T& item, unsigned int index);

        /**
         * Removes the first copy of an item from the list if it exists.
         * Returns true if the element was found (and removed).
         *
         * @param item The item to be removed from the list.
         * @return     True if the element was found and removed.
         */
        bool remove(const T& item);

        /**
         * Returns and removes the last item in the list.
         *
         * @return The last item in the list.
         */
        T pop();

        /**
         * Returns and removes the first item in the list.
         *
         * @return The first item in the list.
         */
        T popFirst();

        /**
         * Returns and removes the item at the index,
         *
         * @return The item in at the index.
         */
        T pop(const unsigned int index);

        /**
         * Removes all copies of an item from the list.
         * Returns the number of elements removed.
         *
         * @param item The item to be removed from the list.
         * @return     The number of elements removed.
         */
        unsigned int removeAll(const T& item);

        /**
         * Removes all items from the list.
         */
        void clear();

        /**
         * Returns whether or not an element exists in the list.
         *
         * @param item The item to check for.
         * @return     Whether or not an element exists in the list.
         */
        bool exists(const T& item) const;

        /**
         * Returns the index of the first item in the list,
         * or -1 if it doesn't exist.
         *
         * @param item  The item to check for.
         * @return      The index of the item in the list or -1 if it doesn't
         *              exist.
         */
        unsigned int index(const T& item) const;

        /**
         * Returns the number of copies of an element in the list.
         *
         * @param item The item to check for.
         * @return     The number of copies of an element in the list.
         */
        unsigned int count(const T& item) const;

        /**
         * Returns an iterator for the list.
         *
         * @return An iterator for the list.
         */
        Iterator<T> getIterator() const;

        /**
         * Returns an iterator pointing to the beginning of the list.
         *
         * @return An iterator pointing to the beginning of the list.
         */
        Iterator<T> first() const;

        /**
         * Returns an iterator pointing to the end of the list.
         *
         * @return An iterator pointing to the end of the list.
         */
        Iterator<T> last() const;

    private:
        LinkedNode<T>* head;
        LinkedNode<T>* tail;
        unsigned int size;
    };

    /**
     * Returns whether or not the lists are equal.
     *
     * @param left  The first list.
     * @param right The second list.
     * @return      Whether or not the lists are equal.
     */
    template <class T> bool operator==(
        const List<T>& left, const List<T>& right);

    /**
     * Returns whether or not the lists are not equal.
     *
     * @param left  The first list.
     * @param right The second list.
     * @return      Whether or not the lists are equal.
     */
    template <class T> bool operator!=(
        const List<T>& left, const List<T>& right);

    /**
     * Input stream operator.
     * Builds a list from an input stream and returns the input stream.
     *
     * @param is            The input stream.
     * @param destination   The list to build.
     * @return              The input stream.
     */
    template <class T> std::istream& operator>>(std::istream& is,
            List<T>& destination);

    /**
     * Output stream operator.
     * Appends the list to the output stream and returns the output stream.
     *
     * @param os     The output stream.
     * @param source The list to stream.
     * @return       The output stream.
     */
    template <class T> std::ostream& operator<<(std::ostream& os,
            const List<T>& source);

// Function definitions.

    template <class T> List<T>::List() :
            head(0), tail(0), size(0)
    {
    }

    template <class T> List<T>::List(const List<T>& source) :
            head(0), tail(0), size(source.size)
    {
        if (source.head != 0)
        {
            head = new LinkedNode<T>(source.head->getData());
            LinkedNode<T>* temp = head;
            LinkedNode<T>* tempSource = source.head;
            while(tempSource->getPrevious() != 0)
            {
                tempSource = tempSource->getPrevious();
                temp->addPrevious(tempSource->getData());
                temp = temp->getPrevious();
            }
            tail = temp;
        }
    }

    template <class T> List<T>::~List()
    {
        if (head != 0) deleteAll(head);
    }

    template <class T> void swap(List<T>& first, List<T>& second)
    {
        std::swap(first.head, second.head);
        std::swap(first.tail, second.tail);
        std::swap(first.size, second.size);
    }

    template <class T> List<T>& List<T>::operator=(List<T> source)
    {
        swap(*this, source);
        return *this;
    }

    template <class T> T List<T>::operator[](const unsigned int index)
    {
        return get(index);
    }

    template <class T> const T List<T>::operator[](const unsigned int index) const
    {
        return get(index);
    }

    template <class T> unsigned int List<T>::getSize() const
    {
        return size;
    }

    template <class T> T List<T>::get(const unsigned int index) const
    {
        unsigned int i = index;
        if (i >= size) i = size - 1;
        LinkedNode<T>* pointer = tail;
        for (unsigned int count = 0; count < i; count++)
        {
            pointer = pointer->getNext();
        }
        T data = pointer->getData();
        return data;
    }

    template <class T> void List<T>::set(const unsigned int index, T data)
    {
        unsigned int i = index;
        if (i >= size) i = size - 1;
        LinkedNode<T>* pointer = tail;
        for (unsigned int count = 0; count < i; count++)
        {
            pointer = pointer->getNext();
        }
        pointer->setData(data);
    }

    template <class T> void List<T>::append(const T& item)
    {
        if (head == 0)
        {
            head = new LinkedNode<T>(item);
            tail = head;
        }
        else
        {
            head->addNext(item);
            head = head->getNext();
        }
        size++;
    }

    template <class T> void List<T>::extend(const List<T>& list)
    {
        Iterator<T> iter = list.getIterator();
        while (iter.hasNext()) append(iter.getNext());
    }

    template <class T> void List<T>::insert(const T& item, unsigned int index)
    {
        if (index >= size) append(item);
        else
        {
            LinkedNode<T>* pointer = tail;
            for (unsigned int count = 0; count < index; count++)
                pointer = pointer->getNext();
            pointer->addPrevious(item);
            if (index == 0) tail = pointer->getPrevious();
            size++;
        }
    }

    template <class T> bool List<T>::remove(const T& item)
    {
        LinkedNode<T>* pointer = tail;
        while(pointer != 0)
        {
            if (pointer->getData() == item)
            {
                if (pointer == tail) tail = pointer->getNext();
                if (pointer == head) head = pointer->getPrevious();
                delete pointer;
                size--;
                if (size == 0)
                {
                    head = 0;
                    tail = 0;
                }
                return true;
            }
            pointer = pointer->getNext();
        }
        return false;
    }

    template <class T> T List<T>::pop()
    {
        return pop(size - 1);
    }

    template <class T> T List<T>::popFirst()
    {
        return pop(0);
    }

    template <class T> T List<T>::pop(const unsigned int index)
    {
        unsigned int i = index;
        if (i >= size) i = size - 1;
        LinkedNode<T>* pointer = tail;
        for (unsigned int count = 0; count < i; count++)
        {
            pointer = pointer->getNext();
        }
        T data = pointer->getData();
        if (pointer == tail) tail = pointer->getNext();
        if (pointer == head) head = pointer->getPrevious();
        delete pointer;
        size--;
        return data;
    }

    template <class T> unsigned int List<T>::removeAll(const T& item)
    {
        unsigned int count = 0;
        LinkedNode<T>* pointer = tail;
        while(pointer != 0)
        {
            if (pointer->getData() == item)
            {
                LinkedNode<T>* temp = pointer;
                if (pointer == tail) tail = pointer->getNext();
                if (pointer == head) head = pointer->getPrevious();
                pointer = pointer->getNext();
                delete temp;
                size--;
                count++;
            }
            else pointer = pointer->getNext();
        }
        return count;
    }

    template <class T> void List<T>::clear()
    {
        if (head != 0) deleteAll(head);
        head = 0;
        tail = 0;
        size = 0;
    }

    template <class T> bool List<T>::exists(const T& item) const
    {
        LinkedNode<T>* pointer = tail;
        while(pointer != 0)
        {
            if (pointer->getData() == item) return true;
            pointer = pointer->getNext();
        }
        return false;
    }

    template <class T> unsigned int List<T>::index(const T& item) const
    {
        LinkedNode<T>* pointer = tail;
        unsigned int counter = 0;
        while(pointer != 0)
        {
            if (pointer->getData() == item) return counter;
            pointer = pointer->getNext();
            counter++;
        }
        return (unsigned int)-1;
    }

    template <class T> unsigned int List<T>::count(const T& item) const
    {
        unsigned int counter = 0;
        LinkedNode<T>* pointer = tail;
        while(pointer != 0)
        {
            if (pointer->getData() == item) counter++;
            pointer = pointer->getNext();
        }
        return counter;
    }

    template <class T> Iterator<T> List<T>::getIterator() const
    {
        Iterator<T> iter(tail);
        return iter;
    }

    template <class T> Iterator<T> List<T>::first() const
    {
        Iterator<T> iter(tail);
        return iter;
    }

    template <class T> Iterator<T> List<T>::last() const
    {
        Iterator<T> iter(head);
        return iter;
    }

    template <class T> bool operator==(const List<T>& left,
            const List<T>& right)
    {
        if (left.getSize() != right.getSize()) return false;
        Iterator<T> iterLeft = left.getIterator();
        while (iterLeft.hasNext())
        {
            T next = iterLeft.getNext();
            if (left.count(next) != right.count(next)) return false;
        }
        return true;
    }

    template <class T> bool operator!=(const List<T>& left,
            const List<T>& right)
    {
        return !(left == right);
    }

    template <class T> std::istream& operator>>(
            std::istream& is, List<T>& destination)
    {
        unsigned int length;
        is >> length;
        if (is.good())
        {
            List<T> result;
            for (unsigned int index = 0; index < length; ++index)
            {
                T data;
                is >> data;
                if (is.good())
                {
                    result.append(data);
                }
            }
            unsigned int tail;
            is >> tail;
            if (is.good() && tail == length)
            {
                swap(destination, result);
            }
        }
        return is;
    }

    template <class T> std::ostream& operator<<(
            std::ostream& os, const List<T>& source)
    {
        os << source.getSize() << '\t';
        for (Iterator<T> iter = source.first(); iter.valid(); ++iter)
        {
            os << *iter << '\t';
        }
        os << source.getSize();  // Tail.
        return os;
    }
}

#endif // LIST_H
