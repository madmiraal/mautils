// Utility functions.
//
// (c) 2016: Marcel Admiraal

#ifndef UTILS_H
#define UTILS_H

#include "str.h"

#include <iostream>

namespace ma
{
    /**
     * Returns whether two doubles are approximately equal.
     *
     * @param left  The first double.
     * @param right The second double.
     * @param error The percentage error allowed. Defaults to 0.01%
     * @return      True if the doubls are within the percentage error.
     */
    bool approxEqual(const double left, const double right,
            const double error = 0.01);

    /**
     * Returns a quick integer approximation of the square root of a integer
     * without using division or a squareroot fuction.
     *
     * @param num   The number whose inverse square root is required.
     * @return      The inverse square root.
     */
    unsigned long intSqrt(unsigned long num);

    /**
     * Returns a quick approximation of the inverse square root of a float
     * without using division or a squareroot fuction.
     *
     * @param num   The number whose inverse square root is required.
     * @return      The inverse square root.
     */
    float invSqrt(const float num);

    /**
     * Displays the contacts of an array of bytes to the specified output.
     *
     * @param packet        The array of bytes.
     * @param packetLength  The length of the array.
     * @param os            The output stream. Defaults to standard output.
     */
    void displayPacket(const unsigned char* packet, const unsigned int packetLength,
            std::ostream& os = std::cout);

    /**
     * Renames a filename by adding or incrementing the version number.
     *
     * @param filename  The filename (without the version number) to rename.
     * @param version   The version number.
     * @return          Returns whether or not the rename was successful.
     */
    bool renameFile(const Str& filename, unsigned int version = 0);
}

#endif // UTILS_H
