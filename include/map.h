// A simple map template class.
//
// (c) 2016: Marcel Admiraal

#ifndef MAP_H
#define MAP_H

#include "list.h"

namespace ma
{
    template <class K, class V> class Map
    {
    public:
        /**
         * Default constructor.
         * Creates an empty map.
         */
        Map();

        /**
         * Copy constructor.
         *
         * @param source The list to copy.
         */
        Map(const Map<K, V>& source);

        /**
         * Default destructor.
         */
        virtual ~Map();

        /**
         * Swap function.
         *
         * @param first  The first map to swap with.
         * @param second The second map to swap with.
         */
        template <class K2, class V2> friend void swap(Map<K2, V2>& first,
                Map<K2, V2>& second);

        /**
         * Assignment operator.
         *
         * @param source The map to assign.
         * @return       A copy of the map.
         */
        Map<K, V>& operator=(Map<K, V> source);

        /**
         * Populates the value associated with the key.
         *
         * @param key   The key to find.
         * @param value The value to populate with the result
         * @return      Whether or not the key was found and the value populated.
         */
        bool getValue(const K& key, V& value) const;

        /**
         * Sets the value associated with the key.
         *
         * @param key   The key to assoicate the value with.
         * @param value The value to be associated with the key.
         * @return      Whether or not the key was found and the value updated.
         */
        bool setValue(const K& key, const V& value);

        /**
         * Returns the number of elements in the map.
         *
         * @return The number of elements in the map.
         */
        unsigned int getSize() const;

        /**
         * Returns a copy of the list of keys in the map.
         *
         * @return The list of keys in the map.
         */
        List<K> getKeyList() const;

        /**
         * Returns a copy of the list of values in the map.
         *
         * @return The list of values in the map.
         */
        List<V> getValueList() const;

        /**
         * Adds an key value pair to the map if the key doesn't exist.
         *
         * @param key   The key to add to the map.
         * @param value The value to add to the map.
         * @return      Whether or not the value was added.
         */
        bool append(const K& key, const V& value);

        /**
         * Adds key value pairs from one map to the end of the map
         * if the key doesn't already exist.
         *
         * @param second    The map to add to the map.
         * @return          The number of key value pairs added.
         */
        unsigned int append(const Map<K, V>& second);

        /**
         * Inserts a key value pair into the map after the index.
         *
         * @param key   The key to add to the map.
         * @param value The value to add to the map.
         */
        void insert(const K& key, const V& value, unsigned int index);

        /**
         * Removes the key value pair from the map.
         * Returns true if the key was found and the key value pair removed.
         *
         * @param key   The key of the key value pair to remove.
         * @return      True if the key was found and the key value pair removed.
         */
        bool remove(const K& key);

        /**
         * Removes all key value pairs from the map.
         */
        void clear();

        /**
         * Returns whether or not a key exists in the map.
         *
         * @param key   The key to check for.
         * @return      Whether or not the key exists in the map.
         */
        bool exists(const K& key) const;

        /**
         * Returns the index of the key in the list, or -1 if it doesn't exist.
         *
         * @param key   The key to check for.
         * @return      The index of the .
         */
        unsigned int index(const K& key) const;

    private:
        List<K> keyList;
        List<V> valueList;
    };

    /**
     * Returns whether or not the dictionaries are equal.
     *
     * @param left  The first map.
     * @param right The second map.
     * @return      Whether or not the dictionaries are equal.
     */
    template <class K, class V> bool operator==(const Map<K, V>& left,
            const Map<K, V>& right);

    /**
     * Returns whether or not the dictionaries are not equal.
     *
     * @param left  The first map.
     * @param right The second map.
     * @return      Whether or not the dictionaries are not equal.
     */
    template <class K, class V> bool operator!=(const Map<K, V>& left,
            const Map<K, V>& right);

    /**
     * Input stream operator.
     * Builds a map from an input stream and returns the input stream.
     *
     * @param is            The input stream.
     * @param destination   The map to build.
     * @return              The input stream.
     */
    template <class K, class V> std::istream& operator>>(std::istream& is,
            Map<K, V>& destination);

    /**
     * Output stream operator.
     * Appends the map to the output stream and returns the output stream.
     *
     * @param os     The output stream.
     * @param source The map to stream.
     * @return       The output stream.
     */
    template <class K, class V> std::ostream& operator<<(std::ostream& os,
            const Map<K, V>& source);

// Function definitions.

    template <class K, class V> Map<K, V>::Map()
    {
    }

    template <class K, class V> Map<K, V>::Map(const Map<K, V>& source)
    {
        keyList = source.keyList;
        valueList = source.valueList;
    }

    template <class K, class V> Map<K, V>::~Map()
    {
    }

    template <class K, class V> void swap(Map<K, V>& first,
            Map<K, V>& second)
    {
        swap(first.keyList, second.keyList);
        swap(first.valueList, second.valueList);
    }

    template <class K, class V> Map<K, V>& Map<K, V>::operator=(
            Map<K, V> source)
    {
        swap(*this, source);
        return *this;
    }

    template <class K, class V> bool Map<K, V>::getValue(const K& key,
            V& value) const
    {
        unsigned int index = keyList.index(key);
        if (index == (unsigned int)-1) return false;
        value = valueList.get(index);
        return true;
    }

    template <class K, class V> bool Map<K, V>::setValue(const K& key,
            const V& value)
    {
        unsigned int index = keyList.index(key);
        if (index == (unsigned int)-1) return false;
        valueList.set(index, value);
        return true;
    }

    template <class K, class V> unsigned int Map<K, V>::getSize() const
    {
        return keyList.getSize();
    }

    template <class K, class V> List<K> Map<K, V>::getKeyList() const
    {
        return keyList;
    }

    template <class K, class V> List<V> Map<K, V>::getValueList() const
    {
        return valueList;
    }

    template <class K, class V> bool Map<K, V>::append(const K& key,
            const V& value)
    {
        if (!keyList.exists(key))
        {
            keyList.append(key);
            valueList.append(value);
            return true;
        }
        return false;
    }

    template <class K, class V> unsigned int Map<K, V>::append(
            const Map<K, V>& second)
    {
        unsigned int count = 0;
        Iterator<K> keyIter = second.keyList.getIterator();
        Iterator<V> valueIter = second.valueList.getIterator();
        while (keyIter.valid() && valueIter.valid())
        {
            if (append(*keyIter, *valueIter)) ++count;
            ++keyIter;
            ++valueIter;
        }
        return count;
    }

    template <class K, class V> void Map<K, V>::insert(const K& key,
            const V& value, unsigned int index)
    {
        if (!keyList.exists(key))
        {
            keyList.insert(key, index);
            valueList.insert(value, index);
        }
    }

    template <class K, class V> bool Map<K, V>::remove(const K& key)
    {
        unsigned int index = keyList.index(key);
        if (index = (unsigned int)-1) return false;
        keyList.pop(index);
        valueList.pop(index);
        return true;
    }

    template <class K, class V> void Map<K, V>::clear()
    {
        keyList.clear();
        valueList.clear();
    }

    template <class K, class V> bool Map<K, V>::exists(const K& key) const
    {
        return keyList.exists(key);
    }

    template <class K, class V> unsigned int Map<K, V>::index(
            const K& key) const
    {
        return keyList.index(key);
    }

    template <class K, class V> bool operator==(const Map<K, V>& left,
            const Map<K, V>& right)
    {
        if (left.getKeyList() != right.getKeyList()) return false;
        if (left.getValueList() != right.getValueList()) return false;
        return true;
    }

    template <class K, class V> bool operator!=(const Map<K, V>& left,
            const Map<K, V>& right)
    {
        return !(left == right);
    }

    template <class K, class V> std::istream& operator>>(std::istream& is,
            Map<K, V>& destination)
    {
        unsigned int length;
        is >> length;
        if (is.good())
        {
            Map<K, V> result;
            for (unsigned int index = 0; index < length; ++index)
            {
                K key;
                V value;
                is >> key;
                is >> value;
                if (is.good())
                {
                    result.append(key, value);
                }
            }
            unsigned int tail;
            is >> tail;
            if (is.good() && tail == length)
            {
                swap(destination, result);
            }
        }
        return is;
    }

    template <class K, class V> std::ostream& operator<<(std::ostream& os,
            const Map<K, V>& source)
    {
        os << source.getSize() << '\t';
        List<K> keyList = source.getKeyList();
        List<V> valueList = source.getValueList();
        Iterator<K> keyIter = keyList.getIterator();
        Iterator<V> valueIter = valueList.getIterator();
        while (keyIter.valid() && valueIter.valid())
        {
            os << *keyIter << '\t';
            os << *valueIter << '\t';
            ++keyIter;
            ++valueIter;
        }
        os << source.getSize();  // Tail.
        return os;
    }
}

#endif // MAP_H
