// A matrix class
//
// (c) 2011 - 2017: Marcel Admiraal

#ifndef MATRIX_H
#define MATRIX_H

#include "vector.h"
#include "vector3d.h"
#include "list.h"
#include "str.h"

#include <iostream>

namespace ma
{
    class Matrix
    {
    public:
        /**
         * Default constructor.
         * Creates a zero dimensional matrix.
         */
        Matrix();

        /**
         * Default destructor.
         */
        ~Matrix();

        /**
         * Size constructor.
         *
         * @param rows The number of rows in the matrix.
         * @param cols The number of columns in the matrix.
         */
        Matrix(const unsigned int rows, const unsigned int cols);

        /**
         * Two dimensional array of integers constructor.
         *
         * @param num  The two dimensional array of elements.
         * @param rows The size of the first dimension -
         *             the number of rows in the array.
         * @param cols The size of the second dimension -
         *             the number of columns in the array.
         */
        Matrix(int const* const* num, const unsigned int rows,
                const unsigned int cols);

        /**
         * One dimensional array of integers constructor.
         *
         * @param num  The one dimensional array of elements.
         * @param rows The size of the first dimension -
         *             the number of rows in the array.
         * @param cols The size of the second dimension -
         *             the number of columns in the array.
         */
        Matrix(int const* num, const unsigned int rows,
                const unsigned int cols);

        /**
         * Two dimensional array of floats constructor.
         *
         * @param num  The two dimensional array of elements.
         * @param rows The size of the first dimension -
         *             the number of rows in the array.
         * @param cols The size of the second dimension -
         *             the number of columns in the array.
         */
        Matrix(float const* const* num, const unsigned int rows,
                const unsigned int cols);

        /**
         * One dimensional array of floats constructor.
         *
         * @param num  The one dimensional array of elements.
         * @param rows The size of the first dimension -
         *             the number of rows in the array.
         * @param cols The size of the second dimension -
         *             the number of columns in the array.
         */
        Matrix(float const* num, const unsigned int rows,
                const unsigned int cols);

        /**
         * Two dimensional array of doubles constructor.
         *
         * @param num  The two dimensional array of elements.
         * @param rows The size of the first dimension -
         *             the number of rows in the array.
         * @param cols The size of the second dimension -
         *             the number of columns in the array.
         */
        Matrix(double const* const* num, const unsigned int rows,
                const unsigned int cols);

        /**
         * One dimensional array of doubles constructor.
         *
         * @param num  The one dimensional array of elements.
         * @param rows The size of the first dimension -
         *             the number of rows in the array.
         * @param cols The size of the second dimension -
         *             the number of columns in the array.
         */
        Matrix(double const* num, const unsigned int rows,
                const unsigned int cols);

        /**
         * List of lists of doubles constructor.
         *
         * @param num  The list of lists of doubles of elements.
         */
        Matrix(const List<List<double> >& num);

        /**
         * List of strings of elements constructor.
         *
         * @param num  The list of strings of elements.
         */
        Matrix(const List<Str>& num);

        /**
         * Vector constuctor.
         * Converts a vertical vector into a Matrix.
         *
         * @param source    The vertical vector to convert into a Matrix.
         */
        Matrix(const Vector& source);

        /**
         * Copy constructor.
         *
         * @param source The matrix to copy.
         */
        Matrix(const Matrix& source);

        /**
         * Constructs a matrix from the specified file.
         *
         * @param filename The name of the file that contains
         *                 the matrix specification.
         */
        Matrix(const char* filename);

        /**
         * Swap function.
         *
         * @param first  The first matrix to swap with.
         * @param second The second matrix to swap with.
         */
        friend void swap(Matrix& first, Matrix& second);

        /**
         * Assignment operator.
         * Returns a copy of the matrix.
         *
         * @param source The matrix to copy.
         * @return       A copy of the matrix.
         */
        Matrix& operator=(Matrix source);

        /**
         * Array subscription operator:
         * Allows direct access to the elements.
         * Returns the row as an array.
         *
         * @param row   The 0 base row to retrieve.
         * @return      The row as an array.
         */
        double* operator[](const unsigned int row);
        const double* operator[](const unsigned int row) const;

        /**
         * Returns the number of rows in the matrix.
         *
         * @return The number of rows in the matrix.
         */
        unsigned int getRows() const;

        /**
         * Returns the number of columns in the matrix.
         *
         * @return The number of columns in the matrix.
         */
        unsigned int getCols() const;

        /**
         * Returns the specified matrix element.
         *
         * @param  row The row of the required element.
         * @param  col The column of the required element.
         * @return     The specified matrix element.
         */
        double getElement(const unsigned int row,
                const unsigned int col) const;

        /**
         * Returns the sub-matrix specified by the rows and cols provided.
         *
         * @param  rowStart The first row of the required sub-matrix.
         * @param  rowEnd   The last row of the required sub-matrix.
         * @param  colStart The first row of the required sub-matrix.
         * @param  colEnd   The last row of the required sub-matrix.
         * @return          The sub-matrix specified.
         */
        Matrix getElements(const unsigned int rowStart,
                const unsigned int rowEnd, const unsigned int colStart,
                const unsigned int colEnd) const;

        /**
         * Sets the specified matrix element.
         *
         * @param row The row of the required element to set.
         * @param col The column of the required element to set.
         * @param val The value to set the element to.
         */
        void setElement(const int row, const int col, const double val);

        /**
         * Copies a matrix into the matrix starting at the location specified.
         *
         * @param row    The row to start the copy at.
         * @param col    The column to start the copy at.
         * @param source The source matrix to copy from.
         *
         * @throw An error if the source matrix doesn't fit.
         */
        void setElements(const int row, const int col, const Matrix&);

        /**
         * Copies the required row into the array passed.
         *
         * @param row    The row required.
         * @param result The array to copy into.
         */
        void getRow(const int row, double* result) const;

        /**
         * Returns a vector of the row required.
         *
         * @param  row The row required.
         * @return     A vector containing the row.
         */
        Vector getRow(const int row) const;

        /**
         * Copies the array of integers passed into the required row.
         *
         * @param row    The row to set.
         * @param source The integer array to copy from.
         */
        void setRow(const int row, const int* source);

        /**
         * Copies the array of floats passed into the required row.
         *
         * @param row    The row to set.
         * @param source The float array to copy from.
         */
        void setRow(const int row, const float* source);

        /**
         * Copies the array of doubles passed into the required row.
         *
         * @param row    The row to set.
         * @param source The double array to copy from.
         */
        void setRow(const int row, const double* source);

        /**
         * Copies the vector into the required row.
         *
         * @param row    The row to set.
         * @param source The vector to copy into the row.
         *
         * @throw An error if the source vector is the wrong size.
         */
        void setRow(const int row, const Vector& source);

        /**
         * Copies the the matrix passed starting at the specified row.
         *
         * @param row    The first row to set.
         * @param source The matrix to copy from.
         */
        void setRows(const int row, const Matrix& source);

        /**
         * Copies the required column into the array passed.
         *
         * @param row    The column required.
         * @param result The array to copy into.
         */
        void getCol(const int col, double* result) const;

        /**
         * Returns a vector of the column required.
         *
         * @param  col The column required.
         * @return     A vector containing the column.
         */
        Vector getCol(const int col) const;

        /**
         * Copies the array of integers passed into the required column.
         *
         * @param col    The column to set.
         * @param source The integer array to copy from.
         */
        void setCol(const int col, const int* source);

        /**
         * Copies the array of floats passed into the required column.
         *
         * @param col    The column to set.
         * @param source The float array to copy from.
         */
        void setCol(const int col, const float* source);

        /**
         * Copies the array of doubles passed into the required column.
         *
         * @param col    The column to set.
         * @param source The double array to copy from.
         */
        void setCol(const int col, const double* source);

        /**
         * Copies the vector into the required column.
         *
         * @param col    The column to set.
         * @param source The vector to copy into the column.
         *
         * @throw An error if the source vector is the wrong size.
         */
        void setCol(const int row, const Vector& source);

        /**
         * Copies the the matrix passed starting at the specified column.
         *
         * @param col    The first column to set.
         * @param source The matrix to copy from.
         */
        void setCols(const int col, const Matrix&);

        /**
         * Adds rows to the matrix.
         *
         * @param r The number of rows to add.
         */
        void addRows(const int r);

        /**
         * Adds columns to the matrix.
         *
         * @param c The number of columns to add.
         */
        void addCols(const int c);

        /**
         * Addition equals operator.
         * Adds the right matrix to this matrix, and returns the sum.
         *
         * @param right The matrix to add to this matrix.
         * @return      The element sum of the two matrices.
         * @throw       A string literal if the dimensions are not equal.
         */
        Matrix& operator+=(const Matrix& right);

        /**
         * Vector broadcast addition equals operator.
         * Returns the matrix summed with the broadcast vector.
         *
         * @param right The vector to broadcast.
         * @return      The matrix summed with the broadcast vector.
         * @throw       A string literal if the vector length doesn't equal the
         *              number of columns.
         */
        Matrix& operator+=(const Vector& right);

        /**
         * Scalar broadcast addition equals operator.
         * Returns the matrix summed with the broadcast scalar.
         *
         * @param right The scalar to broadcast.
         * @return      The matrix summed with the broadcast scalar.
         */
        Matrix& operator+=(const double& right);

        /**
         * Subtraction equals operator.
         * Subtracts the right matrix from this matrix, and
         * returns the differnce.
         *
         * @param right The matrix to subtract.
         * @return      The element subtraction of the two matrices.
         * @throw       A string literal if the dimensions are not equal.
         */
        Matrix& operator-=(const Matrix& right);

        /**
         * Vector broadcast subtraction equals operator.
         * Returns the broadcast vector subtracted from the matrix.
         *
         * @param right The vector to broadcast.
         * @return      The broadcast vector subtracted from the matrix.
         * @throw       A string literal if the vector length doesn't equal the
         *              number of columns.
         */
        Matrix& operator-=(const Vector& right);

        /**
         * Scalar broadcast subtraction equals operator.
         * Returns the broadcast scalar subtracted from the matrix.
         *
         * @param right The scalar to broadcast.
         * @return      The broadcast scalar subtracted from the matrix.
         */
        Matrix& operator-=(const double& right);

        /**
         * Double multiplication equals operator.
         * Scales this matrix by the scalar and returns the scaled matrix.
         *
         * @param right The scalar to multiply the matrix by.
         * @return      The scaled matrix.
         */
        Matrix& operator*=(const double right);

        /**
         * Double division equals operator.
         * Reduces this matrix by the scalar and returns the reduced matrix.
         *
         * @param right The scalar to divide the matrix by.
         * @return      The reduced matrix.
         */
        Matrix& operator/=(const double right);

        /**
         * Returns the element product of this matrix with another.
         *
         * @param other The matrix to multiply with.
         * @return      The element product of this matrix with the other.
         * @throw       A string literal if the dimensions are not equal.
         */
        Matrix dot(const Matrix& other) const;

        /**
         * Returns the sum of the elements.
         *
         * @return The sum of the elements.
         */
        double elementSum() const;

        /**
         * Returns the squared sum of the elements.
         *
         * @return The squared sum of the elements.
         */
        double elementSquaredSum() const;

        /**
         * Returns the determinant of the matrix; using the Laplace expansion.
         *
         * @return The determinant of the matrix.
         * @throw  A string literal if the matrix is not square.
         */
        double det() const;

        /**
         * Returns the required co-factor of this matrix.
         * The co-factor matrix is the determinant of the minor matrix
         * mulitplied by the sign defined by the indices defined as:
         * + if row + col is even
         * - if row + col is odd
         *
         * @param row The required row.
         * @param col The required column.
         * @return    The required co-factor of this matrix.
         */
        double coFactor(const unsigned int row, const unsigned int col) const;

        /**
         * Returns the required minor matrix of this matrix:
         * The matrix minus the row and column provided.
         *
         * @param row The required row.
         * @param col The required column.
         * @return    The required minor matrix of this matrix.
         */
        Matrix getMinor(const unsigned int row, const unsigned int col) const;

        /**
         * Fills the matrix with the value.
         *
         * @param value The value to fill the matrix with.
         */
        void fillWith(const double value);

        /**
         * Swaps two rows in the matrix.
         *
         * @param row1 The first row to swap.
         * @param row2 The other row to swap with.
         */
        void swapRows(const unsigned int row1, const unsigned int row2);

        /**
         * Swaps two columns in the matrix.
         *
         * @param col1 The first column to swap.
         * @param col2 The other column to swap with.
         */
        void swapCols(const unsigned int col1, const unsigned int col2);

        /**
         * Linearly combines two rows.
         * The first row specified is updated with the linear sum
         * of it multiplied by its specified factor and
         * the second row specified multiplied by its specified
         * factor.
         * If the second row is not specified the first row of
         * the matrix is used.
         * If the second factor is not speceified it simply
         * multiplies the first row by its factor.
         *
         * @param row1    The first row and the row that will be updated.
         * @param factor1 The factor to multiply the first row by.
         * @param row2    The second row.
         * @param factor2 The factor to multiply the second row by.
         */
        void linearCombineRows(
            const unsigned int row1, const double factor1,
            const unsigned int row2 = 0, const double factor2 = 0.0);

        /**
         * Merges this matrix with another horizontally and returns the result.
         *
         * @param right The matrix to merge to the right.
         * @return      A horizontal merge of this matrix with another.
         * @throw       A string literal if the number of rows are not equal.
         */
        Matrix horizontalMerge(const Matrix& right);

        /**
         * Merges this matrix with another vertically and returns the result.
         *
         * @param bottom The matrix to merge below.
         * @return       A vertical merge of this matrix with another.
         * @throw        A string literal if the number of columns are not equal.
         */
        Matrix verticalMerge(const Matrix& bottom);

        /**
         * Reshapes this matrix and returns the result.
         *
         * @param rows  The new number of rows.
         * @param cols  The new number of cols.
         * @return      The reshaped matrix.
         * @throw       A string literal if the number of elements aren't equal.
         */
        Matrix reshape(const unsigned int rows, const unsigned int cols);

        /**
         * Transposes this matrix and returns the result.
         *
         * @return       The matrix transposed.
         */
        Matrix transpose();

        /**
         * Inverts this matrix using Gauss-Jordna elimination and returns the
         * result.
         *
         * @return       The Gauss-Jordna elimination inverse of the matrix.
         * @throw        A string literal if the matrix is not square.
         */
        Matrix inverse();

        /**
         * Returns the minimums of a matrix.
         * By default the minimums of the columns are returned.
         *
         * @param source The matrix whose minimums are required.
         * @param dim    If set to 1, the minimums of the rows
         *               are returned.
         * @return       The minimums of the matrix.
         */
        Vector min(const int dim = 0) const;

        /**
         * Returns the indices of the minimums of a matrix.
         * By default the indices of the minimums of the columns are returned.
         *
         * @param source The matrix whose minimum indices are required.
         * @param dim    If set to 1, the indices of the minimums of the rows
         *               are returned.
         * @return       The indices of the minimums of the matrix.
         */
        Vector minIndex(const int dim = 0) const;

        /**
         * Returns the maximums of a matrix.
         * By default the maximums of the columns are returned.
         *
         * @param source The matrix whose maximums are required.
         * @param dim    If set to 1, the maximums of the rows
         *               are returned.
         * @return       The maximums of the matrix.
         */
        Vector max(const int dim = 0) const;

        /**
         * Returns the indices of the maximums of a matrix.
         * By default the indices of the maximums of the columns are returned.
         *
         * @param source The matrix whose maximum indices are required.
         * @param dim    If set to 1, the indices of the maximums of the rows
         *               are returned.
         * @return       The indices of the maximums of the matrix.
         */
        Vector maxIndex(const int dim = 0) const;

        /**
         * Returns the means of a matrix.
         * By default the means of the columns are returned.
         *
         * @param source The matrix whose means are required.
         * @param dim    If set to 1, the means of the rows
         *               are returned.
         * @return       The means of the matrix.
         */
        Vector mean(const int dim = 0) const;

        /**
         * Returns the variances of a matrix.
         * By default the variances of the columns are returned.
         *
         * @param source The matrix whose variances are required.
         * @param dim    If set to 1, the variances of the rows
         *               are returned.
         * @return       The variances of the matrix.
         */
        Vector var(const int dim = 0) const;

        /**
         * Returns the standard deviations of a matrix.
         * By default the standard deviations of the columns
         * are returned.
         *
         * @param source The matrix whose standard deviations
         *               are required.
         * @param dim    If set to 1, the standard deviations
         *               of the rows are returned.
         * @return       The standard deviations of the matrix.
         */
        Vector sd(const int dim = 0) const;

        /**
         * Returns the standard errors of the mean of a matrix.
         * By default the standard errors of the mean of the
         * columns are returned.
         *
         * @param source The matrix whose standard errors of
         *               the mean are required.
         * @param dim    If set to 1, the standard errors of
         *               the mean of the rows are returned.
         * @return       The standard errors of the mean of the matrix.
         */
        Vector sem(const int dim = 0) const;

        /**
         * Returns the relative standard errors of the mean of
         * a matrix.
         * The relative standard error of the means is the
         * percentage standard error of the mean.
         * By default the relative standard errors of the mean
         * of the columns are returned.
         *
         * @param source The matrix whose relative standard
         *               errors of the mean are required.
         * @param dim    If set to 1, the relative standard
         *               errors of the mean of the rows are
         *               returned.
         * @return       The relative standard errors of the mean
         *               of the matrix.
         */
        Vector rsem(const int dim = 0) const;

        /**
         * Streams the contents of the matrix laid out in human readable form.
         *
         * @param os     The output stream to stream to.
         */
        void print(std::ostream& os = std::cout) const;

        /**
         * Streams the contents of the matrix using the delimiter specified.
         *
         * @param os     The output stream to stream to.
         */
        void printDelimited(std::ostream& os = std::cout,
                const char delimiter = '\t') const;

        /**
         * Streams the contents of the matrix laid out in tab delimited form.
         *
         * @param os     The output stream to stream to.
         */
        void outputASCII(std::ostream& os = std::cout) const;

        /**
         * Streams the contents of the matrix laid out in comma separated form.
         *
         * @param os     The output stream to stream to.
         */
        void outputCSV(std::ostream& os = std::cout) const;

        /**
         * Saves the matrix to the specified file name.
         * Uses the file extension to determine the format.
         *
         * @param filename The name of the file to save the matrix to.
         * @return          Whether or not the save was successful.
         */
        bool save(const char* filename) const;

        /**
         * Loads the contents of the matrix from a print format.
         *
         * @param is     The input stream to stream from.
         */
        bool loadPrint(std::istream& is = std::cin);

        /**
         * Loads the contents of the matrix laid out in tab delimited form.
         *
         * @param is     The input stream to stream from.
         */
        bool loadASCII(std::istream& is = std::cin);

        /**
         * Loads the contents of the matrix laid out in comma separated form.
         *
         * @param is     The input stream to stream from.
         */
        bool loadCSV(std::istream& is = std::cin);

        /**
         * Loads the contents of the matrix from an output stream format.
         *
         * @param filename The name of the file to load the matrix from.
         * @return          Whether or not the load was successful.
         */
        bool loadStream(std::istream& is = std::cin);

        /**
         * Loads the matrix from the specified file name.
         * Uses the file extension to determine the format.
         *
         * @param filename The name of the file to load the matrix from.
         * @return          Whether or not the load was successful.
         */
        bool load(const char* filename);

    private:
        unsigned int rows, cols;
        double** element;
    };

    /**
     * Addition operator.
     * Returns the element sum of the two matrices.
     *
     * @param left  The first matrix to sum.
     * @param right The second matrix to sum.
     * @return      The element sum of the two matrices.
     * @throw       A string literal if the dimensions are not equal.
     */
    Matrix operator+(Matrix left, const Matrix& right);

    /**
     * Vector broadcast addition operator.
     * Returns the broadcast vector summed with the matrix.
     *
     * @param left  The matrix to sum.
     * @param right The vector to broadcast.
     * @return      The broadcast vector summed with the matrix.
     * @throw       A string literal if the vector length doesn't equal the
     *              number of columns.
     */
    Matrix operator+(Matrix left, const Vector& right);

    /**
     * Vector broadcast addition operator.
     * Returns the broadcast vector summed with the matrix.
     *
     * @param left  The vector to broadcast.
     * @param right The matrix to sum.
     * @return      The broadcast vector summed with the matrix.
     * @throw       A string literal if the vector length doesn't equal the
     *              number of columns.
     */
    Matrix operator+(const Vector& left, Matrix right);

    /**
     * Scalar broadcast addition operator.
     * Returns the broadcast scalar summed with the matrix.
     *
     * @param left  The matrix to sum.
     * @param right The scalar to broadcast.
     * @return      The broadcast scalar summed with the matrix.
     */
    Matrix operator+(Matrix left, const double& right);

    /**
     * Scalar broadcast addition operator.
     * Returns the broadcast scalar summed with the matrix.
     *
     * @param left  The scalar to broadcast.
     * @param right The matrix to sum.
     * @return      The broadcast scalar summed with the matrix.
     */
    Matrix operator+(const double& left, Matrix right);

    /**
     * Subtraction operator.
     * Returns the element subtraction of the two matrices.
     *
     * @param left  The matrix to subtract from.
     * @param right The matrix to subtract.
     * @return      The element subtraction of the two matrices.
     * @throw       A string literal if the dimensions are not equal.
     */
    Matrix operator-(Matrix left, const Matrix& right);

    /**
     * Vector broadcast subtraction operator.
     * Returns the broadcast vector subtracted from the matrix.
     *
     * @param left  The matrix to sum.
     * @param right The vector to broadcast.
     * @return      The broadcast vector subtracted from the matrix.
     * @throw       A string literal if the vector length doesn't equal the
     *              number of columns.
     */
    Matrix operator-(Matrix left, const Vector& right);

    /**
     * Vector broadcast subtraction operator.
     * Returns the matrix subtracted from the broadcast vector.
     *
     * @param left  The vector to broadcast.
     * @param right The matrix to sum.
     * @return      The matrix subtracted from the broadcast vector.
     * @throw       A string literal if the vector length doesn't equal the
     *              number of columns.
     */
    Matrix operator-(const Vector& left, Matrix right);

    /**
     * Scalar broadcast subtraction operator.
     * Returns the broadcast scalar subtracted from the matrix.
     *
     * @param left  The matrix to sum.
     * @param right The scalar to broadcast.
     * @return      The broadcast scalar subtracted from the matrix.
     */
    Matrix operator-(Matrix left, const double& right);

    /**
     * Scalar broadcast subtraction operator.
     * Returns the matrix subtracted from the broadcast scalar.
     *
     * @param left  The scalar to broadcast.
     * @param right The matrix to sum.
     * @return      The matrix subtracted from the broadcast scalar.
     */
    Matrix operator-(const double& left, Matrix right);

    /**
     * Negation operator.
     * Returns the negated matrix.
     *
     * @param matrix    The matrix to negate.
     * @return          The negated matrix.
     */
    Matrix operator-(const Matrix& matrix);

    /**
     * Multiplication operator.
     * Returns the matrix product of the two matrices.
     *
     * @param left  The left-hand matrix to multiply.
     * @param right The right-hand matrix to multiply by.
     * @return      The matrix product of the two matrices.
     * @throw       A string literal if the inner dimensions are not equal.
     */
    Matrix operator*(const Matrix& left, const Matrix& right);

    /**
     * Vector multiplication operator.
     * Returns the vector result of a matrix multiplied by a (vertical) vector.
     *
     * @param left  The matrix to multiply.
     * @param right The (vertical) vector to multiply by.
     * @return      The vector product.
     * @throw       A string literal if the matrix columns and vetor length are
     *              not equal.
     */
    Vector operator*(const Matrix& left, const Vector& right);

    /**
     * Double multiplication operator.
     * Returns the matrix scaled by a scalar.
     *
     * @param left  The matrix to multiply.
     * @param right The scalar to multiply the matrix by.
     * @return      The scaled matrix.
     */
    Matrix operator*(Matrix left, const double right);

    /**
     * Double multiplication operator.
     * Returns the matrix scaled by a scalar.
     *
     * @param left  The scalar to multiply the matrix by.
     * @param right The matrix to multiply.
     * @return      The scaled matrix.
     */
    Matrix operator*(const double left, Matrix right);

    /**
     * Double division operator.
     * Returns the matrix reduced by a scalar.
     *
     * @param left  The matrix to divide.
     * @param right The scalar to divide the matrix by.
     * @return      The reduced matrix.
     */
    Matrix operator/(Matrix left, const double right);

    /**
     * Equals operator.
     * Returns whether or not the matrices are equal.
     *
     * @param left  The first matrix.
     * @param right The second matrix.
     * @return      True if the matrices are of equal size and
     *              all the elements are equal.
     */
    bool operator==(const Matrix& left, const Matrix& right);

    /**
     * Not equals operator.
     * Returns whether or not the matrices are not equal.
     *
     * @param left  The first matrix.
     * @param right The second matrix.
     * @return      True if the matrices are not of equal size or
     *              at least one element is not equal.
     */
    bool operator!=(const Matrix& left, const Matrix& right);

    /**
     * Returns whether or not two matrices are the same size and
     * all the elements are approximately equal.
     *
     * @param left  The first matrix.
     * @param right The second matrix.
     * @param error The percentage error allowed. Defaults to 0.01%
     * @return      True if the matrices are of equal size and
     *              all the elements are approximately equal.
     */
    bool approxEqual(const Matrix& left, const Matrix& right,
            const double error = 0.01);

    /**
     * Input stream operator.
     * Builds a matrix from the input stream and returns the input stream.
     *
     * @param is            The input stream.
     * @param destination   The matrix to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, Matrix& destination);

    /**
     * Output stream operator.
     * Appends the matrix to the output stream and returns the output stream.
     *
     * @param os     The output stream.
     * @param source The matrix to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const Matrix& source);

    /**
     * Returns the Matrix created by the outer product of two vectors:
     * left*right'.
     *
     * @param left  The first vector to outer multiply.
     * @param right The second vector to outer multiply.
     * @return      The Matrix created by the outer product.
     */
    Matrix outer(const Vector& left, const Vector& right);

    /**
     * Returns a matrix of the specified size filled with zeros.
     *
     * @param rows The number of rows in the matrix.
     * @param cols The number of columns in the matrix.
     * @return     A matrix of the specified size filled with zeros.
     */
    Matrix zeros(const unsigned int rows, const unsigned int cols);

    /**
     * Returns a matrix of the specified size filled with ones.
     *
     * @param rows The number of rows in the matrix.
     * @param cols The number of columns in the matrix.
     * @return     A matrix of the specified size filled with ones.
     */
    Matrix ones(const unsigned int rows, const unsigned int cols);

    /**
     * Returns a diagonal matrix using a vector.
     *
     * @param d The vector to use for the diagonal elements.
     * @return  An diagonal matrix created from the vector.
     */
    Matrix diag(const Vector d);

    /**
     * Returns the Householder transformation matrix.
     * The Householder transformation is a numerically stable way of
     * converting a matrix into upper triangular form.
     * It's more computationally expensive than Gaussian elimination,
     * but also works on non-square matrices.
     * Used for QR factorization: Q = H^T, R = HA.
     *
     * @param source The matrix to transform.
     * @return       The Householder transformation matrix.
     */
    Matrix householder(const Matrix&);

    /**
     * Returns an identity matrix of the specified size.
     *
     * @param n The number of rows and columns in the matrix.
     * @return  An identity matrix of the specified size.
     */
    Matrix identity(const unsigned int n);

    /**
     * Returns a horizontal merge of two matrices.
     *
     * @param left  The left-hand matrix to merge.
     * @param right The right-hand matrix to merge.
     * @return      A horizontal merge of the two matrices.
     * @throw       A string literal if the number of rows are not equal.
     */
    Matrix horizontalMerge(Matrix left, const Matrix& right);

    /**
     * Returns a vertical merge of two matrices.
     *
     * @param top    The upper matrix to merge.
     * @param bottom The lower matrix to merge.
     * @return       A vertical merge of the two matrices.
     * @throw        A string literal if the number of columns are not equal.
     */
    Matrix verticalMerge(Matrix top, const Matrix& bottom);

    /**
     * Returns a reshaped matrix.
     *
     * @param source    The matrix to reshape.
     * @param rows      The new number of rows.
     * @param cols      The new number of cols.
     * @return          The reshaped matrix.
     * @throw           A string literal if the number of elements aren't equal.
     */
    Matrix reshape(Matrix source, const unsigned int rows,
            const unsigned int cols);

    /**
     * Returns the transpose of a matrix.
     *
     * @param source The matrix to transpose.
     * @return       The transpose of the matrix.
     */
    Matrix transpose(Matrix source);

    /**
     * Returns the Gauss-Jordna elimination inverse of a square matrix.
     *
     * @param source The square matrix to invert.
     * @return       The Gauss-Jordna elimination inverse of the matrix.
     * @throw        A string literal if the matrix is not square.
     */
    Matrix inverse(Matrix source);

    /**
     * Returns the minimums of a matrix.
     * By default the minimums of the columns are returned.
     *
     * @param source The matrix whose minimums are required.
     * @param dim    If set to 1, the minimums of the rows
     *               are returned.
     * @return       The minimums of the matrix.
     */
    Vector min(const Matrix& source, const int dim = 0);

    /**
     * Returns the indices of the minimums of a matrix.
     * By default the indices of the minimums of the columns are returned.
     *
     * @param source The matrix whose minimum indices are required.
     * @param dim    If set to 1, the indices of the minimums of the rows
     *               are returned.
     * @return       The indices of the minimums of the matrix.
     */
    Vector minIndex(const Matrix& source, const int dim = 0);

    /**
     * Returns the maximums of a matrix.
     * By default the maximums of the columns are returned.
     *
     * @param source The matrix whose maximums are required.
     * @param dim    If set to 1, the maximums of the rows
     *               are returned.
     * @return       The maximums of the matrix.
     */
    Vector max(const Matrix& source, const int dim = 0);

    /**
     * Returns the indices of the maximums of a matrix.
     * By default the indices of the maximums of the columns are returned.
     *
     * @param source The matrix whose maximum indices are required.
     * @param dim    If set to 1, the indices of the maximums of the rows
     *               are returned.
     * @return       The indices of the maximums of the matrix.
     */
    Vector maxIndex(const Matrix& source, const int dim = 0);

    /**
     * Returns the means of a matrix.
     * By default the means of the columns are returned.
     *
     * @param source The matrix whose means are required.
     * @param dim    If set to 1, the means of the rows
     *               are returned.
     * @return       The means of the matrix.
     */
    Vector mean(const Matrix& source, const int dim = 0);

    /**
     * Returns the variances of a matrix.
     * By default the variances of the columns are returned.
     *
     * @param source The matrix whose variances are required.
     * @param dim    If set to 1, the variances of the rows
     *               are returned.
     * @return       The variances of the matrix.
     */
    Vector var(const Matrix& source, const int dim = 0);

    /**
     * Returns the standard deviations of a matrix.
     * By default the standard deviations of the columns
     * are returned.
     *
     * @param source The matrix whose standard deviations
     *               are required.
     * @param dim    If set to 1, the standard deviations
     *               of the rows are returned.
     * @return       The standard deviations of the matrix.
     */
    Vector sd(const Matrix& source, const int dim = 0);

    /**
     * Returns the standard errors of the mean of a matrix.
     * By default the standard errors of the mean of the
     * columns are returned.
     *
     * @param source The matrix whose standard errors of
     *               the mean are required.
     * @param dim    If set to 1, the standard errors of
     *               the mean of the rows are returned.
     * @return       The standard errors of the mean of the matrix.
     */
    Vector sem(const Matrix& source, const int dim = 0);

    /**
     * Returns the relative standard errors of the mean of
     * a matrix.
     * The relative standard error of the means is the
     * percentage standard error of the mean.
     * By default the relative standard errors of the mean
     * of the columns are returned.
     *
     * @param source The matrix whose relative standard
     *               errors of the mean are required.
     * @param dim    If set to 1, the relative standard
     *               errors of the mean of the rows are
     *               returned.
     * @return       The relative standard errors of the mean
     *               of the matrix.
     */
    Vector rsem(const Matrix& source, const int dim = 0);

    /**
     * Streams the contents of the matrix laid out in human readable form.
     *
     * @param source The matrix to stream.
     * @param os     The output stream to stream to.
     */
    void print(const Matrix& source, std::ostream& os = std::cout);

    /**
     * Streams the contents of the matrix using the delimiter specified.
     *
     * @param source The matrix to stream.
     * @param os     The output stream to stream to.
     */
    void printDelimited(const Matrix& source, std::ostream& os = std::cout,
            const char delimiter = '\t');

    /**
     * Streams the contents of the matrix laid out in tab delimited form.
     *
     * @param source The matrix to stream.
     * @param os     The output stream to stream to.
     */
    void outputASCII(const Matrix& source, std::ostream& os = std::cout);

    /**
     * Streams the contents of the matrix laid out in comma separated form.
     *
     * @param source The matrix to stream.
     * @param os     The output stream to stream to.
     */
    void outputCSV(const Matrix& source, std::ostream& os = std::cout);

    /**
     * Streams the contents of the matrix laid out in data loadable form.
     *
     * @param source The matrix to stream.
     * @param os     The output stream to stream to.
     */
    void outputDAT(const Matrix& source, std::ostream& os = std::cout);

    /**
     * Saves a matrix to the specified file name.
     * Uses the file extension to determine the format.
     *
     * @param source    The matrix to save.
     * @param filename  The name of the file to save the matrix to.
     * @return          Whether or not the save was successful.
     */
    bool save(const Matrix& source, const char* filename);

    /**
     * Loads a matrix from a print formated stream.
     *
     * @param destination   The matrix to load.
     * @param is            The input stream to stream from.
     * @return              Whether or not the load was successful.
     */
    bool loadPrint(Matrix& destination, std::istream& is = std::cin);

    /**
     * Loads a matrix from a tab delimited stream.
     *
     * @param destination   The matrix to load.
     * @param is            The input stream to stream from.
     * @return              Whether or not the load was successful.
     */
    bool loadASCII(Matrix& destination, std::istream& is = std::cin);

    /**
     * Loads a matrix from a comma separated stream.
     *
     * @param destination   The matrix to load.
     * @param is            The input stream to stream from.
     * @return              Whether or not the load was successful.
     */
    bool loadCSV(Matrix& destination, std::istream& is = std::cin);

    /**
     * Loads a matrix from an output stream format stream.
     *
     * @param destination   The matrix to load.
     * @param filename      The name of the file to load the matrix from.
     * @return              Whether or not the load was successful.
     */
    bool loadStream(Matrix& destination, std::istream& is = std::cin);

    /**
     * Loads a matrix from the specified file name.
     * Uses the file extension to determine the format.
     *
     * @param destination   The matrix to load.
     * @param filename      The name of the file to load the matrix from.
     * @return              Whether or not the load was successful.
     */
    bool load(Matrix& destination, const char* filename);
}
#endif // MATRIX_H
