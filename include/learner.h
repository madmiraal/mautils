// Abstract class for a temporal learning algorithm.

// (c) 2016 - 2017: Marcel Admiraal

#ifndef LEARNER_H
#define LEARNER_H

#include "vector.h"

#include <fstream>

namespace ma
{
    class Learner
    {
    public:
        /**
         * Constructor.
         *
         * @param stateLength   The input state vector length. Default: 2.
         * @param outputLength  The output vector length. Default: 1.
         * @param learnRate     The learning rate. Default: 0.1.
         * @param discountRate  The reward discount rate. Default: 1.0.
         */
        Learner(const unsigned int stateLength = 2,
                const unsigned int outputLength = 1,
                const double learnRate = 0.1,
                const double discountRate = 1.0);

        /**
         * Destructor.
         */
        virtual ~Learner();

        /**
         * Copy constructor.
         *
         * @param source    The learner to copy.
         */
        Learner(const Learner& source);

        /**
         * Swap function.
         *
         * @param first  The first learner to swap with.
         * @param second The second learner to swap with.
         */
        friend void swap(Learner& first, Learner& second);

        /**
         * Saves the current learner to a file with the given filename.
         *
         * @param filename  The filename to save the learner to.
         * @return          Return true if the save was successful.
         */
        virtual bool save(const char* filename) const = 0;

        /**
         * Loads a previously saved learner from a file with the given filename.
         *
         * @param filename  The filename to save the learner to.
         * @return          Return true if the load was successful.
         */
        virtual bool load(const char* filename) = 0;

        /**
         * Returns the current value of the state.
         *
         * @param state A vector representation of the state.
         * @return      The learner's current value of the state.
         */
        virtual Vector getStateValue(const Vector& state) = 0;

        /**
         * Inform the learner of the next state in a sequence, and any interim
         * reward received.
         *
         * @param state     The next state in the sequence.
         * @param reward    Interim reward. Defaults to zero.
         */
        virtual void nextState(const Vector& state,
                const Vector& reward = Vector()) = 0;

        /**
         * Inform the learner of the final reward received at the end of a
         * sequence.
         *
         * @param reward    The final reward received.
         */
        virtual void finalReward(const Vector& reward) = 0;

        /**
         * Inform the learner to discard the current sequence, because
         * squence terminated before a final reward was received.
         */
        virtual void sequenceReset() = 0;

        /**
         * Returns the current input state vector length.
         *
         * @return The current input state vector length.
         */
        virtual unsigned int getStateLength() const;

        /**
         * Returns the current output vector length.
         *
         * @return The current output vector length.
         */
        virtual unsigned int getOutputLength() const;

        /**
         * Adjusts the input and output state vector lengths.
         *
         * @param stateLength   The new state vector length.
         * @param outputLength  The new output vector length.
         */
        virtual void adjustLearner(const unsigned int stateLength,
                const unsigned int outputLength);

        /**
         * Returns the current learning rate.
         *
         * @return The current learning rate.
         */
        double getLearningRate() const;

        /**
         * Sets the learning rate.
         *
         * @param newRate   The new learning rate.
         */
        void setLearningRate(const double newRate);

        /**
         * Returns the current reward discount rate.
         *
         * @return The current reward discount rate.
         */
        double getDiscountRate() const;

        /**
         * Sets the reward discount rate.
         *
         * @param newRate   The new reward discount rate.
         */
        void setDiscountRate(const double newRate);

protected:
        // The input vector length.
        unsigned int stateLength;
        // The output vector length;
        unsigned int outputLength;
        // The learning rate.
        double learnRate;
        // The reward discount rate.
        double discountRate;
    };
}

#endif //LEARNER_H
