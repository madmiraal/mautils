// A simple complex number class
//
// (c) 2011 - 2017: Marcel Admiraal

#ifndef COMPLEX_H
#define COMPLEX_H

#include <iostream>

namespace ma
{
    class Complex
    {
    public:
        /**
         * Default constructor.
         * Creates a zero.
         */
        Complex();

        /**
         * Double constructor.
         *
         * @param real The real part for the new complex number.
         */
        Complex(double real);

        /**
         * Doubles constructor.
         *
         * @param real      The real part for the new complex number.
         * @param imaginary The imaginary part for the new complex number.
         */
        Complex(double real, double imaginary);

        /**
         * Copy constructor.
         *
         * @param source    The complex number to copy.
         */
        Complex(const Complex& source);

        /**
         * Default destructor.
         */
        virtual ~Complex();

        /**
         * Swap function.
         *
         * @param first  The first complex number to swap with.
         * @param second The second complex number to swap with.
         */
        friend void swap(Complex& first, Complex& second);

        /**
         * Assignment operator.
         * Returns a copy of the complex number.
         *
         * @param source The complex number to copy.
         * @return       A copy of the complex number.
         */
        Complex& operator=(Complex source);

        /**
         * Returns the real part of this complex number.
         *
         * @return The real part of this complex number.
         */
        double real() const;

        /**
         * Returns whether or not the complex number is real.
         * Imaginary part is zero to within the threshold x precision.
         *
         * @param threshold The number to multiply the precision by.
         * @return          Whether or not the complex number is real.
         */
        bool isReal(double threshold = 100) const;

        /**
         * Sets the real part of this complex number.
         *
         * @param real The real part of this complex number.
         */
        void setReal(double real);

        /**
         * Returns the imaginary part of this complex number.
         *
         * @return The imaginary part of this complex number.
         */
        double imag() const;

        /**
         * Sets the imaginary part of this complex number.
         *
         * @param imaginary The imaginary part of this complex number.
         */
        void setImaginary(double imaginary);

        /**
         * Returns the absolute size of this complex number.
         *
         * @return The absolute size of this complex number.
         */
        double abs() const;

        /**
         * Returns the argument (angle from the real) of this complex number.
         *
         * @return The real argument in radians of this complex number.
         */
        double arg() const;

        /**
         * Addition equals operator.
         * Adds the right complex number to this this complex number,
         * and returns the sum.
         *
         * @param right The complex number to add to this complex number.
         * @return      The sum of the two complex numbers.
         */
        Complex& operator+=(const Complex& right);

        /**
         * Addition equals double operator.
         * Adds the real number to this this complex number,
         * and returns the sum.
         *
         * @param right The real number to add.
         * @return      The sum of the complex number and the real number.
         */
        Complex& operator+=(const double right);

        /**
         * Subtraction equals operator.
         * Subtracts the right complex number from this this complex number,
         * and returns the difference.
         *
         * @param right The complex number to subtract.
         * @return      The difference between the two complex numbers.
         */
        Complex& operator-=(const Complex& right);

        /**
         * Subtraction equals double operator.
         * Subtracts the real number from this this complex number,
         * and returns the difference.
         *
         * @param right The real number to subtract.
         * @return      The difference betwen the complex number and
         *              the real number.
         */
        Complex& operator-=(const double right);

        /**
         * Multiplication equals operator.
         * Multiplies this complex number by the right complex number,
         * and returns the product.
         *
         * @param right The complex number to multiply by.
         * @return      The product of the two complex numbers.
         */
        Complex& operator*=(const Complex& right);

        /**
         * Multiplication equals double operator.
         * Multiplies this complex number by the real number,
         * and returns the product.
         *
         * @param right The real number to multiply by.
         * @return      The product of the complex number and the real number.
         */
        Complex& operator*=(const double right);

        /**
         * Division equals operator.
         * Divides this complex number by the right complex number,
         * and returns the quotient.
         *
         * @param right The complex divisor.
         * @return      The quotient of the two complex numbers.
         */
        Complex& operator/=(const Complex& right);

        /**
         * Division double operator.
         * Divides this complex number by the real number,
         * and returns the quotient.
         *
         * @param right The double divisor.
         * @return      The quotient of the complex number and the real number.
         */
        Complex& operator/=(const double right);

        /**
         * Returns the conjugate of this complex number.
         *
         * @return The conjugate of this complex number.
         */
        Complex conj() const;

        /**
         * Returns the natural exponential of this complex number.
         *
         * @return The natural exponential of this complex number.
         */
        Complex exp() const;

        /**
         * Returns a natural logarithm of this complex number.
         * <br />
         * <b>Note:</b> Because the polar form of a complex number
         * is periodic there are actually an infinite number of
         * logarithms.
         *
         * @return A natural log of this complex number.
         */
        Complex ln() const;

        /**
         * Returns this complex number raised by the integer power.
         *
         * @param n The power to raise this complex number by.
         * @return  This complex number raised to the power.
         */
        Complex pow(const int n) const;

        /**
         * Returns this complex number raised by another complex number.
         * <br />
         * <b>Note:</b> This uses a<sup>b</sup> = e<sup>ln a<sup>b</sup></sup>
         *      = e<sup>b ln a</sup>
         * Since ln(a) has infinite many solutions; so does the power.
         *
         * @param n The power to raise this complex number by.
         * @return  This complex number raised by the power.
         */
        Complex pow(const Complex& n) const;

        /**
         * Copies the n, nth roots of this complex number into the array passed.
         * <br />
         * <b>Note:</b> the nth root of a complex number has n roots.
         *
         * @param n     The root required.
         * @param roots The array into which the complex roots will be copied.
         */
        void root(const int n, Complex* roots) const;

    private:
        double realnum, imagnum;
    };

    /**
     * Addition operator.
     * Returns the sum of the two complex numbers.
     *
     * @param left  The first complex number to add.
     * @param right The second complex number to add.
     * @return      The sum of the two complex numbers.
     */
    Complex operator+(Complex left, const Complex& right);

    /**
     * Complex addition double operator.
     * Returns the sum of the complex number and the real number.
     *
     * @param left  The complex number to add.
     * @param right The real number to add.
     * @return      The sum of the complex number and the real number.
     */
    Complex operator+(Complex left, const double right);

    /**
     * Double addition complex operator.
     * Returns the sum of the real number and the complex number.
     *
     * @param left  The real number to add.
     * @param right The complex number to add.
     * @return      The sum of the real number and the complex number.
     */
    Complex operator+(const double left, const Complex& right);

    /**
     * Subtraction operator.
     * Returns the difference between the two complex numbers.
     *
     * @param left  The complex number to subtract from.
     * @param right The complex number to subtract.
     * @return      The difference between the two complex numbers.
     */
    Complex operator-(Complex left, const Complex& right);

    /**
     * Negative operator.
     * Returns the negative complex number.
     *
     * @param  source The complex number to form the negative of.
     * @return        The negative complex number.
     */
    Complex operator-(const Complex& source);

    /**
     * Compex subtraction double operator.
     * Returns the difference betwen the complex number and the real number.
     *
     * @param left  The complex number to subtract from.
     * @param right The real number to subtract.
     * @return      The difference betwen the complex number and
     *              the real number.
     */
    Complex operator-(Complex left, const double right);

    /**
     * Double subtraction complex operator.
     * Returns the difference between the real number and the complex number.
     *
     * @param left  The real number to subtract from.
     * @param right The complex number to subtract.
     * @return      The difference between the real number and
     *              the complex number.
     */
    Complex operator-(const double left, const Complex& right);

    /**
     * Multiplication operator.
     * Returns the product of the two complex numbers.
     *
     * @param left  The first complex number to multiply.
     * @param right The second complex number to multiply.
     * @return      The product of the two complex numbers.
     */
    Complex operator*(Complex left, const Complex& right);

    /**
     * Complex multiplication double operator.
     * Returns the product of the complex number and the real number.
     *
     * @param left  The complex number to multiply by.
     * @param right The real number to multiply by.
     * @return      The product of the complex number and the real number.
     */
    Complex operator*(Complex left, const double right);

    /**
     * Double multiplication complex operator.
     * Returns the product of the real number and the complex number.
     *
     * @param left  The real number to multiply by.
     * @param right The complex number to multiply by.
     * @return      The product of the real number and the complex number.
     */
    Complex operator*(const double left, const Complex& right);

    /**
     * Division operator.
     * Returns the quotient of the two complex numbers.
     *
     * @param left  The complex dividend.
     * @param right The complex divisor.
     * @return      The quotient of the two complex numbers.
     */
    Complex operator/(Complex left, const Complex& right);

    /**
     * Complex division double operator.
     * Returns the quotient of the complex number and the real number.
     *
     * @param left  The complex dividend.
     * @param right The real divisor.
     * @return      The quotient of the complex number and the real number.
     */
    Complex operator/(Complex left, const double right);

    /**
     * Double division complex operator.
     * Returns the quotient of the real number and the complex number.
     *
     * @param left  The real dividend.
     * @param right The complex divisor.
     * @return      The quotient of the real number and the complex number.
     */
    Complex operator/(const double left, const Complex& right);

    /**
     * Equals operator.
     * Returns whether or not the complex numbers are equal.
     *
     * @param left  The first complex number to compare.
     * @param right The second complex number to compare.
     * @return      Whether or not the complex numbers are equal.
     */
    bool operator==(const Complex& left, const Complex& right);

    /**
     * Not equals operator.
     * Returns whether or not the complex numbers are not equal.
     *
     * @param left  The first complex number to compare.
     * @param right The second complex number to compare.
     * @return      Whether or not the complex numbers are not equal.
     */
    bool operator!=(const Complex& left, const Complex& right);

    /**
     * Less than operator.
     * Returns whether or not the left complex number is less than the right
     * complex number.
     * First the magnitude (abs()) is compared and then the angle from the real
     * (arg()).
     *
     * @param left  The first complex number to compare.
     * @param right The second complex number to compare.
     * @return      Whether or not the left complex number is less than the
     *              right complex number.
     */
    bool operator<(const Complex& left, const Complex& right);

    /**
     * Less than or equals operator.
     * Returns whether or not the left complex number is less than or equal to
     * the right complex number.
     * First the magnitude (abs()) is compared and then the angle from the real
     * (arg()).
     *
     * @param left  The first complex number to compare.
     * @param right The second complex number to compare.
     * @return      Whether or not the left complex number is less than or equal
     *              to the right complex number.
     */
    bool operator<=(const Complex& left, const Complex& right);

    /**
     * Greater than operator.
     * Returns whether or not the left complex number is greater than the right
     * complex number.
     * First the magnitude (abs()) is compared and then the angle from the real
     * (arg()).
     *
     * @param left  The first complex number to compare.
     * @param right The second complex number to compare.
     * @return      Whether or not the left complex number is greater than the
     *              right complex number.
     */
    bool operator>(const Complex& left, const Complex& right);

    /**
     * Less than or equals operator.
     * Returns whether or not the left complex number is greater than or equal
     * to the right complex number.
     * First the magnitude (abs()) is compared and then the angle from the real
     * (arg()).
     *
     * @param left  The first complex number to compare.
     * @param right The second complex number to compare.
     * @return      Whether or not the left complex number is greater than or
     *              equal to the right complex number.
     */
    bool operator>=(const Complex& left, const Complex& right);

    /**
     * Returns whether two complex numbers are approximately equal.
     *
     * @param left  The first complex number to compare.
     * @param right The second complex number to compare.
     * @param error The percentage error allowed. Defaults to 0.01%
     * @return      Whether or not the complex numbers are approximately equal.
     */
    bool approxEqual(const Complex& left, const Complex& right,
            const double error = 0.01);

    /**
     * Output stream operator.
     * Appends the complex number to the output stream and
     * returns the output stream.
     *
     * @param os     The output stream.
     * @param source The complex number to append to the output stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const Complex& source);

    /**
     * Input stream operator.
     * Inputs a complex number into the destination (if possible) and
     * returns the input stream.
     *
     * @param is          The input stream.
     * @param destination The complex number to input into.
     * @return            The input stream.
     */
    std::istream& operator>>(std::istream& is, Complex& destination);

    /**
     * Returns the conjugate of the complex number.
     *
     * @param source    The complex number.
     * @return          The conjugate of the complex number.
     */
    Complex conj(const Complex& source);

    /**
     * Returns the natural exponential of the complex number.
     *
     * @param  power    The complex number.
     * @return          The natural exponential of the complex number.
     */
    Complex exp(const Complex& power);

    /**
     * Returns a natural log of the complex number.
     * <br />
     * <b>Note:</b> Because the polar form of a complex number
     * is periodic there are actually an infinite number of
     * logarithms.
     *
     * @param  root The complex number.
     * @return      A natural log of the complex number.
     */
    Complex ln(const Complex& root);

    /**
     * Returns the complex number raised to the integer power.
     *
     * @param base  The complex number to raise.
     * @param power The power to raise by.
     * @return      This complex number raised by the power.
     */
    Complex pow(const Complex& base, const int power);

    /**
     * Returns the first complex number raised by the second complex number.
     * <br />
     * <b>Note:</b> This uses a<sup>b</sup> = e<sup>ln a<sup>b</sup></sup>
     *      = e<sup>b ln a</sup>
     * Since ln(a) has infinite many solutions; so does the power.
     *
     * @param base  The complex number to raise.
     * @param power The complex number to raise by.
     * @return      This complex number raised by the power.
     */
    Complex pow(const Complex& base, const Complex& power);

    /**
     * Copies the n, nth roots of the complex number into the array passed.
     * <br />
     * <b>Note:</b> the nth root of a complex number has n roots.
     *
     * @param base  The complex number whose roots are sought.
     * @param n     The root required.
     * @param roots The array into which the complex roots will be copied.
     */
    void root(const Complex& base, const int n, Complex* roots);
}

#endif // COMPLEX_H
