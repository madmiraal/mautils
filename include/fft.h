// Cooley-Tukey Fast Fourier Transform, in-place, breadth-first,
// decimation-in-frequency
//
// (c) 2016: Marcel Admiraal

#ifndef FFT_H
#define FFT_H

#include "complex.h"

namespace ma
{
    /**
     * Performs an in-place fast Fourier transform on the array of complex
     * numbers.
     *
     * @param data      The array of complex numbers.
     * @param length    The length of the array, must be an order of 2.
     */
    void fft(Complex data[], const unsigned int length);

    /**
     * Performs an in-place inverse fast Fourier transform on the array of
     * complex numbers.
     *
     * @param data      The array of complex numbers.
     * @param length    The length of the array, must be an order of 2.
     */
    void ifft(Complex data[], const unsigned int length);
}

#endif // FFT_H
