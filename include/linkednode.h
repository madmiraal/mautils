// A simple linked node template class
//
// (c) 2014: Marcel Admiraal

#ifndef LINKEDNODE_H
#define LINKEDNODE_H

#include <algorithm>
#include <iostream>

namespace ma
{
    template <class T> class LinkedNode
    {
    public:
        /**
         * Constructor for an unlinked data item.
         *
         * @param data The data to be stored in this node.
         */
        LinkedNode(T data);

        /**
         * Copy constructor.
         *
         * @param source The node to copy.
         */
        LinkedNode(const LinkedNode<T>& source);

        /**
         * Default destructor.
         */
        virtual ~LinkedNode();

        /**
         * Swap function.
         *
         * @param first  The first collection to swap with.
         * @param second The second collection to swap with.
         */
        template <class S> friend void swap(
            LinkedNode<S>& first, LinkedNode<S>& second);

        /**
         * Assignment operator.
         * Returns a copy of the node.
         *
         * @param source The node to copy.
         * @return       A copy of the node.
         */
        LinkedNode<T>& operator=(LinkedNode<T> source);

        /**
         * Adds a node before the current node.
         *
         * @param data The data to be stored in the new node.
         */
        void addPrevious(T data);

        /**
         * Adds a node after the current node.
         *
         * @param data The data to be stored in the new node.
         */
        void addNext(T data);

        /**
         * Returns the data associated with this node.
         *
         * @return The data associated with this node.
         */
        T getData() const;

        /**
         * Sets the data associated with this node.
         *
         * @param The new data to be associated with this node.
         */
        void setData(const T& data);

        /**
         * Returns the previous node.
         *
         * @return The previous node.
         */
        LinkedNode<T>* getPrevious() const;

        /**
         * Returns the next node.
         *
         * @return The next node.
         */
        LinkedNode<T>* getNext() const;

        /**
         * Deletes this node and all the nodes attached.
         *
         * @param node A node in the linked list to delete.
         */
        template <class S> friend void deleteAll(LinkedNode<S>* node);

    private:
        void setPrevious(LinkedNode* previous);
        void setNext(LinkedNode* next);

        T data;
        LinkedNode* previous;
        LinkedNode* next;
    };

// Function definitions.

    template <class T> LinkedNode<T>::LinkedNode(T data) :
            data(data), previous(0), next(0)
    {
    }

    template <class T> LinkedNode<T>::LinkedNode(const LinkedNode<T>& source) :
            data(source.data), previous(source.previous), next(source.next)
    {
    }

    template <class T> LinkedNode<T>::~LinkedNode()
    {
        LinkedNode* previous = getPrevious();
        if (previous != 0) previous->setNext(getNext());
        LinkedNode* next = getNext();
        if (next != 0) next->setPrevious(getPrevious());
    }

    template <class T> void swap(LinkedNode<T>& first, LinkedNode<T>& second)
    {
        std::swap(first.data, second.data);
        std::swap(first.previous, second.previous);
        std::swap(first.next, second.next);
    }

    template <class T> LinkedNode<T>& LinkedNode<T>::operator=(
            LinkedNode<T> source)
    {
        swap(*this, source);
        return *this;
    }

    template <class T> void LinkedNode<T>::addPrevious(T data)
    {
        LinkedNode* newLink = new LinkedNode(data);
        newLink->setPrevious(this->getPrevious());
        newLink->setNext(this);
        if(this->getPrevious() != 0) this->getPrevious()->setNext(newLink);
        this->setPrevious(newLink);
    }

    template <class T> void LinkedNode<T>::addNext(T data)
    {
        LinkedNode* newLink = new LinkedNode(data);
        newLink->setPrevious(this);
        newLink->setNext(this->getNext());
        if(this->getNext() != 0) this->getNext()->setPrevious(newLink);
        this->setNext(newLink);
    }

    template <class T> T LinkedNode<T>::getData() const
    {
        return data;
    }

    template <class T> void LinkedNode<T>::setData(const T& data)
    {
        this->data = data;
    }

    template <class T> LinkedNode<T>* LinkedNode<T>::getPrevious() const
    {
        return previous;
    }

    template <class T> LinkedNode<T>* LinkedNode<T>::getNext() const
    {
        return next;
    }

    template <class T> void deleteAll(LinkedNode<T>* node)
    {
        LinkedNode<T>* previous = node->getPrevious();
        while(previous != 0)
        {
            LinkedNode<T>* temp = previous->getPrevious();
            delete previous;
            previous = temp;
        }
        LinkedNode<T>* next = node->getNext();
        while(next != 0)
        {
            LinkedNode<T>* temp = next->getNext();
            delete next;
            next = temp;
        }
        delete node;
    }

    template <class T> void LinkedNode<T>::setPrevious(LinkedNode* previous)
    {
        this->previous = previous;
    }

    template <class T> void LinkedNode<T>::setNext(LinkedNode* next)
    {
        this->next = next;
    }
}

#endif // LINKEDNODE_H
