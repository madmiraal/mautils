// Simple class for reading csv files.
// (c) 2018: Marcel Admiraal

#ifndef CSV_PARSER_H
#define CSV_PARSER_H

#include "list.h"
#include "str.h"

namespace ma
{
    class CSVParser
    {
    public:
        /**
         * Constructor.
         *
         * @param csvFile   The filename of the CSV file to parse.
         */
        CSVParser(const Str& csvFile);

        /**
         * Defualt destructor.
         */
        ~CSVParser();

        /**
         * Array subscription operator:
         * Allows direct access to the rows.
         * Returns the row as an array of strings.
         *
         * @param row   The 0 base row to retrieve.
         * @return      The row as an array.
         */
        List<Str> operator[](const unsigned int row);
        const List<Str> operator[](const unsigned int row) const;

        /**
         * Returns a row iterator for the table.
         *
         * @return A row iterator for the table.
         */
        Iterator<List<Str> > getIterator() const;

        /**
         * Returns the number of rows in the CSV file.
         *
         * @return The number of rows in the CSV file.
         */
        unsigned int getRows() const;

        /**
         * Returns the number of columns in the CSV file.
         *
         * @return The number of columns in the CSV file.
         */
        unsigned int getColumns() const;

    private:
        List<List<Str> > elements;
        unsigned int rows;
        unsigned int columns;
    };
}

#endif // CSV_PARSER_H
