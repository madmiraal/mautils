// A simple cross-platform serial class for basic
// reading and writing to a serial port
//
// To connect to the serial port you need to call:
// - setPort("<portname>"), with the name of the port
//   e.g. COM1 or /dev/tty1
// - setParameters(), with optional parameters.
//
// (c) 2013 - 2016: Marcel Admiraal

#ifndef SERIAL_H
#define SERIAL_H

#include "str.h"

#ifdef _WIN32
    #ifndef __MINGW32__
        #define NOMINMAX
    #endif // __MINGW32__
    #include <windows.h>
#endif // _WIN32
#if defined(__unix) || defined(__APPLE__)
    #define ONESTOPBIT 1
    #define NOPARITY 0
#endif // __unix || __APPLE__

namespace ma
{
    class Serial
    {
    public:
        /**
         * Default constructor.
         */
        Serial();

        /**
         * Default destructor.
         */
        virtual ~Serial();

        /**
         * Returns the port name used in setPort.
         *
         * @return The port name used in setPort.
         */
        virtual Str getPortString() const;

        /**
         * Connects to a port e.g. COM1 or /dev/tty1.
         * Returns whether or not the connection was successful.
         *
         * @param portName A string containing the port name.
         * @return         Whether or not the connection was successful.
         */
        virtual bool setPort(const Str& portName);

        /**
         * Sets the serial port port parameters.
         * Returns whether or not setting the serial port parameters was
         * successful.
         *
         * @param baud     Default: 9600       - The baud rate in bps.
         * @param bits     Default: 8          - The number of bits in a packet.
         * @param parity   Default: NOPARITY   - The number of parity bits.
         * @param stopbits Default: ONESTOPBIT - The number of stop bits.
         * @param block    Default: false      - Whether or not to block until
         *                                       bytes available when reading.
         * @return         Whether or not setting the serial port parameters was
         *                 successful.
         */
        virtual bool setParameters(
            const int baud = 9600,
            const int bits = 8,
            const int parity = NOPARITY,
            const int stopBits = ONESTOPBIT,
            const bool block = false
            );

        /**
         * Writes data to the serial port.
         * Returns the number of bytes actually written.
         *
         * @param buffer       An unsigned char array of bytes that
         *                     contains the data to be written.
         * @param bufferLength The length of the array.
         * @return             The number of bytes actually written.
         */
        virtual unsigned long writeData(const unsigned char *buffer,
            const unsigned int bufferLength);

        /**
         * Reads data from the serial port.
         * Returns the number of bytes actually read.
         *
         * @param buffer       An unsigned char array of bytes to
         *                     write the data read to.
         * @param bufferLength The length of the array
         *                     (the maximum number of bytes to read).
         * @return             The number of bytes actually read.
         */
        virtual unsigned long readData(unsigned char *buffer,
            const unsigned int bufferLength);

        /**
         * Clears the write buffer.
         */
        virtual void clearWriteBuffer();

        /**
         * Clears the read buffer.
         */
        virtual void clearReadBuffer();

        /**
         * Returns the connection status.
         * Note: If a connection was attempted
         * it returns whether or not the last connection was successful
         * so it may not reflect the acutal status of the serial port.
         */
        virtual bool isConnected() const;

        /**
         * Disconnects from the serial port.
         */
        virtual void disconnect();

    private:
        #ifdef _WIN32
            // Stores the handle to the serial port file
            HANDLE hSerial;
            // Cache's the last status structure.
            COMSTAT status;
            // Cache's the last error code.
            DWORD error;
            // Flag for whether reads should be blocking or not.
        #endif // _WIN32
        #if defined(__unix) || defined(__APPLE__)
            // Stores the handle to the serial port file
            int fileDescriptor;
        #endif // __unix || __APPLE__
        // Cache's whether or not the serial port connected successfully.
        bool connected;
        Str portNameString;
    };
}

#endif // SERIAL_H
