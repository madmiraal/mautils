// A simple stack template class
//
// (c) 2014: Marcel Admiraal

#ifndef STACK_H
#define STACK_H

#include "linkednode.h"

namespace ma
{
    template <class T> class Stack
    {
    public:
        /**
         * Default constructor.
         * Creates an empty stack.
         */
        Stack();

        /**
         * Copy constructor.
         *
         * @param source The stack to copy.
         */
        Stack(const Stack<T>&);

        /**
         * Default destructor.
         */
        virtual ~Stack();

        /**
         * Swap function.
         *
         * @param first  The first collection to swap with.
         * @param second The second collection to swap with.
         */
        template <class S> friend void swap(Stack<S>&, Stack<S>&);

        /**
         * Assignment operator.
         * Returns a copy of the stack.
         *
         * @param source The collection to copy.
         * @return       A copy of the stack.
         */
        Stack<T>& operator=(Stack<T>);

        /**
         * Adds an item to the top of the stack.
         *
         * @param The item to add to the top of the stack.
         */
        void push(const T item);

        /**
         * Removes an item from the top of the stack,
         * and returns the item.
         *
         * @return The item to removed from the top of the stack.
         * @throw  String literal of the error if the stack is empty.
         */
        T pop();

    private:
        LinkedNode<T>* head;
    };

// Function definitions.

    template <class T> Stack<T>::Stack()
    {
        head = 0;
    }

    template <class T> Stack<T>::Stack(const Stack<T>& source)
    {
        head = 0;
        if (source.head != 0)
        {
            head = new LinkedNode<T>(source.head->getData());
            LinkedNode<T>* temp = head;
            LinkedNode<T>* tempSource = source.head;
            while(tempSource->getPrevious() != 0)
            {
                tempSource = tempSource->getPrevious();
                temp->addPrevious(tempSource->getData());
                temp = temp->getPrevious();
            }
        }
    }

    template <class T> Stack<T>::~Stack()
    {
        if (head != 0)
        {
            deleteAll(head);
        }
    }

    template <class T> void swap(Stack<T>& first, Stack<T>& second)
    {
        std::swap(first.head, second.head);
    }

    template <class T> Stack<T>& Stack<T>::operator=(Stack<T> source)
    {
        swap(*this, source);
        return *this;
    }

    template <class T> void Stack<T>::push(const T data)
    {
        if (head == 0)
        {
            head = new LinkedNode<T>(data);
        }
        else
        {
            head->addNext(data);
            head = head->getNext();
        }
    }

    template <class T> T Stack<T>::pop()
    {
        if (head == 0)
        {
            throw "Empty stack!";
        }
        T data = head->getData();
        LinkedNode<T>* previous = head->getPrevious();
        delete head;
        head = previous;
        return data;
    }
}

#endif // STACK_H
