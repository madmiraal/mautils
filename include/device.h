// Classes and helper functions for working with Linux Devices.
// The main function is the List<Devices> getInputDevices() static function.
//
// (c) 2016: Marcel Admiraal

#ifdef __unix

#ifndef DEVICE_H
#define DEVICE_H

#include "str.h"
#include "list.h"

#include <iostream>

namespace ma
{
    class IOEvent
    {
    public:
        /**
         * Constructor.
         *
         * @param name  The event's name.
         */
        IOEvent(Str name);

        /**
         * Default destructor.
         */
        virtual ~IOEvent();

        /**
         * Adds a value to the event.
         *
         * @param value The value to add.
         */
        void addValue(Str value);

        /**
         * Returns the event name.
         *
         * @param value The value to add.
         */
        Str getName();

    private:
        // Stores the event name.
        Str name;
        // Stores the event values.
        List<Str> values;
    };

    class Device
    {
    public:
        /**
         * Default construtor.
         */
        Device();

        /**
         * Default destrutor.
         */
        virtual ~Device();

        /**
         * Copy construtor.
         *
         * @param source    The device to copy.
         */
        Device(const Device& source);

        /**
         * Swap function.
         *
         * @param first  The first device to swap with.
         * @param second The second device to swap with.
         */
        friend void swap(Device& first, Device& second);

        /**
         * Assignment operator.
         * Returns a copy of the device.
         *
         * @param source The device to copy.
         * @return       A copy of the device.
         */
        Device& operator=(Device source);

        /**
         * Returns the bus name as a string.
         *
         * @return The bus name as a string.
         */
        Str getBus() const;

        /**
         * Returns the vendor name as a string.
         *
         * @return The vendor name as a string.
         */
        Str getVendor() const;

        /**
         * Returns the product name as a string.
         *
         * @return The product name as a string.
         */
        Str getProduct() const;

        /**
         * Returns the version as a string.
         *
         * @return The version as a string.
         */
        Str getVersion() const;

        /**
         * Returns the device name as a string.
         *
         * @return The device name as a string.
         */
        Str getName() const;

        /**
         * Returns the physical device name as a string.
         *
         * @return The physical device name as a string.
         */
        Str getPhysicalDevice() const;

        /**
         * Returns the system file as a string.
         *
         * @return The system file as a string.
         */
        Str getSystemFile() const;

        /**
         * Returns the unique identifier as a string.
         *
         * @return The unique identifier as a string.
         */
        Str getUniqueIdentifier() const;

        /**
         * Returns the handlers as a string.
         *
         * @return The handlers as a string.
         */
        Str getHandlers() const;

        /**
         * Returns whether or not this device is null
         * i.e. contains no information.
         *
         * @return Whether or not this device is null.
         */
        bool isNull() const;

        /**
         * Sets the device identity.
         *
         * @param line  The device identity as a string.
         * @return      Whether or not the devices identity could be extracted.
         */
        bool setIdentity(Str& line);

        /**
         * Sets the device's name.
         *
         * @param line  The device's name as a string.
         * @return      Whether or not the device's name could be extracted.
         */
        bool setName(const Str& line);

        /**
         * Sets the physical device name.
         *
         * @param line  The physical device name as a string.
         * @return      Whether or not the physical device name could be
         *              extracted.
         */
        bool setPhysicalDevice(const Str& line);

        /**
         * Sets the system file name.
         *
         * @param line  The system file name as a string.
         * @return      Whether or not the system file name could be extracted.
         */
        bool setSystemFile(const Str& line);

        /**
         * Sets the unique identifier.
         *
         * @param line  The unique identifier as a string.
         * @return      Whether or not the unique identifier could be extracted.
         */
        bool setUniqueIdentifier(const Str& line);

        /**
         * Sets the handlers.
         *
         * @param line  The handlers as a string.
         * @return      Whether or not the handlers could be extracted.
         */
        bool setHandlers(const Str& line);

        /**
         * Adds a capability.
         *
         * @param line  The capability as a string.
         * @return      Whether or not the capability could be extracted.
         */
        bool addCapability(const Str& line);

        /**
         * Outputs the input device details to the output stream specified.
         * The default output stream is the standard output.
         *
         * @param os    The output stream to output to.
         */
        void print(std::ostream& os = std::cout);

        /**
         * Returns whether or not this device handles mouse input.
         *
         * @return Whether or not this device handles mouse input.
         */
        bool isMouse();

        /**
         * Returns the mouse event handler, if it handles mouse input.
         *
         * @return The mouse event handler.
         */
        Str getMouseEvent();

    private:
        void getEventTypes(Str bitTypes);
        Str getEventBitCode();
        void getKeyCodes(Str keyCodes);

        Str bus, vendor, product, version;
        Str name, physicalDevice, systemFile, uniqueId;
        List<Str> handlers;
        List<IOEvent> events;
        bool null;
    };

    /**
     * Returns the list of input devices currently connected.
     *
     * @return The list of input devices currently connected.
     */
    List<Device> getInputDevices();

    /**
     * Returns the event type as a string given the event type code.
     *
     * @return The event type as a string.
     */
    Str getEventType(const unsigned int code);

    /**
     * Returns the event code given the type as a string.
     *
     * @return The event code.
     */
    unsigned int getEventCode(const Str& type);

    /**
     * Returns a description of the event given the event type code.
     *
     * @return A description of the event.
     */
    Str getEventDescription(const unsigned int code);
}

#endif // DEVICE_H

#endif // __unix
