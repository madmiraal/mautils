// A simple collection template class
//
// (c) 2014: Marcel Admiraal

#ifndef COLLECTION_H
#define COLLECTION_H

#include "linkednode.h"
#include "iterator.h"

namespace ma
{
    template <class T> class Collection
    {
    public:
        /**
         * Default constructor.
         * Creates an empty collection.
         */
        Collection();

        /**
         * Copy constructor.
         *
         * @param source The collection to copy.
         */
        Collection(const Collection<T>& source);

        /**
         * Default destructor.
         */
        virtual ~Collection();

        /**
         * Swap function.
         *
         * @param first  The first collection to swap with.
         * @param second The second collection to swap with.
         */
        template <class S> friend void swap(
            Collection<S>& first, Collection<S>& second);

        /**
         * Assignment operator.
         * Returns a copy of the collection.
         *
         * @param source The collection to copy.
         * @return       A copy of the collection.
         */
        Collection<T>& operator=(Collection<T> source);

        /**
         * Returns the number of elements in the collection.
         *
         * @return The number of elements in the collection.
         */
        unsigned int getSize() const;

        /**
         * Adds an item to the collection.
         *
         * @param item The item to add to the collection.
         */
        void add(const T item);

        /**
         * Removes one copy of an item from the collection if it exists.
         * Returns true if the element was found (and removed).
         *
         * @param item The item to be removed from the collection.
         * @return     True if the element was found and removed.
         */
        bool remove(const T item);

        /**
         * Removes all copies of an item from the collection.
         * Returns the number of elements removed.
         *
         * @param item The item to be removed from the collection.
         * @return     The number of elements removed.
         */
        unsigned int removeAll(const T item);

        /**
         * Returns whether or not an element exists in the collection.
         *
         * @param item The item to check for.
         * @return     Whether or not an element exists in the collection.
         */
        bool exists(const T item) const;

        /**
         * Returns the number of copies of an element in the collection.
         *
         * @param item The item to check for.
         * @return     The number of copies of an element in the collection.
         */
        unsigned int count(const T item) const;

        /**
         * Returns an iterator for the collection.
         *
         * @return An iterator for the colleciton.
         */
        Iterator<T> getIterator() const;

    private:
        LinkedNode<T>* head;
        unsigned int size;
    };

    /**
     * Returns whether or not the collections are equal.
     *
     * @param left  The first collection.
     * @param right The second collection.
     * @return      Whether or not the collections are equal.
     */
    template <class T> bool operator==(
        const Collection<T>& left, const Collection<T>& right);

    /**
     * Returns whether or not the collections are not equal.
     *
     * @param left  The first collection.
     * @param right The second collection.
     * @return      Whether or not the collections are equal.
     */
    template <class T> bool operator!=(
        const Collection<T>& left, const Collection<T>& right);

    /**
     * Input stream operator.
     * Builds a collection from an input stream and returns the input stream.
     *
     * @param is            The input stream.
     * @param destination   The collection to build.
     * @return              The input stream.
     */
    template <class T> std::istream& operator>>(std::istream& is,
            Collection<T>& destination);

    /**
     * Output stream operator.
     * Appends the collection to the output stream and returns the output stream.
     *
     * @param os     The output stream.
     * @param source The collection to stream.
     * @return       The output stream.
     */
    template <class T>
        std::ostream& operator<<(std::ostream& os, const Collection<T>& source);

// Function definitions.

    template <class T> Collection<T>::Collection() :
        head(0), size(0)
    {
    }

    template <class T> Collection<T>::Collection(const Collection<T>& source) :
        head(0), size(source.size)
    {
        if (source.head != 0)
        {
            head = new LinkedNode<T>(source.head->getData());
            LinkedNode<T>* temp = head;
            LinkedNode<T>* tempSource = source.head;
            while(tempSource->getPrevious() != 0)
            {
                tempSource = tempSource->getPrevious();
                temp->addPrevious(tempSource->getData());
                temp = temp->getPrevious();
            }
        }
    }

    template <class T> Collection<T>::~Collection()
    {
        if (head != 0)
        {
            deleteAll(head);
        }
    }

    template <class T> void swap(Collection<T>& first, Collection<T>& second)
    {
        std::swap(first.head, second.head);
        std::swap(first.size, second.size);
    }

    template <class T> Collection<T>& Collection<T>::operator=(
            Collection<T> source)
    {
        swap(*this, source);
        return *this;
    }

    template <class T> unsigned int Collection<T>::getSize() const
    {
        return size;
    }

    template <class T> void Collection<T>::add(T item)
    {
        if (head == 0)
        {
            head = new LinkedNode<T>(item);
        }
        else
        {
            head->addNext(item);
            head = head->getNext();
        }
        size++;
    }

    template <class T> bool Collection<T>::remove(const T item)
    {
        LinkedNode<T>* pointer = head;
        while(pointer != 0)
        {
            if (pointer->getData() == item)
            {
                if (pointer == head)
                {
                    head = pointer->getPrevious();
                }
                delete pointer;
                size--;
                return true;
            }
            pointer = pointer->getPrevious();
        }
        return false;
    }

    template <class T> unsigned int Collection<T>::removeAll(const T item)
    {
        unsigned int count = 0;
        LinkedNode<T>* pointer = head;
        while(pointer != 0)
        {
            if (pointer->getData() == item)
            {
                LinkedNode<T>* temp = pointer;
                if (pointer == head)
                {
                    head = pointer->getPrevious();
                }
                pointer = pointer->getPrevious();
                delete temp;
                size--;
                count++;
            }
            else
            {
                pointer = pointer->getPrevious();
            }
        }
        return count;
    }

    template <class T> bool Collection<T>::exists(const T item) const
    {
        LinkedNode<T>* pointer = head;
        while(pointer != 0)
        {
            if (pointer->getData() == item)
            {
                return true;
            }
            pointer = pointer->getPrevious();
        }
        return false;
    }

    template <class T> unsigned int Collection<T>::count(const T item) const
    {
        unsigned int count = 0;
        LinkedNode<T>* pointer = head;
        while(pointer != 0)
        {
            if (pointer->getData() == item)
            {
                count++;
            }
            pointer = pointer->getPrevious();
        }
        return count;
    }

    template <class T> Iterator<T> Collection<T>::getIterator() const
    {
        Iterator<T> iter(head);
        return iter;
    }

    template <class T> bool operator==(
        const Collection<T>& left, const Collection<T>& right)
    {
        if (left.getSize() != right.getSize()) return false;
        Iterator<T> iterLeft = left.getIterator();
        while (iterLeft.hasNext())
        {
            T next = iterLeft.getNext();
            if (left.count(next) != right.count(next)) return false;
        }
        return true;
    }

    template <class T> bool operator!=(
        const Collection<T>& left, const Collection<T>& right)
    {
        return !(left == right);
    }

    template <class T> std::istream& operator>>(std::istream& is,
            Collection<T>& destination)
    {
        unsigned int length;
        is >> length;
        if (is.good())
        {
            Collection<T> result;
            for (unsigned int index = 0; index < length; ++index)
            {
                T data;
                is >> data;
                if (is.good())
                {
                    result.append(data);
                }
            }
            unsigned int tail;
            is >> tail;
            if (is.good() && tail == length)
            {
                swap(destination, result);
            }
        }
        return is;
    }

    template <class T>
        std::ostream& operator<<(std::ostream& os, const Collection<T>& source)
    {
        os << source.getSize() << '\t';
        for (Iterator<T> iter = source.getIterator(); iter.valid(); --iter)
        {
            os << *iter << '\t';
        }
        os << source.getSize();  // Tail.
        return os;
    }
}

#endif // COLLECTION_H
