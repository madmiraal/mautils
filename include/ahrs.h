// Attitude and heading reference system quaternion calculations and corrections
// from gyroscopse, accelerometer and magnetometer readings.

// (c) 2017: Marcel Admiraal
// Based on original algorithms by Madgwick and Wilson.

#ifndef AHRS_H
#define AHRS_H

#include "quaternion.h"

namespace ma
{
    /**
     * Updates the orientation quaternion of the earth relative to the device
     * given gyroscope, accelerometer and magnetometer readings.
     * Note: Only the accelerometer and magnetometer vector direction is used;
     *       the vector is normalised before being used so the units are not
     *       important.
     *
     * @param quat          The orientation quaternion.
     * @param gx            The gyroscope's x value in radians per second.
     * @param gy            The gyroscope's y value in radians per second.
     * @param gz            The gyroscope's z value in radians per second.
     * @param ax            The accelerometer vector's x value.
     * @param ay            The accelerometer vector's y value.
     * @param az            The accelerometer vector's z value.
     * @param mx            The magnetometer vector's x value.
     * @param my            The magnetometer vector's y value.
     * @param mz            The magnetometer vector's z value.
     * @param samplePeriod  The period in seconds between samples.
     * @param beta          The corrective step size. Defaults to 0.1
     */
    void AHRS(Quaternion& quat,
            float gx, float gy, float gz,
            float ax, float ay, float az,
            float mx, float my, float mz,
            float samplePeriod, float alpha=.1f);

    /**
     * Updates the orientation quaternion of the earth relative to the device
     * given gyroscope, accelerometer and magnetometer readings.
     * Note: Only the accelerometer and magnetometer vector direction is used;
     *       the vector is normalised before being used so the units are not
     *       important.
     * Note: Long values approximate float values by multiplying float values by
     *       2^14.
     *
     * @param quat          The orientation quaternion.
     * @param gx            The gyroscope's x value in radians per second.
     * @param gy            The gyroscope's y value in radians per second.
     * @param gz            The gyroscope's z value in radians per second.
     * @param ax            The accelerometer vector's x value.
     * @param ay            The accelerometer vector's y value.
     * @param az            The accelerometer vector's z value.
     * @param mx            The magnetometer vector's x value.
     * @param my            The magnetometer vector's y value.
     * @param mz            The magnetometer vector's z value.
     * @param samplePeriod  The period in seconds between samples.
     * @param ilbeta        The inverse log of the corrective step size:
     *                      Note: The corrective step is divided by 2^ilbeta.
     *                      Defaults to 3: i.e. a corrective step of 0.125.
     */
    void AHRS(Quaternion& quat,
            long gx, long gy, long gz,
            long ax, long ay, long az,
            long mx, long my, long mz,
            float samplePeriod, long ilalpha=3L);
}

#endif // AHRS_H
